wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=1000000

# Distributions
task_name='gen_displaced_muons'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-displaced-muons --nevents="$n_events"
cd "$wd"
