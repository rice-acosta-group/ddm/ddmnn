wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags check-performance"

# Inputs
n_events=-1

# Replacement
model_name='association_validation_wNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --num-opt='_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_NLAY' \
  --den-opt='_VTX_NPP_NS_NH_HLT_REPGEN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_NLAY' \
  --dpa-protobuf "../association/association" \
  --pairing-protobuf "../pairing/pairing" \
  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"

model_name='association_validation_noNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --num-opt='_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_NLAY' \
  --den-opt='_VTX_NPP_NS_NH_HLT_REPGEN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_NLAY' \
  --dpa-protobuf "../association/association" \
  --pairing-protobuf "../pairing/pairing" \
  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"

# Pairing
model_name='pairing_validation_wNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --num-opt='_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY' \
  --den-opt='_VTX_NPP_NS_NH_HLT_REPGEN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCGEN_NLAY' \
  --dpa-protobuf "../association/association" \
  --pairing-protobuf "../pairing/pairing" \
  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"

model_name='pairing_validation_noNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --num-opt='_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY' \
  --den-opt='_VTX_NPP_NS_NH_HLT_REPGEN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCGEN_NLAY' \
  --dpa-protobuf "../association/association" \
  --pairing-protobuf "../pairing/pairing" \
  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"
