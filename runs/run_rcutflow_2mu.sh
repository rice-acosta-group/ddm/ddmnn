wd=$(pwd)

gflags=""
run_command="../bin/run $gflags r-check-cutflow"

# Inputs
n_events=-1

# Cuts
base_cuts='REP'

dimuon_type='pat'
qcd_cuts='REP SS DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO DPHI0p1 HLTRun3'  # QCD
dy_cuts='REP OS DPHI4T LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'  # DY
sr_cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO DPHI0p1 HLTRun3'  # SR

#dimuon_type='hyb'
#qcd_cuts='REP SS DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV NTRKLAYSLXY ISGLOB HITSBEFVTX DPHIMULXY FPTE DSATIME ISO HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4'  # QCD
#dy_cuts='REP OS DPHI4 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV NTRKLAYSLXY ISGLOB HITSBEFVTX DPHIMULXY FPTE DSATIME ISO HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4'  # DY
#sr_cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV NTRKLAYSLXY ISGLOB HITSBEFVTX DPHIMULXY FPTE DSATIME ISO HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4'  # SR

#dimuon_type='dsa'
#qcd_cuts='REP OS DPHI LXYE MASS CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF IDSAISO0p15 LXYE20 DPHIb10 HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4' # QCD
#dy_cuts='REP OS IDPHI3 LXYE MASS CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 LXYE20 iDPHIb10 HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4' # DY
#sr_cuts='REP OS DPHI LXYE MASS CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 LXYE20 DPHIb10 HLTRun3 ISOCRRLTN1 ISOCRRLTN0p4' # SR

# Data
#$run_command --nevents="$n_events" \
#  --input ../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
#  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $qcd_cuts
#$run_command --nevents="$n_events" \
#  --input ../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
#  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $dy_cuts
$run_command --nevents="$n_events" \
  --input ../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $sr_cuts

#$run_command --nevents="$n_events" \
#  --input ../in/rTuples/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
#  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $qcd_cuts
#$run_command --nevents="$n_events" \
#  --input ../in/rTuples/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
#  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $dy_cuts
$run_command --nevents="$n_events" \
  --input ../in/rTuples/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J*.root \
  --dimuon-type $dimuon_type --base-cuts $base_cuts --cuts $sr_cuts
