wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

# Cuts
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO DPHI0p1 HLTRun3 PSCORE0p25'  # SR

# Data
model_name='restimation_REPNN_PCNN_data'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var "mind0sigpv"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var "mind0sigpv"
cd "$wd"

model_name='restimation_REP_PC_data'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var "mind0sigpv"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var "mind0sigpv"
cd "$wd"
