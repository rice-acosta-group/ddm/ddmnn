wd=$(pwd)

gflags=""
run_command="../bin/run $gflags run-retupler"

# Inputs
n_events=-1

# With NNs
lxysigcut=0.0
lxysigcutstr=`echo $lxysigcut | tr '.' 'p'`
options='_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY'
#options='_VTX_NPP_NS_NH_HLT_DSAPATLINKNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY'

$run_command --nevents="$n_events" \
  --opt="${options}" --lxysigcut=$lxysigcut \
  --sample-name='slim' \
  --dpa-protobuf='association/association' \
  --pairing-protobuf='pairing/pairing' \
  --input ../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root \
  --output "/home/om15/Datasets/DDM/rTuples4/2022/${options}_LxySig${lxysigcutstr}/rntuple_2022_HTo2XTo2Mu2J.root"

## Without NNs
#lxysigcut=0.0
#lxysigcutstr=`echo $lxysigcut | tr '.' 'p'`
#options='_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY'
##options='_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY'
#
#$run_command --nevents="$n_events" \
#  --opt="${options}" --lxysigcut=$lxysigcut \
#  --sample-name='slim' \
#  --dpa-protobuf='association/association' \
#  --pairing-protobuf='pairing/pairing' \
#  --input ../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root \
#  --output "/home/om15/Datasets/DDM/rTuples3/2022/${options}_LxySig${lxysigcutstr}/rntuple_2022_HTo2XTo2Mu2J.root"
