wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

# Samples
task_name='association_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-dpa-signal --nevents="$n_events" \
  --input ../../in/MiniNTuples/ntuple_XTo2LongLivedTo4Mu_for_ML.root \
  --filename='llp_sample'
#$run_command generate-dpa-signal --nevents="$n_events" --ftrain=0.0 \
#  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root \
#  --filename='val_sample'
cd "$wd"

task_name='pairing_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-pairing-signal --nevents="$n_events" \
  --input ../../in/MiniNTuples/ntuple_XTo2LongLivedTo4Mu_for_ML.root \
  --filename='llp_sample'
#$run_command generate-pairing-signal --nevents="$n_events" --ftrain=0.0 \
#  --input ../../in/MiniNTuples/ntuple_2022_HTo2XTo2Mu2J_*.root \
#  --filename='val_sample'
cd "$wd"