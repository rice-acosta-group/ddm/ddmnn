wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1
rdatafile_repnn_pcnn="../rTuples/REPNN_PCNN_2mu.root"
rdatafile_rep_pc="../rTuples/REP_PC_2mu.root"
rdatafile_dsapatlinknn_pcnn="../rTuples/DSAPATLINKNN_PCNN_2mu.root"
rdatafile_dsapatlink_pc="../rTuples/DSAPATLINK_PC_2mu.root"

# Variables
variable_name='mass'

# Monte Carlo
model_name='restimation_REPNN_PCNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input "$rdatafile_repnn_pcnn" --var "$variable_name"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input "$rdatafile_repnn_pcnn" --var "$variable_name"
$run_command r-dsa-qcd-estimation --nevents="$n_events" \
  --input "$rdatafile_dsapatlinknn_pcnn" --var "mass"
$run_command r-dsa-dy-estimation --nevents="$n_events" \
  --input "$rdatafile_dsapatlinknn_pcnn" --var "mass"
cd "$wd"

model_name='restimation_REP_PC_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input "$rdatafile_rep_pc" --var "$variable_name"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input "$rdatafile_rep_pc" --var "$variable_name"
$run_command r-dsa-qcd-estimation --nevents="$n_events" \
  --input "$rdatafile_dsapatlink_pc" --var "mass"
$run_command r-dsa-dy-estimation --nevents="$n_events" \
  --input "$rdatafile_dsapatlink_pc" --var "mass"
cd "$wd"