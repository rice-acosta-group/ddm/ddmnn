wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

# Distribution of deltaphi
dimuon_type='pat'
bin_edges='0.0,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6'
cuts='REP OS LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV2 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'DPHI' --var 'deltaphi' --bin-edges "$bin_edges"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'iDPHI' --var 'ideltaphi' --bin-edges "$bin_edges"
$run_command plot-ratio \
  --output 'fig_39' \
  --xtitle "|#Delta#Phi*|" --ytitle "Events / 0.20 units" --rtitle "R_{DY}" \
  --rmin 0.5 --rmax 1.5 --rtarget 1 \
  --num "outputs.root:dim_pat_deltaphi" \
  --den "outputs.root:dim_pat_ideltaphi" \
  --num-label "|#Delta#Phi*| #equiv |#Delta#Phi|        (|#Delta#Phi|<#pi/2)" \
  --den-label "|#Delta#Phi*| #equiv #pi - |#Delta#Phi| (|#Delta#Phi|>#pi/2)"
cd "$wd"

# Distribution of iso
dimuon_type='pat'
cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV2 PXL MAXPT25 NTRKLAYSLXY HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'OS' --var 'maxlargeiso' --prefix 'os'
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'SS' --var 'maxlargeiso' --prefix 'ss'
$run_command plot-ratio \
  --output 'fig_41a' \
  --xtitle "max(rel largeiso)" --ytitle "Events / bin" --rtitle "SS/OS" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 0 --rmax 1 \
  --num "outputs.root:dim_pat_ss_maxlargeiso" \
  --den "outputs.root:dim_pat_os_maxlargeiso" \
  --num-label "Data (SS)" \
  --den-label "Data (OS)"
cd "$wd"

# Distribution of iso
dimuon_type='pat'
cuts='REP DPHI4 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV2 PXL MAXPT25 NTRKLAYSLXY HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'OS' --var 'maxlargeiso' --prefix 'os'
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'SS' --var 'maxlargeiso' --prefix 'ss'
$run_command plot-ratio \
  --output 'fig_41b' \
  --xtitle "max(rel largeiso)" --ytitle "Events / bin" --rtitle "SS/OS" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 0 --rmax 1 \
  --num "outputs.root:dim_pat_ss_maxlargeiso" \
  --den "outputs.root:dim_pat_os_maxlargeiso" \
  --num-label "Data (SS)" \
  --den-label "Data (OS)"
cd "$wd"

# Validation mass in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='mind0sigpv'
bin_edges='0, 1, 2, 3, 4, 6'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_46_mind0sigpv6_dytf1p0' --normalize-by-bin --scale 1.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_mind0sigpv" \
  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
cd "$wd"

# Validation mass in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='mind0sigpv'
bin_edges='0, 1, 2, 3, 4, 6'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command stack-yields \
  --output 'fig_46_mind0sigpv6' --normalize-by-bin --scale 1.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_mind0sigpv" \
  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
$run_command plot-ratio \
  --output 'fig_46_dy_tf_mind0sigpv6' --normalize-by-bin --scale 1.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" --rtitle "R_{DY}" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 0 --rmax 2 --rtarget 1 \
  --num "outputs.root:dim_pat_dy_mind0sigpv_tf_num" \
  --den "outputs.root:dim_pat_dy_mind0sigpv_tf_den" \
  --num-label "Data (#Delta#phi < #pi/2)" \
  --den-label "Data (#Delta#phi > 3#pi/4)"
$run_command plot-ratio \
  --output 'fig_46_qcd_tf_mind0sigpv6' --normalize-by-bin --scale 1.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" --rtitle "R_{QCD}" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 2 --rmax 3.5 \
  --num "outputs.root:dim_pat_qcd_mind0sigpv_tf_num" \
  --den "outputs.root:dim_pat_qcd_mind0sigpv_tf_den" \
  --num-label "Data (OS)" \
  --den-label "Data (SS)"
cd "$wd"

# Validation mass in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='minsmalld0sigpv'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV2 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REPNN_PCNN_debug'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples2/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var
$run_command stack-yields \
  --output 'fig_46_mind0sigpv0p2' --normalize-by-bin --scale 0.2 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 0.2 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_minsmalld0sigpv" \
  --qcd "outputs.root:dim_pat_qcd_minsmalld0sigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_minsmalld0sigpv_estimate"
$run_command plot-ratio \
  --output 'fig_46_dy_tf_mind0sigpv0p2' --normalize-by-bin --scale 0.2 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 0.2 units" --rtitle "R_{DY}" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 0 --rmax 2 --rtarget 1 \
  --num "outputs.root:dim_pat_dy_minsmalld0sigpv_tf_num" \
  --den "outputs.root:dim_pat_dy_minsmalld0sigpv_tf_den" \
  --num-label "Data (#Delta#phi < #pi/2)" \
  --den-label "Data (#Delta#phi > 3#pi/4)"
$run_command plot-ratio \
  --output 'fig_46_qcd_tf_mind0sigpv0p2' --normalize-by-bin --scale 0.2 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 0.2 units" --rtitle "R_{QCD}" \
  --num-color 600 --den-color 632 \
  --ymin 1 --ymax 1e5 \
  --rmin 2 --rmax 3.5 \
  --num "outputs.root:dim_pat_qcd_minsmalld0sigpv_tf_num" \
  --den "outputs.root:dim_pat_qcd_minsmalld0sigpv_tf_den" \
  --num-label "Data (OS)" \
  --den-label "Data (SS)"
cd "$wd"