wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

## Distribution of deltaphi
#dimuon_type='pat'
#bin_edges='0.0,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6'
#cuts='REP OS LXYE MASS CHI2 COSA LXYS DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHI' --var 'deltaphi' --bin-edges "$bin_edges"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'iDPHI' --var 'ideltaphi' --bin-edges "$bin_edges"
#$run_command plot-ratio \
#  --output 'fig_39' \
#  --xtitle "|#Delta#Phi*|" --ytitle "Events / 0.20 units" --rtitle "R_{DY}" \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_pat_deltaphi" \
#  --den "outputs.root:dim_pat_ideltaphi" \
#  --num-label "|#Delta#Phi*| #equiv |#Delta#Phi|        (|#Delta#Phi|<#pi/2)" \
#  --den-label "|#Delta#Phi*| #equiv #pi - |#Delta#Phi| (|#Delta#Phi|>#pi/2)"
#cd "$wd"

## Distribution of mass
#dimuon_type='pat'
#bin_edges='10, 30, 50, 70, 90, 110, 150, 200'
#cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV ID0SIGPV10 PXL MAXPT25 NTRKLAYSLXY IISO ISO0P5 HLTRun3'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'PSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'os'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IPSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'ss'
#$run_command plot-ratio \
#  --output 'fig_40a' \
#  --xtitle "m_{#mu#mu}" --ytitle "Events / bin" --rtitle "R_{QCD}" \
#  --ymin 0.1 --ymax 1e4 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --num "outputs.root:dim_pat_os_mass" \
#  --den "outputs.root:dim_pat_ss_mass" \
#  --num-label "Data (OS)" \
#  --den-label "Data (SS)"
#cd "$wd"
#
## Distribution of mass
#dimuon_type='pat'
#bin_edges='10, 30, 50, 70, 90, 110, 150, 200'
#cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV10 ID0SIGPV20 PXL MAXPT25 NTRKLAYSLXY IISO ISO0P5 HLTRun3'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'PSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'os'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IPSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'ss'
#$run_command plot-ratio \
#  --output 'fig_40b' \
#  --xtitle "m_{#mu#mu}" --ytitle "Events / bin" --rtitle "R_{QCD}" \
#  --ymin 0.1 --ymax 1e4 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --num "outputs.root:dim_pat_os_mass" \
#  --den "outputs.root:dim_pat_ss_mass" \
#  --num-label "Data (OS)" \
#  --den-label "Data (SS)"
#cd "$wd"
#
## Distribution of mass
#dimuon_type='pat'
#bin_edges='10, 30, 50, 70, 90, 110, 150, 200'
#cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV20 PXL MAXPT25 NTRKLAYSLXY IISO ISO0P5 HLTRun3'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'PSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'os'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IPSCORE0p5' --var 'mass' --bin-edges "$bin_edges" --prefix 'ss'
#$run_command plot-ratio \
#  --output 'fig_40c' \
#  --xtitle "m_{#mu#mu}" --ytitle "Events / bin" --rtitle "R_{QCD}" \
#  --ymin 0.1 --ymax 1e4 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --num "outputs.root:dim_pat_os_mass" \
#  --den "outputs.root:dim_pat_ss_mass" \
#  --num-label "Data (OS)" \
#  --den-label "Data (SS)"
#cd "$wd"
#
## Distribution of iso
#dimuon_type='pat'
#cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY HLTRun3'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'PSCORE0p5' 'IISO' --var 'maxlargeiso' --prefix 'os'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IPSCORE0p5' --var 'maxlargeiso' --prefix 'ss'
#$run_command plot-ratio \
#  --output 'fig_41a' \
#  --xtitle "max(rel largeiso)" --ytitle "Events / bin" --rtitle "SS/OS" \
#  --num-color 600 --den-color 632 \
#  --ymin 1 --ymax 1e5 \
#  --rmin 0 --rmax 1 \
#  --num "outputs.root:dim_pat_ss_maxlargeiso" \
#  --den "outputs.root:dim_pat_os_maxlargeiso" \
#  --num-label "Data (SS)" \
#  --den-label "Data (OS, fails iso < 0.075)"
#cd "$wd"
#
## Distribution of iso
#dimuon_type='pat'
#cuts='REP DPHI4 LXYE MASS CHI2 COSA LXYS DCA D0SIGPV PXL MAXPT25 NTRKLAYSLXY HLTRun3'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'PSCORE0p5' --var 'maxlargeiso' --prefix 'os'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IPSCORE0p5' --var 'maxlargeiso' --prefix 'ss'
#$run_command plot-ratio \
#  --output 'fig_41b' \
#  --xtitle "max(rel largeiso)" --ytitle "Events / bin" --rtitle "SS/OS" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e5 \
#  --rmin 0 --rmax 1 \
#  --num "outputs.root:dim_pat_ss_maxlargeiso" \
#  --den "outputs.root:dim_pat_os_maxlargeiso" \
#  --num-label "Data (SS)" \
#  --den-label "Data (OS)"
#cd "$wd"

## Validation mind0sigpv in pi/4 < |dphi| < pi/2
#dimuon_type='pat'
#var='mind0sigpv'
#bin_edges='0, 2, 4, 6, 10, 20, 30'
#cuts='REP OS DPHI2 LXYE MASS CHI2 COSA LXYS DCA PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command stack-yields \
#  --output 'fig_42' --normalize-by-bin --scale 2.0 \
#  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 2 units" \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_mind0sigpv" \
#  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
#  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
#cd "$wd"
#
## Validation lxysigpv in |dphi| < pi/4 and 2 < mind0sigpv < 6
#dimuon_type='pat'
#var='lxysigpv'
#bin_edges='0, 2, 4, 6, 8, 10, 20, 30'
#cuts='REP OS DPHI1 LXYE MASS CHI2 COSA DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command stack-yields \
#  --output 'fig_43a' --normalize-by-bin --scale 2.0 \
#  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 2 units" \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_lxysigpv" \
#  --qcd "outputs.root:dim_pat_qcd_lxysigpv_estimate" \
#  --dy "outputs.root:dim_pat_dy_lxysigpv_estimate"
#cd "$wd"
#
## Validation lxysigpv in |dphi| < pi/30 and 2 < mind0sigpv < 6
#dimuon_type='pat'
#var='lxysigpv'
#bin_edges='0, 2, 4, 6, 8, 10, 20, 30'
#cuts='REP OS DPHIb30 LXYE MASS CHI2 COSA DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
#$run_command stack-yields \
#  --output 'fig_43b' --normalize-by-bin --scale 2.0 \
#  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 2 units" \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_lxysigpv" \
#  --qcd "outputs.root:dim_pat_qcd_lxysigpv_estimate" \
#  --dy "outputs.root:dim_pat_dy_lxysigpv_estimate"
#cd "$wd"
#
## Validation mass in |dphi| < pi/4 and 2 < mind0sigpv < 6
#dimuon_type='pat'
#var='mass'
#bin_edges='10, 30, 50, 80, 100, 150, 200'
#cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command stack-yields \
#  --output 'fig_44' --normalize-by-bin --scale 20.0 \
#  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events / 20 GeV" \
#  --ymin 0.1 --ymax 1e4 \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_mass" \
#  --qcd "outputs.root:dim_pat_qcd_mass_estimate" \
#  --dy "outputs.root:dim_pat_dy_mass_estimate"
#cd "$wd"
#
## Validation masscor in |dphi| < pi/4 and 2 < mind0sigpv < 6
#dimuon_type='pat'
#var='masscor'
#bin_edges='10, 30, 50, 80, 100, 150, 200, 250, 300, 350'
#cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command stack-yields \
#  --output 'fig_45' --normalize-by-bin --scale 20.0 \
#  --xtitle "Corrected m_{#mu#mu} [GeV]" --ytitle "Events / 20 GeV" \
#  --ymin 0.1 --ymax 1e4 \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_masscor" \
#  --qcd "outputs.root:dim_pat_qcd_masscor_estimate" \
#  --dy "outputs.root:dim_pat_dy_masscor_estimate"
#cd "$wd"
#
## Validation mass in |dphi| < pi/4 and mind0sigpv < 6
#dimuon_type='pat'
#var='mind0sigpv'
#bin_edges='0, 1, 2, 3, 4, 6'
#cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'
#
#model_name='rvalidation_REPNN_PCNN_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-qcd-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command r-pat-dy-estimation --nevents="$n_events" \
#  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --cuts $cuts --var $var --bin-edges "$bin_edges"
#$run_command stack-yields \
#  --output 'fig_46' --normalize-by-bin --scale 1.0 \
#  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" \
#  --rmin 0 --rmax 2 \
#  --observed "outputs.root:dim_pat_mind0sigpv" \
#  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
#  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
#cd "$wd"

# Validation pscore in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='pscore'
bin_edges='n:10:0:1'
cuts='REP DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY HLTRun3'

model_name='rvalidation_REPNN_PCNN_data'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'ISO' --var $var --bin-edges "$bin_edges" --prefix 'ss_iso' \
  --min 0.9 --max 1e5
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'OS' 'IISO' 'ISO0P5' --var $var --bin-edges "$bin_edges" --prefix 'os_iiso' \
  --min 0.9 --max 1e5
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'SS' 'IISO' 'ISO0P5' --var $var --bin-edges "$bin_edges" --prefix 'ss_iiso' \
  --min 0.9 --max 1e5
cd "$wd"

# Validation pscore in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='pscore'
bin_edges='n:10:0:1'
cuts='REP OS LXYE MASS COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'

model_name='rvalidation_REPNN_PCNN_data'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'DPHI4' 'CHI2' --var $var --bin-edges "$bin_edges" --prefix 'dphi4_chi2' \
  --min 0.9 --max 1e5
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'DPHI1' 'ICHI2' --var $var --bin-edges "$bin_edges" --prefix 'dphi1_ichi2' \
  --min 0.9 --max 1e5
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples4/2022/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts 'DPHI4' 'ICHI2' --var $var --bin-edges "$bin_edges" --prefix 'dphi4_ichi2' \
  --min 0.9 --max 1e5
cd "$wd"