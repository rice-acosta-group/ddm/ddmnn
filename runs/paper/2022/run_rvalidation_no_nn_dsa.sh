wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

## Distribution of deltaphi
#dimuon_type='dsa'
#bin_edges='n:10:0:1.5708'
#cuts='DSA3 OS DYTFPATCUTS LXYE20 MASS CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHIb2' --var 'deltaphi' --bin-edges "$bin_edges"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'iDPHIb2' --var 'ideltaphi' --bin-edges "$bin_edges"
#$run_command plot-ratio \
#  --output 'fig_32a' \
#  --xtitle "|#Delta#Phi*|" --ytitle "Events" --rtitle "#frac{N(|#Delta#Phi|<#pi/2)}{N(|#Delta#Phi|>#pi/2)}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_deltaphi" \
#  --den "outputs.root:dim_dsa_ideltaphi" \
#  --num-label "|#Delta#Phi*| #equiv |#Delta#Phi|        (|#Delta#Phi|<#pi/2)" \
#  --den-label "|#Delta#Phi*| #equiv #pi - |#Delta#Phi| (|#Delta#Phi|>#pi/2)"
#cd "$wd"
#
## Distribution of lxysigpv
#dimuon_type='dsa'
#bin_edges='n:15:0:30'
#cuts='DSA3 OS DYTFPATCUTS LXYE20 MASS CHI2 COSA DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHI' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'dphi'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDPHI3' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'idphi3'
#$run_command plot-ratio \
#  --output 'fig_32b' \
#  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events" --rtitle "#frac{N(|#Delta#Phi|<#pi/4)}{N(|#Delta#Phi|>#pi/4)}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_dphi_lxysigpv" \
#  --den "outputs.root:dim_dsa_idphi3_lxysigpv" \
#  --num-label "|#Delta#Phi|<#pi/4" \
#  --den-label "|#Delta#Phi|>3#pi/4"
#cd "$wd"

## Validation DY dphi
#dimuon_type='dsa'
#bin_edges='n:5:0:1.5708'
#cuts='REP OS LXYE20 MASS15 CHI2 COSA ILXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 HLTRun3'
#tf_cuts='DSA3 OS DYTFPATCUTS LXYE20 MASS15 CHI2 COSA ILXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHIb2' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'dy_vr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'iDPHIb2' --var 'ideltaphi' --bin-edges "$bin_edges" --prefix 'dy_cr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'DPHIb2' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'dy_tf_num'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'iDPHIb2' --var 'ideltaphi' --bin-edges "$bin_edges" --prefix 'dy_tf_den'
#$run_command plot-ratio \
#  --output 'fig_34a_tf' --normalize-by-bin --scale 0.314 \
#  --xtitle "|#Delta#Phi*|" --ytitle "Events / 0.314 units" --rtitle "R_{DY}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_dy_tf_num_deltaphi" \
#  --den "outputs.root:dim_dsa_dy_tf_den_ideltaphi" \
#  --num-label "|#Delta#Phi|<#pi/2" \
#  --den-label "|#Delta#Phi|>#pi/2"
#$run_command estimate-yields \
#  --estimate 'outputs.root:dim_dsa_dy_pred_deltaphi' \
#  --control 'outputs.root:dim_dsa_dy_cr_ideltaphi' \
#  --tf-num 'outputs.root:dim_dsa_dy_tf_num_deltaphi' \
#  --tf-den 'outputs.root:dim_dsa_dy_tf_den_ideltaphi'
#$run_command stack-yields \
#  --output 'fig_34a' --normalize-by-bin --scale 0.314 \
#  --xtitle "|#Delta#Phi|" --ytitle "Events / 0.314 units" \
#  --ymin 0 --ymax 15 --logy 0 \
#  --rmin 0 --rmax 4 --rtarget 1 \
#  --observed "outputs.root:dim_dsa_dy_vr_deltaphi" \
#  --dy "outputs.root:dim_dsa_dy_pred_deltaphi"
#cd "$wd"
#
## Validation DY lxysigpv
#dimuon_type='dsa'
#bin_edges='n:6:0:6'
#cuts='REP OS LXYE20 MASS15 CHI2 COSA ILXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 HLTRun3'
#tf_cuts='DSA3 OS DYTFPATCUTS LXYE20 MASS15 CHI2 COSA ILXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHI' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'dy_vr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDPHI3' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'dy_cr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'DPHI' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'dy_tf_num'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'IDPHI3' --var 'lxysigpv' --bin-edges "$bin_edges" --prefix 'dy_tf_den'
#$run_command plot-ratio \
#  --output 'fig_34b_tf' --normalize-by-bin --scale 1.0 \
#  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 1 unit" --rtitle "R_{DY}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_dy_tf_num_lxysigpv" \
#  --den "outputs.root:dim_dsa_dy_tf_den_lxysigpv" \
#  --num-label "|#Delta#Phi|<#pi/4" \
#  --den-label "|#Delta#Phi|>3#pi/4"
#$run_command estimate-yields \
#  --estimate 'outputs.root:dim_dsa_dy_pred_lxysigpv' \
#  --control 'outputs.root:dim_dsa_dy_cr_lxysigpv' \
#  --tf-num 'outputs.root:dim_dsa_dy_tf_num_lxysigpv' \
#  --tf-den 'outputs.root:dim_dsa_dy_tf_den_lxysigpv'
#$run_command stack-yields \
#  --output 'fig_34b' --normalize-by-bin --scale 1.0 \
#  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 1 unit" \
#  --ymin 0 --ymax 15 --logy 0 \
#  --rmin 0 --rmax 4 --rtarget 1 \
#  --observed "outputs.root:dim_dsa_dy_vr_lxysigpv" \
#  --dy "outputs.root:dim_dsa_dy_pred_lxysigpv"
#cd "$wd"

## Distribution of deltaphi
#dimuon_type='dsa'
#bin_edges='n:5:0:0.785'
#cuts='REP SS LXYE20 MASS CHI2 COSA LXYS DCA DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'ss_iso'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'ss_noniso'
#$run_command plot-ratio \
#  --output 'fig_35a' \
#  --xtitle "|#Delta#Phi|" --ytitle "Events" --rtitle "#frac{N(SS Iso)}{N(SS Non Iso)}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_ss_iso_deltaphi" \
#  --den "outputs.root:dim_dsa_ss_noniso_deltaphi" \
#  --num-label "STA Rel Iso < 0.15" \
#  --den-label "fails STA Rel Iso < 0.15"
#cd "$wd"
#
## Distribution of mass
#dimuon_type='dsa'
#bin_edges='0,6,10,20,30,50,80,100'
#cuts='REP SS LXYE20 CHI2 COSA LXYS DCA DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'ss_iso'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'ss_noniso'
#$run_command plot-ratio \
#  --output 'fig_35b' \
#  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events" --rtitle "#frac{N(SS Iso)}{N(SS Non Iso)}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_ss_iso_mass" \
#  --den "outputs.root:dim_dsa_ss_noniso_mass" \
#  --num-label "STA Rel Iso < 0.15" \
#  --den-label "fails STA Rel Iso < 0.15"
#cd "$wd"

## Validation QCD dphi
#dimuon_type='dsa'
#bin_edges='n:5:0:0.785'
#cuts='REP OS LXYE20 MASS6TO10 CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#tf_cuts='REP SS LXYE20 MASS6TO10 CHI2 COSA LXYS DCA DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'qcd_vr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'qcd_cr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'DSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'qcd_tf_num'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'IDSAISO0p15' --var 'deltaphi' --bin-edges "$bin_edges" --prefix 'qcd_tf_den'
#$run_command plot-ratio \
#  --output 'fig_36a_tf' --normalize-by-bin --scale 0.157 \
#  --xtitle "|#Delta#Phi|" --ytitle "Events / 0.157 units" --rtitle "R_{QCD}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --num "outputs.root:dim_dsa_qcd_tf_num_deltaphi" \
#  --den "outputs.root:dim_dsa_qcd_tf_den_deltaphi" \
#  --num-label "STA Rel Iso < 0.15" \
#  --den-label "fails STA Rel Iso < 0.15"
#$run_command estimate-yields \
#  --estimate 'outputs.root:dim_dsa_qcd_pred_deltaphi' \
#  --control 'outputs.root:dim_dsa_qcd_cr_deltaphi' \
#  --tf-num 'outputs.root:dim_dsa_qcd_tf_num_deltaphi' \
#  --tf-den 'outputs.root:dim_dsa_qcd_tf_den_deltaphi'
#$run_command stack-yields \
#  --output 'fig_36a' --normalize-by-bin --scale 0.157 \
#  --xtitle "|#Delta#Phi|" --ytitle "Events / 0.157 units" \
#  --ymin 0.1 --ymax 2e3 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --observed "outputs.root:dim_dsa_qcd_vr_deltaphi" \
#  --qcd "outputs.root:dim_dsa_qcd_pred_deltaphi"
#cd "$wd"
#
## Validation QCD mass
#dimuon_type='dsa'
#bin_edges='n:4:6:10'
#cuts='REP OS LXYE20 MASS6TO10 CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#tf_cuts='REP SS LXYE20 MASS6TO10 CHI2 COSA LXYS DCA DSATIME DIR BBDSATIMEDIFF DPHI HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'qcd_vr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'qcd_cr'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'DSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'qcd_tf_num'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $tf_cuts 'IDSAISO0p15' --var 'mass' --bin-edges "$bin_edges" --prefix 'qcd_tf_den'
#$run_command plot-ratio \
#  --output 'fig_36b_tf' --normalize-by-bin --scale 1.0 \
#  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events / 1 GeV" --rtitle "R_{QCD}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 1 \
#  --num "outputs.root:dim_dsa_qcd_tf_num_mass" \
#  --den "outputs.root:dim_dsa_qcd_tf_den_mass" \
#  --num-label "STA Rel Iso < 0.15" \
#  --den-label "fails STA Rel Iso < 0.15"
#$run_command estimate-yields \
#  --estimate 'outputs.root:dim_dsa_qcd_pred_mass' \
#  --control 'outputs.root:dim_dsa_qcd_cr_mass' \
#  --tf-num 'outputs.root:dim_dsa_qcd_tf_num_mass' \
#  --tf-den 'outputs.root:dim_dsa_qcd_tf_den_mass'
#$run_command stack-yields \
#  --output 'fig_36b' --normalize-by-bin --scale 1.0 \
#  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events / 1 GeV" \
#  --ymin 0 --ymax 105 --logy 0 \
#  --rmin 0 --rmax 3 --rtarget 1 \
#  --observed "outputs.root:dim_dsa_qcd_vr_mass" \
#  --qcd "outputs.root:dim_dsa_qcd_pred_mass"
#cd "$wd"

## Distribution of mass
#dimuon_type='dsa'
#bin_edges='n:40:0:200'
#cuts='DSA3 OS DYTFPATCUTS LXYE20 MASS CHI2 COSA ILXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3'
#
#model_name='rvalidation_REP_PC_data'
#mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'DPHI' --var 'mass' --bin-edges "$bin_edges" --prefix 'dphi'
#$run_command r-check-yields --nevents="$n_events" \
#  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --cuts $cuts 'IDPHI3' --var 'mass' --bin-edges "$bin_edges" --prefix 'idphi3'
#$run_command plot-ratio \
#  --output 'fig_38' \
#  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events" --rtitle "#frac{N(|#Delta#Phi|<#pi/4)}{N(|#Delta#Phi|>#pi/4)}" \
#  --num-color 600 --den-color 632 \
#  --ymin 0.1 --ymax 1e8 \
#  --rmin 0 --rmax 2 --rtarget 1 \
#  --num "outputs.root:dim_dsa_dphi_mass" \
#  --den "outputs.root:dim_dsa_idphi3_mass" \
#  --num-label "|#Delta#Phi|<#pi/4" \
#  --den-label "|#Delta#Phi|>3#pi/4"
#cd "$wd"

## UPDATE
#run_command="../bin/run $gflags"
#
## Check estimations
#dimuon_type='dsa'
#masswindow='MASS10TO15'
#
#cuts="REP OS LXYE20 ${masswindow} CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 DPHIb10 HLTRun3"
#
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'REP' --cuts $cuts
#
#cuts="REP OS LXYE20 ${masswindow} CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DSAISO0p15 HLTRun3"
#tf_cuts="DSA3 OS DYTFPATCUTS LXYE20 ${masswindow} CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF HLTRun3"
#
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'REP' --cuts $cuts 'iDPHIb10'
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'DSA3' --cuts $tf_cuts 'iDPHIb10'
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_DSAPATLINK_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'DSA3' --cuts $tf_cuts 'DPHIb10'
#
#cuts="REP OS LXYE20 ${masswindow} CHI2 COSA LXYS DCA DETANDT DETASEG SEG DSATIME DIR BBDSATIMEDIFF DPHIb10 HLTRun3"
#tf_cuts="REP SS LXYE20 ${masswindow} CHI2 COSA LXYS DCA DSATIME DIR BBDSATIMEDIFF DPHIb10 HLTRun3"
#
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'REP' --cuts $cuts 'IDSAISO0p15'
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'REP' --cuts $tf_cuts 'IDSAISO0p15'
#$run_command r-check-cutflow --nevents="$n_events" \
#  --input ../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
#  --dimuon-type $dimuon_type --base-cuts 'REP' --cuts $tf_cuts 'DSAISO0p15'