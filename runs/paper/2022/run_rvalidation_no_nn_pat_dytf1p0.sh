wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1

# Validation mind0sigpv in pi/4 < |dphi| < pi/2
dimuon_type='pat'
var='mind0sigpv'
bin_edges='0, 2, 4, 6, 10, 20, 30'
cuts='REP OS DPHI2 LXYE MASS CHI2 COSA LXYS DCA PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_42' --normalize-by-bin --scale 2.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 2 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_mind0sigpv" \
  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
cd "$wd"

# Validation lxysigpv in |dphi| < pi/4 and 2 < mind0sigpv < 6
dimuon_type='pat'
var='lxysigpv'
bin_edges='0, 2, 4, 6, 8, 10, 20, 30'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_43a' --normalize-by-bin --scale 2.0 \
  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 2 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_lxysigpv" \
  --qcd "outputs.root:dim_pat_qcd_lxysigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_lxysigpv_estimate"
cd "$wd"

# Validation lxysigpv in |dphi| < pi/30 and 2 < mind0sigpv < 6
dimuon_type='pat'
var='lxysigpv'
bin_edges='0, 2, 4, 6, 8, 10, 20, 30'
cuts='REP OS DPHIb30 LXYE MASS CHI2 COSA DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3 PSCORE0p5'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig0p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_43b' --normalize-by-bin --scale 2.0 \
  --xtitle "L_{xy}/#sigma_{Lxy}" --ytitle "Events / 2 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_lxysigpv" \
  --qcd "outputs.root:dim_pat_qcd_lxysigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_lxysigpv_estimate"
cd "$wd"

# Validation mass in |dphi| < pi/4 and 2 < mind0sigpv < 6
dimuon_type='pat'
var='mass'
bin_edges='10, 30, 50, 80, 100, 150, 200'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_44' --normalize-by-bin --scale 20.0 \
  --xtitle "m_{#mu#mu} [GeV]" --ytitle "Events / 20 GeV" \
  --ymin 0.1 --ymax 1e4 \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_mass" \
  --qcd "outputs.root:dim_pat_qcd_mass_estimate" \
  --dy "outputs.root:dim_pat_dy_mass_estimate"
cd "$wd"

# Validation masscor in |dphi| < pi/4 and 2 < mind0sigpv < 6
dimuon_type='pat'
var='masscor'
bin_edges='10, 30, 50, 80, 100, 150, 200, 250, 300, 350'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA MIND0SIGPV2TO6 PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_45' --normalize-by-bin --scale 20.0 \
  --xtitle "Corrected m_{#mu#mu} [GeV]" --ytitle "Events / 20 GeV" \
  --ymin 0.1 --ymax 1e4 \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_masscor" \
  --qcd "outputs.root:dim_pat_qcd_masscor_estimate" \
  --dy "outputs.root:dim_pat_dy_masscor_estimate"
cd "$wd"

# Validation mass in |dphi| < pi/4 and mind0sigpv < 6
dimuon_type='pat'
var='mind0sigpv'
bin_edges='0, 1, 2, 3, 4, 6'
cuts='REP OS DPHI1 LXYE MASS CHI2 COSA LXYS DCA ID0SIGPV PXL MAXPT25 NTRKLAYSLXY ISO HLTRun3'

model_name='rvalidation_REP_PC_data_dytf1p0'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command r-check-yields --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --dimuon-type $dimuon_type --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-qcd-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges"
$run_command r-pat-dy-estimation --nevents="$n_events" \
  --input ../../in/rTuples3/2022/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_*MuonRun2022*.root \
  --cuts $cuts --var $var --bin-edges "$bin_edges" --tf 1.0
$run_command stack-yields \
  --output 'fig_46' --normalize-by-bin --scale 1.0 \
  --xtitle "min(d_{0}/#sigma_{d_{0}})" --ytitle "Events / 1 units" \
  --rmin 0 --rmax 2 \
  --observed "outputs.root:dim_pat_mind0sigpv" \
  --qcd "outputs.root:dim_pat_qcd_mind0sigpv_estimate" \
  --dy "outputs.root:dim_pat_dy_mind0sigpv_estimate"
cd "$wd"