lxysigcut=6.0
lxysigcutstr=`echo $lxysigcut | tr '.' 'p'`
selection='Max$(dim_LxySig_pv) > '"${lxysigcut}"
options='_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY'
#options='_VTX_NPP_NS_NH_HLT_DSAPATLINKNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY'
sourcedir="/home/om15/Datasets/DDM/rTuples4/2022/${options}_LxySig0p0"
targetdir="/home/om15/Datasets/DDM/rTuples4/2022/${options}_LxySig${lxysigcutstr}"

mkdir -p "$targetdir"
echo "Selection: ${selection}"
echo "Source Directory: ${sourcedir}"
echo "Target Directory: ${targetdir}"

#filename="rntuple_2022_DoubleMuonRun2022C-ReReco-v1_0.root"
#rooteventselector --recreate -s "$selection" \
#  "${sourcedir}/${filename}:DDTree" \
#  "${targetdir}/${filename}"
#
#filename="rntuple_2022_MuonRun2022C-ReReco-v1_0.root"
#rooteventselector --recreate -s "$selection" \
#  "${sourcedir}/${filename}:DDTree" \
#  "${targetdir}/${filename}"
#
#filename="rntuple_2022_MuonRun2022D-ReReco-v1_0.root"
#rooteventselector --recreate -s "$selection" \
#  "${sourcedir}/${filename}:DDTree" \
#  "${targetdir}/${filename}"
#
#filename="rntuple_2022_MuonRun2022E-ReReco-v1_0.root"
#rooteventselector --recreate -s "$selection" \
#  "${sourcedir}/${filename}:DDTree" \
#  "${targetdir}/${filename}"

filename="rntuple_2022_MuonRun2022F-PromptReco-v2_0.root"
rooteventselector --recreate -s "$selection" \
  "${sourcedir}/${filename}:DDTree" \
  "${targetdir}/${filename}"

#filename="rntuple_2022_MuonRun2022G-PromptReco-v1_0.root"
#rooteventselector --recreate -s "$selection" \
#  "${sourcedir}/${filename}:DDTree" \
#  "${targetdir}/${filename}"
