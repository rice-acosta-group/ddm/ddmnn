wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags r-check-performance"

# Inputs
n_events=-1

# Monte Carlo
model_name='rperformance_REPNN_PCNN_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --input ../../in/rTuples/_VTX_NPP_NS_NH_HLT_REPNN_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PCNN_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"

model_name='rperformance_REP_PC_2mu'
mkdir -p "$model_name" && cd "$wd/$model_name"
$run_command --nevents="$n_events" \
  --input ../../in/rTuples/_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY_LxySig6p0/rntuple_2022_HTo2XTo2Mu2J_*.root
cd "$wd"
