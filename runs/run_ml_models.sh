wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# DPA
model_name='association'
mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command association --train --lr 0.0002 \
#  "$model_name" "../association_ml_samples/llp_sample.npz"
$run_command association --lr 0.0002 \
  "$model_name" "../association_ml_samples/val_sample.npz"
cd "$wd"

# Pairing
model_name='pairing'
mkdir -p "$model_name" && cd "$wd/$model_name"
#$run_command pairing --train --lr 0.001 \
#  "$model_name" "../pairing_ml_samples/llp_sample.npz"
$run_command pairing --lr 0.0002 \
  "$model_name" "../pairing_ml_samples/val_sample.npz"
cd "$wd"
