# Macros

Numerical macros used to calculate complicated functions used in TTree draw (for variable expressions or selections).
Each macro be named according to the function defined inside of it. See the
link [here](https://root.cern.ch/root/htmldoc/guides/primer/ROOTPrimer.html#general-remarks-on-root-macros) for more
details.

Macros should be used sparingly, as they can non-negligibly increase processing time of the ROOT Tree.