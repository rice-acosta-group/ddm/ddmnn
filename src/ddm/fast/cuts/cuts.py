from abc import ABC, abstractmethod

from ddm.fast.commons.expressions import compile_expression


class BaseCut(ABC):

    @abstractmethod
    def get_expression(self, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def get_latex(self, **kwargs):
        raise NotImplementedError()

    @abstractmethod
    def invert(self):
        raise NotImplementedError()


class Cut(BaseCut):

    def __init__(self, expression, latex):
        self.expression = expression
        self.latex = latex

    def get_expression(self, **kwargs):
        return self.expression

    def get_latex(self, **kwargs):
        return self.latex

    def invert(self):
        return Cut(
            '!(' + self.expression + ')',
            'fails ' + self.latex
        )


class YearDependantCut(BaseCut):

    def __init__(self, cuts_by_year):
        self.cuts_by_year = cuts_by_year

    def get_cut(self, year=None, **kwargs):
        if year is None:
            raise KeyError("Year is not defined as keyword argument in {}".format(kwargs))

        return self.cuts_by_year[year]

    def get_expression(self, **kwargs):
        return self.get_cut(**kwargs).get_expression()

    def get_latex(self, **kwargs):
        return self.get_cut(**kwargs).get_latex()

    def invert(self):
        inverted_cuts = {}

        for year, cut in self.cuts_by_year.items():
            inverted_cuts[year] = cut.invert()

        return YearDependantCut(inverted_cuts)


class CutTemplate:
    def __init__(self, expression, latex=None):
        self.expression = expression

        if latex is None:
            latex = self.expression

        self.latex = latex

    def compile(self, **kwargs):
        # Compile
        compiled_expression = compile_expression(self.expression, **kwargs)

        # Return
        return Cut(compiled_expression, self.latex)


class YearDependentCutTemplate:
    def __init__(self, cuts_by_year):
        self.cuts_by_year = cuts_by_year

    def compile(self, **kwargs):
        compiled_cuts = dict()

        for year, template in self.cuts_by_year.items():
            compiled_cuts[year] = template.compile(**kwargs)

        return YearDependantCut(compiled_cuts)
