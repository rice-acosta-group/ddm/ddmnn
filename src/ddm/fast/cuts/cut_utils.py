def remove_duplicate_cuts(cuts):
    unique_cuts = list()

    for cut in cuts:
        if cut in unique_cuts:
            continue

        unique_cuts.append(cut)

    return unique_cuts
