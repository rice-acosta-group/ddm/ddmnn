from ddm.core.commons.caching import Memoize
from ddm.fast.commons import constants
from ddm.fast.commons.expressions import dimuon_specs
from ddm.fast.cuts.cut_definitions import prerepdict, evtdict, patmudict, patdimdict, dsamudict, \
    dsadimdict, hybmudict, hybdsamudict, hybpatmudict, hybdimdict
from ddm.fast.cuts.cuts import CutTemplate, Cut


def get_cutstring(dimuon_type, cutlist, year, veto_signal_region_en=True):
    return get_cut_registry(dimuon_type).get_cutstring(
        cutlist, year, veto_signal_region_en=veto_signal_region_en
    )


@Memoize()
def get_cut_registry(dimuon_type):
    if dimuon_type == 'pat':
        return CutRegistry('pat')
    elif dimuon_type == 'hyb':
        return CutRegistry('hyb')
    elif dimuon_type == 'dsa':
        return CutRegistry('dsa')

    raise Exception('Invalid dimuon type. Valid types are pat, hyb, and dsa')


class CutRegistry(dict):

    def __init__(self, dimuon_type):
        # Init variables
        self.dimuon_type = dimuon_type

        # Unpack dimuon expression specs
        dim_specs = dimuon_specs.get(dimuon_type, None)

        if dim_specs is None:
            raise Exception("Non-implemented dimuon type")

        mu1_id, mu2_id, mu1_type, mu2_type = dim_specs

        dim_tokens = {
            'num1': 1, 'id1': mu1_id, 'type1': mu1_type,
            'num2': 2, 'id2': mu2_id, 'type2': mu2_type
        }
        mu1_tokens = {
            'num': 1, 'id': mu1_id, 'type': mu1_type
        }
        mu2_tokens = {
            'num': 2, 'id': mu2_id, 'type': mu2_type
        }

        # Select templates
        if dimuon_type == 'pat':
            sel_mudict = patmudict
            sel_dimdict = patdimdict
        elif dimuon_type == 'dsa':
            sel_mudict = dsamudict
            sel_dimdict = dsadimdict
        else:
            sel_mu1dict = hybdsamudict
            sel_mu2dict = hybpatmudict

            sel_mudict = hybmudict
            sel_dimdict = hybdimdict

        # Load templates
        cutdict = dict()
        cutdict.update(prerepdict)
        cutdict.update(evtdict)
        cutdict.update(sel_dimdict)

        # compile cutdict
        for cut_name, cut in cutdict.items():
            if isinstance(cut, list):
                continue

            cutdict[cut_name] = cut.compile(**dim_tokens)

        # add all muon only cuts
        for cut_name, cut in sel_mudict.items():
            # adding together year dependent per muon cuts is not yet implemented
            if not isinstance(cut, CutTemplate):
                raise AttributeError("Cut {} cannot be added together, is it year dependent?".format(cut_name))

            # Compile individual formulas
            mu1_cut = cut.compile(**mu1_tokens, **dim_tokens)
            mu2_cut = cut.compile(**mu2_tokens, **dim_tokens)

            # Save compiled cut
            cutdict[cut_name] = Cut(
                mu1_cut.get_expression() + '&&' + mu2_cut.get_expression(),
                mu1_cut.get_latex()
            )

        # Hybrid dsa or pat specific permuon cuts
        if dimuon_type not in ('pat', 'dsa'):
            for cut_name, cut in sel_mu1dict.items():
                if not isinstance(cut, CutTemplate):
                    raise AttributeError("Cut {} cannot be added together, is it year dependent?".format(cut_name))

                cutdict[cut_name] = cut.compile(**mu1_tokens, **dim_tokens)

            for cut_name, cut in sel_mu2dict.items():
                if not isinstance(cut, CutTemplate):
                    raise AttributeError("Cut {} cannot be added together, is it year dependent?".format(cut_name))

                cutdict[cut_name] = cut.compile(**mu2_tokens, **dim_tokens)

        # Make inverted versions of each (non-list) cut
        inverted_cuts = dict()

        for cut_name, cut in cutdict.items():
            if isinstance(cut, list):
                continue

            inverted_name = 'I' + cut_name
            inverted_cuts[inverted_name] = cut.invert()

        cutdict.update(inverted_cuts)

        # Save Cuts
        self.clear()
        self.update(cutdict)

    def unpack_cuts(self, cutlist):
        unpacked = []

        for cut_name in cutlist:
            cut = self.get(cut_name, None)

            if cut is None:
                raise NotImplementedError("Cut {} is not in the registry".format(cut_name))

            if isinstance(cut, list):
                unpacked += self.unpack_cuts(cut)
            else:
                unpacked.append(cut_name)

        return unpacked

    def sanatize_cutlist(self, cutlist, veto_signal_region_en=True):
        assert (isinstance(cutlist, list)), 'Must input list to cutter method'

        # Unpack cuts, since some are collections of others
        unpacked_cutlist = self.unpack_cuts(cutlist)

        # Short-Circuit: No need to check cuts
        if not veto_signal_region_en:
            return unpacked_cutlist

        # Check cuts
        covers_controlregion = any(
            cut in unpacked_cutlist
            for cut in constants.CONTROLCUTS[self.dimuon_type]
        )

        if not covers_controlregion:
            raise RuntimeError("Maintaining blinding. No control cut: {} in applied cuts: {}".format(
                constants.CONTROLCUTS[self.dimuon_type],
                unpacked_cutlist
            ))

        # Return
        return unpacked_cutlist

    def get_cutstring(self, cutlist, year, veto_signal_region_en=True):
        # Unpack cutlist
        cutlist = self.sanatize_cutlist(
            cutlist, veto_signal_region_en=veto_signal_region_en
        )

        # Sanity check
        if self.dimuon_type in ('pat', 'hyb') and 'REP' not in cutlist:
            raise Exception("Must add REP to look at pat/hyb muons!")

        # Build cutstring
        cutstring = '&&'.join([
            self[cut_name].get_expression(year=year)
            for cut_name in cutlist
        ])

        # Return
        return cutstring
