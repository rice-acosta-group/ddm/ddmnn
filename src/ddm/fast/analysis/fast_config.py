from ddm.core.entries import SimpleProducer
from ddm.fast.producers.DimuonVariableProducer import DimuonVariableProducer
from ddm.mini.commons.ntuple_primitives import get_event_info, get_hlt_paths, get_hlt_muons, get_l1t_muons, \
    get_l1t_seeds, get_met, get_filters, get_beamspot, get_vertex, get_gen_particles, get_pat_muons, get_dsa_muons, \
    get_dimuons, get_gen_dimuons, get_hlt_dimuons
from ddm.mini.producers.GENDimuonBranchProducer import GENDimuonBranchProducer
from ddm.mini.producers.HLTDimuonBranchProducer import HLTDimuonBranchProducer
from ddm.mini.producers.LLPGunDimuonBranchProducer import LLPGunDimuonBranchProducer

base_producers = [
    HLTDimuonBranchProducer(),
    GENDimuonBranchProducer(),
    LLPGunDimuonBranchProducer(),
    DimuonVariableProducer('dsa'),
    DimuonVariableProducer('hyb'),
    DimuonVariableProducer('pat'),
    SimpleProducer('ext_info', lambda e: get_event_info(e)),
    SimpleProducer('ext_hlt_paths', lambda e: get_hlt_paths(e)),
    SimpleProducer('ext_hlt_muons', lambda e: get_hlt_muons(e)),
    SimpleProducer('ext_hlt_dimuons', lambda e: get_hlt_dimuons(e)),
    SimpleProducer('ext_l1t_muons', lambda e: get_l1t_muons(e)),
    SimpleProducer('ext_l1t_seeds', lambda e: get_l1t_seeds(e)),
    SimpleProducer('ext_met', lambda e: get_met(e)),
    SimpleProducer('ext_filters', lambda e: get_filters(e)),
    SimpleProducer('ext_beamspot', lambda e: get_beamspot(e)),
    SimpleProducer('ext_vertex', lambda e: get_vertex(e)),
    SimpleProducer('ext_gen_particles', lambda e: get_gen_particles(e)),
    SimpleProducer('ext_gen_dimuons', lambda e: get_gen_dimuons(e)),
    SimpleProducer('ext_pat_muons', lambda e: get_pat_muons(e)),
    SimpleProducer('ext_dsa_muons', lambda e: get_dsa_muons(e)),
    SimpleProducer('ext_dimuons', lambda e: get_dimuons(e)),
]
