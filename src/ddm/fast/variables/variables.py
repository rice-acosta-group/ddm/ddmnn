from ddm.fast.commons.expressions import compile_expression


class Variable:
    def __init__(
            self, expression, latex, units=None, bin_edges=None,
            is_per_muon=False, is_gen=False, is_event_var=False
    ):
        self.expression = expression  # expression evaluated by root
        self.latex = latex  # latex name of variable
        self.units = units  # measurement unit of the variable
        self.bin_edges = bin_edges  # binning edges
        self.is_per_muon = is_per_muon  # if the quantity is plotted per muon
        self.is_gen = is_gen
        self.is_event_var = is_event_var  # if the variable is a per-event variable

    def __eq__(self, other):
        if not isinstance(other, Variable):
            return False

        return self.expression == other.expression

    def get_expression(self):
        return self.expression

    def get_latex(self):
        if self.units:
            return self.latex + ' [{}]'.format(self.units)
        else:
            return self.latex


class VariableTemplate:
    def __init__(
            self, expression, latex, units=None, bin_edges=None,
            is_per_muon=False, is_gen=False, is_event_var=False
    ):
        self.expression = expression  # expression evaluated by root
        self.latex = latex  # latex name of variable
        self.units = units  # measurement unit of the variable
        self.bin_edges = bin_edges  # histogram bin_edges
        self.is_per_muon = is_per_muon  # if the quantity is plotted per muon
        self.is_gen = is_gen
        self.is_event_var = is_event_var  # if the variable is a per-event variable

    def compile(self, **kwargs):
        # Per muon variables
        if self.is_per_muon:
            # Compile expression per ref muon (mu1, mu2)
            mu1_expression = compile_expression(
                self.expression,
                num=kwargs['num1'], id=kwargs['id1'], type=kwargs['type1']
            )
            mu2_expression = compile_expression(
                self.expression,
                num=kwargs['num2'], id=kwargs['id2'], type=kwargs['type2']
            )

            # Build vars
            var1 = Variable(
                mu1_expression, self.latex, self.units, self.bin_edges,
                self.is_per_muon, self.is_gen, self.is_event_var
            )

            var2 = Variable(
                mu2_expression, self.latex, self.units, self.bin_edges,
                self.is_per_muon, self.is_gen, self.is_event_var
            )

            # Return two variables, one for each ref muon
            return var1, var2
        else:
            # Anything else
            # Only needs to be compiled once
            expression = compile_expression(self.expression, **kwargs)

            # Build var
            var = Variable(
                expression, self.latex, self.units, self.bin_edges,
                self.is_per_muon, self.is_gen, self.is_event_var
            )

            # Return
            return var
