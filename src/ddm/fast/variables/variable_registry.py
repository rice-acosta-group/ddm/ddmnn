from ddm.core.commons.caching import Memoize
from ddm.fast.commons.expressions import dimuon_specs
from ddm.fast.variables.variable_definitions import genmudict, gendict, evtdict, mudict, dimdict
from ddm.fast.variables.variables import Variable


def get_var(dimuon_type, var):
    return get_var_registry(dimuon_type)[var]


@Memoize()
def get_var_registry(dimuon_type):
    if dimuon_type == 'pat':
        return VariableRegistry('pat')
    elif dimuon_type == 'hyb':
        return VariableRegistry('hyb')
    elif dimuon_type == 'dsa':
        return VariableRegistry('dsa')

    raise Exception('Invalid dimuon type. Valid types are pat, hyb, and dsa')


class VariableRegistry(dict):

    def __init__(self, dimuon_type):
        # Init variables
        self.dimuon_type = dimuon_type

        # Unpack dimuon expression specs
        dim_specs = dimuon_specs.get(dimuon_type, None)

        if dim_specs is None:
            raise Exception("Non-implemented dimuon type")

        mu1_id, mu2_id, mu1_type, mu2_type = dim_specs

        dim_tokens = {
            'num1': 1, 'id1': mu1_id, 'type1': mu1_type,
            'num2': 2, 'id2': mu2_id, 'type2': mu2_type
        }

        # Select templates
        if dimuon_type == 'gen':
            sel_mudict = genmudict
            sel_dimdict = gendict
        else:
            sel_mudict = mudict
            sel_dimdict = dimdict

        # Load templates
        vardict = dict()
        vardict.update(evtdict)
        vardict.update(sel_dimdict)

        # Compile base variables
        for var_name, var in vardict.items():
            vardict[var_name] = var.compile(**dim_tokens)

        # Compile muon vars and create variations
        for var_name, var in sel_mudict.items():
            if var.is_per_muon:
                # Compile variable
                mu1_var, mu2_var = var.compile(**dim_tokens)

                # Save compiled variables
                vardict[var_name] = (mu1_var, mu2_var)

                # Save min/max variations
                vardict['max' + var_name] = Variable(
                    'max({},{})'.format(mu1_var.get_expression(), mu2_var.get_expression()),
                    'max({})'.format(mu1_var.latex),
                    mu1_var.units, mu1_var.bin_edges
                )

                vardict['min' + var_name] = Variable(
                    'min({},{})'.format(mu1_var.get_expression(), mu2_var.get_expression()),
                    'min({})'.format(mu1_var.latex),
                    mu1_var.units, mu1_var.bin_edges
                )
            else:
                # Compile and save variable
                vardict[var_name] = var.compile(**dim_tokens)

        # Save Variables
        self.clear()
        self.update(vardict)
