import json
import re

import numpy as np

from ddm.core.commons.binning import digitize_inclusive


class ScaleFactorLUT:

    def __init__(self, x1_edges, x2_edges, values):
        self.x1_edges = np.asarray(x1_edges)
        self.x2_edges = np.asarray(x2_edges)
        self.values = values

    def lookup(self, val1, val2):
        bin_x1 = digitize_inclusive(val1, self.x1_edges)
        bin_x2 = digitize_inclusive(val2, self.x2_edges)
        return self.values[bin_x1][bin_x2]


def get_sf_lut(filename, tag, projection):
    # Get info
    var1 = projection.split("_")[0]
    var2 = projection.split("_")[1]

    # Load file
    with open(filename, "r") as f:
        sf_json = json.load(f)

    # Find bin edges
    bin1_edges_col, bin2_edges_col = [], []

    for bin1 in sf_json[tag][projection]:
        res = re.search(r".*:\[(-?\d+\.\d+|-?\d+),(-?\d+\.\d+|-?\d+)\]", bin1)

        if not res:
            continue

        low, up = res.groups()
        bin1_edges_col.append((float(low), float(up)))

        for bin2 in sf_json[tag][projection][bin1]:
            res = re.search(r".*:\[(-?\d+\.\d+|-?\d+),(-?\d+\.\d+|-?\d+)\]", bin2)

            if not res:
                continue

            low, up = res.groups()
            edges = (float(low), float(up))

            if edges not in bin2_edges_col:
                bin2_edges_col.append(edges)

    # Sanitize Edges
    bin1_edges_col = sorted(bin1_edges_col, lambda edges: edges[0])
    bin2_edges_col = sorted(bin2_edges_col, lambda edges: edges[0])

    # Parse LUT
    x1_edges = list()
    x2_edges = list()
    sf_values = dict()

    for bin1_idx, bin1_edges in enumerate(bin1_edges_col):
        # Save x1 edges
        if bin1_idx == 0:
            x1_edges.append(bin1_edges[0])

        x1_edges.append(bin1_edges[1])

        # Init scale factors
        if bin1_edges not in sf_values:
            sf_values[bin1_idx] = dict()

        # Read scale factor
        bin1_json = "{}:[{:.1f},{:.1f}]".format(var1, bin1_edges[0], bin1_edges[1])

        for bin2_idx, bin2_edges in enumerate(bin2_edges_col):
            # Save x2 edges
            if bin1_idx == 0:
                if bin2_idx == 0:
                    x2_edges.append(bin2_edges[0])

                x2_edges.append(bin2_edges[1])

            # Read scale factor
            bin2_json = "{}:[{:.0f},{:.0f}]".format(var2, bin2_edges[0], bin2_edges[1])

            sf_values[bin1_idx][bin2_idx] = sf_json[tag][projection][bin1_json][bin2_json]["value"]

    # Return
    return ScaleFactorLUT(x1_edges, x2_edges, sf_values)
