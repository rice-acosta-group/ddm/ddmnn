from ddm.core.execution.runtime import resource

filename_by_year = {
    2016: resource('scale_factors/pat_extra/NUM_HitsPreselection_DEN_dSAMuons_eta_pt_2016.json'),
    2018: resource('scale_factors/pat_extra/NUM_HitsPreselection_DEN_dSAMuons_eta_pt_2018.json'),
}

tags_by_year = {
    2016: "NUM_HitsPreselection_DEN_dSAMuons",
    2018: "NUM_HitsPreselection_DEN_dSAMuons",
}
