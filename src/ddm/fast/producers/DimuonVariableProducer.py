from ddm.core.commons.lazy import LazyMeta
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer
from ddm.fast.variables.variable_registry import get_var_registry
from ddm.fast.variables.variables import Variable


class DimuonVariableProducer(FormulaBranchProducer, metaclass=LazyMeta):

    def __init__(self, dimuon_type):
        # Build dictionary
        branch_expressions = dict()

        for varname, var in get_var_registry(dimuon_type).items():
            if isinstance(var, Variable):
                branch = f'dim_fvar_{dimuon_type}_{varname}'
                branch_expressions[branch] = var.get_expression()
            else:
                var1, var2 = var
                branch1 = f'dim_mu1_fvar_{dimuon_type}_{varname}'
                branch2 = f'dim_mu2_fvar_{dimuon_type}_{varname}'
                branch_expressions[branch1] = var1.get_expression()
                branch_expressions[branch2] = var2.get_expression()

        # Initialize
        super().__init__(**branch_expressions)
