import re

from ROOT import TTreeFormula

from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer


class FormulaResult:

    def __init__(self, parent, name, expression):
        self.parent = parent

        self.name = name
        self.expression = expression
        self.formula = None
        self.branches_used = None

        self.current_source_id = None
        self.current_entry_id = None
        self.entry_is_ready = False

        self.multiplicity = None
        self.vsize_results = None

    def __len__(self):
        return self.vsize_results

    def __getitem__(self, idx):
        # Check if entry changed
        tree_changed = (self.current_source_id is None) or (self.current_source_id != self.parent.entry._source_id)
        entry_changed = tree_changed or (self.current_entry_id != self.parent.entry._entry_id)

        # Update tree
        if tree_changed:
            self.current_source_id = self.parent.entry._source_id
            self.formula = None

            self.multiplicity = None
            self.vsize_results = None

        # Update entry
        if entry_changed:
            self.current_entry_id = self.parent.entry._entry_id
            self.entry_is_ready = False

        # Lazy initialization of formula
        if self.formula is None:
            # Make sure branches are available
            if self.branches_used is None:
                # Collect branches
                self.branches_used = set()

                for branch_name in self.parent.entry._branch_info.keys():
                    match = re.search(rf'\b{branch_name}\b', self.expression)

                    if not match:
                        continue

                    self.branches_used.add(branch_name)

                # Make sure all branches needed are enabled
                for branch_name in self.branches_used:
                    self.parent.entry.enable_branch(branch_name)

            # Init Formula
            self.formula = TTreeFormula(self.name, self.expression, self.parent.entry._tree)
            self.formula.SetQuickLoad(True)

        # Initialize vector
        if not self.entry_is_ready:
            self.entry_is_ready = True

            # Meaning of multiplicity
            # -1: The number of elements is not known until the formula is evaluated. This is the case when the formula depends on the values in the tree.
            # 0: The formula always returns a single value (scalar).
            # 1: The formula always returns an array with a fixed size.
            # 2: The formula can return an array with a variable size.
            if self.multiplicity is None:
                self.multiplicity = self.formula.GetMultiplicity()

            if self.multiplicity == 0:
                self.vsize_results = 1
            elif self.multiplicity in [-1, 1, 2] or self.vsize_results is None:
                self.vsize_results = self.formula.GetNdata()

        # Short-Circuit: Single value
        if self.multiplicity == 0:
            return self.formula.EvalInstance(0)

        # Return
        return self.formula.EvalInstance(idx)

    def get_branches_used(self):
        if self.branches_used is None:
            return set()

        return self.branches_used


class FormulaBranchProducer(Producer, metaclass=LazyMeta):

    def __init__(self, **expressions):
        self.entry = None
        self.results = {
            field_name: FormulaResult(self, field_name + '_expression', expression)
            for field_name, expression in expressions.items()
        }

    def provides(self):
        return self.results.keys()

    def extract(self, entry):
        # Store current entry
        self.entry = entry

        # Return
        return self.results

    def get_branches_used(self):
        branches_used = set()

        for result in self.results.values():
            branches_used.update(result.get_branches_used())

        return branches_used
