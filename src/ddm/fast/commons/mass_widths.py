from collections import OrderedDict

# List of PAT-PAT Mass windows determined via
# linear fit to widths of 2Mu signal masses
PATPAT_MASS_WIDTHS = OrderedDict()
PATPAT_MASS_WIDTHS[11.429] = 1.429
PATPAT_MASS_WIDTHS[14.481] = 1.623
PATPAT_MASS_WIDTHS[17.946] = 1.843
PATPAT_MASS_WIDTHS[21.882] = 2.093
PATPAT_MASS_WIDTHS[26.351] = 2.377
PATPAT_MASS_WIDTHS[31.427] = 2.699
PATPAT_MASS_WIDTHS[37.191] = 3.065
PATPAT_MASS_WIDTHS[43.736] = 3.480
PATPAT_MASS_WIDTHS[51.169] = 3.952
PATPAT_MASS_WIDTHS[59.610] = 4.488
PATPAT_MASS_WIDTHS[69.196] = 5.097
PATPAT_MASS_WIDTHS[80.081] = 5.788
PATPAT_MASS_WIDTHS[92.443] = 6.573
PATPAT_MASS_WIDTHS[106.481] = 7.465
PATPAT_MASS_WIDTHS[122.423] = 8.477
PATPAT_MASS_WIDTHS[140.527] = 9.627
PATPAT_MASS_WIDTHS[161.086] = 10.932
PATPAT_MASS_WIDTHS[184.433] = 12.415
PATPAT_MASS_WIDTHS[210.946] = 14.098
PATPAT_MASS_WIDTHS[241.054] = 16.010
PATPAT_MASS_WIDTHS[275.246] = 18.181
PATPAT_MASS_WIDTHS[314.074] = 20.647
PATPAT_MASS_WIDTHS[358.168] = 23.447
PATPAT_MASS_WIDTHS[408.242] = 26.627
# integrated
PATPAT_MASS_WIDTHS[222.435] = 212.435
# signal bins
PATPAT_MASS_WIDTHS[10.] = 1.
PATPAT_MASS_WIDTHS[11.] = 1.
PATPAT_MASS_WIDTHS[20.] = 2.
PATPAT_MASS_WIDTHS[30.] = 3.
PATPAT_MASS_WIDTHS[40.] = 3.
PATPAT_MASS_WIDTHS[50.] = 4.
PATPAT_MASS_WIDTHS[60.] = 5.
PATPAT_MASS_WIDTHS[150.] = 10.
PATPAT_MASS_WIDTHS[350.] = 24.
# interpolated dark photon limits
PATPAT_MASS_WIDTHS[13.] = 1.
PATPAT_MASS_WIDTHS[16.] = 2.
PATPAT_MASS_WIDTHS[25.] = 3.
PATPAT_MASS_WIDTHS[35.] = 3.
PATPAT_MASS_WIDTHS[45.] = 3.
PATPAT_MASS_WIDTHS[55.] = 4.
