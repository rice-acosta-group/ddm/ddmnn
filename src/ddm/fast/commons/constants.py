# Cuts
# default cuts applied to PAT muons (** without deltaPhi, typically used as a control region **)
BASEPATCUTS = ['REP', 'LXYE', 'MASS', 'CHI2', 'COSA', 'LXYS', 'DCA', 'D0SIGPV', 'ISO', 'PXL', 'MAXPT25', 'HB4V', 'NTRKLAYSLXY', 'HLTRun3']
BASEHYBCUTS = ['REP', 'LXYE', 'MASS', 'CHI2', 'COSA', 'LXYS', 'DCA', 'D0SIGPV', 'ISO', 'NTRKLAYSLXY', 'ISGLOB', 'HITSBEFVTX', 'DPHIMULXY', 'FPTE', 'DSATIME', 'HLTRun3']
BASEDSACUTS = ['REP', 'LXYE', 'MASS', 'CHI2', 'COSA', 'LXYS', 'DCA', 'DSATIME', 'DIR', 'BBDSATIMEDIFF', 'SEG', 'DETASR', 'HLTRun3']

CONTROLCUTS = {
    # todo: DPHI definition different between PAT and DSA, fix!
    'pat': ['IDPHI', 'SS', 'DPHI2', 'DPHI3', 'DPHI4', 'DPHIPIBY30OPP', 'IDPHI1', 'IISO', 'IISO0P1', 'IISO0P2',
            'IISO0P15', 'ICHI2', 'ICHI2LT20', 'ID0SIGPV', 'ONEORMORENONISO', 'ONEMEDISO0P5', 'ONEMEDISO1P0', 'NONE',
            'D0SIGPV4TO6', 'D0SIGPV2TO6', 'IVTX', 'IMASS5', 'IMASS8', 'JPSIMASS', 'GEN_OR_IMASS5', 'iDPHI',
            'ISOGT0P075', 'ILXYS'],
    'hyb': ['IDPHI', 'SS', 'DPHI2', 'DPHI3', 'DPHI4', 'IISO', 'ICHI2', 'DYCONTROLLXYS', 'DYCONTROLD0SIG', 'QCDCONTROL',
            'QCDCONTROLTWO', 'QCDCONTROLONE', 'IISO0P125', 'ID0SIGPV', 'IDSAPATD0SIG'],
    'dsa': ['IDPHI', 'iDPHI', 'IDPHI1', 'IDPHI2', 'IDPHI3', 'SS', 'ILXYS', 'IMASS', 'NONISOPATDIM0p1',
            'NONISOHYBDIM0p1', 'NONISOASSOCPATS', 'DYTFPATCUTS', 'DYTFPATCUTSLOOSE', 'DYTFHYBCUTS', 'IDETANDTIDETASEG',
            'IDETASEG', 'ISEG', 'ISEGDETASEG', 'IBBDSATIMEDIFF', 'IDSAISO0p15', 'MASS6TO10', 'iDPHIb10', 'ICOSA',
            'IDIR', 'iTMSDPHI', 'ITMSLXYS1'],
    'gen': ['ALL'],
}
