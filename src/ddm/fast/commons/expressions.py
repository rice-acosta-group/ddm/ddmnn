import re

token_pattern = re.compile(r'\{(\w+)\}')

dimuon_specs = {
    "dsa": ("dim_mu1_idx", "dim_mu2_idx", "dsa", "dsa"),
    "pat": ("dim_mu1_idx-1000", "dim_mu2_idx-1000", "pat", "pat"),
    "hyb": ("dim_mu1_idx", "dim_mu2_idx-1000", "dsa", "pat"),
    "gen": ("0", "1", "gen", "gen")
}


def compile_expression(expression, **kwargs):
    # Replace tokens
    token_count = 0
    substring_begin = 0
    compiled_expression = ''

    for match in token_pattern.finditer(expression):
        # Increase Counters
        token_count += 1

        # Resolve token
        token = match.group(1)
        replacement = kwargs.get(token, None)

        if replacement is None:
            raise ValueError(
                "Value for '{token}' was not given. Can not compile: '{expression}'.".format(
                    token=token, expression=expression
                )
            )

        match_begin, match_end = match.span()
        compiled_expression += expression[substring_begin:match_begin]
        compiled_expression += str(replacement)
        substring_begin = match_end

    # Return compiled expression
    if token_count > 0:
        # Append tail
        compiled_expression += expression[substring_begin: len(expression)]

        # Return
        return compiled_expression

    # Return original expression
    return expression
