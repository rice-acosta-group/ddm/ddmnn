import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.commons.plotting import plot_var_walls
from ddm.core.execution.runtime import get_logger
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.mini.commons.bins import ddm_pt_bins, ddm_abs_eta_bins, ddm_phi_deg_bins, ddm_lxy_bins, ddm_lz_bins, \
    ddm_min_threshold
from ddm.mini.utils.dimuon_matching import compare_gen_dimuons
from ddm.mini.utils.dimuon_utils import align_dimuons
from ddm.mini.utils.nn_pairing import get_pairing_vars, get_pairing_params

# Bins
lxy_bins = np.concatenate((
    np.arange(0.0, 100.0, 2.0),
    np.arange(100.0, 208.0, 4.0),
    np.array([
        208.0, 214.0, 220.0, 230.0,
        240.0, 255.0, 270.0, 290.0,
        310.0, 340.0, 370.0, 400.0,
    ]),
))


class SignalAnalyzer(AbstractAnalyzer):

    def __init__(self, filename, train_fraction):
        super().__init__()

        self.filename = filename
        self.train_fraction = train_fraction
        self.list_variables = list()
        self.list_parameters = list()

        # GEN
        self.llp_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_pt',
            'LLP Distribution',
            'True p_{T} [GeV]', 'a.u.',
            xbins=ddm_pt_bins,
            density=True,
            logy=True)

        self.llp_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_eta',
            'LLP Distribution',
            'True #left| #eta #right|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.llp_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_phi',
            'LLP Distribution',
            'True #phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

        self.llp_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_lxy',
            'LLP Distribution',
            'True L_{xy} [cm]', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.llp_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_lz',
            'LLP Distribution',
            'True |L_{z}| [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.llp_lxy_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_lxy_vs_lz', 'LLP Distribution',
            'True |L_{z}| [cm]', 'True L_{xy} [cm]',
            xbins=ddm_lz_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.llp_dim_deltaVtx_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_gen_deltaVtx', 'LLP Distribution',
            '#Delta Vertex(True LLP, Dimuon)', 'a.u.',
            xbins=uniform_step_axis(1, 0, 100),
            density=True,
            logy=True)

        self.llp_dim_deltaR_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_gen_deltaR', 'LLP Distribution',
            '#Delta R(True LLP, Dimuon)', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

    def process_entry(self, entry):
        # Must Pass Trigger
        if len(entry.ext_hlt_paths) == 0:
            return

        # Map matches by uid
        dimuon_matches_by_uid = dict()

        for reco_dimuon in entry.sel_dimuons:
            # Short-Circuit: Only used the matched dimuons
            if reco_dimuon.matched_gen_dimuon is None:
                continue

            # Collect dimuon uids
            dimuon_matches_by_uid[reco_dimuon.uid] = (reco_dimuon.matched_gen_dimuon, 1)

            for alt_dimuon_uid in reco_dimuon.alternate_uid_col:
                dimuon_matches_by_uid[alt_dimuon_uid] = (reco_dimuon.matched_gen_dimuon, 2)

        # Collect muon indices
        sel_dsa_muon_idx_col = set()
        sel_pat_muon_idx_col = set()

        for reco_dimuon in entry.sel_dimuons:
            # Short-Circuit: Not a match
            if reco_dimuon.matched_gen_dimuon is None:
                continue

            # Collect muon indices
            for ref_mu in (reco_dimuon.mu1, reco_dimuon.mu2):
                if ref_mu.kind == 'PAT':
                    sel_pat_muon_idx_col.add(ref_mu.idx)
                else:
                    sel_dsa_muon_idx_col.add(ref_mu.idx)

        # Collect alternative muon indices
        alt_dsa_muon_idx_col = set()
        alt_pat_muon_idx_col = set()

        for reco_dimuon in entry.alt_sel_dimuons:
            # Get match
            gen_dimuon, match_status = dimuon_matches_by_uid.get(reco_dimuon.uid, (None, 0))

            # Short-Circuit: Not an alternative
            if match_status != 2:
                continue

            # Short-Circuit: Not a match
            if reco_dimuon.matched_gen_dimuon is None:
                continue

            # Short-Circuit: Disagree on dimuon
            if reco_dimuon.matched_gen_dimuon.idx != gen_dimuon.idx:
                continue

            # Collect muon indices
            for ref_mu in (reco_dimuon.mu1, reco_dimuon.mu2):
                if ref_mu.kind == 'PAT':
                    alt_pat_muon_idx_col.add(ref_mu.idx)
                else:
                    alt_dsa_muon_idx_col.add(ref_mu.idx)

        # Select input dimuons
        input_dimuons = align_dimuons(
            entry.sel_dimuons,
            dsa_muon_idx_col=sel_dsa_muon_idx_col,
            pat_muon_idx_col=sel_pat_muon_idx_col,
            require_both=False
        )

        input_dimuons += align_dimuons(
            entry.alt_sel_dimuons,
            dsa_muon_idx_col=alt_dsa_muon_idx_col,
            pat_muon_idx_col=alt_pat_muon_idx_col,
            require_both=False
        )

        # Refine inputs
        sel_dimuons = list()
        sel_dimuon_uid_col = set()

        for reco_dimuon in input_dimuons:
            # Short-Circuit: Already selected
            if reco_dimuon.uid in sel_dimuon_uid_col:
                continue

            # Get match info
            _, match_status = dimuon_matches_by_uid.get(reco_dimuon.uid, (None, 0))

            # Short-Circuit: Non matched has a decent match
            if match_status == 0:
                is_decent = False

                for gen_dimuon in entry.ext_llp_gun_dimuons:
                    metric, are_aligned = compare_gen_dimuons(
                        gen_dimuon, reco_dimuon,
                        deltaR_threshold=0.2,
                        deltaVtx_threshold=20.0
                    )

                    if metric is not None:
                        is_decent = True
                        break

                if is_decent:
                    continue

            # Select dimuon
            sel_dimuon_uid_col.add(reco_dimuon.uid)
            sel_dimuons.append(reco_dimuon)

        # Loop over dimuons
        used_gen_dimuon_idx_col = set()

        for reco_dimuon in sel_dimuons:
            # Get matching GEN dimuon
            gen_dimuon, match_status = dimuon_matches_by_uid.get(reco_dimuon.uid, (None, 0))

            # Get Parameters and Variables
            parameters = get_pairing_params(gen_dimuon, reco_dimuon)
            variables = get_pairing_vars(reco_dimuon, entry.all_dsa_muons, entry.all_pat_muons)

            # Append
            self.list_variables.append(variables)
            self.list_parameters.append(parameters)

            # Fill GEN plots
            if gen_dimuon is not None:
                # Fill delta plots
                deltaR = gen_dimuon.p4.DeltaR(reco_dimuon.p4)
                deltaVtx = (gen_dimuon.pos - reco_dimuon.pos).Mag()

                self.llp_dim_deltaVtx_plotter.fill(deltaVtx)
                self.llp_dim_deltaR_plotter.fill(deltaR)

                # Fill GEN plots
                if gen_dimuon.idx not in used_gen_dimuon_idx_col:
                    used_gen_dimuon_idx_col.add(gen_dimuon.idx)
                    self.llp_pt_plotter.fill(gen_dimuon.p4.Pt())
                    self.llp_eta_plotter.fill(abs(gen_dimuon.p4.Eta()))
                    self.llp_phi_plotter.fill(np.rad2deg(gen_dimuon.p4.Phi()))
                    self.llp_lxy_plotter.fill(gen_dimuon.lxy)
                    self.llp_lz_plotter.fill(abs(gen_dimuon.z))
                    self.llp_lxy_vs_lz_plotter.fill(abs(gen_dimuon.z), gen_dimuon.lxy)

    def merge(self, other):
        self.list_variables += other.list_variables
        self.list_parameters += other.list_parameters

        self.llp_pt_plotter.add(other.llp_pt_plotter)
        self.llp_eta_plotter.add(other.llp_eta_plotter)
        self.llp_phi_plotter.add(other.llp_phi_plotter)
        self.llp_lxy_plotter.add(other.llp_lxy_plotter)
        self.llp_lz_plotter.add(other.llp_lz_plotter)
        self.llp_lxy_vs_lz_plotter.add(other.llp_lxy_vs_lz_plotter)
        self.llp_dim_deltaVtx_plotter.add(other.llp_dim_deltaVtx_plotter)
        self.llp_dim_deltaR_plotter.add(other.llp_dim_deltaR_plotter)

    def post_production(self):
        # Short-Circuit: Sample empty
        if len(self.list_variables) == 0:
            return

        # Convert
        self.list_variables = np.asarray(self.list_variables, dtype=np.float64)
        self.list_parameters = np.asarray(self.list_parameters, dtype=np.float64)

        # Remove NaN
        for i in range(self.list_variables.shape[1]):
            col = self.list_variables[:, i]
            self.list_variables[:, i] = np.where(np.isnan(col), np.zeros_like(col), col)

        # Shuffle
        random_index_array = np.arange(self.list_variables.shape[0])
        np.random.shuffle(random_index_array)

        self.list_variables = self.list_variables[random_index_array]
        self.list_parameters = self.list_parameters[random_index_array]

        # Plot
        entries_that_match = (self.list_parameters[:, 0] == 1)
        entries_that_dont_match = (self.list_parameters[:, 0] == 0)

        plot_var_walls('variables', self.list_variables)
        plot_var_walls('parameters', self.list_parameters)
        plot_var_walls('variables_match', self.list_variables[entries_that_match])
        plot_var_walls('parameters_match', self.list_parameters[entries_that_match])
        plot_var_walls('variables_no_match', self.list_variables[entries_that_dont_match])
        plot_var_walls('parameters_no_match', self.list_parameters[entries_that_dont_match])

        # Save
        self.save_npz(self.filename, self.list_variables, self.list_parameters)

    def save_npz(self, name, variables, parameters):
        # Split Datasets
        total_events = int(variables.shape[0])
        train_events = int(self.train_fraction * total_events)
        test_events = total_events - train_events

        from_event = 0
        to_event = from_event + train_events
        variables_train = variables[from_event:to_event]
        parameters_train = parameters[from_event:to_event]

        from_event = to_event
        to_event = from_event + test_events
        variables_test = variables[from_event:to_event]
        parameters_test = parameters[from_event:to_event]

        # Log
        get_logger().info(f"""
        *********************************************************
        Signal Sample Summary
        *********************************************************
        Total Events: {total_events}
        Train Events: {train_events}
        Test Events: {test_events}
        """)

        # Save
        np.savez_compressed(
            name + '.npz',
            variables_train=variables_train, parameters_train=parameters_train,
            variables_test=variables_test, parameters_test=parameters_test
        )
