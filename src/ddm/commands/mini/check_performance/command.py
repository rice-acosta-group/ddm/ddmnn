from ROOT import TFile

from ddm.commands.mini.check_performance.analyzers.dimuon_distributions import DimuonDistributions
from ddm.commands.mini.check_performance.analyzers.dimuon_performance import DimuonPerfomance
from ddm.commands.mini.check_performance.analyzers.gen_distributions import GENDistributions
from ddm.commands.mini.check_performance.analyzers.replacement import ReplacementAnalyzer
from ddm.mini.analysis.mini_analysis import MiniAnalysis
from ddm.mini.commons.user_options import UserOptions
from ddm.mini.producers.DPANNBranchProducer import DPANNBranchProducer
from ddm.mini.producers.PairingNNBranchProducer import PairingNNBranchProducer
from ddm.mini.selection.cutflow import CutflowAnalyzer
from ddm.mini.selection.selector import Selector


class CheckPerformance(MiniAnalysis):

    def __init__(self):
        super().__init__()
        self.branches_required = set()

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='Fraction of events to process')

        parser.add_argument('--dpa-protobuf', dest='dpa_protobuf', metavar='DPA_PROTOBUF',
                            type=str, default=None,
                            help='DPA protobuf')

        parser.add_argument('--pairing-protobuf', dest='pairing_protobuf', metavar='PAIRING_PROTOBUF',
                            type=str, default=None,
                            help='Pairing protobuf')

        parser.add_argument('--num-opt', dest='num_opt_string', metavar='NUM_OPT_STRING',
                            type=str, default='_NS_NH_HLT_REP_PT',
                            help='Numerator Option String')

        parser.add_argument('--den-opt', dest='den_opt_string', metavar='DEN_OPT_STRING',
                            type=str, default='_NS_NH_HLT_PT',
                            help='Denominator Option String')

    def configure(self, args):
        # Unpack Args
        self.dpa_protobuf = args.dpa_protobuf
        self.pairing_protobuf = args.pairing_protobuf

        self.num_options = UserOptions(args.num_opt_string)
        self.den_options = UserOptions(args.den_opt_string)

    def create_producers(self):
        producers = [
            Selector(self.num_options, branch_prefix='num_'),
            Selector(self.den_options, branch_prefix='den_')
        ]

        if self.dpa_protobuf is not None:
            producers.append(DPANNBranchProducer(self.dpa_protobuf))

        if self.pairing_protobuf is not None:
            producers.append(PairingNNBranchProducer(self.pairing_protobuf))

        return producers

    def create_analyzers(self):
        return [
            ReplacementAnalyzer(),
            GENDistributions(),
            DimuonDistributions(),
            DimuonPerfomance(),
            CutflowAnalyzer('num_sel_cutflow')
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'RECREATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
