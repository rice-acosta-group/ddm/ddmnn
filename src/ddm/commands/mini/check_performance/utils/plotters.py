from ROOT import TCanvas
from ROOT import TEfficiency
from ROOT import TH1D
from ROOT import THStack
from ROOT import TLegend
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kBlue
from ROOT import kGray
from ROOT import kGreen
from ROOT import kRed

from ddm.core.analyzers import AbstractPlotter
from ddm.core.commons.histograms import add_flows
from ddm.core.root.labels import draw_fancy_label


class DimuonEfficiencyPlotter(AbstractPlotter):

    def __init__(
            self, name, title, x_title, y_title,
            nxbins=None, x_low=None, x_up=None, xbins=None,
            logx=False, logy=False
    ):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title

        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [x_low, x_up]):
            self.tms_plot = TEfficiency(name + '_tms', title, nxbins, x_low, x_up)
            self.hyb_plot = TEfficiency(name + '_hyb', title, nxbins, x_low, x_up)
            self.sta_plot = TEfficiency(name + '_sta', title, nxbins, x_low, x_up)
            self.total_plot = TEfficiency(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.tms_plot = TEfficiency(name + '_tms', title, nxbins, xbins)
            self.hyb_plot = TEfficiency(name + '_hyb', title, nxbins, xbins)
            self.sta_plot = TEfficiency(name + '_sta', title, nxbins, xbins)
            self.total_plot = TEfficiency(name, title, nxbins, xbins)

    def add(self, other):
        self.tms_plot.Add(other.tms_plot)
        self.hyb_plot.Add(other.hyb_plot)
        self.sta_plot.Add(other.sta_plot)
        self.total_plot.Add(other.total_plot)

    def fill(self, composition, value):
        is_tms = composition == 'PAT'
        is_hybrid = composition == 'HYBRID'
        is_sta = composition == 'DSA'

        self.tms_plot.Fill(is_tms, value)
        self.hyb_plot.Fill(is_hybrid, value)
        self.sta_plot.Fill(is_sta, value)
        self.total_plot.Fill(is_tms or is_hybrid or is_sta, value)

    def consolidate(self):
        pass

    def write(self):
        # Write
        self.tms_plot.Write()
        self.hyb_plot.Write()
        self.sta_plot.Write()
        self.total_plot.Write()

        # Select Plots
        eff_entries = [
            (self.sta_plot, 'STA-STA', kGreen + 2, 21),
            (self.hyb_plot, 'STA-TMS', kBlue, 22),
            (self.tms_plot, 'TMS-TMS', kRed, 20),
            (self.total_plot, 'Total', kGray + 3, 23),
        ]

        # Image
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.cd()

        # Init Eff Plots
        for plot, label, color, marker in eff_entries:
            plot.Paint("")
            gPad.Update()

        # Draw
        frame = plot_canvas.DrawFrame(self.xbins[0], 0, self.xbins[-1], 1.15, self.title)
        frame.GetXaxis().SetTitle(self.x_title)
        frame.GetYaxis().SetTitle(self.y_title)
        frame.GetYaxis().SetMaxDigits(3)

        legend_x0 = 0.525
        legend_y0 = 0.225

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.225)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kBlack, 0.1)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        # Stack Plots
        for plot, label, color, marker in eff_entries:
            plot.SetMarkerColor(color)
            plot.SetMarkerSize(1)
            plot.SetMarkerStyle(marker)
            plot.SetLineColor(color)
            plot.SetLineWidth(2)

            plot.Draw('P SAME')

            legend.AddEntry(plot, label, 'P')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.215, 0.835)

        gPad.Modified()
        gPad.Update()

        plot_canvas.SaveAs(self.name + '.png')


class DimuonCompositionPlotter(AbstractPlotter):

    def __init__(
            self, name, title, x_title, y_title,
            nxbins=None, x_low=None, x_up=None, xbins=None,
            logx=False, logy=False
    ):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title

        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        self.show_missing_en = False

        if all(v is not None for v in [x_low, x_up]):
            self.tms_plot = TEfficiency(name + '_tms', title, nxbins, x_low, x_up)
            self.hyb_plot = TEfficiency(name + '_hyb', title, nxbins, x_low, x_up)
            self.sta_plot = TEfficiency(name + '_sta', title, nxbins, x_low, x_up)
            self.miss_plot = TEfficiency(name + '_miss', title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.tms_plot = TEfficiency(name + '_tms', title, nxbins, xbins)
            self.hyb_plot = TEfficiency(name + '_hyb', title, nxbins, xbins)
            self.sta_plot = TEfficiency(name + '_sta', title, nxbins, xbins)
            self.miss_plot = TEfficiency(name + '_miss', title, nxbins, xbins)

    def add(self, other):
        self.show_missing_en = self.show_missing_en or other.show_missing_en

        self.tms_plot.Add(other.tms_plot)
        self.hyb_plot.Add(other.hyb_plot)
        self.sta_plot.Add(other.sta_plot)
        self.miss_plot.Add(other.miss_plot)

    def fill(self, composition, value):
        is_tms = composition == 'PAT'
        is_hybrid = composition == 'HYBRID'
        is_sta = composition == 'DSA'
        is_missing = not (is_tms or is_hybrid or is_sta)

        self.tms_plot.Fill(is_tms, value)
        self.hyb_plot.Fill(is_hybrid, value)
        self.sta_plot.Fill(is_sta, value)
        self.miss_plot.Fill(is_missing, value)

        if is_missing:
            self.show_missing_en = True

    def fill_expected(self, true_comp, reco_comp, value):
        if true_comp == 'PAT':
            self.tms_plot.Fill(reco_comp == true_comp, value)
        elif true_comp == 'HYBRID':
            self.hyb_plot.Fill(reco_comp == true_comp, value)
        elif true_comp == 'DSA':
            self.sta_plot.Fill(reco_comp == true_comp, value)

    def consolidate(self):
        pass

    def write(self):
        # Write
        self.tms_plot.Write()
        self.hyb_plot.Write()
        self.sta_plot.Write()
        self.miss_plot.Write()

        # Select Plots
        eff_entries = [
            (self.sta_plot, 'STA-STA', kGreen + 2, 21),
            (self.hyb_plot, 'STA-TMS', kBlue, 22),
            (self.tms_plot, 'TMS-TMS', kRed, 20),
        ]

        if self.show_missing_en:
            eff_entries.insert(0, (self.miss_plot, 'Missing', kGray + 3, 23))

        # Image
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.cd()

        # Init Eff Plots
        for plot, label, color, marker in eff_entries:
            plot.Paint("")
            gPad.Update()

        # Draw
        frame = plot_canvas.DrawFrame(self.xbins[0], 0, self.xbins[-1], 1.15, self.title)
        frame.GetXaxis().SetTitle(self.x_title)
        frame.GetYaxis().SetTitle(self.y_title)
        frame.GetYaxis().SetMaxDigits(3)

        legend_x0 = 0.525
        legend_y0 = 0.225

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.225)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kBlack, 0.1)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        # Stack Plots
        for plot, label, color, marker in eff_entries:
            plot.SetMarkerColor(color)
            plot.SetMarkerSize(1)
            plot.SetMarkerStyle(marker)
            plot.SetLineColor(color)
            plot.SetLineWidth(2)

            plot.Draw('P SAME')

            legend.AddEntry(plot, label, 'P')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.215, 0.835)

        gPad.Modified()
        gPad.Update()

        plot_canvas.SaveAs(self.name + '.png')


class DimuonHistPlotter(AbstractPlotter):

    def __init__(
            self, name, title, x_title, y_title,
            nxbins=None, x_low=None, x_up=None, xbins=None,
            logx=False, logy=True
    ):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title

        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        self.show_missing_en = False

        if all(v is not None for v in [x_low, x_up]):
            self.tms_plot = TH1D(name + '_tms', title, nxbins, x_low, x_up)
            self.hyb_plot = TH1D(name + '_hyb', title, nxbins, x_low, x_up)
            self.sta_plot = TH1D(name + '_sta', title, nxbins, x_low, x_up)
            self.miss_plot = TH1D(name + '_miss', title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.tms_plot = TH1D(name + '_tms', title, nxbins, xbins)
            self.hyb_plot = TH1D(name + '_hyb', title, nxbins, xbins)
            self.sta_plot = TH1D(name + '_sta', title, nxbins, xbins)
            self.miss_plot = TH1D(name + '_miss', title, nxbins, xbins)

    def add(self, other):
        self.show_missing_en = self.show_missing_en or other.show_missing_en

        self.tms_plot.Add(other.tms_plot)
        self.hyb_plot.Add(other.hyb_plot)
        self.sta_plot.Add(other.sta_plot)
        self.miss_plot.Add(other.miss_plot)

    def fill(self, composition, value):
        if composition == 'PAT':
            self.tms_plot.Fill(value)
        elif composition == 'HYBRID':
            self.hyb_plot.Fill(value)
        elif composition == 'DSA':
            self.sta_plot.Fill(value)
        else:
            self.show_missing_en = True
            self.miss_plot.Fill(value)

    def consolidate(self):
        add_flows(self.tms_plot)
        add_flows(self.hyb_plot)
        add_flows(self.sta_plot)
        add_flows(self.miss_plot)

    def write(self):
        # Write
        self.tms_plot.Write()
        self.hyb_plot.Write()
        self.sta_plot.Write()
        self.miss_plot.Write()

        # Select Plots
        hist_entries = [
            (self.sta_plot, 'STA-STA', kGreen + 2, 21),
            (self.hyb_plot, 'STA-TMS', kBlue, 22),
            (self.tms_plot, 'TMS-TMS', kRed, 20),
        ]

        if self.show_missing_en:
            hist_entries.insert(0, (self.miss_plot, 'Missing', kGray + 3, 23))

        # Stack plots
        self.total_plot = THStack(self.name, self.title)

        for plot, label, color, marker in hist_entries:
            plot.SetMarkerColor(color)
            plot.SetMarkerSize(1)
            plot.SetMarkerStyle(marker)
            plot.SetLineColor(color)
            plot.SetFillColorAlpha(color, 0.85)
            plot.SetLineWidth(2)

            self.total_plot.Add(plot, 'HIST')

        # Image
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd()

        # Find y range
        y_max = (self.sta_plot + self.hyb_plot + self.tms_plot).GetMaximum() * 1.1

        # Draw
        frame = plot_canvas.DrawFrame(self.xbins[0], 1, self.xbins[-1], y_max, self.title)
        frame.GetXaxis().SetTitle(self.x_title)
        frame.GetYaxis().SetTitle(self.y_title)
        frame.GetYaxis().SetMaxDigits(3)

        legend_x0 = 0.525
        legend_y0 = 0.225

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.225)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kBlack, 0.1)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        # Populate Legend
        for plot, label, color, marker in hist_entries:
            legend.AddEntry(plot, label, 'L')

        self.total_plot.Draw('SAME')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.215, 0.835)

        gPad.Modified()
        gPad.Update()

        plot_canvas.SaveAs(self.name + '.png')
