def encode_pat_flags(muon):
    flags = 0

    if muon.isGlobal:
        flags += 1

    if muon.isMedium:
        flags += 2

    if muon.isTracker:
        flags += 4

    if muon.isDisp:
        flags += 8

    return flags
