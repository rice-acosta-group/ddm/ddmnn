import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis, uniform_axis
from ddm.core.plotters.basic import Hist1DPlotter, CorrelationPlotter
from ddm.mini.commons.bins import ddm_lxy_bins, ddm_abs_eta_bins, \
    ddm_phi_deg_bins, ddm_min_threshold


class DimuonDistributions(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # Dimuons
        self.dim_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_mass',
            'Dimuon Distribution',
            'mass [GeV]', 'a.u.',
            xbins=uniform_step_axis(2, 0, 500),
            density=True,
            logy=True)

        self.dim_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_pt',
            'Dimuon Distribution',
            'p_{T} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.dim_beta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_beta',
            'Dimuon Distribution',
            '#beta', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.dim_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_eta',
            'Dimuon Distribution',
            '#left| #eta #right|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.dim_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_phi',
            'Dimuon Distribution',
            '#phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

        self.dim_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_lxy',
            'Dimuon Distribution',
            'L_{xy} [cm]', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.dim_lxy_sig_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_lxy_sig',
            'Dimuon Distribution',
            'L_{xy} / #sigma_{L_{xy}}', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.dim_pscore_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_pscore',
            'Dimuon Distribution',
            'pscore', 'a.u.',
            xbins=uniform_axis(10, 0, 1),
            density=True,
            logy=True)

        self.dim_normChi2_vs_pscore_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'dim_normChi2_vs_pscore', 'Dimuon Distribution',
            'pscore', 'vtx #chi^{2}/dof',
            xbins=uniform_axis(10, 0, 1),
            ybins=uniform_axis(20, 0., 20.),
            min_val=ddm_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.dim_dca_vs_pscore_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'dim_dca_vs_pscore', 'Dimuon Distribution',
            'pscore', 'D.C.A. [cm]',
            xbins=uniform_axis(10, 0, 1),
            ybins=uniform_axis(50, 0., 50.),
            min_val=ddm_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

    def process_entry(self, entry):
        # Must Pass Trigger
        if len(entry.ext_hlt_paths) == 0:
            return

        # Fill Dimuon plots
        for dimuon in entry.num_sel_dimuons:
            self.dim_mass_plotter.fill(dimuon.mass)
            self.dim_pt_plotter.fill(dimuon.pt)
            self.dim_beta_plotter.fill(dimuon.p4.Beta())
            self.dim_eta_plotter.fill(abs(dimuon.eta))
            self.dim_phi_plotter.fill(np.rad2deg(dimuon.phi))
            self.dim_lxy_plotter.fill(dimuon.Lxy_pv)
            self.dim_lxy_sig_plotter.fill(dimuon.LxySig_pv)
            self.dim_pscore_plotter.fill(dimuon.pscore)
            self.dim_normChi2_vs_pscore_plotter.fill(dimuon.pscore, dimuon.normChi2)
            self.dim_dca_vs_pscore_plotter.fill(dimuon.pscore, dimuon.dca)

    def merge(self, other):
        # Dimuon plots
        self.dim_mass_plotter.add(other.dim_mass_plotter)
        self.dim_pt_plotter.add(other.dim_pt_plotter)
        self.dim_beta_plotter.add(other.dim_beta_plotter)
        self.dim_eta_plotter.add(other.dim_eta_plotter)
        self.dim_phi_plotter.add(other.dim_phi_plotter)
        self.dim_lxy_plotter.add(other.dim_lxy_plotter)
        self.dim_lxy_sig_plotter.add(other.dim_lxy_sig_plotter)
        self.dim_pscore_plotter.add(other.dim_pscore_plotter)
        self.dim_normChi2_vs_pscore_plotter.add(other.dim_normChi2_vs_pscore_plotter)
        self.dim_dca_vs_pscore_plotter.add(other.dim_dca_vs_pscore_plotter)
