from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis, uniform_axis
from ddm.core.commons.tools import safe_divide
from ddm.core.plotters.basic import Hist1DPlotter
from ddm.mini.utils.dsa_seg_match import DEFAULT_DSA_PAT_SEG_MATCH


class ReplacementAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # General
        self.rep_status_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'rep_status', 'REP Distributions',
            'status', 'a.u.',
            xbins=uniform_step_axis(1, 0, 60, align='center'),
            density=True,
            logy=True)

        self.rep_fsm_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'rep_fsm', 'REP Distributions',
            'Fraction of Segments Matched', 'a.u.',
            xbins=uniform_axis(10, 0, 1),
            density=True,
            logy=True)

        self.rep_mscore_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'rep_mscore', 'REP Distributions',
            'mscore', 'a.u.',
            xbins=uniform_axis(10, 0.5, 1),
            density=True,
            logy=True)

    def process_entry(self, entry):
        # Must Pass Trigger
        if len(entry.ext_hlt_paths) == 0:
            return

        # Loop dimuons
        for dimuon in entry.num_sel_dimuons:
            ref_mu1 = dimuon.mu1
            ref_mu2 = dimuon.mu2

            pat_muons_idx_col = list()

            if ref_mu1.kind == 'PAT':
                pat_muons_idx_col.append(ref_mu1.idx)

            if ref_mu2.kind == 'PAT':
                pat_muons_idx_col.append(ref_mu2.idx)

            for pat_idx in pat_muons_idx_col:
                # Find replaced DSA muon
                pat_muon = entry.num_all_pat_muons[pat_idx]
                dsa_muon = pat_muon.replaced_dsa_muon

                # Short-Circuit: Didn't replace DSA muon
                if dsa_muon is None:
                    continue

                # Unpack DSA muon
                if dsa_muon.replacement_status == 40:
                    dsapat_nsegments = dsa_muon.nSegms_NNMatch
                elif dsa_muon.replacement_status in [30, 31]:
                    dsapat_nsegments, _, _ = entry.dsa_pat_seg_match.get(
                        (dsa_muon.idx, pat_muon.idx),
                        DEFAULT_DSA_PAT_SEG_MATCH
                    )
                else:
                    continue

                # Fill Plots
                self.rep_status_plotter.fill(dsa_muon.replacement_status)
                self.rep_fsm_plotter.fill(safe_divide(dsapat_nsegments, dsa_muon.nSegments))

                if dsa_muon.idx_NNMatch == pat_idx:
                    self.rep_mscore_plotter.fill(dsa_muon.mscore_NNMatch)

    def merge(self, other):
        self.rep_status_plotter.add(other.rep_status_plotter)
        self.rep_fsm_plotter.add(other.rep_fsm_plotter)
        self.rep_mscore_plotter.add(other.rep_mscore_plotter)
