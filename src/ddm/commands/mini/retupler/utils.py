def has_any_gen_branch(tree):
    # If it has any gen branch consider this true
    for branch in tree.GetListOfBranches():
        branch_name = branch.GetName()

        if branch_name[:4] == "gen_":
            return True

    return False
