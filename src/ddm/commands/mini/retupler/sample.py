from ddm.core.commons.duck_typing import Duck


class Sample:

    def __init__(self, name, signal_point=None):
        self.name = name
        self.signal_point = signal_point
        self.extra = Duck()
