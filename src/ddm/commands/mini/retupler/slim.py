from array import array

from ROOT import TFile
from ROOT import std

from ddm.mini.commons import constants
from ddm.mini.commons.constants import YEAR
from ddm.mini.commons.isolation import yield_iso_thresholds


class SlimNtuple:

    def __init__(self, sample, entry, output_path):
        # Open root file
        self.output_file = TFile.Open(output_path, 'RECREATE')

        # Copy the tree structure but don't copy any of the entries
        # Note: Any changes done to the original tree will be propagated to the tree clone.
        # Changes done to the tree clone WILL NOT be propagated to the original tree
        # See: https://root.cern.ch/doc/master/classTTree.html#a76927d17a94d091d4ef1ed0ba7ef3383
        self.output_file.cd()
        self.output_tree = entry._tree.CloneTree(0)
        entry._on_tree_change_handlers.append(self.link_input_tree)

        # Track number of entries
        self.n_entries = 0

        # Track Added Branches
        self.scaler_branches = list()
        self.vector_branches = list()
        self.branch_values = dict()

        # Extract the list of branches
        self.ListOfPATBranches = []
        self.ListOfDSABranches = []
        self.ListOfDIMBranches = []

        for branch in entry._tree.GetListOfBranches():
            branch_name = branch.GetName()

            if branch_name[:6] == 'patmu_':
                self.ListOfPATBranches.append(branch_name)
            elif branch_name[:6] == 'dsamu_':
                self.ListOfDSABranches.append(branch_name)
            elif branch_name[:4] == 'dim_':
                self.ListOfDIMBranches.append(branch_name)

        self.register_branches(sample)
        self.resize_branches()

        # Create fields for DSA-PAT Link
        self.new_dim_type = []
        self.new_dim_idx = []
        self.new_dim_idxtohyb1 = []
        self.new_dim_idxtohyb2 = []
        self.new_dim_mu1_linkidx, new_dim_mu2_linkidx = ([] for _ in range(2))

    def link_input_tree(self, entry):
        # Copy the new input tree addresses to our tree
        entry._tree.CopyAddresses(self.output_tree)

        # Reset branch values
        self.branch_values.clear()

    def get_from_tree(self, branch_name):
        branch_value = self.branch_values.get(branch_name, None)

        if branch_value is None:
            branch_value = getattr(self.output_tree, branch_name)
            self.branch_values[branch_name] = branch_value

        return branch_value

    def fill(self):
        # Fill
        self.output_tree.Fill()

        # Partial Write
        self.n_entries += 1

        if (self.n_entries % 10000) == 0:
            # Write the current state of the TTree to disk
            self.output_tree.AutoSave()

        # Clear branches after filling
        self.clear()

    def clear(self):
        # Reset DSA-PAT Link fields
        self.new_dim_type = []
        self.new_dim_idx = []
        self.new_dim_idxtohyb1 = []
        self.new_dim_idxtohyb2 = []
        self.new_dim_mu1_linkidx, new_dim_mu2_linkidx = ([] for _ in range(2))

        # Reset new branches
        for branch_name, init_value in self.scaler_branches:
            branch = getattr(self, branch_name)
            branch[0] = init_value

        for branch_name in self.vector_branches:
            branch = getattr(self, branch_name)
            branch.clear()

    def write(self):
        self.output_tree.Write()
        self.output_file.Write()
        self.output_file.Close('R')

    def add_scalar_branch(self, branch_name, dtype, init_value):
        # Register scaler
        self.scaler_branches.append((branch_name, init_value))

        # Create scalar
        branch_val = array(dtype, [init_value])

        # Save scalar as field
        setattr(self, branch_name, branch_val)

        # Associate scalar with a branch
        self.output_tree.Branch(branch_name, branch_val, branch_name + '/' + dtype.upper())

    def add_vector_branch(self, branch_name, dtype, maxsize):
        # Register vector
        self.vector_branches.append(branch_name)

        # Create vector
        branch_val = std.vector(dtype)()

        # Reserve space for the vector
        branch_val.reserve(maxsize)

        # Save vector as field
        setattr(self, branch_name, branch_val)

        # Associate vector with a branch
        self.output_tree.Branch(branch_name, branch_val)

    def resize_branches(self, MAX_DSA=500, MAX_PAT=500, MAX_DIM=1000):
        # Resize PAT Branches
        for branch_name in self.ListOfPATBranches:
            branch = self.get_from_tree(branch_name)
            branch.resize(MAX_PAT)

        # Resize DSA Branches
        for branch_name in self.ListOfDSABranches:
            branch = self.get_from_tree(branch_name)
            branch.resize(MAX_DSA)

        # Resize Dimuon Branches
        for branch_name in self.ListOfDIMBranches:
            branch = self.get_from_tree(branch_name)
            branch.resize(MAX_DIM)

    def register_branches(self, sample):
        # Constants
        MAX_HLTDIM = 100
        MAX_DSA = 500
        MAX_PAT = 500
        MAX_DIM = 1000

        # -- Event trigger information
        self.add_scalar_branch('evt_hltmatch', 'i', 0)
        self.add_scalar_branch('evt_dsa_hltmatch_pp', 'i', 0)
        self.add_scalar_branch('evt_dsa_hltmatch_purepp', 'i', 0)
        self.add_scalar_branch('evt_dsa_hltmatch_cosmic', 'i', 0)
        self.add_scalar_branch('evt_dsa_hltmatch_purecosmic', 'i', 0)
        self.add_scalar_branch('evt_L1T_15_5_OR_5_3_3', 'i', 0)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_scalar_branch(f'evt_dsa_hltmatch_{path}', 'i', 0)
            self.add_scalar_branch(f'evt_hltrigger_{path}', 'i', 0)

        for seed in constants.L1TSEEDS[YEAR]:
            self.add_scalar_branch(f'evt_l1trigger_{seed}', 'i', 0)

        # -- Event counting information
        self.add_scalar_branch('evt_nDSAdim', 'i', 0)
        self.add_scalar_branch('evt_nPATdim', 'i', 0)
        self.add_scalar_branch('evt_nHYBdim', 'i', 0)
        self.add_scalar_branch('evt_nDSASel', 'i', 0)
        self.add_scalar_branch('evt_nPATSel', 'i', 0)
        self.add_scalar_branch('evt_nHYBSel', 'i', 0)

        # -- number of parallel DSA muon pairs
        self.add_scalar_branch('evt_nParallelPairs', 'i', 0)
        self.add_scalar_branch('evt_nParallelPairsPlus', 'i', 0)
        self.add_scalar_branch('evt_nParallelPairsMinus', 'i', 0)

        # -- HLT muon pairs -> HLT muons
        self.add_vector_branch('hltdim_dsa_hltmu_pp_idx', 'vector<int>', MAX_HLTDIM)
        self.add_vector_branch('hltdim_dsa_hltmu_cosmic_idx', 'vector<int>', MAX_HLTDIM)
        self.add_vector_branch('hltdim_pat_hltmu_pp_idx', 'vector<int>', MAX_HLTDIM)
        self.add_vector_branch('hltdim_pat_hltmu_cosmic_idx', 'vector<int>', MAX_HLTDIM)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_vector_branch(f'hltdim_dsa_hltmu_{path}_idx', 'vector<int>', MAX_HLTDIM)
            self.add_vector_branch(f'hltdim_pat_hltmu_{path}_idx', 'vector<int>', MAX_HLTDIM)

        # -- DSA -> GEN
        if sample.extra.is_mc:
            self.add_vector_branch('dsamu_gen_idx', 'int', MAX_DSA)

        # -- DSA branches
        self.add_vector_branch('dsamu_pt', 'float', MAX_DSA)
        self.add_vector_branch('dsamu_iso', 'float', MAX_DSA)
        self.add_vector_branch('dsamu_cleanedIso', 'float', MAX_DSA)
        self.add_vector_branch('dsamu_PATcorrectedIso', 'float', MAX_DSA)
        self.add_vector_branch('dsamu_PATcleanedIso', 'float', MAX_DSA)
        self.add_vector_branch('dsamu_repflag', 'int', MAX_DSA)

        if sample.extra.is_mc and sample.signal_point is not None:
            self.add_vector_branch('dsamu_SF_trig', 'float', MAX_DSA)
            self.add_vector_branch('dsamu_SF_trig_syst_up', 'float', MAX_DSA)
            self.add_vector_branch('dsamu_SF_trig_syst_down', 'float', MAX_DSA)
            self.add_vector_branch('dsamu_SF_ID', 'float', MAX_DSA)
            self.add_vector_branch('dsamu_SF_reco', 'float', MAX_DSA)

        # -- DSA -> HLT pair
        self.add_vector_branch('dsamu_hltdim_pp_idx', 'int', MAX_DSA)
        self.add_vector_branch('dsamu_hltdim_cosmic_idx', 'int', MAX_DSA)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_vector_branch(f'dsamu_hltdim_{path}_idx', 'int', MAX_DSA)

        # -- DSA -> HLT
        self.add_vector_branch('dsamu_hltmu_pp_idx', 'int', MAX_DSA)
        self.add_vector_branch('dsamu_hltmu_cosmic_idx', 'int', MAX_DSA)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_vector_branch(f'dsamu_hltmu_{path}_idx', 'int', MAX_DSA)

        # -- PAT -> GEN
        if sample.extra.is_mc:
            self.add_vector_branch('patmu_gen_idx', 'int', MAX_PAT)

        # -- PAT -> HLT pair
        self.add_vector_branch('patmu_hltdim_pp_idx', 'int', MAX_PAT)
        self.add_vector_branch('patmu_hltdim_cosmic_idx', 'int', MAX_PAT)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_vector_branch(f'patmu_hltdim_{path}_idx', 'int', MAX_PAT)

        # -- PAT -> HLT
        self.add_vector_branch('patmu_hltmu_pp_idx', 'int', MAX_PAT)
        self.add_vector_branch('patmu_hltmu_cosmic_idx', 'int', MAX_PAT)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            self.add_vector_branch(f'patmu_hltmu_{path}_idx', 'int', MAX_PAT)

        # -- PAT pt
        self.add_vector_branch('patmu_pt', 'float', MAX_PAT)

        # -- PAT cleanedIso
        self.add_vector_branch('patmu_cleanedIso', 'float', MAX_PAT)

        # -- PAT scale factors
        if sample.extra.is_mc and sample.signal_point is not None:
            self.add_vector_branch('patmu_SF_trig', 'float', MAX_PAT)
            self.add_vector_branch('patmu_SF_trig_syst_up', 'float', MAX_PAT)
            self.add_vector_branch('patmu_SF_trig_syst_down', 'float', MAX_PAT)
            self.add_vector_branch('patmu_SF_ID', 'float', MAX_PAT)
            self.add_vector_branch('patmu_SF_reco', 'float', MAX_PAT)
            self.add_vector_branch('patmu_SF_preselection', 'float', MAX_PAT)

        if 'patmu_tkiso0p3_sumPt_dz0p2_dxy0p1' in self.ListOfPATBranches:
            for dR, dz, d0 in yield_iso_thresholds():
                cleaned_trk_branch_name = 'clnd_tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
                    int(10 * dR), int(10 * dz), int(10 * d0)
                )

                self.add_vector_branch(f'patmu_{cleaned_trk_branch_name}', 'float', MAX_PAT)
                self.add_vector_branch(f'dim_mu1_{cleaned_trk_branch_name}', 'float', MAX_DIM)
                self.add_vector_branch(f'dim_mu2_{cleaned_trk_branch_name}', 'float', MAX_DIM)

        # -- DIM isolation
        self.add_vector_branch('dim_mu1_cleanedIso', 'float', MAX_DIM)
        self.add_vector_branch('dim_mu2_cleanedIso', 'float', MAX_DIM)

        # -- DIM -> GEN
        if sample.extra.is_mc:
            self.add_vector_branch('dim_gen_idx', 'int', MAX_DIM)
            self.add_vector_branch('dim_mu1_gen_idx', 'int', MAX_DIM)
            self.add_vector_branch('dim_mu2_gen_idx', 'int', MAX_DIM)

        # -- DIM type after DSA <-> PAT association
        self.add_vector_branch('dim_type', 'int', MAX_DIM)
        self.add_vector_branch('dim_dim_idx', 'int', MAX_DIM)
        self.add_vector_branch('dim_dim_idxtohyb1', 'int', MAX_DIM)
        self.add_vector_branch('dim_dim_idxtohyb2', 'int', MAX_DIM)
        self.add_vector_branch('dim_mu1_linkidx', 'int', MAX_DIM)
        self.add_vector_branch('dim_mu2_linkidx', 'int', MAX_DIM)

        # -- DIM back-to-back DSA muon quantities
        self.add_vector_branch('dim_BBDSA_cosAlpha_with_dim', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_minabstimediff', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_timeAtIpInOut', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_timeAtIpOutIn', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_direction', 'int', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_direction_RPC', 'int', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_pt', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_eta', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_phi', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_d0', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_dz', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_nStations', 'int', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_nMuonHits', 'int', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_FPTE', 'float', MAX_DIM)
        self.add_vector_branch('dim_BBDSA_charge', 'int', MAX_DIM)

        # -- Further DSA-DSA custom variables
        self.add_vector_branch('dim_mu1_pt', 'float', MAX_DIM)
        self.add_vector_branch('dim_mu2_pt', 'float', MAX_DIM)

        # -- NN Pairing Score
        self.add_vector_branch('dim_pscore', 'float', MAX_DIM)

        # -- Boolean branches for selection criteria (per dimuon): common post-ReTupler cuts
        self.add_vector_branch('_REP', 'int', MAX_DIM)
        self.add_vector_branch('_LXYE', 'int', MAX_DIM)
        self.add_vector_branch('_MASS', 'int', MAX_DIM)
        self.add_vector_branch('_CHI2', 'int', MAX_DIM)
        self.add_vector_branch('_COSA', 'int', MAX_DIM)
        self.add_vector_branch('_LXYS', 'int', MAX_DIM)
        self.add_vector_branch('_DCA', 'int', MAX_DIM)
        self.add_vector_branch('_OS', 'int', MAX_DIM)
        self.add_vector_branch('_DPHI', 'int', MAX_DIM)

        # -- Boolean branches for selection criteria (per dimuon): DSA-DSA post-ReTupler cuts
        self.add_vector_branch('_DENDT', 'int', MAX_DIM)
        self.add_vector_branch('_DESEG', 'int', MAX_DIM)
        self.add_vector_branch('_SEG', 'int', MAX_DIM)
        self.add_vector_branch('_DDSAT', 'int', MAX_DIM)
        self.add_vector_branch('_DIR', 'int', MAX_DIM)
        self.add_vector_branch('_BBTD', 'int', MAX_DIM)

        # -- Boolean branches for selection criteria (per dimuon): PAT-PAT post-ReTupler cuts
        self.add_vector_branch('_D0SIG', 'int', MAX_DIM)
        self.add_vector_branch('_ISO', 'int', MAX_DIM)
        self.add_vector_branch('_PXL', 'int', MAX_DIM)
        self.add_vector_branch('_MAXPT', 'int', MAX_DIM)
        self.add_vector_branch('_HB4V', 'int', MAX_DIM)
        self.add_vector_branch('_NLLXY', 'int', MAX_DIM)

        # -- Boolean branches for selection criteria (per dimuon): DSA-PAT post-ReTupler cuts
        # self.add_vector_branch('_D0SIG', 'int', MAX_DIM)  # no need to set up again
        # self.add_vector_branch('_ISO', 'int', MAX_DIM)  # no need to set up again
        # self.add_vector_branch('_NLLXY', 'int', MAX_DIM)  # no need to set up again
        self.add_vector_branch('_GLOB', 'int', MAX_DIM)
        # self.add_vector_branch('_HB4V', 'int', MAX_DIM)  # no need to set up again
        self.add_vector_branch('_DPML', 'int', MAX_DIM)
        self.add_vector_branch('_HFPTE', 'int', MAX_DIM)
        self.add_vector_branch('_HDSAT', 'int', MAX_DIM)

        # -- Flagging dimuons as passing/failing (parts of) the analysis selection
        self.add_vector_branch('dim_DSAPreSel', 'int', MAX_DIM)
        self.add_vector_branch('dim_DSASel', 'int', MAX_DIM)
        self.add_vector_branch('dim_PATPreSel', 'int', MAX_DIM)
        self.add_vector_branch('dim_PATSel', 'int', MAX_DIM)
        self.add_vector_branch('dim_HYBPreSel', 'int', MAX_DIM)
        self.add_vector_branch('dim_HYBSel', 'int', MAX_DIM)

        # -- Dimuon scale factors
        if sample.extra.is_mc and sample.signal_point is not None:
            self.add_vector_branch('dim_DSA_SF_trig', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_trig_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_trig_syst_down', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_ID', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_reco', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_all', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_HIP', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_HIP_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_DSA_SF_HIP_syst_down', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_trig', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_trig_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_trig_syst_down', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_ID', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_reco', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_preselection', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_all', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_HIP', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_HIP_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_PAT_SF_HIP_syst_down', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_trig', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_trig_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_trig_syst_down', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_ID', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_reco', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_preselection', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_all', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_HIP', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_HIP_syst_up', 'float', MAX_DIM)
            self.add_vector_branch('dim_HYB_SF_HIP_syst_down', 'float', MAX_DIM)

            self.add_vector_branch('evt_PileUpWeight', 'float', MAX_DIM)
            self.add_vector_branch('evt_PileUpWeightErr', 'float', MAX_DIM)
