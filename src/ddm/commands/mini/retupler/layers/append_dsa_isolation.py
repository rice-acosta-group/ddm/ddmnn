from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.utils.dsa_isolation import append_dsa_isolation


class AppendDSAIsolation(Layer):

    def apply(self, sample, entry, ntuple, options):
        append_dsa_isolation(
            entry.sel_dimuons,
            entry.sel_dsa_muons,
            entry.ext_pat_muons
        )
