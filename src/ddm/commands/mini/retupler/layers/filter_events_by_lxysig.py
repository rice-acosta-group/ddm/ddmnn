from ddm.commands.mini.retupler.layers.layer import Layer


class FilterEventsByLxySignificance(Layer):

    def __init__(self, lxysigcut):
        self.lxysigcut = lxysigcut

    def apply(self, sample, entry, ntuple, options):
        # Short-Circuit: Ignore HTo2XTo and HTo2ZdTo samples
        if 'HTo2XTo' in sample.name or 'HTo2ZdTo' in sample.name:
            return

        # Kill events with small lxysig
        skip_event = True

        for dimuon in entry.sel_dimuons:
            if dimuon.LxySig_pv > self.lxysigcut:
                skip_event = False
                break

        if skip_event:
            entry.sel_dimuons = list()
