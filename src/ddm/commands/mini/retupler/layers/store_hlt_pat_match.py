from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.utils.hlt_matching import match_hlt_dimuons


class StoreHLTPATMatch(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Get matches
        hlt_dimuon_matches = match_hlt_dimuons(
            entry.ext_hlt_paths, entry.ext_hlt_dimuons, entry.sel_pat_muons
        )

        # Get number of matches
        vsize_hlt_dimuon_matches = len(hlt_dimuon_matches)

        # Allocate space
        ntuple.hltdim_pat_hltmu_pp_idx.resize(vsize_hlt_dimuon_matches)
        ntuple.hltdim_pat_hltmu_cosmic_idx.resize(vsize_hlt_dimuon_matches)

        # Populate branches
        for hlt_dimuon_idx, hlt_dimuon_id in enumerate(hlt_dimuon_matches):
            hlt_match = hlt_dimuon_matches[hlt_dimuon_id]
            hltdim_pat_hltmu_pp_idx = ntuple.hltdim_pat_hltmu_pp_idx[hlt_dimuon_idx]
            hltdim_pat_hltmu_cosmic_idx = ntuple.hltdim_pat_hltmu_cosmic_idx[hlt_dimuon_idx]

            hltdim_pat_hltmu_pp_idx.resize(2)
            hltdim_pat_hltmu_cosmic_idx.resize(2)

            for i in (0, 1):
                if hlt_match['matchFound_pp']:
                    best_matches = hlt_match['bestMatches_pp'][i]
                    best_hlt_idx = best_matches['hlt_idx']
                    best_rec_idx = best_matches['rec_idx']

                    hltdim_pat_hltmu_pp_idx[i] = best_hlt_idx
                    ntuple.patmu_hltdim_pp_idx[best_rec_idx] = hlt_dimuon_idx
                    ntuple.patmu_hltmu_pp_idx[best_rec_idx] = best_hlt_idx
                else:
                    hltdim_pat_hltmu_pp_idx[i] = 100

                if hlt_match.get('matchFound_cosmic', False):
                    best_matches = hlt_match['bestMatches_cosmic'][i]
                    best_hlt_idx = best_matches['hlt_idx']
                    best_rec_idx = best_matches['rec_idx']

                    hltdim_pat_hltmu_cosmic_idx[i] = best_hlt_idx
                    ntuple.patmu_hltdim_cosmic_idx[best_rec_idx] = hlt_dimuon_idx
                    ntuple.patmu_hltmu_cosmic_idx[best_rec_idx] = best_hlt_idx
                else:
                    hltdim_pat_hltmu_cosmic_idx[i] = 100

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            all_hltdim_pat_hltmu_path_idx = getattr(ntuple, f'hltdim_pat_hltmu_{path}_idx')
            all_hltdim_pat_hltmu_path_idx.resize(vsize_hlt_dimuon_matches)

            if vsize_hlt_dimuon_matches == 0:
                continue

            patmu_hltdim_path_idx = getattr(ntuple, f'patmu_hltdim_{path}_idx')
            patmu_hltmu_path_idx = getattr(ntuple, f'patmu_hltmu_{path}_idx')

            for hlt_dimuon_idx, hlt_dimuon_match in enumerate(hlt_dimuon_matches.values()):
                hltdim_pat_hltmu_path_idx = all_hltdim_pat_hltmu_path_idx.at(hlt_dimuon_idx)
                hltdim_pat_hltmu_path_idx.resize(2)

                for i in (0, 1):
                    if hlt_dimuon_match.get('matchFound_' + path, False):
                        best_matches = hlt_dimuon_match['bestMatches_' + path][i]
                        best_hlt_idx = best_matches['hlt_idx']
                        best_rec_idx = best_matches['rec_idx']

                        hltdim_pat_hltmu_path_idx[i] = best_hlt_idx
                        patmu_hltdim_path_idx[best_rec_idx] = hlt_dimuon_idx
                        patmu_hltmu_path_idx[best_rec_idx] = best_hlt_idx
                    else:
                        hltdim_pat_hltmu_path_idx[i] = 100
