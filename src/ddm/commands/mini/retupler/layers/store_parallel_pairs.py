from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.utils.cosmics import get_number_of_parallel_pairs


class StoreParallelPairs(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Store number of parallel DSA muon pairs
        n_minus, n_plus = get_number_of_parallel_pairs(entry.ext_dsa_muons)
        ntuple.evt_nParallelPairs[0] = n_minus + n_plus
        ntuple.evt_nParallelPairsPlus[0] = n_plus
        ntuple.evt_nParallelPairsMinus[0] = n_minus
