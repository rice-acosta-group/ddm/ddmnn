from ddm.commands.mini.retupler.layers.layer import Layer


class SortRecoByIndex(Layer):

    def apply(self, sample, entry, ntuple, options):
        entry.sel_dsa_muons.sort(key=lambda mu: mu.idx)
        entry.sel_pat_muons.sort(key=lambda mu: mu.idx)
