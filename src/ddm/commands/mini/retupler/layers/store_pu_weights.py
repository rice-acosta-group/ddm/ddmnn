from ddm.commands.mini.retupler.layers.layer import Layer


class StorePileupWeights(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Short-Circuit: Only signal samples
        if not (sample.extra.is_mc and sample.signal_point is not None):
            return
