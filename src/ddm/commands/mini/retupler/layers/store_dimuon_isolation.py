from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons.isolation import yield_iso_thresholds


class StoreDimuonIsolation(Layer):

    def apply(self, sample, entry, ntuple, options):
        for ridx, dim in enumerate(entry.sel_dimuons):
            ntuple.dim_mu1_cleanedIso[ridx] = dim.mu1.cleanedIso
            ntuple.dim_mu2_cleanedIso[ridx] = dim.mu2.cleanedIso

            if 'patmu_tkiso0p3_sumPt_dz0p2_dxy0p1' not in ntuple.ListOfPATBranches:
                continue

            for dR, dz, d0 in yield_iso_thresholds():
                cleaned_trk_branch_name = 'clnd_tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
                    int(10 * dR), int(10 * dz), int(10 * d0)
                )

                if dim.composition[:3] in ('PAT', 'HYB'):
                    clean_branch_1 = getattr(ntuple, f'dim_mu1_{cleaned_trk_branch_name}')
                    clean_branch_2 = getattr(ntuple, f'dim_mu2_{cleaned_trk_branch_name}')
                    clean_iso_1 = getattr(dim.mu1, cleaned_trk_branch_name)
                    clean_iso_2 = getattr(dim.mu2, cleaned_trk_branch_name)
                    clean_branch_1[ridx] = clean_iso_1
                    clean_branch_2[ridx] = clean_iso_2
                else:
                    clean_branch_1 = getattr(ntuple, f'dim_mu1_{cleaned_trk_branch_name}')
                    clean_branch_2 = getattr(ntuple, f'dim_mu2_{cleaned_trk_branch_name}')
                    clean_branch_1[ridx] = -1.
                    clean_branch_2[ridx] = -1.
