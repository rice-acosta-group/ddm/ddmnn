from ddm.commands.mini.retupler.layers.layer import Layer


class RestructureBranches(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Unpack trees
        target_tree = ntuple.output_tree

        #################################### PAT ##############################################
        # Move branch data (Inplace!)
        patmu_idx_swaps = dict()
        vsize_patmu = len(entry.sel_pat_muons)

        for ridx, patmu in enumerate(entry.sel_pat_muons):
            patmu_idx_swaps[patmu.idx] = ridx

        for branch_name in ntuple.ListOfPATBranches:
            target_branch = ntuple.get_from_tree(branch_name)
            source_branch = entry.get_from_tree(branch_name)

            # Read values
            target_buffer = [None] * vsize_patmu

            for idx, ridx in patmu_idx_swaps.items():
                target_buffer[ridx] = source_branch[idx]

            # Write values
            for ridx, value in enumerate(target_buffer):
                # Short-Circuit: None is invalid, no need to write it
                if value is None:
                    continue

                # Write
                target_branch[ridx] = value

            # Clear cache
            target_buffer.clear()

        # Update indices
        for ridx, patmu in enumerate(entry.sel_pat_muons):
            target_tree.patmu_idx[ridx] = ridx

        # Truncate branches
        for branch_name in ntuple.ListOfPATBranches:
            target_branch = ntuple.get_from_tree(branch_name)
            target_branch.resize(vsize_patmu)

        #################################### DSA ##############################################
        # Move branch data (Inplace!)
        dsamu_idx_swaps = dict()
        vsize_dsamu = len(entry.sel_dsa_muons)

        for ridx, dsamu in enumerate(entry.sel_dsa_muons):
            dsamu_idx_swaps[dsamu.idx] = ridx

        for branch_name in ntuple.ListOfDSABranches:
            target_branch = ntuple.get_from_tree(branch_name)
            source_branch = entry.get_from_tree(branch_name)

            # Read values
            target_buffer = [None] * vsize_dsamu

            for idx, ridx in dsamu_idx_swaps.items():
                target_buffer[ridx] = source_branch[idx]

            # Write values
            for ridx, value in enumerate(target_buffer):
                # Short-Circuit: None is invalid, no need to write it
                if value is None:
                    continue

                # Write
                target_branch[ridx] = value

            # Clear cache
            target_buffer.clear()

        # Update indices
        for ridx, dsamu in enumerate(entry.sel_dsa_muons):
            target_tree.dsamu_idx[ridx] = ridx

        # Truncate branches
        for branch_name in ntuple.ListOfDSABranches:
            target_branch = ntuple.get_from_tree(branch_name)
            target_branch.resize(vsize_dsamu)

        # Re-index pointers in dsamu_
        for ridx in range(vsize_dsamu):
            match_ridx = 0

            for match_idx, pat_idx in enumerate(target_tree.dsamu_idx_SegmMatch[ridx]):
                pat_ridx = patmu_idx_swaps.get(pat_idx, None)

                if pat_ridx is None:
                    continue

                target_tree.dsamu_idx_SegmMatch[ridx][match_ridx] = pat_ridx
                match_ridx += 1

            target_tree.dsamu_idx_SegmMatch[ridx].resize(match_ridx)

            pat_idx = target_tree.dsamu_idx_pd_ProxMatch[ridx]
            pat_ridx = patmu_idx_swaps.get(pat_idx, -999)
            target_tree.dsamu_idx_pd_ProxMatch[ridx] = pat_ridx if pat_idx >= 0 else pat_idx

            pat_idx = target_tree.dsamu_idx_pp_ProxMatch[ridx]
            pat_ridx = patmu_idx_swaps.get(pat_idx, -999)
            target_tree.dsamu_idx_pp_ProxMatch[ridx] = pat_ridx if pat_idx >= 0 else pat_idx

            # pat_idx = target_tree.dsamu_idx_NNMatch[ridx]
            # pat_ridx = patmu_idx_swaps.get(pat_idx, -999)
            # target_tree.dsamu_idx_NNMatch[ridx] = pat_ridx if pat_idx >= 0 else pat_idx

            # pat_idx = target_tree.dsamu_stdMatch_idx[ridx]
            # pat_ridx = patmu_idx_swaps.get(pat_idx, -999)
            # target_tree.dsamu_stdMatch_idx[ridx] = pat_ridx if pat_idx >= 0 else pat_idx

        ################################### DIMUONS ###########################################

        # Collect muon ref
        dim_mu1_ridx_lut = dict()
        dim_mu2_ridx_lut = dict()

        for ridx, dim in enumerate(entry.sel_dimuons):
            # Muon 1 reference
            if dim.mu1_idx > 999:
                dim_mu1_ridx_lut[ridx] = patmu_idx_swaps[dim.mu1_idx - 1000] + 1000
            elif dim.mu1_idx >= 0:
                dim_mu1_ridx_lut[ridx] = dsamu_idx_swaps[dim.mu1_idx]

            # Muon 2 reference
            if dim.mu2_idx > 999:
                dim_mu2_ridx_lut[ridx] = patmu_idx_swaps[dim.mu2_idx - 1000] + 1000
            elif dim.mu2_idx >= 0:
                dim_mu2_ridx_lut[ridx] = dsamu_idx_swaps[dim.mu2_idx]

        # Move branch data (Inplace!)
        dim_index_swaps = list()
        vsize_dim = len(entry.sel_dimuons)

        for ridx, dim in enumerate(entry.sel_dimuons):
            dim_index_swaps.append((dim.idx, ridx))

        for branch_name in ntuple.ListOfDIMBranches:
            target_branch = ntuple.get_from_tree(branch_name)
            source_branch = entry.get_from_tree(branch_name)

            # Read values
            target_buffer = [None] * vsize_dim

            for idx, ridx in dim_index_swaps:
                target_buffer[ridx] = source_branch[idx]

            # Write values
            for ridx, value in enumerate(target_buffer):
                # Short-Circuit: None is invalid, no need to write it
                if value is None:
                    continue

                # Write
                target_branch[ridx] = value

            # Clear cache
            target_buffer.clear()

        # Re-index muon pointers
        for ridx in range(vsize_dim):
            # Muon References
            target_tree.dim_mu1_idx[ridx] = dim_mu1_ridx_lut[ridx]
            target_tree.dim_mu2_idx[ridx] = dim_mu2_ridx_lut[ridx]

            # Muon 1 linking indices
            mu1_linkidx = target_tree.dim_mu1_linkidx[ridx]

            if mu1_linkidx != -1:
                if mu1_linkidx > 999:
                    target_tree.dim_mu1_linkidx[ridx] = patmu_idx_swaps[mu1_linkidx - 1000] + 1000
                else:
                    target_tree.dim_mu1_linkidx[ridx] = dsamu_idx_swaps[mu1_linkidx]

            # Muon 2 linking indices
            mu2_linkidx = target_tree.dim_mu2_linkidx[ridx]

            if mu2_linkidx != -1:
                if mu2_linkidx > 999:
                    target_tree.dim_mu2_linkidx[ridx] = patmu_idx_swaps[mu2_linkidx - 1000] + 1000
                else:
                    target_tree.dim_mu2_linkidx[ridx] = dsamu_idx_swaps[mu2_linkidx]

        # Truncate branches
        for branch_name in ntuple.ListOfDIMBranches:
            target_branch = ntuple.get_from_tree(branch_name)
            target_branch.resize(vsize_dim)
