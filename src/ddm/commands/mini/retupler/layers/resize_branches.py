from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.commons.isolation import yield_iso_thresholds


class ResizeNewBranches(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Get required space
        vsize_dsa_muons = len(entry.sel_dsa_muons)
        vsize_pat_muons = len(entry.sel_pat_muons)
        vsize_dimuons = len(entry.sel_dimuons)

        # resize new branches
        if sample.extra.is_mc:
            ntuple.dsamu_gen_idx.resize(vsize_dsa_muons)

        ntuple.dsamu_hltdim_pp_idx.resize(vsize_dsa_muons)
        ntuple.dsamu_hltdim_cosmic_idx.resize(vsize_dsa_muons)
        ntuple.dsamu_hltmu_pp_idx.resize(vsize_dsa_muons)
        ntuple.dsamu_hltmu_cosmic_idx.resize(vsize_dsa_muons)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            getattr(ntuple, f'dsamu_hltdim_{path}_idx').resize(vsize_dsa_muons)
            getattr(ntuple, f'dsamu_hltmu_{path}_idx').resize(vsize_dsa_muons)

        ntuple.dsamu_pt.resize(vsize_dsa_muons)
        ntuple.dsamu_iso.resize(vsize_dsa_muons)
        ntuple.dsamu_cleanedIso.resize(vsize_dsa_muons)
        ntuple.dsamu_PATcorrectedIso.resize(vsize_dsa_muons)
        ntuple.dsamu_PATcleanedIso.resize(vsize_dsa_muons)
        ntuple.dsamu_repflag.resize(vsize_dsa_muons)

        if sample.extra.is_mc and sample.signal_point is not None:
            ntuple.dsamu_SF_trig.resize(vsize_dsa_muons)
            ntuple.dsamu_SF_trig_syst_up.resize(vsize_dsa_muons)
            ntuple.dsamu_SF_trig_syst_down.resize(vsize_dsa_muons)
            ntuple.dsamu_SF_ID.resize(vsize_dsa_muons)
            ntuple.dsamu_SF_reco.resize(vsize_dsa_muons)

        if sample.extra.is_mc:
            ntuple.patmu_gen_idx.resize(vsize_pat_muons)

        ntuple.patmu_hltdim_pp_idx.resize(vsize_pat_muons)
        ntuple.patmu_hltdim_cosmic_idx.resize(vsize_pat_muons)
        ntuple.patmu_hltmu_pp_idx.resize(vsize_pat_muons)
        ntuple.patmu_hltmu_cosmic_idx.resize(vsize_pat_muons)
        ntuple.patmu_pt.resize(vsize_pat_muons)
        ntuple.patmu_cleanedIso.resize(vsize_pat_muons)

        for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
            getattr(ntuple, f'patmu_hltdim_{path}_idx').resize(vsize_pat_muons)
            getattr(ntuple, f'patmu_hltmu_{path}_idx').resize(vsize_pat_muons)

        if sample.extra.is_mc and sample.signal_point is not None:
            ntuple.patmu_SF_trig.resize(vsize_pat_muons)
            ntuple.patmu_SF_trig_syst_up.resize(vsize_pat_muons)
            ntuple.patmu_SF_trig_syst_down.resize(vsize_pat_muons)
            ntuple.patmu_SF_ID.resize(vsize_pat_muons)
            ntuple.patmu_SF_reco.resize(vsize_pat_muons)
            ntuple.patmu_SF_preselection.resize(vsize_pat_muons)

        if sample.extra.is_mc:
            ntuple.dim_gen_idx.resize(vsize_dimuons)
            ntuple.dim_mu1_gen_idx.resize(vsize_dimuons)
            ntuple.dim_mu2_gen_idx.resize(vsize_dimuons)

        ntuple.dim_mu1_cleanedIso.resize(vsize_dimuons)
        ntuple.dim_mu2_cleanedIso.resize(vsize_dimuons)
        ntuple.dim_type.resize(vsize_dimuons)
        ntuple.dim_dim_idx.resize(vsize_dimuons)
        ntuple.dim_dim_idxtohyb1.resize(vsize_dimuons)
        ntuple.dim_dim_idxtohyb2.resize(vsize_dimuons)
        ntuple.dim_mu1_linkidx.resize(vsize_dimuons)
        ntuple.dim_mu2_linkidx.resize(vsize_dimuons)
        ntuple.dim_BBDSA_cosAlpha_with_dim.resize(vsize_dimuons)
        ntuple.dim_BBDSA_minabstimediff.resize(vsize_dimuons)
        ntuple.dim_BBDSA_timeAtIpInOut.resize(vsize_dimuons)
        ntuple.dim_BBDSA_timeAtIpOutIn.resize(vsize_dimuons)
        ntuple.dim_BBDSA_direction.resize(vsize_dimuons)
        ntuple.dim_BBDSA_direction_RPC.resize(vsize_dimuons)
        ntuple.dim_BBDSA_pt.resize(vsize_dimuons)
        ntuple.dim_BBDSA_eta.resize(vsize_dimuons)
        ntuple.dim_BBDSA_phi.resize(vsize_dimuons)
        ntuple.dim_BBDSA_d0.resize(vsize_dimuons)
        ntuple.dim_BBDSA_dz.resize(vsize_dimuons)
        ntuple.dim_BBDSA_nStations.resize(vsize_dimuons)
        ntuple.dim_BBDSA_nMuonHits.resize(vsize_dimuons)
        ntuple.dim_BBDSA_FPTE.resize(vsize_dimuons)
        ntuple.dim_BBDSA_charge.resize(vsize_dimuons)
        ntuple.dim_mu1_pt.resize(vsize_dimuons)
        ntuple.dim_mu2_pt.resize(vsize_dimuons)
        ntuple.dim_pscore.resize(vsize_dimuons)
        ntuple._REP.resize(vsize_dimuons)
        ntuple._LXYE.resize(vsize_dimuons)
        ntuple._MASS.resize(vsize_dimuons)
        ntuple._CHI2.resize(vsize_dimuons)
        ntuple._COSA.resize(vsize_dimuons)
        ntuple._LXYS.resize(vsize_dimuons)
        ntuple._DCA.resize(vsize_dimuons)
        ntuple._OS.resize(vsize_dimuons)
        ntuple._DPHI.resize(vsize_dimuons)
        ntuple._DENDT.resize(vsize_dimuons)
        ntuple._DESEG.resize(vsize_dimuons)
        ntuple._SEG.resize(vsize_dimuons)
        ntuple._DDSAT.resize(vsize_dimuons)
        ntuple._DIR.resize(vsize_dimuons)
        ntuple._BBTD.resize(vsize_dimuons)
        ntuple._D0SIG.resize(vsize_dimuons)
        ntuple._ISO.resize(vsize_dimuons)
        ntuple._PXL.resize(vsize_dimuons)
        ntuple._MAXPT.resize(vsize_dimuons)
        ntuple._HB4V.resize(vsize_dimuons)
        ntuple._NLLXY.resize(vsize_dimuons)
        ntuple._GLOB.resize(vsize_dimuons)
        ntuple._DPML.resize(vsize_dimuons)
        ntuple._HFPTE.resize(vsize_dimuons)
        ntuple._HDSAT.resize(vsize_dimuons)
        ntuple.dim_DSAPreSel.resize(vsize_dimuons)
        ntuple.dim_DSASel.resize(vsize_dimuons)
        ntuple.dim_PATPreSel.resize(vsize_dimuons)
        ntuple.dim_PATSel.resize(vsize_dimuons)
        ntuple.dim_HYBPreSel.resize(vsize_dimuons)
        ntuple.dim_HYBSel.resize(vsize_dimuons)

        if sample.extra.is_mc and sample.signal_point is not None:
            ntuple.dim_DSA_SF_trig.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_trig_syst_up.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_trig_syst_down.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_ID.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_reco.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_all.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_HIP.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_HIP_syst_up.resize(vsize_dimuons)
            ntuple.dim_DSA_SF_HIP_syst_down.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_trig.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_trig_syst_up.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_trig_syst_down.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_ID.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_reco.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_preselection.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_all.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_HIP.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_HIP_syst_up.resize(vsize_dimuons)
            ntuple.dim_PAT_SF_HIP_syst_down.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_trig.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_trig_syst_up.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_trig_syst_down.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_ID.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_reco.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_preselection.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_all.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_HIP.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_HIP_syst_up.resize(vsize_dimuons)
            ntuple.dim_HYB_SF_HIP_syst_down.resize(vsize_dimuons)

            ntuple.evt_PileUpWeight.resize(1)
            ntuple.evt_PileUpWeightErr.resize(1)

        # -- custom settings cleaned isolation
        if 'patmu_tkiso0p3_sumPt_dz0p2_dxy0p1' in ntuple.ListOfPATBranches:
            for dR, dz, d0 in yield_iso_thresholds():
                cleaned_trk_branch_name = 'clnd_tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
                    int(10 * dR), int(10 * dz), int(10 * d0)
                )

                branch = getattr(ntuple, f'patmu_{cleaned_trk_branch_name}')
                branch.resize(vsize_pat_muons)

                branch = getattr(ntuple, f'dim_mu1_{cleaned_trk_branch_name}')
                branch.resize(vsize_dimuons)

                branch = getattr(ntuple, f'dim_mu2_{cleaned_trk_branch_name}')
                branch.resize(vsize_dimuons)
