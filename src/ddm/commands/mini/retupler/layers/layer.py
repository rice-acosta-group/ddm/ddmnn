from abc import abstractmethod, ABC


class Graph:

    def __init__(self):
        self.graph = None

    def append(self, layer):
        # Build new graph
        parent_graph = self.graph

        def new_graph(sample, entry, ntuple, options):
            # Run parent
            if parent_graph is not None:
                is_valid = parent_graph(sample, entry, ntuple, options)

                # Short-Circuit: Invalid entry found
                if not is_valid:
                    return False

            # Run layer
            layer.apply(sample, entry, ntuple, options)

            # Return
            return len(entry.sel_dimuons) > 0

        # Update graph
        self.graph = new_graph

    def apply(self, sample, entry, ntuple, options):
        # Short-Circuit: No layers
        if self.graph is None:
            raise Exception('No layers have been added.')

        # Run graph
        return self.graph(sample, entry, ntuple, options)


class Layer(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def apply(self, sample, entry, ntuple, options):
        raise NotImplementedError
