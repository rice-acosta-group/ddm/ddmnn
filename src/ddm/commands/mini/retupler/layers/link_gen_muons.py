from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.utils.muon_matching import match_muons


class LinkGENMuons(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Short-Circuit: Only process MC sample
        if not sample.extra.is_mc:
            return

        # Initialize gen links to the default value
        for ridx in range(len(entry.sel_dsa_muons)):
            ntuple.dsamu_gen_idx[ridx] = 100

        for ridx in range(len(entry.sel_pat_muons)):
            ntuple.patmu_gen_idx[ridx] = 100

        # Get GEN muons
        gen_muons = [
            gen for gen in entry.ext_gen_particles
            if abs(gen.pdgID) == 13 and gen.is_final_state and (gen.kind == 'signal')
        ]

        # Match GEN muons to DSA muons
        match_results = match_muons(
            gen_muons, entry.sel_dsa_muons,
        )

        dsa_idx_to_ridx = {
            dsa_muon.idx: ridx
            for ridx, dsa_muon in enumerate(entry.sel_dsa_muons)
        }

        for gen_muon, dsa_muon, _ in match_results:
            ridx = dsa_idx_to_ridx[dsa_muon.idx]
            ntuple.dsamu_gen_idx[ridx] = gen_muon.idx

        # Match GEN muons to PAT muons
        match_results = match_muons(
            gen_muons, entry.sel_pat_muons,
        )

        pat_idx_to_ridx = {
            pat_muon.idx: ridx
            for ridx, pat_muon in enumerate(entry.sel_pat_muons)
        }

        for gen_muon, pat_muon, _ in match_results:
            ridx = pat_idx_to_ridx[pat_muon.idx]
            ntuple.patmu_gen_idx[ridx] = gen_muon.idx
