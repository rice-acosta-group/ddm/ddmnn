from ddm.commands.mini.retupler.layers.layer import Layer


class StoreL1TSelection(Layer):

    def apply(self, sample, entry, ntuple, options):
        # L1T Filter decisions (to be applied only to 2018 MC)
        nL1Mu15 = ([(abs(l1mu.pt) >= 14.9 and l1mu.idx == 0) for l1mu in entry.ext_l1t_muons]).count(True)
        nL1Mu5 = ([(abs(l1mu.pt) >= 4.9 and l1mu.idx == 0) for l1mu in entry.ext_l1t_muons]).count(True)
        nL1Mu3 = ([(abs(l1mu.pt) >= 2.9 and l1mu.idx == 0) for l1mu in entry.ext_l1t_muons]).count(True)

        passedL1T_15_5 = nL1Mu15 > 0 and nL1Mu5 > 1
        passedL1T_5_3_3 = nL1Mu5 > 0 and nL1Mu3 > 2

        ntuple.evt_L1T_15_5_OR_5_3_3[0] = int(passedL1T_15_5 or passedL1T_5_3_3)
