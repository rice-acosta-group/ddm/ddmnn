from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants


class StoreDSAInfo(Layer):

    def apply(self, sample, entry, ntuple, options):
        for ridx, muon in enumerate(entry.sel_dsa_muons):
            # Reset HLT links?
            ntuple.dsamu_hltdim_pp_idx[ridx] = 100
            ntuple.dsamu_hltdim_cosmic_idx[ridx] = 100
            ntuple.dsamu_hltmu_pp_idx[ridx] = 100
            ntuple.dsamu_hltmu_cosmic_idx[ridx] = 100

            for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
                getattr(ntuple, f'dsamu_hltdim_{path}_idx')[ridx] = 100
                getattr(ntuple, f'dsamu_hltmu_{path}_idx')[ridx] = 100

            # Save new data
            ntuple.dsamu_pt[ridx] = muon.pt
            ntuple.dsamu_iso[ridx] = muon.iso
            ntuple.dsamu_cleanedIso[ridx] = muon.cleanedIso
            ntuple.dsamu_PATcorrectedIso[ridx] = muon.PATcorrectedIso
            ntuple.dsamu_PATcleanedIso[ridx] = muon.PATcleanedIso
            ntuple.dsamu_repflag[ridx] = muon.replacement_status
