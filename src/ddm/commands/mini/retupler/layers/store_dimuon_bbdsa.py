from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS


class StoreDimuonBBDSA(Layer):

    def apply(self, sample, entry, ntuple, options):
        for ridx, dim in enumerate(entry.sel_dimuons):
            # Initialize branches
            ntuple.dim_BBDSA_cosAlpha_with_dim[ridx] = +999
            ntuple.dim_BBDSA_minabstimediff[ridx] = -999
            ntuple.dim_BBDSA_timeAtIpInOut[ridx] = -999
            ntuple.dim_BBDSA_timeAtIpOutIn[ridx] = -999
            ntuple.dim_BBDSA_direction[ridx] = -999
            ntuple.dim_BBDSA_direction_RPC[ridx] = -999
            ntuple.dim_BBDSA_pt[ridx] = -999
            ntuple.dim_BBDSA_eta[ridx] = -999
            ntuple.dim_BBDSA_phi[ridx] = -999
            ntuple.dim_BBDSA_d0[ridx] = -999
            ntuple.dim_BBDSA_dz[ridx] = -999
            ntuple.dim_BBDSA_nStations[ridx] = -999
            ntuple.dim_BBDSA_nMuonHits[ridx] = -999
            ntuple.dim_BBDSA_FPTE[ridx] = -999
            ntuple.dim_BBDSA_charge[ridx] = -999

            if dim.composition != "DSA":
                continue

            # if any DSA dimuons have survived so far, check if they are back-to-back
            # with a third muon whose time is 20+ ns off (indicating a cosmic-ray event)
            BBDSA_candidates = []

            for dsa_muon in entry.ext_dsa_muons:
                # Short-Circuit: Skip DSA muons that are part of the dimuon
                if dsa_muon.idx in dim.id:
                    continue

                cosa_with_dim1 = dim.mu1.p4.Vect().Dot(dsa_muon.p4.Vect()) / dim.mu1.p4.P() / dsa_muon.p4.P()
                cosa_with_dim2 = dim.mu2.p4.Vect().Dot(dsa_muon.p4.Vect()) / dim.mu2.p4.P() / dsa_muon.p4.P()
                min_cosa_with_dim = min(cosa_with_dim1, cosa_with_dim2)

                # Short-Circuit: min cosa too large
                if min_cosa_with_dim >= -0.9:
                    continue

                # Short-Circuit: doesn't pass quality cut
                if not CUT_DEFINITIONS["m_pT"].apply(dsa_muon):
                    continue

                # Calculate time difference
                dim_dsamu1 = entry.all_dsa_muons[dim.mu1.idx]
                dim_dsamu2 = entry.all_dsa_muons[dim.mu2.idx]

                minabstimediff = min(
                    abs(dsa_muon.timeAtIP_InOut - dim_dsamu1.timeAtIP_InOut),
                    abs(dsa_muon.timeAtIP_InOut - dim_dsamu2.timeAtIP_InOut)
                )

                # Append
                BBDSA_candidates.append((dsa_muon, min_cosa_with_dim, minabstimediff))

            # Short-Circuit: No candidates found
            if len(BBDSA_candidates) == 0:
                continue

            # Save BBDSA candidate
            # choose BBDSA muon with the largest time difference
            BBDSA_candidates.sort(key=lambda tup: tup[2], reverse=True)
            bbdsamuon, min_cosa_with_dim, minabstimediff = BBDSA_candidates[0]

            ntuple.dim_BBDSA_cosAlpha_with_dim[ridx] = min_cosa_with_dim
            ntuple.dim_BBDSA_minabstimediff[ridx] = minabstimediff
            ntuple.dim_BBDSA_timeAtIpInOut[ridx] = bbdsamuon.timeAtIP_InOut
            ntuple.dim_BBDSA_timeAtIpOutIn[ridx] = bbdsamuon.timeAtIP_OutIn
            ntuple.dim_BBDSA_direction[ridx] = bbdsamuon.direction
            ntuple.dim_BBDSA_direction[ridx] = bbdsamuon.direction_RPC
            ntuple.dim_BBDSA_pt[ridx] = bbdsamuon.pt
            ntuple.dim_BBDSA_eta[ridx] = bbdsamuon.eta
            ntuple.dim_BBDSA_phi[ridx] = bbdsamuon.phi
            ntuple.dim_BBDSA_d0[ridx] = bbdsamuon.d0_pv
            ntuple.dim_BBDSA_dz[ridx] = bbdsamuon.dz_pv
            ntuple.dim_BBDSA_nStations[ridx] = bbdsamuon.nDTStations + bbdsamuon.nCSCStations
            ntuple.dim_BBDSA_nMuonHits[ridx] = bbdsamuon.nDTHits + bbdsamuon.nCSCHits
            ntuple.dim_BBDSA_FPTE[ridx] = bbdsamuon.ptError / bbdsamuon.pt
            ntuple.dim_BBDSA_charge[ridx] = bbdsamuon.charge
