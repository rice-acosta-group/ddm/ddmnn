from ddm.commands.mini.retupler.layers.layer import Layer


class StoreDimuonDSAPATLink(Layer):

    def apply(self, sample, entry, ntuple, options):
        dsa_pat_link_en = options.DSAPATLINK or options.DSAPATLINKNN

        for ridx, dim in enumerate(entry.sel_dimuons):
            types_tmp = {"DSA": 0, "PAT": 23, "HYBRID": 12}
            ntuple.dim_type[ridx] = types_tmp[dim.composition] if not dsa_pat_link_en else ntuple.new_dim_type[ridx]
            ntuple.dim_dim_idx[ridx] = -1 if not dsa_pat_link_en else ntuple.new_dim_idx[ridx]
            ntuple.dim_dim_idxtohyb1[ridx] = -1 if not dsa_pat_link_en else ntuple.new_dim_idxtohyb1[ridx]
            ntuple.dim_dim_idxtohyb2[ridx] = -1 if not dsa_pat_link_en else ntuple.new_dim_idxtohyb2[ridx]
            ntuple.dim_mu1_linkidx[ridx] = -1 if not dsa_pat_link_en else ntuple.new_dim_mu1_linkidx[ridx]
            ntuple.dim_mu2_linkidx[ridx] = -1 if not dsa_pat_link_en else ntuple.new_dim_mu2_linkidx[ridx]
