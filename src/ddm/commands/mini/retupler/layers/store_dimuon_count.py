from ddm.commands.mini.retupler.layers.layer import Layer


class StoreDimuonCount(Layer):

    def apply(self, sample, entry, ntuple, options):
        sel_dimuons = entry.sel_dimuons

        # Count selected dimuons per entry (for the full, post-ReTupler analysis selection)
        ntuple.evt_nDSASel[0] = ([ntuple.dim_DSASel[ridx] == 1 for ridx, _ in enumerate(sel_dimuons)]).count(True)
        ntuple.evt_nPATSel[0] = ([ntuple.dim_PATSel[ridx] == 1 for ridx, _ in enumerate(sel_dimuons)]).count(True)
        ntuple.evt_nHYBSel[0] = ([ntuple.dim_HYBSel[ridx] == 1 for ridx, _ in enumerate(sel_dimuons)]).count(True)

        # Count events with dimuons selected by the ReTupler cutstring
        ntuple.evt_nDSAdim[0] = len([dim for dim in sel_dimuons if dim.composition == "DSA"])
        ntuple.evt_nPATdim[0] = len([dim for dim in sel_dimuons if dim.composition == "PAT"])
        ntuple.evt_nHYBdim[0] = len([dim for dim in sel_dimuons if dim.composition == "HYBRID"])
