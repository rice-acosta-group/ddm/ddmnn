from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.utils.hlt_matching import match_hlt_dimuons


class StoreHLTDSAMatch(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Get matches
        hlt_dimuon_matches = match_hlt_dimuons(
            entry.ext_hlt_paths, entry.ext_hlt_dimuons, entry.sel_dsa_muons
        )

        # Get number of matches
        vsize_hlt_dimuon_matches = len(hlt_dimuon_matches)

        # Allocate space
        ntuple.hltdim_dsa_hltmu_pp_idx.resize(vsize_hlt_dimuon_matches)
        ntuple.hltdim_dsa_hltmu_cosmic_idx.resize(vsize_hlt_dimuon_matches)

        # Populate branches
        for hlt_dimuon_idx, hlt_dimuon_id in enumerate(hlt_dimuon_matches):
            hlt_dimuon_match = hlt_dimuon_matches[hlt_dimuon_id]
            hltdim_dsa_hltmu_pp_idx = ntuple.hltdim_dsa_hltmu_pp_idx[hlt_dimuon_idx]
            hltdim_dsa_hltmu_cosmic_idx = ntuple.hltdim_dsa_hltmu_cosmic_idx[hlt_dimuon_idx]

            hltdim_dsa_hltmu_pp_idx.resize(2)
            hltdim_dsa_hltmu_cosmic_idx.resize(2)

            for i in (0, 1):
                if hlt_dimuon_match['matchFound_pp']:
                    best_matches = hlt_dimuon_match['bestMatches_pp'][i]
                    best_hlt_idx = best_matches['hlt_idx']
                    best_rec_idx = best_matches['rec_idx']

                    hltdim_dsa_hltmu_pp_idx[i] = best_hlt_idx
                    ntuple.dsamu_hltdim_pp_idx[best_rec_idx] = hlt_dimuon_idx
                    ntuple.dsamu_hltmu_pp_idx[best_rec_idx] = best_hlt_idx
                else:
                    hltdim_dsa_hltmu_pp_idx[i] = 100

                if hlt_dimuon_match.get('matchFound_cosmic', False):
                    best_matches = hlt_dimuon_match['bestMatches_cosmic'][i]
                    best_hlt_idx = best_matches['hlt_idx']
                    best_rec_idx = best_matches['rec_idx']

                    hltdim_dsa_hltmu_cosmic_idx[i] = best_hlt_idx
                    ntuple.dsamu_hltdim_cosmic_idx[best_rec_idx] = hlt_dimuon_idx
                    ntuple.dsamu_hltmu_cosmic_idx[best_rec_idx] = best_hlt_idx
                else:
                    hltdim_dsa_hltmu_cosmic_idx[i] = 100

        # Save flags
        hlt_flags = entry.sel_info.hlt_flags

        ntuple.evt_hltmatch[0] = 1
        ntuple.evt_dsa_hltmatch_pp[0] = 1 if hlt_flags.get('HLTPP', False) is True else 0
        ntuple.evt_dsa_hltmatch_purepp[0] = 1 if hlt_flags.get('HLTPUREPP', False) is True else 0
        ntuple.evt_dsa_hltmatch_cosmic[0] = 1 if hlt_flags.get('HLTCOSMIC', False) is True else 0
        ntuple.evt_dsa_hltmatch_purecosmic[0] = 1 if hlt_flags.get('HLTPURECOSMIC', False) is True else 0

        for path, hlt_path_base in constants.SIGNAL_HLT_PATH[constants.YEAR].items():
            # Check if the path is in the event
            is_path_in_event = any([
                str(hlt_path.name).startswith(hlt_path_base)
                for hlt_path in entry.ext_hlt_paths
            ])

            # Save flags
            evt_hltrigger = getattr(ntuple, f'evt_hltrigger_{path}')
            evt_dsa_hltmatch = getattr(ntuple, f'evt_dsa_hltmatch_{path}')

            evt_hltrigger[0] = 1 if is_path_in_event is True else 0
            evt_dsa_hltmatch[0] = 1 if hlt_flags.get(path, False) is True else 0

            # Save matches
            all_hltdim_dsa_hltmu_path_idx = getattr(ntuple, f'hltdim_dsa_hltmu_{path}_idx')
            all_hltdim_dsa_hltmu_path_idx.resize(vsize_hlt_dimuon_matches)

            if vsize_hlt_dimuon_matches == 0:
                continue

            dsamu_hltdim_path_idx = getattr(ntuple, f'dsamu_hltdim_{path}_idx')
            dsamu_hltmu_path_idx = getattr(ntuple, f'dsamu_hltmu_{path}_idx')

            for hlt_dimuon_idx, hlt_dimuon_match in enumerate(hlt_dimuon_matches.values()):
                hltdim_dsa_hltmu_path_idx = all_hltdim_dsa_hltmu_path_idx.at(hlt_dimuon_idx)
                hltdim_dsa_hltmu_path_idx.resize(2)

                for i in (0, 1):
                    if hlt_dimuon_match.get('matchFound_' + path, False):
                        best_matches = hlt_dimuon_match['bestMatches_' + path][i]
                        best_hlt_idx = best_matches['hlt_idx']
                        best_rec_idx = best_matches['rec_idx']

                        hltdim_dsa_hltmu_path_idx[i] = best_hlt_idx
                        dsamu_hltdim_path_idx[best_rec_idx] = hlt_dimuon_idx
                        dsamu_hltmu_path_idx[best_rec_idx] = best_hlt_idx
                    else:
                        hltdim_dsa_hltmu_path_idx[i] = 100
