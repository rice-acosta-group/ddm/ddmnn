from ddm.commands.mini.retupler.layers.layer import Layer


class StoreDimuonInfo(Layer):

    def apply(self, sample, entry, ntuple, options):
        for ridx, dim in enumerate(entry.sel_dimuons):
            ntuple.dim_mu1_pt[ridx] = dim.mu1.pt
            ntuple.dim_mu2_pt[ridx] = dim.mu2.pt
            ntuple.dim_pscore[ridx] = dim.pscore
