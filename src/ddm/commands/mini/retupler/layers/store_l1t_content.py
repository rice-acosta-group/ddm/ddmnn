from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.commons.constants import YEAR


class StoreL1TContent(Layer):

    def apply(self, sample, entry, ntuple, options):
        if constants.YEAR != 2022:
            return

        l1_content = [
            str(l1t_seed.name)
            for l1t_seed in entry.ext_l1t_seeds
        ]

        for seed in constants.L1TSEEDS[YEAR]:
            l1trigger = getattr(ntuple, f'evt_l1trigger_{seed}')
            l1trigger[0] = int(seed in l1_content)
