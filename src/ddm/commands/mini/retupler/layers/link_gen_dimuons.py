from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.utils.dimuon_matching import match_gen_dimuons


class LinkGENDimuons(Layer):

    def apply(self, sample, entry, ntuple, options):
        # Short-Circuit: Only process MC sample
        if not sample.extra.is_mc:
            return

        # Initialize gen links to the default value
        for ridx in range(len(entry.sel_dimuons)):
            ntuple.dim_gen_idx[ridx] = 100
            ntuple.dim_mu1_gen_idx[ridx] = 100
            ntuple.dim_mu2_gen_idx[ridx] = 100

        # Match gen dimuons to reco dimuons
        match_result = match_gen_dimuons(
            entry.ext_gen_dimuons, entry.sel_dimuons,
            deltaR_threshold=0.05, deltaVtx_threshold=20.0
        )

        if len(match_result) == 0:
            return

        # Build LUTs
        reco_dimuon_ridx_by_idx = {
            dimuon.idx: ridx
            for ridx, dimuon in enumerate(entry.sel_dimuons)
        }

        # Store links
        for gen_dimuon, reco_dimuon, are_aligned in match_result:
            # Lookup new object index
            reco_dimuon_ridx = reco_dimuon_ridx_by_idx[reco_dimuon.idx]

            # Save gen idx for dimuon
            ntuple.dim_gen_idx[reco_dimuon_ridx] = gen_dimuon.gen_idx

            # Save gen idx for each muon in dimuon
            if are_aligned:
                ntuple.dim_mu1_gen_idx[reco_dimuon_ridx] = gen_dimuon.mu1.idx
                ntuple.dim_mu2_gen_idx[reco_dimuon_ridx] = gen_dimuon.mu2.idx
            else:
                ntuple.dim_mu1_gen_idx[reco_dimuon_ridx] = gen_dimuon.mu2.idx
                ntuple.dim_mu2_gen_idx[reco_dimuon_ridx] = gen_dimuon.mu1.idx
