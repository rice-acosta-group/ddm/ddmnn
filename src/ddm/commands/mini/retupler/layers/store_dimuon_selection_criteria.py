import math

from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.commons.variables import calc_deltaphi, calc_phi


class StoreDimuonSelectionCriteria(Layer):

    def apply(self, sample, entry, ntuple, options):
        vertex = entry.ext_vertex

        for ridx, dim in enumerate(entry.sel_dimuons):
            # Set default values for dimuon selection criteria
            ntuple._REP[ridx] = 0
            ntuple._LXYE[ridx] = 0
            ntuple._MASS[ridx] = 0
            ntuple._CHI2[ridx] = 0
            ntuple._COSA[ridx] = 0
            ntuple._LXYS[ridx] = 0
            ntuple._DCA[ridx] = 0
            ntuple._OS[ridx] = 0
            ntuple._DPHI[ridx] = 0
            ntuple._DENDT[ridx] = 0
            ntuple._DESEG[ridx] = 0
            ntuple._SEG[ridx] = 0
            ntuple._DDSAT[ridx] = 0
            ntuple._DIR[ridx] = 0
            ntuple._BBTD[ridx] = 0
            ntuple._D0SIG[ridx] = 0
            ntuple._ISO[ridx] = 0
            ntuple._PXL[ridx] = 0
            ntuple._MAXPT[ridx] = 0
            ntuple._HB4V[ridx] = 0
            ntuple._NLLXY[ridx] = 0
            ntuple._GLOB[ridx] = 0
            ntuple._DPML[ridx] = 0
            ntuple._HFPTE[ridx] = 0
            ntuple._HDSAT[ridx] = 0

            # Evaluate DSA-DSA dimuon selection criteria
            if dim.composition == "DSA":
                ntuple.dim_PATPreSel[ridx] = -1
                ntuple.dim_PATSel[ridx] = -1
                ntuple.dim_HYBPreSel[ridx] = -1
                ntuple.dim_HYBSel[ridx] = -1

                dsamu1 = entry.all_dsa_muons[dim.mu1.idx]
                dsamu2 = entry.all_dsa_muons[dim.mu2.idx]

                ntuple._REP[ridx] = ntuple.dim_type[ridx] == 0
                ntuple._LXYE[ridx] = (dim.Lxy_pv / dim.LxySig_pv) < 99.0
                ntuple._MASS[ridx] = dim.mass > 10.0
                ntuple._CHI2[ridx] = dim.normChi2 < 10.0
                ntuple._COSA[ridx] = (constants.YEAR == 2016 and dim.cosAlpha > -0.8 and dim.cosAlphaOriginal > -0.8) \
                                     or (constants.YEAR in [2018,
                                                            2022] and dim.cosAlpha > -0.9 and dim.cosAlphaOriginal > -0.9)
                ntuple._LXYS[ridx] = dim.LxySig_pv > 6.0
                ntuple._DCA[ridx] = dim.DCA < 15.0
                ntuple._OS[ridx] = dim.mu1.charge != dim.mu2.charge
                ntuple._DPHI[ridx] = dim.deltaPhi < (0.25 * math.pi)
                ntuple._DENDT[ridx] = int(((dsamu1.nDTHits > 24 or dsamu1.nCSCHits > 0) and (
                        dsamu2.nDTHits > 24 or dsamu2.nCSCHits > 0)) or abs(dsamu1.eta - dsamu2.eta) > 0.10)
                ntuple._DESEG[ridx] = int(not ((abs(dsamu1.eta - dsamu2.eta) < 0.10) and (
                        dsamu1.nDTStations + dsamu2.nDTStations + dsamu1.nCSCStations + dsamu2.nCSCStations) <= 5))
                ntuple._SEG[ridx] = int(
                    (dsamu1.nDTStations + dsamu2.nDTStations + dsamu1.nCSCStations + dsamu2.nCSCStations) >= 5)
                ntuple._DDSAT[ridx] = abs(dsamu1.timeAtIP_InOut) < 12.0 and abs(dsamu2.timeAtIP_InOut) < 12.0
                ntuple._DIR[ridx] = dsamu1.direction == 1 and dsamu2.direction == 1
                ntuple._BBTD[ridx] = int(
                    ntuple.dim_BBDSA_pt[ridx] < -998 or ntuple.dim_BBDSA_minabstimediff[ridx] <= 20.0)

                DSAPreSelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "DENDT", "DESEG", "SEG",
                                   "DDSAT", "DIR", "BBTD"]
                DSASelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "DENDT", "DESEG", "SEG", "DDSAT",
                                "DIR", "BBTD", "OS", "DPHI"]
                ntuple.dim_DSAPreSel[ridx] = int(
                    all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in DSAPreSelection]))
                ntuple.dim_DSASel[ridx] = int(all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in DSASelection]))

            # Evaluate PAT-PAT dimuon selection criteria
            elif dim.composition == "PAT":
                ntuple.dim_DSAPreSel[ridx] = -1
                ntuple.dim_DSASel[ridx] = -1
                ntuple.dim_HYBPreSel[ridx] = -1
                ntuple.dim_HYBSel[ridx] = -1

                patmu1 = entry.all_pat_muons[dim.mu1.idx]
                patmu2 = entry.all_pat_muons[dim.mu2.idx]

                ntuple._REP[ridx] = ntuple.dim_type[ridx] == 23
                ntuple._LXYE[ridx] = (dim.Lxy_pv / dim.LxySig_pv) < 99.0
                ntuple._MASS[ridx] = dim.mass > 10.0
                ntuple._CHI2[ridx] = dim.normChi2 < 10.0
                ntuple._COSA[ridx] = (constants.YEAR == 2016 and dim.cosAlpha > -0.8 and dim.cosAlphaOriginal > -0.8) \
                                     or (constants.YEAR in [2018,
                                                            2022] and dim.cosAlpha > -0.99 and dim.cosAlphaOriginal > -0.99)
                ntuple._LXYS[ridx] = dim.LxySig_pv > 6.0
                ntuple._DCA[ridx] = dim.DCA < 15.0
                ntuple._OS[ridx] = dim.mu1.charge != dim.mu2.charge
                ntuple._DPHI[ridx] = dim.deltaPhi < (0.25 * math.pi)
                ntuple._D0SIG[ridx] = patmu1.d0sig_pv > 6.0 and patmu2.d0sig_pv > 6.0
                ntuple._ISO[ridx] = (dim.mu1.clnd_tkiso0p3_sumPt_dz0p5_dxy0p2 / patmu1.pt) < 0.075 and (
                        dim.mu2.clnd_tkiso0p3_sumPt_dz0p5_dxy0p2 / patmu2.pt) < 0.075
                ntuple._PXL[ridx] = abs(patmu1.nPixelHits - patmu2.nPixelHits) < 3
                ntuple._MAXPT[ridx] = max(patmu1.pt, patmu2.pt) > 25
                ntuple._HB4V[ridx] = dim.mu1.hitsBeforeVtx < 3 and dim.mu2.hitsBeforeVtx < 3
                ntuple._NLLXY[ridx] = (min(patmu1.nTrackerLayers, patmu2.nTrackerLayers) + math.floor(
                    dim.Lxy_pv / 15.0)) > 5.0

                PATPreSelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "D0SIG", "ISO", "PXL", "MAXPT",
                                   "HB4V", "NLLXY"]
                PATSelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "D0SIG", "ISO", "PXL", "MAXPT",
                                "HB4V", "NLLXY", "OS", "DPHI"]
                ntuple.dim_PATPreSel[ridx] = int(
                    all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in PATPreSelection]))
                ntuple.dim_PATSel[ridx] = int(all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in PATSelection]))

            # Evaluate PAT-DSA dimuon selection criteria
            elif dim.composition == "HYBRID":
                ntuple.dim_DSAPreSel[ridx] = -1
                ntuple.dim_DSASel[ridx] = -1
                ntuple.dim_PATPreSel[ridx] = -1
                ntuple.dim_PATSel[ridx] = -1

                dsamu = entry.all_dsa_muons[dim.mu1.idx]
                patmu = entry.all_pat_muons[dim.mu2.idx]

                ntuple._REP[ridx] = ntuple.dim_type[ridx] == 12
                ntuple._LXYE[ridx] = (dim.Lxy_pv / dim.LxySig_pv) < 99.0
                ntuple._MASS[ridx] = dim.mass > 10.0
                ntuple._CHI2[ridx] = dim.normChi2 < 20.0
                ntuple._COSA[ridx] = (constants.YEAR == 2016 and dim.cosAlpha > -0.8 and dim.cosAlphaOriginal > -0.8) \
                                     or (constants.YEAR in [2018,
                                                            2022] and dim.cosAlpha > -0.9 and dim.cosAlphaOriginal > -0.9)
                ntuple._LXYS[ridx] = dim.LxySig_pv > 3.0
                ntuple._DCA[ridx] = dim.DCA < 15.0
                ntuple._OS[ridx] = dim.mu1.charge != dim.mu2.charge
                ntuple._DPHI[ridx] = dim.deltaPhi < (0.25 * math.pi)
                ntuple._D0SIG[ridx] = patmu.d0sig_pv > 6.0
                ntuple._ISO[ridx] = (dim.mu2.clnd_tkiso0p3_sumPt_dz0p5_dxy0p2 / patmu.pt) < 0.075
                ntuple._HB4V[ridx] = dim.mu2.hitsBeforeVtx < 6
                ntuple._NLLXY[ridx] = (patmu.nTrackerLayers + math.floor(dim.Lxy_pv / 15.0)) > 5.0
                ntuple._GLOB[ridx] = patmu.isGlobal == 1
                ntuple._DPML[ridx] = calc_deltaphi(float(patmu.phi), float(
                    calc_phi(dim.x, dim.y, dim.z, vertex.x, vertex.y, vertex.z))) < 2.9
                ntuple._HFPTE[
                    ridx] = dsamu.ptError / dsamu.pt < 1.0 and dim.mu1.ptError / dim.mu1.pt < 1.0 and patmu.ptError / patmu.pt < 1.0 and dim.mu2.ptError / dim.mu2.pt < 1.0
                ntuple._HDSAT[ridx] = abs(dsamu.timeAtIP_InOut) < 12.0

                HYBPreSelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "D0SIG", "ISO", "GLOB", "HB4V",
                                   "DPML", "HFPTE", "HDSAT", "NLLXY"]
                HYBSelection = ["REP", "LXYE", "MASS", "CHI2", "COSA", "LXYS", "DCA", "D0SIG", "ISO", "GLOB", "HB4V",
                                "DPML", "HFPTE", "HDSAT", "NLLXY", "OS", "DPHI"]
                ntuple.dim_HYBPreSel[ridx] = int(
                    all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in HYBPreSelection]))
                ntuple.dim_HYBSel[ridx] = int(all([bool(getattr(ntuple, "_" + cut)[ridx]) for cut in HYBSelection]))
