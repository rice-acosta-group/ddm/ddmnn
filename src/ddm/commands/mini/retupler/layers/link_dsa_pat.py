from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.utils.dimuon_isolation import append_dimuon_isolation
from ddm.mini.utils.dsa_replacement import replace_dsa_muons
from ddm.mini.utils.pat_isolation import append_pat_isolation


class LinkDSAPAT(Layer):

    def apply(self, sample, entry, ntuple, options):
        temp_dsa_dimuons = []
        temp_dsa_dimuons_type = []
        temp_dsa_dimuons_idx = []
        temp_dsa_dimuons_idxtohyb1 = []
        temp_dsa_dimuons_idxtohyb2 = []
        temp_dsa_dimuons_mu1_linkidx, temp_dsa_dimuons_mu2_linkidx = ([] for _ in range(2))
        temp_replacing_dimuons = []
        temp_replacing_dimuons_type = []
        temp_replacing_dimuons_idx = []
        temp_replacing_dimuons_idxtohyb1 = []
        temp_replacing_dimuons_idxtohyb2 = []
        temp_replacing_dimuons_mu1_linkidx, temp_replacing_dimuons_mu2_linkidx = ([] for _ in range(2))

        for dsa_dimuon_ridx, dsa_dimuon in enumerate(entry.sel_dimuons):
            # Make sure all dimuons are DSA-DSA implying no replacement has occurred
            assert dsa_dimuon.composition == 'DSA', "Non-zero number of non-DSA dimuons although no replacement happened"

            # get the composite DSA muons of the DSAdimuon
            comp_dsa_muons = [
                entry.ext_dsa_muons[dsa_dimuon.mu1.idx],
                entry.ext_dsa_muons[dsa_dimuon.mu2.idx]
            ]

            # do the PAT replacement procedure
            # (DSA muons in comp_dsa_muons will get their replacement_status updated in this process)
            non_replaced_dsa_muons, replacing_pat_muons, __ = replace_dsa_muons(
                comp_dsa_muons, entry.ext_pat_muons, [dsa_dimuon],
                nn_en=options.DSAPATLINKNN
            )

            # propagate the replacement_status to the selected dsa muons used outside the DSAPATLINK block
            for mu_with_rep_flag in comp_dsa_muons:
                mu = entry.all_dsa_muons[mu_with_rep_flag.idx]
                mu.replacement_status = mu_with_rep_flag.replacement_status

            # tag dimuons according to their replacement status:
            # dim_type == 0:  DSA dimuons with none of the muons replaced
            # dim_type == 1:  DSA dimuons with one muon replaced, but no HYB dimuon
            #                 found
            # dim_type == 11: DSA dimuons with one muon replaced and with the
            #                 corresponding HYB dimuon stored
            # dim_type == 2:  DSA dimuons with both muons replaced, but no PAT dimuon
            #                 found
            # dim_type == 3:  DSA dimuons with both muons replaced and with the
            #                 corresponding PAT dimuon stored
            # dim_type == 12: HYB dimuons which correspond to replaced DSA dimuons
            # dim_type == 23: PAT dimuons which correspond to replaced DSA dimuons
            #
            # types for linking PAT-PAT dimuons with the corresponding hybrids:
            # dim_type == 13: HYB dimuons which correspond to replacing PAT dimuons, with the first PAT muon replacement inverted
            # dim_type == 14: HYB dimuons which correspond to replacing PAT dimuons, with the second PAT muon replacement inverted

            if len(replacing_pat_muons) == 0:
                temp_dsa_dimuons.append(dsa_dimuon)
                temp_dsa_dimuons_type.append(0)
                temp_dsa_dimuons_idx.append(999)
                temp_dsa_dimuons_idxtohyb1.append(999)
                temp_dsa_dimuons_idxtohyb2.append(999)
                temp_dsa_dimuons_mu1_linkidx.append(-1)
                temp_dsa_dimuons_mu2_linkidx.append(-1)
            elif len(non_replaced_dsa_muons) == len(replacing_pat_muons) == 1:
                replacing_hyb_dimuon = None
                non_replaced_dsa_muon = non_replaced_dsa_muons[0]
                replacing_pat_muon = replacing_pat_muons[0]

                for hyb_dimuon in entry.ext_dimuons:
                    # Short-Circuit: Not a hybrid dimuon
                    if hyb_dimuon.composition != 'HYBRID':
                        continue

                    # Short-Circuit: Must match first muon
                    if hyb_dimuon.mu1.idx != non_replaced_dsa_muon.idx:
                        continue

                    # Short-Circuit: Must match second muon
                    if hyb_dimuon.mu2.idx != replacing_pat_muon.idx:
                        continue

                    # Found a match
                    replacing_hyb_dimuon = hyb_dimuon
                    break

                replacing_hyb_dimuon_idx = len(entry.sel_dimuons) + len(temp_replacing_dimuons)

                temp_dsa_dimuons.append(dsa_dimuon)
                temp_dsa_dimuons_type.append(1 if replacing_hyb_dimuon is None else 11)
                temp_dsa_dimuons_idx.append(replacing_hyb_dimuon_idx)
                temp_dsa_dimuons_idxtohyb1.append(999)
                temp_dsa_dimuons_idxtohyb2.append(999)

                if replacing_hyb_dimuon is None:
                    temp_dsa_dimuons_mu1_linkidx.append(-1)
                    temp_dsa_dimuons_mu2_linkidx.append(-1)
                elif non_replaced_dsa_muon.idx == dsa_dimuon.mu1.idx:
                    temp_dsa_dimuons_mu1_linkidx.append(-1)
                    setattr(dsa_dimuon.mu1, "assocPATidx", -1)

                    temp_dsa_dimuons_mu2_linkidx.append(replacing_pat_muon.idx + 1000)
                    setattr(dsa_dimuon.mu2, "assocPATidx", replacing_pat_muon.idx + 1000)
                elif non_replaced_dsa_muon.idx == dsa_dimuon.mu2.idx:
                    temp_dsa_dimuons_mu1_linkidx.append(replacing_pat_muon.idx + 1000)
                    setattr(dsa_dimuon.mu1, "assocPATidx", replacing_pat_muon.idx + 1000)

                    temp_dsa_dimuons_mu2_linkidx.append(-1)
                    setattr(dsa_dimuon.mu2, "assocPATidx", -1)

                # also store the replacing HYB dimuon
                if replacing_hyb_dimuon is not None:
                    temp_replacing_dimuons.append(replacing_hyb_dimuon)
                    temp_replacing_dimuons_type.append(12)
                    temp_replacing_dimuons_idx.append(dsa_dimuon_ridx)  # links back to type-11 DSA dimuon
                    temp_replacing_dimuons_idxtohyb1.append(999)
                    temp_replacing_dimuons_idxtohyb2.append(999)
                    temp_replacing_dimuons_mu1_linkidx.append(-1)
                    temp_replacing_dimuons_mu2_linkidx.append(replacing_pat_muon.replaced_dsa_muon.idx)

            elif len(replacing_pat_muons) == 2:
                replacing_pat_dimuon = None
                replacing_pat_muon1 = replacing_pat_muons[0]
                replacing_pat_muon2 = replacing_pat_muons[1]

                for pat_dimuon in entry.ext_dimuons:
                    # Short-Circuit: Not a pat dimuon
                    if pat_dimuon.composition != 'PAT':
                        continue

                    # Match muons
                    if (pat_dimuon.mu1.idx == replacing_pat_muon1.idx) and (pat_dimuon.mu2.idx == replacing_pat_muon2.idx):
                        replacing_pat_dimuon = pat_dimuon
                        break
                    elif (pat_dimuon.mu1.idx == replacing_pat_muon2.idx) and (pat_dimuon.mu2.idx == replacing_pat_muon1.idx):
                        replacing_pat_dimuon = pat_dimuon
                        replacing_pat_muon1 = replacing_pat_muons[1]
                        replacing_pat_muon2 = replacing_pat_muons[0]
                        break

                replacing_pat_dimuon_idx = len(entry.sel_dimuons) + len(temp_replacing_dimuons)

                temp_dsa_dimuons.append(dsa_dimuon)
                temp_dsa_dimuons_type.append(2 if replacing_pat_dimuon is None else 3)
                temp_dsa_dimuons_idx.append(replacing_pat_dimuon_idx)
                temp_dsa_dimuons_idxtohyb1.append(999)
                temp_dsa_dimuons_idxtohyb2.append(999)

                if replacing_pat_dimuon is None:
                    temp_dsa_dimuons_mu1_linkidx.append(-1)
                    temp_dsa_dimuons_mu2_linkidx.append(-1)
                elif (replacing_pat_muon1.replaced_dsa_muon.idx == dsa_dimuon.mu1.idx):
                    temp_dsa_dimuons_mu1_linkidx.append(replacing_pat_muon1.idx + 1000)
                    setattr(dsa_dimuon.mu1, "assocPATidx", replacing_pat_muon1.idx + 1000)

                    temp_dsa_dimuons_mu2_linkidx.append(replacing_pat_muon2.idx + 1000)
                    setattr(dsa_dimuon.mu2, "assocPATidx", replacing_pat_muon2.idx + 1000)
                elif (replacing_pat_muon1.replaced_dsa_muon.idx == dsa_dimuon.mu2.idx):
                    temp_dsa_dimuons_mu1_linkidx.append(replacing_pat_muon2.idx + 1000)
                    setattr(dsa_dimuon.mu1, "assocPATidx", replacing_pat_muon2.idx + 1000)

                    temp_dsa_dimuons_mu2_linkidx.append(replacing_pat_muon1.idx + 1000)
                    setattr(dsa_dimuon.mu2, "assocPATidx", replacing_pat_muon1.idx + 1000)

                # also store the replacing PAT dimuon
                if replacing_pat_dimuon is not None:
                    temp_replacing_dimuons.append(replacing_pat_dimuon)
                    temp_replacing_dimuons_type.append(23)
                    temp_replacing_dimuons_idx.append(dsa_dimuon_ridx)  # links back to type-3 DSA dimuon
                    temp_replacing_dimuons_mu1_linkidx.append(replacing_pat_muon1.replaced_dsa_muon.idx)
                    temp_replacing_dimuons_mu2_linkidx.append(replacing_pat_muon2.replaced_dsa_muon.idx)

                    # also store the hybrid dimuons obtained by inverting replacement of one PAT muon
                    found_hyb_dimuon1 = False
                    found_hyb_dimuon2 = False
                    associated_hyb_dimuon1 = None
                    associated_hyb_dimuon2 = None

                    for hyb_dimuon in entry.ext_dimuons:
                        # Short-Circuit: Not a hybrid dimuon
                        if hyb_dimuon.composition != 'HYBRID':
                            continue

                        # Match hybrids
                        if (not found_hyb_dimuon1) and (hyb_dimuon.mu1.idx == replacing_pat_muon1.replaced_dsa_muon.idx and hyb_dimuon.mu2.idx == replacing_pat_muon2.idx):
                            found_hyb_dimuon1 = True
                            associated_hyb_dimuon1 = hyb_dimuon
                        elif (not found_hyb_dimuon2) and (hyb_dimuon.mu1.idx == replacing_pat_muon2.replaced_dsa_muon.idx and hyb_dimuon.mu2.idx == replacing_pat_muon1.idx):
                            found_hyb_dimuon2 = True
                            associated_hyb_dimuon2 = hyb_dimuon

                        # Short-Circuit: Both found
                        if found_hyb_dimuon1 and found_hyb_dimuon2:
                            break

                    if found_hyb_dimuon1:
                        temp_replacing_dimuons.append(associated_hyb_dimuon1)
                        temp_replacing_dimuons_type.append(13)
                        temp_replacing_dimuons_idx.append(replacing_pat_dimuon_idx)  # link to the PAT dimuon
                        temp_replacing_dimuons_idxtohyb1.append(len(entry.sel_dimuons) + len(temp_replacing_dimuons) - 1)  # link from the PAT to this hybrid dimuon
                        temp_replacing_dimuons_mu1_linkidx.append(replacing_pat_muon1.idx + 1000)
                        temp_replacing_dimuons_mu2_linkidx.append(replacing_pat_muon2.replaced_dsa_muon.idx)

                    temp_replacing_dimuons_idxtohyb1.append(999)  # in case associatedHYBdimuon1 is None, this is default value from PAT to hybrid, otherwise, this is default from associatedHYBdimuon1 to PAT

                    if found_hyb_dimuon2:
                        temp_replacing_dimuons.append(associated_hyb_dimuon2)
                        temp_replacing_dimuons_type.append(14)
                        temp_replacing_dimuons_idx.append(replacing_pat_dimuon_idx)  # link to the PAT dimuon
                        temp_replacing_dimuons_idxtohyb1.append(999)
                        temp_replacing_dimuons_idxtohyb2.append(len(entry.sel_dimuons) + len(temp_replacing_dimuons) - 1)  # link from the PAT to this hybrid dimuon
                        temp_replacing_dimuons_mu1_linkidx.append(replacing_pat_muon2.idx + 1000)
                        temp_replacing_dimuons_mu2_linkidx.append(replacing_pat_muon1.replaced_dsa_muon.idx)

                    temp_replacing_dimuons_idxtohyb2.append(999)  # in case associatedHYBdimuon2 is None, this is default value from PAT to hybrid, otherwise, this is default from associatedHYBdimuon2 to PAT

                    if associated_hyb_dimuon1 is not None:
                        temp_replacing_dimuons_idxtohyb2.append(999)

        # Append dimuon isolation
        append_dimuon_isolation(temp_replacing_dimuons, entry.ext_pat_muons)

        # Append dimuons to selection
        entry.sel_dimuons = temp_dsa_dimuons + temp_replacing_dimuons
        ntuple.new_dim_type = temp_dsa_dimuons_type + temp_replacing_dimuons_type
        ntuple.new_dim_idx = temp_dsa_dimuons_idx + temp_replacing_dimuons_idx
        ntuple.new_dim_idxtohyb1 = temp_dsa_dimuons_idxtohyb1 + temp_replacing_dimuons_idxtohyb1
        ntuple.new_dim_idxtohyb2 = temp_dsa_dimuons_idxtohyb2 + temp_replacing_dimuons_idxtohyb2
        ntuple.new_dim_mu1_linkidx = temp_dsa_dimuons_mu1_linkidx + temp_replacing_dimuons_mu1_linkidx
        ntuple.new_dim_mu2_linkidx = temp_dsa_dimuons_mu2_linkidx + temp_replacing_dimuons_mu2_linkidx

        # Append PAT muon isolation
        sel_pat_muons = list()
        sel_pat_muon_idx = set()

        for replacing_dimuon in temp_replacing_dimuons:
            for ref_muon in (replacing_dimuon.mu1, replacing_dimuon.mu2):
                # Short-Circuit: Only pat muons
                if ref_muon.kind != 'PAT':
                    continue

                # Short-Circuit: Already used
                if ref_muon.idx in sel_pat_muon_idx:
                    continue

                # Append PAT muon
                sel_pat_muon = entry.ext_pat_muons[ref_muon.idx]
                sel_pat_muons.append(sel_pat_muon)
                sel_pat_muon_idx.add(ref_muon.idx)

        # Append pat isolation
        append_pat_isolation(sel_pat_muons)

        # Append pat muons to selection
        entry.sel_pat_muons = sel_pat_muons

        # Cross-Check
        if len(entry.sel_dimuons) != len(ntuple.new_dim_type) != len(ntuple.new_dim_idx) != len(ntuple.new_dim_mu1_linkidx) != len(ntuple.new_dim_mu2_linkidx):
            raise Exception("Inconsistent sizes: len(Dimuons3) = {}, len(new_dim_type) = {}, len(new_dim_idx) = {}, len(new_dim_idxtohyb1) = {}, len(new_dim_idxtohyb2) = {}, len(new_dim_mu1_linkidx) = {}, len(new_dim_mu2_linkidx) = {}".format(
                len(entry.sel_dimuons), len(ntuple.new_dim_type), len(ntuple.new_dim_idx),
                len(ntuple.new_dim_idxtohyb1), len(ntuple.new_dim_idxtohyb2),
                len(ntuple.new_dim_mu1_linkidx), len(ntuple.new_dim_mu2_linkidx)
            ))
