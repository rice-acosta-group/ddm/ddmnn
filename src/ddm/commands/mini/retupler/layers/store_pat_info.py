from ddm.commands.mini.retupler.layers.layer import Layer
from ddm.mini.commons import constants
from ddm.mini.commons.isolation import yield_iso_thresholds


class StorePATInfo(Layer):

    def apply(self, sample, entry, ntuple, options):
        for ridx, muon in enumerate(entry.sel_pat_muons):
            # Reset HLT links?
            ntuple.patmu_hltdim_pp_idx[ridx] = 100
            ntuple.patmu_hltdim_cosmic_idx[ridx] = 100
            ntuple.patmu_hltmu_pp_idx[ridx] = 100
            ntuple.patmu_hltmu_cosmic_idx[ridx] = 100

            for path in constants.SIGNAL_HLT_PATH[constants.YEAR]:
                getattr(ntuple, f'patmu_hltdim_{path}_idx')[ridx] = 100
                getattr(ntuple, f'patmu_hltmu_{path}_idx')[ridx] = 100

            # Save new data
            ntuple.patmu_pt[ridx] = muon.pt
            ntuple.patmu_cleanedIso[ridx] = muon.cleanedIso

            # -- custom settings cleaned isolation
            if 'patmu_tkiso0p3_sumPt_dz0p2_dxy0p1' in ntuple.ListOfPATBranches:
                for dR, dz, d0 in yield_iso_thresholds():
                    cleaned_trk_branch_name = 'clnd_tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
                        int(10 * dR), int(10 * dz), int(10 * d0)
                    )

                    branch = getattr(ntuple, f'patmu_{cleaned_trk_branch_name}')
                    value = getattr(muon, cleaned_trk_branch_name)
                    branch[ridx] = value
