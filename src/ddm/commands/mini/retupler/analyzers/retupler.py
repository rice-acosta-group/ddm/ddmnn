import os
import uuid

from ROOT import TFile
from ROOT import TFileMerger

from ddm.commands.mini.retupler.layers.append_dsa_isolation import AppendDSAIsolation
from ddm.commands.mini.retupler.layers.filter_events_by_lxysig import FilterEventsByLxySignificance
from ddm.commands.mini.retupler.layers.layer import Graph
from ddm.commands.mini.retupler.layers.link_dsa_pat import LinkDSAPAT
from ddm.commands.mini.retupler.layers.link_gen_dimuons import LinkGENDimuons
from ddm.commands.mini.retupler.layers.link_gen_muons import LinkGENMuons
from ddm.commands.mini.retupler.layers.resize_branches import ResizeNewBranches
from ddm.commands.mini.retupler.layers.restructure_branches import RestructureBranches
from ddm.commands.mini.retupler.layers.sort_reco_by_idx import SortRecoByIndex
from ddm.commands.mini.retupler.layers.store_dimuon_bbdsa import StoreDimuonBBDSA
from ddm.commands.mini.retupler.layers.store_dimuon_count import StoreDimuonCount
from ddm.commands.mini.retupler.layers.store_dimuon_dsapatlink import StoreDimuonDSAPATLink
from ddm.commands.mini.retupler.layers.store_dimuon_info import StoreDimuonInfo
from ddm.commands.mini.retupler.layers.store_dimuon_isolation import StoreDimuonIsolation
from ddm.commands.mini.retupler.layers.store_dimuon_selection_criteria import StoreDimuonSelectionCriteria
from ddm.commands.mini.retupler.layers.store_dimuon_sf import StoreDimuonScaleFactors
from ddm.commands.mini.retupler.layers.store_dsa_info import StoreDSAInfo
from ddm.commands.mini.retupler.layers.store_dsa_sf import StoreDSAScaleFactors
from ddm.commands.mini.retupler.layers.store_hlt_dsa_match import StoreHLTDSAMatch
from ddm.commands.mini.retupler.layers.store_hlt_pat_match import StoreHLTPATMatch
from ddm.commands.mini.retupler.layers.store_l1t_content import StoreL1TContent
from ddm.commands.mini.retupler.layers.store_l1t_selection import StoreL1TSelection
from ddm.commands.mini.retupler.layers.store_parallel_pairs import StoreParallelPairs
from ddm.commands.mini.retupler.layers.store_pat_info import StorePATInfo
from ddm.commands.mini.retupler.layers.store_pat_sf import StorePATScaleFactors
from ddm.commands.mini.retupler.layers.store_pu_weights import StorePileupWeights
from ddm.commands.mini.retupler.slim import SlimNtuple
from ddm.commands.mini.retupler.utils import has_any_gen_branch
from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.execution.runtime import get_logger


class ReTupler(AbstractAnalyzer):

    def __init__(self, output_file, partial_dir, sample, options, lxysigcut, skip_merge_en):
        super().__init__()

        # Arguments
        self.output_file = output_file
        self.partial_dir = partial_dir
        self.sample = sample
        self.options = options
        self.skip_merge_en = skip_merge_en

        # Output
        self.output_id_col = list()
        self.output_id = None
        self.ntuple = None

        # Graph
        self.graph = self.build_graph(lxysigcut)

    def build_graph(self, lxysigcut):
        # Init graph
        graph = Graph()

        # Append layers
        graph.append(StoreParallelPairs())

        if self.options.DSAPATLINK or self.options.DSAPATLINKNN:
            graph.append(LinkDSAPAT())

        graph.append(AppendDSAIsolation())
        graph.append(FilterEventsByLxySignificance(lxysigcut))
        graph.append(SortRecoByIndex())
        graph.append(ResizeNewBranches())
        graph.append(StoreHLTDSAMatch())
        graph.append(StoreL1TContent())
        graph.append(StoreL1TSelection())
        graph.append(LinkGENDimuons())
        graph.append(LinkGENMuons())
        graph.append(StoreDSAInfo())
        graph.append(StorePileupWeights())
        graph.append(StoreDSAScaleFactors())
        graph.append(StorePATScaleFactors())
        graph.append(StorePATInfo())
        graph.append(StoreHLTPATMatch())
        graph.append(StoreDimuonInfo())
        graph.append(StoreDimuonDSAPATLink())
        graph.append(StoreDimuonIsolation())
        graph.append(StoreDimuonBBDSA())
        graph.append(StoreDimuonSelectionCriteria())
        graph.append(StoreDimuonScaleFactors())
        graph.append(StoreDimuonCount())
        graph.append(RestructureBranches())

        # Return
        return graph

    def before_run(self, entry):
        # Forces the entry to read all the enabled branches
        entry._sparse_read_en = False

        # Create output
        self.output_id = str(uuid.uuid4()).replace('-', '')
        self.sample.extra.is_mc = has_any_gen_branch(entry._tree)
        self.ntuple = SlimNtuple(
            self.sample, entry,
            '%s/%s.root' % (self.partial_dir, self.output_id)
        )

    def process_entry(self, entry):
        # Short-Circuit: No layers
        if self.graph is None:
            raise Exception('No layers have been added.')

        # Short-Circuit: No dimuons
        if len(entry.sel_dimuons) == 0:
            return

        # Run graph
        is_valid = self.graph.apply(self.sample, entry, self.ntuple, self.options)

        # Only record events with dimuons
        if is_valid:
            self.ntuple.fill()
        else:
            self.ntuple.clear()

    def after_run(self):
        # Write output
        self.ntuple.write()

        # Clear variables that can't be pickled properly
        self.sample = None
        self.ntuple = None
        self.graph = None

    def merge(self, other):
        self.output_id_col.append(other.output_id)

    def post_production(self):
        # Short-Circuit: Skip merge
        if self.skip_merge_en:
            return

        # Constants
        max_group_size = 25 * 1024 * 1024 * 1024  # 25 GB

        # Create groups
        groups = list()
        current_group = list()
        current_group_size = 0  # bytes

        for output_id in self.output_id_col:
            filepath = '{}/{}.root'.format(self.partial_dir, output_id)

            # Open file
            try:
                root_file = TFile.Open(filepath, 'READ')
            except Exception as err:
                get_logger().error(f"Failed to open file {filepath}: {err}")
                continue

            # Short-Circuit: Corrupt file
            if not (root_file and not root_file.IsZombie()):
                get_logger().warn(f"Skipping corrupt or inaccessible file: {filepath}")
                continue

            # Close file
            root_file.Close()

            # Check size
            filesize = os.path.getsize(filepath)
            next_size = current_group_size + filesize

            if next_size > max_group_size:
                next_size = filesize
                current_group = list()
                groups.append(current_group)

            current_group.append(filepath)
            current_group_size = next_size

        if len(current_group) > 0:
            groups.append(current_group)

        # Merge partial outputs
        ext_idx = self.output_file.rfind('.')
        output_file_base = self.output_file[:ext_idx]
        output_file_ext = self.output_file[ext_idx + 1:]

        for group_id, group in enumerate(groups):
            output_filepath = '{}_{}.{}'.format(output_file_base, group_id, output_file_ext)

            file_merger = TFileMerger()
            file_merger.OutputFile(output_filepath, "RECREATE")

            for filepath in group:
                file_merger.AddFile(filepath)

            merge_ok = file_merger.Merge()

            # Short-Circuit: Merge failed
            if not merge_ok:
                continue

            # Delete merged outputs
            for filepath in group:
                os.remove(filepath)
