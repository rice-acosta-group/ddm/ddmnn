import os

from ddm.commands.mini.retupler.analyzers.retupler import ReTupler
from ddm.commands.mini.retupler.sample import Sample
from ddm.mini.analysis.mini_analysis import MiniAnalysis
from ddm.mini.commons.user_options import UserOptions
from ddm.mini.producers.DPANNBranchProducer import DPANNBranchProducer
from ddm.mini.producers.PairingNNBranchProducer import PairingNNBranchProducer
from ddm.mini.selection.cutflow import CutflowAnalyzer
from ddm.mini.selection.selector import Selector


class RunRetupler(MiniAnalysis):

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='Fraction of events to process')

        parser.add_argument('--dpa-protobuf', dest='dpa_protobuf', metavar='DPA_PROTOBUF',
                            type=str, default=None,
                            help='DPA protobuf')

        parser.add_argument('--pairing-protobuf', dest='pairing_protobuf', metavar='PAIRING_PROTOBUF',
                            type=str, default=None,
                            help='Pairing protobuf')

        parser.add_argument('--output', dest='output_file', metavar='OUTPUT_FILE',
                            type=str, default=None,
                            help='Output File')

        parser.add_argument('--sample-name', dest='sample_name', metavar='SAMPLE_NAME',
                            type=str, default=None,
                            help='Sample Name')

        parser.add_argument('--signal-point', dest='signal_point', metavar='SIGNAL_POINT',
                            type=str, default=None,
                            help='Signal Point')

        parser.add_argument('--opt', dest='opt_string', metavar='OPT_STRING',
                            type=str, default='_VTX_NPP_NS_NH_HLT_REP_FPTE_PT_TRK_NDT_TSTAT_ISTM_DCA_PC_NLAY',
                            help='Option String')

        parser.add_argument('--lxysigcut', dest='lxysigcut', metavar='Lxy significance cut',
                            type=float, default=6.0,
                            help='Apply manual min. Lxy significance cut on dimuons (default: 6.0)')

        parser.add_argument('--skip-merge', dest='skip_merge_en',
                            action='store_true', default=False,
                            help='Skips the merge step')

    def configure(self, args):
        # Unpack Args
        self.dpa_protobuf = args.dpa_protobuf
        self.pairing_protobuf = args.pairing_protobuf

        self.output_file = args.output_file
        self.partial_dir = self.output_file + '.parts'
        self.sample = Sample(args.sample_name, args.signal_point)
        self.options = UserOptions(args.opt_string)
        self.lxysigcut = args.lxysigcut
        self.skip_merge_en = args.skip_merge_en

        # Create directory for partial outputs
        os.makedirs(self.partial_dir, exist_ok=True)

    def create_producers(self):
        producers = [Selector(self.options)]

        if self.dpa_protobuf is not None:
            producers.append(DPANNBranchProducer(self.dpa_protobuf))

        if self.pairing_protobuf is not None:
            producers.append(PairingNNBranchProducer(self.pairing_protobuf))

        return producers

    def create_analyzers(self):
        return [
            CutflowAnalyzer('sel_cutflow'),
            ReTupler(
                self.output_file, self.partial_dir, self.sample,
                self.options, self.lxysigcut, self.skip_merge_en
            ),
        ]

    def consolidate(self):
        for analyzer in self.analyzers:
            analyzer.consolidate()
