learning_rate = 0.01

final_learning_rate = learning_rate * 0.01

gradient_clipnorm = 10.

warmup_epochs = 30

epochs = 300

batch_size = 4096
