import tensorflow
from keras import Layer


class GateIndexLayer(Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def call(self, inputs, mask=None):
        mu1_is_pat = inputs[:, 0]
        mu2_is_pat = inputs[:, 31]
        dim_type = mu1_is_pat + mu2_is_pat
        dim_type = tensorflow.cast(dim_type, dtype=tensorflow.int64)
        dim_type = tensorflow.expand_dims(dim_type, axis=1)
        return dim_type

    def compute_output_shape(self, input_shape):
        return tensorflow.TensorShape([input_shape[0], 1])

    def get_config(self):
        config = super().get_config()
        return config


class GateSelectionLayer(Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def call(self, inputs, mask=None):
        gate_idx = inputs[:, 0]

        # Offset gates by 1
        gate_idx += 1

        # Gather the outputs based on the selected index.
        batch_size = tensorflow.shape(inputs)[0]
        indices = tensorflow.stack([tensorflow.range(batch_size), tensorflow.cast(gate_idx, tensorflow.int32)], axis=1)

        # Combine the outputs using a conditional selection.
        output = tensorflow.gather_nd(inputs, indices)
        output = tensorflow.expand_dims(output, axis=1)

        # Return
        return output

    def compute_output_shape(self, input_shape):
        return tensorflow.TensorShape([input_shape[0], 1])

    def get_config(self):
        config = super().get_config()
        return config
