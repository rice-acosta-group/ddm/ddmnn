import tensorflow as tf
from keras import Layer


class PairingActivation(Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def call(self, inputs, mask=None):
        # Calculate Activation
        out_rels = tf.keras.activations.sigmoid(inputs)

        # Return
        return out_rels

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super().get_config()

        return config
