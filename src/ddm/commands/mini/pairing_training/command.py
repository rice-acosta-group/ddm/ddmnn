import numpy as np
from ROOT import TCanvas
from ROOT import TH1D
from ROOT import kBlue
from keras.src.callbacks import ModelCheckpoint
from keras.src.layers import BatchNormalization

from ddm.commands.mini.pairing_training import constants
from ddm.commands.mini.pairing_training.analyzers.general import GeneralAnalyzer
from ddm.commands.mini.pairing_training.analyzers.roc import ROCAnalyzer
from ddm.commands.mini.pairing_training.custom.activation import PairingActivation
from ddm.commands.mini.pairing_training.custom.gating import GateIndexLayer, GateSelectionLayer
from ddm.commands.mini.pairing_training.datasets import Dataset
from ddm.commands.mini.pairing_training.models import create_model, create_lr_schedule, create_optimizer, pairing_loss
from ddm.core.commands import Command
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.commons.combinatorics import yield_combinations
from ddm.core.commons.histograms import get_th1_ideal_ranges
from ddm.core.commons.ml_models import load_model
from ddm.core.commons.ml_models import save_model
from ddm.core.commons.ml_trainers import ModelTrainer
from ddm.core.commons.plotting import plot_var_walls
from ddm.core.execution.runtime import get_logger
from ddm.keras.loggers import LearningRateLogger
from ddm.keras.optimizers import WarmupCosineDecay
from ddm.mini.utils.nn_pairing import pairing_var_names


class TrainPairingNN(Command):

    def configure_parser(self, parser):
        parser.add_argument(dest='model_name', metavar='MODEL_NAME', type=str, help='Model Name')
        parser.add_argument(dest='data_file', metavar='DATA_FILE', type=str, help='Data File')

        parser.add_argument('--lr', dest='lr', metavar='LEARNING_RATE',
                            type=float, default=None,
                            help='Learning Rate')

        parser.add_argument('--epochs', dest='epochs', metavar='EPOCHS',
                            type=int, default=None,
                            help='Number of epochs')

        parser.add_argument('--train', dest='train_en', action='store_const',
                            const=True, default=False,
                            help='Train')

    def run(self, args):
        # Unpack Args
        model_name = args.model_name
        data_file = args.data_file

        lr = args.lr
        epochs = args.epochs
        train_en = args.train_en

        # Update Constants
        if lr is not None:
            constants.learning_rate = lr
            constants.final_learning_rate = lr * 0.01

        if epochs is not None:
            constants.epochs = epochs

        # Load Dataset
        dataset = Dataset(data_file)

        x_train, y_train, \
            x_test, y_test = dataset.get_samples()

        # Get Training Weights
        weights = self.get_weights(np.vstack((y_train, y_test)))
        weights_train = weights[:y_train.shape[0]]
        weights_test = weights[y_train.shape[0]:]

        # Plot Variables
        plot_var_walls('train_variables', x_train, weights_train)
        plot_var_walls('train_parameters', y_train, weights_train)
        plot_var_walls('test_variables', x_test, weights_test)
        plot_var_walls('test_parameters', y_test, weights_test)

        match_mask = (y_train[:, 0] == 1)
        no_match_mask = (y_train[:, 0] == 0)
        plot_var_walls('train_match_variables', x_train[match_mask], weights_train[match_mask])
        plot_var_walls('train_match_parameters', y_train[match_mask], weights_train[match_mask])
        plot_var_walls('train_no_match_variables', x_train[no_match_mask], weights_train[no_match_mask])
        plot_var_walls('train_no_match_parameters', y_train[no_match_mask], weights_train[no_match_mask])

        match_mask = (y_test[:, 0] == 1)
        no_match_mask = (y_test[:, 0] == 0)
        plot_var_walls('test_match_variables', x_test[match_mask], weights_test[match_mask])
        plot_var_walls('test_match_parameters', y_test[match_mask], weights_test[match_mask])
        plot_var_walls('test_no_match_variables', x_test[no_match_mask], weights_test[no_match_mask])
        plot_var_walls('test_no_match_parameters', y_test[no_match_mask], weights_test[no_match_mask])

        # Train
        if train_en:
            _y_train = y_train[:, :1]
            _y_test = y_test[:, :1]

            self.train_model(
                model_name,
                x_train, _y_train, weights_train,
                x_test, _y_test, weights_test
            )

        # Analyze Performance
        self.analyze_performance(
            model_name, x_test, y_test
        )

        # Analyze Features
        self.analyze_features(
            model_name, x_test, y_test
        )

    def get_weights(self, y_arr):
        # Get masks
        entries_dsa = (y_arr[:, 8] == 1)
        entries_hyb = (y_arr[:, 8] == 2)
        entries_pat = (y_arr[:, 8] == 3)
        entries_that_match = (y_arr[:, 0] == 1)
        entries_that_dont_match = (y_arr[:, 0] == 0)

        # Calculate weights
        # The weight is such that the weighted population of each category
        # is equal to the population of the smallest category
        weights = np.ones((y_arr.shape[0],))
        min_entries = y_arr.shape[0]

        for cat_mask, match_mask in yield_combinations(
                (entries_dsa, entries_hyb, entries_pat),
                (entries_that_match, entries_that_dont_match)
        ):
            mask = cat_mask & match_mask
            n_entries = np.count_nonzero(mask)

            weights[mask] = n_entries
            min_entries = min(min_entries, n_entries)

        weights = min_entries / weights

        # Return
        return weights

    def train_model(
            self, model_name,
            x_train, y_train, weight_train,
            x_test, y_test, weight_test
    ):
        model = create_model(
            in_nodes=x_train.shape[1],
            out_nodes=y_train.shape[1])

        lr_schedule = create_lr_schedule(
            x_train.shape[0],
            epochs=constants.epochs,
            warmup_epochs=constants.warmup_epochs,
            batch_size=constants.batch_size,
            learning_rate=constants.learning_rate,
            final_learning_rate=constants.final_learning_rate)

        optimizer = create_optimizer(
            lr_schedule,
            gradient_clipnorm=constants.gradient_clipnorm)

        # Compile Model
        model.compile(
            optimizer=optimizer,
            loss=pairing_loss,
            loss_weights=1e4,
            weighted_metrics=[]
        )
        model.summary()

        # Callbacks
        lr_logger = LearningRateLogger()

        model_best_check = ModelCheckpoint(
            filepath='model_bchk.keras', monitor='val_loss', verbose=1, save_best_only=True
        )

        model_best_check_weights = ModelCheckpoint(
            filepath='model_bchk.weights.h5', monitor='val_loss', verbose=1,
            save_best_only=True, save_weights_only=True
        )

        # Train Model
        get_logger().info(
            f"Training model - "
            f"learning_rate: {constants.learning_rate} "
            f"final_learning_rate: {constants.final_learning_rate} "
            f"epochs: {constants.epochs} "
            f"batch_size: {constants.batch_size}"
        )

        history = ModelTrainer(model).fit(
            x_train, y_train,
            sample_weight=weight_train,
            epochs=constants.epochs,
            batch_size=constants.batch_size,
            callbacks=[lr_logger, model_best_check, model_best_check_weights],
            validation_data=(x_test, y_test, weight_test),
            shuffle=True, verbose=2
        )

        metrics = [len(history.history['loss']), history.history['loss'][-1], history.history['val_loss'][-1]]
        get_logger().info('Epoch {0}/{0} - loss: {1} - val_loss: {2}'.format(*metrics))

        # Save Model
        save_model(model, name=model_name)

    def analyze_performance(
            self, model_name,
            x_test, y_test
    ):
        # Load model and predict outputs
        loaded_model = self.load_static_model(model_name)

        # Make predictions and unscale
        predictions = loaded_model.predict(x_test)

        # Unpack prediction parameters
        is_match_pred = predictions[:, 0]

        # Prepare parameters
        pred_param = np.column_stack((is_match_pred,))

        # Select analysis
        analyzers = [
            GeneralAnalyzer(),
            ROCAnalyzer()
        ]

        for analyzer in analyzers:
            analyzer.process_entry((y_test, pred_param))

        for analyzer in analyzers:
            analyzer.write()

    def analyze_features(
            self, model_name,
            x_test, y_test
    ):
        # Load model and predict outputs
        loaded_model = self.load_static_model(model_name)

        # Make predictions and unscale
        base_predictions = loaded_model.predict(x_test)

        # Calculate error
        base_loss = pairing_loss(y_test, base_predictions)

        # Evaluate permutations
        hist = TH1D(
            'feature_importance', 'Feature Importance',
            x_test.shape[1], uniform_step_axis(1, 0, x_test.shape[1], align='center')
        )

        for feature_id in range(x_test.shape[1]):
            # Shuffle Indices
            random_index_array = np.arange(x_test.shape[0])
            np.random.shuffle(random_index_array)

            # Shuffle Column
            perm_x_test = x_test.copy()
            perm_x_test[:, feature_id] = x_test[random_index_array, feature_id]

            # Prediction and Loss
            perm_predictions = loaded_model.predict(perm_x_test)
            perm_loss = pairing_loss(y_test, perm_predictions)

            # Collect
            hist.Fill(feature_id, float(perm_loss / base_loss))

        # Plot
        plot_canvas = TCanvas('feature_importance', '', 1080, 1920)
        plot_canvas.SetTopMargin(0.065)
        plot_canvas.SetBottomMargin(0.100)
        plot_canvas.SetLeftMargin(0.250)
        plot_canvas.SetLogx(1)
        plot_canvas.SetLogy(0)
        plot_canvas.cd(0)

        hist.SetLineColor(kBlue)
        hist.SetFillColorAlpha(kBlue, 0.5)
        hist.GetYaxis().SetTitle('Permutation Loss / Base Loss')

        _, _, ymin, ymax = get_th1_ideal_ranges(
            hist,
            logy=True
        )

        hist.GetYaxis().SetRangeUser(max(0.9, ymin), max(10., ymax))

        # Set Labels
        for col_id in range(x_test.shape[1]):
            bin_id = hist.GetXaxis().FindBin(col_id)
            hist.GetXaxis().SetBinLabel(bin_id, pairing_var_names[col_id])

        hist.Draw('HBAR')
        hist.Draw('SAME AXIG')

        plot_canvas.SaveAs('feature_importance.png')

    def load_static_model(self, model_name):
        loaded_model = load_model(
            name=model_name,
            custom_objects={
                'GateIndexLayer': GateIndexLayer,
                'GateSelectionLayer': GateSelectionLayer,
                'WarmupCosineDecay': WarmupCosineDecay,
                'BatchNormalization': BatchNormalization,
                'PairingActivation': PairingActivation,
                'pairing_loss': pairing_loss,
            }
        )
        loaded_model.trainable = False

        print(loaded_model.summary())

        return loaded_model
