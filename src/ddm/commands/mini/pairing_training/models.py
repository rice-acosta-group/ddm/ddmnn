import math

import numpy as np
import tensorflow as tf
from keras import Input
from keras.src.layers import BatchNormalization, Dropout, Dense, Activation
from keras.src.optimizers import Adam

from ddm.commands.mini.pairing_training.custom.activation import PairingActivation
from ddm.commands.mini.pairing_training.custom.gating import GateIndexLayer, GateSelectionLayer
from ddm.keras.optimizers import WarmupCosineDecay, WarmupExponentialDecay


###########################################################################
# Optimizers
##########################################################################
def create_lr_schedule(
        num_train_samples,
        epochs=100,
        warmup_epochs=30,
        batch_size=32,
        learning_rate=0.001,
        final_learning_rate=0.00001
):
    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs
    total_steps = steps_per_epoch * epochs
    cosine_decay_alpha = final_learning_rate / learning_rate

    assert warmup_steps <= total_steps

    return WarmupCosineDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=(total_steps - warmup_steps), alpha=cosine_decay_alpha)


def create_exponential_lr_schedule(
        num_train_samples,
        epochs=100,
        warmup_epochs=30,
        batch_size=32,
        learning_rate=0.001,
        final_learning_rate=0.00001,
        decay_rate=0.9,
        staircase=True
):
    assert warmup_epochs <= epochs

    # Create learning rate schedule with warmup and cosine decay
    steps_per_epoch = int(np.ceil(num_train_samples / float(batch_size)))
    warmup_steps = steps_per_epoch * warmup_epochs

    decay_epochs = (epochs - warmup_epochs)
    decay_step = (decay_epochs * steps_per_epoch) * math.log(decay_rate) / math.log(final_learning_rate / learning_rate)

    return WarmupExponentialDecay(
        initial_learning_rate=learning_rate, warmup_steps=warmup_steps,
        decay_steps=decay_step, decay_rate=decay_rate,
        staircase=staircase)


def create_optimizer(lr_schedule, gradient_clipnorm=10000):
    # Create optimizer with lr_schedule and clipnorm
    optimizer = Adam(learning_rate=lr_schedule, clipnorm=gradient_clipnorm)

    return optimizer


###########################################################################
# Loss Functions
###########################################################################

def pairing_loss(y_true, y_pred):
    is_match_true = y_true[:, 0]
    is_match_pred = y_pred[:, 0]

    is_match_loss = tf.keras.losses.BinaryCrossentropy()(is_match_true, is_match_pred)

    return is_match_loss


###########################################################################
# Models
###########################################################################

def create_model(
        in_nodes=40,
        out_nodes=1
):
    # Parameters
    momentum = 0.85
    epsilon = 1e-4

    # Hidden Layers
    hidden_layer_nodes = [64, 32, 16, 8]

    # Create
    inputs = Input(shape=(in_nodes,), name='inputs')

    def _gen_bn_constraints():
        beta_constraint = None
        gamma_constraint = None

        return beta_constraint, gamma_constraint

    def _gen_nn_constraints():
        kernel_constraint = None
        bias_constraint = None

        return kernel_constraint, bias_constraint

    def _build_subnn(lm1):
        # Add Dense Layers
        for n_nodes in hidden_layer_nodes:
            kernel_constraint, bias_constraint = _gen_nn_constraints()

            # Dense Layers no Bias
            lm1 = Dense(
                n_nodes,
                kernel_initializer='glorot_uniform',
                kernel_constraint=kernel_constraint,
                bias_constraint=bias_constraint,
                use_bias=False
            )(lm1)

            # Add Batch Normalization
            beta_constraint, gamma_constraint = _gen_bn_constraints()

            lm1 = BatchNormalization(
                epsilon=epsilon, momentum=momentum,
                beta_constraint=beta_constraint,
                gamma_constraint=gamma_constraint
            )(lm1)

            # Activations
            lm1 = Activation('tanh')(lm1)

            # Add Dropout
            lm1 = Dropout(0.05)(lm1)

        # Output
        kernel_constraint, bias_constraint = _gen_nn_constraints()

        lm1 = Dense(
            out_nodes,
            kernel_initializer='glorot_uniform',
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint,
            use_bias=False
        )(lm1)

        # Return
        return lm1

    # Adding 1 BN layer right after the input layer
    bn0_beta_constraint, bn0_gamma_constraint = _gen_bn_constraints()

    bn0 = BatchNormalization(
        epsilon=epsilon, momentum=momentum,
        beta_constraint=bn0_beta_constraint,
        gamma_constraint=bn0_gamma_constraint
    )(inputs)

    # Add Dropout
    do0 = Dropout(0.05)(bn0)

    # Build Experts
    dsa_nn = _build_subnn(do0)
    hyb_nn = _build_subnn(do0)
    pat_nn = _build_subnn(do0)

    # Output
    gate_nn = GateIndexLayer()(inputs)
    output = tf.keras.layers.Concatenate(axis=-1)([gate_nn, dsa_nn, hyb_nn, pat_nn])
    output = GateSelectionLayer()(output)
    output = PairingActivation()(output)

    # Build model
    model = tf.keras.Model(inputs=inputs, outputs=output)

    return model
