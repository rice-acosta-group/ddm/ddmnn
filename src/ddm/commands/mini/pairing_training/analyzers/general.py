import math

from ROOT import TCanvas
from ROOT import TLegend
from ROOT import gPad
from ROOT import kBlack
from ROOT import kFullDotLarge

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.plotters.basic import Hist1DPlotter, EfficiencyPlotter, CorrelationPlotter
from ddm.core.root.environment import cm_tab20
from ddm.core.root.labels import draw_fancy_label
from ddm.mini.commons.bins import lxy_eff_bins, lz_eff_bins, eta_eff_bins, ddm_min_threshold


class GeneralAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.pscore_pred_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'pscore_pred', 'Model Prediction',
            'Pred pscore', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            logy=True,
            density=True)

        self.pscore_true_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'pscore_true', 'Model Test',
            'True is_match', 'a.u.',
            xbins=uniform_step_axis(1, 0, 1, align='center'),
            logy=True,
            density=True)

        self.vld_pscore_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pscore', 'Model Validation',
            'True is_match', 'Pred pscore',
            xbins=uniform_step_axis(1, 0, 1, align='center'),
            ybins=uniform_step_axis(0.1, 0, 1, align='center'),
            xlabels=['FALSE', 'TRUE'],
            show_text=True,
            min_val=ddm_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        # Define efficiencies
        self.eff_definitions = list()

        pscore_thresholds = [
            0.5, 0.75, 0.95, 0.99
        ]

        eta_thresholds = [
            (0, 0.9),
            (0.9, 1.2),
            (1.2, 2.4),
            (0, 2.4),
        ]

        def num_pred_factory(pscore_threshold):
            def pred(entry):
                return entry['pscore'] > pscore_threshold

            return pred

        def denom_pred_factory(eta_low, eta_up, require_match=False):
            def pred(entry):
                base_cut = (eta_low <= abs(entry['true_eta']) < eta_up)

                if require_match:
                    base_cut = base_cut and (entry['true_pscore'] == 1)

                return base_cut

            return pred

        for eta_threshold in eta_thresholds:
            eta_low = eta_threshold[0]
            eta_up = eta_threshold[1]

            # Num: Is Match
            # Denom: Eta Region
            for pscore_threshold in pscore_thresholds:
                name = 'num_pscore_ge_%s' % str(pscore_threshold).replace('.', 'p')
                name += '_denom_eta_ge_%s_lt_%s' % (
                    str(eta_low).replace('.', 'p'),
                    str(eta_up).replace('.', 'p')
                )

                num_label = 'pscore #geq %0.2f' % pscore_threshold
                denom_label = '%0.2f #leq #left| #eta #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pscore_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up, require_match=False),
                })

            # Num: Is Match
            # Denom: Is Match & Eta Region
            for pscore_threshold in pscore_thresholds:
                name = 'num_pscore_ge_%s' % str(pscore_threshold).replace('.', 'p')
                name += '_denom_is_match_eta_ge_%s_lt_%s' % (
                    str(eta_low).replace('.', 'p'),
                    str(eta_up).replace('.', 'p')
                )

                num_label = 'pscore #geq %0.2f' % pscore_threshold
                denom_label = '%0.2f #leq #left| #eta #right| < %0.2f' % (eta_low, eta_up)

                self.eff_definitions.append({
                    'name': name,
                    'num_label': num_label,
                    'denom_label': denom_label,
                    'numerator': num_pred_factory(pscore_threshold),
                    'denominator': denom_pred_factory(eta_low, eta_up, require_match=True),
                })

        # Init Eff plots
        self.eff_vs_lxy_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_lxy_%s' % eff_definition['name'], '',
                'True #left| L_{xy} #right| [cm]', 'Efficiency',
                xbins=lxy_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_lxy_plots[eff_definition['name']] = eff_plot

        self.eff_vs_lz_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_lz_%s' % eff_definition['name'], '',
                'True|L_{z}| [cm]', 'Efficiency',
                xbins=lz_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_lz_plots[eff_definition['name']] = eff_plot

        self.eff_vs_eta_plots = dict()

        for eff_definition in self.eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'vld_eff_vs_eta_%s' % eff_definition['name'], '',
                'True #left| #eta #right|', 'Efficiency',
                xbins=eta_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_eta_plots[eff_definition['name']] = eff_plot

    def process_entry(self, data):
        true_values = data[0]
        pred_values = data[1]

        n_entries = true_values.shape[0]
        n_entries_chk = pred_values.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        for i in range(n_entries):
            # Unpack values
            true_pscore = true_values[i, 0]
            pred_pscore = pred_values[i, 0]

            true_q = true_values[i, 1]
            true_pt = true_values[i, 2]
            true_eta = true_values[i, 3]
            true_phi = true_values[i, 4]
            true_vx = true_values[i, 5]
            true_vy = true_values[i, 6]
            true_vz = true_values[i, 7]
            true_lxy = math.hypot(true_vx, true_vy)

            entry = {
                'pscore': pred_pscore,
                'true_pscore': true_pscore,
                'true_q': true_q,
                'true_pt': true_pt,
                'true_eta': true_eta,
                'true_phi': true_phi,
                'true_vx': true_vx,
                'true_vy': true_vy,
                'true_vz': true_vz,
            }

            # Histograms
            self.pscore_pred_plotter.fill(pred_pscore)
            self.pscore_true_plotter.fill(true_pscore)
            self.vld_pscore_plotter.fill(true_pscore, pred_pscore)

            # Efficiency
            for eff_definition in self.eff_definitions:
                passes_denominator = eff_definition['denominator'](entry)

                if not passes_denominator:
                    continue

                passes_numerator = eff_definition['numerator'](entry)

                # Eff vs lxy
                eff_plot = self.eff_vs_lxy_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_lxy)

                # Eff vs lz
                eff_plot = self.eff_vs_lz_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_vz)

                # Eff vs eta
                eff_plot = self.eff_vs_eta_plots[eff_definition['name']]
                eff_plot.fill(passes_numerator, true_eta)

    def post_production(self):
        #####################################################################
        # Efficiency Plots
        #####################################################################        
        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_is_match_' in eff_def['name']
                            and '_eta_ge_0_lt_2p4' in eff_def['name']
                            and 'num_pscore_ge_' in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            lxy_plot_en=True,
            lz_plot_en=True,
            eta_plot_en=True
        )

        self.plot_efficiencies(
            '_pscore_ge_0p99',
            lambda eff_def: 'denom_is_match_' in eff_def['name']
                            and '_eta_ge_0_lt_2p4' not in eff_def['name']
                            and 'num_pscore_ge_0p99' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            comment='Eff: pscore #geq 0.99',
            lxy_plot_en=True,
            lz_plot_en=True,
            eta_plot_en=False
        )

    def plot_efficiencies(
            self, suffix, group_pred, label_fn,
            comment=None,
            lxy_plot_en=False,
            lz_plot_en=False,
            eta_plot_en=False
    ):
        #####################################################################
        # Selection
        #####################################################################
        selected_eff = []

        for eff_definition in self.eff_definitions:
            if not group_pred(eff_definition):
                continue

            selected_eff.append(eff_definition)

        #####################################################################
        # Eff vs lxy
        #####################################################################
        if lxy_plot_en:
            plot_canvas = TCanvas('vld_pscore_eff_vs_lxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_lxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(lxy_eff_bins[0], 0, lxy_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| L_{xy} #right| [cm]')
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.225
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.475, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_lxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pscore_eff_vs_lxy' + suffix + '.png')

        #####################################################################
        # Eff vs lz
        #####################################################################
        if lz_plot_en:
            plot_canvas = TCanvas('vld_pscore_eff_vs_lz_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_lz_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(lz_eff_bins[0], 0, lz_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True|L_{z}| [cm]')
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.225
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.475, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_lz_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pscore_eff_vs_lz' + suffix + '.png')

        #####################################################################
        # Eff vs eta
        #####################################################################
        if eta_plot_en:
            plot_canvas = TCanvas('vld_pscore_eff_vs_eta_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(eta_eff_bins[0], 0, eta_eff_bins[-1], 1.15, 'Model Validation')
            frame.GetXaxis().SetTitle('True #left| #eta #right|')
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.450
            legend_y0 = 0.225

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.475, legend_y0 + 0.225)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.1)
            legend.SetBorderSize(0)
            legend.SetTextSize(.035)

            cm = iter(cm_tab20)

            # Stack Plots
            for eff_definition in selected_eff:
                eff_plot = self.eff_vs_eta_plots[eff_definition['name']].plot.GetPaintedGraph()

                eff_plot.SetMarkerColor(next(cm))
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(next(cm))
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_fn(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('vld_pscore_eff_vs_eta' + suffix + '.png')
