import array

from ROOT import TCanvas
from ROOT import TGraphAsymmErrors
from ROOT import TLine
from ROOT import gPad
from ROOT import gStyle
from ROOT import kSpring

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_axis
from ddm.core.commons.measurements import Measurement
from ddm.core.commons.roc import ConfusionMatrix
from ddm.core.execution.runtime import get_logger
from ddm.core.root.environment import cm_tab10


class ROCAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # Build Entries
        self.trigger_setups = list()
        self.thresholds = uniform_axis(100, 0.5, 1.)[:-1]

        for threshold in self.thresholds:
            self.trigger_setups.append({
                'threshold': threshold,
                'confusion': ConfusionMatrix(True)
            })

    def process_entry(self, data):
        true_values = data[0]
        pred_values = data[1]

        n_entries = true_values.shape[0]
        n_entries_chk = pred_values.shape[0]

        assert n_entries_chk == n_entries, 'Entries don\'t match in length'

        for i in range(n_entries):
            # Unpack values
            true_pscore = true_values[i, 0]
            pred_pscore = pred_values[i, 0]

            # Process Values
            for trigger_setup in self.trigger_setups:
                threshold = trigger_setup['threshold']
                confusion = trigger_setup['confusion']

                true_pass = (true_pscore == 1)
                confusion.on_truth(true_pass)

                pred_pass = (pred_pscore >= threshold)
                confusion.on_prediction(pred_pass)

                confusion.fill()

    def merge(self, other):
        for i in range(len(self.trigger_setups)):
            this_trigger = self.trigger_setups[i]
            other_trigger = other.trigger_setups[i]

            this_trigger['confusion'].add(other_trigger['confusion'])

    def post_production(self):
        # Loop
        threshold_arr = list()
        fpr_arr = list()
        tpr_arr = list()
        fpr_elow_arr = list()
        fpr_eup_arr = list()
        tpr_elow_arr = list()
        tpr_eup_arr = list()

        min_x = 1e-2
        min_y = 0.8

        for trigger_setup in self.trigger_setups:
            threshold = trigger_setup['threshold']
            confusion = trigger_setup['confusion']

            # Calculate TPR and FPR
            tpr = confusion.calc_tpr()
            fpr = confusion.calc_fpr()
            acc = confusion.calc_accuracy()

            # Log Stats
            get_logger().info(
                f'threshold: {threshold:0.5f}, '
                f'tpr: {tpr.value * 100:0.2f} +- {tpr.error * 100:0.2f}, '
                f'fpr: {fpr.value * 100:0.2f} +- {fpr.error * 100:0.2f}, '
                f'acc: {acc.value * 100:0.2f} +- {acc.error * 100:0.2f}'
            )

            # Floor
            if fpr.value < min_x:
                fpr = Measurement(min_x, 0.)

            if tpr.value < min_y:
                tpr = Measurement(min_y, 0.)

            # Collect
            threshold_arr.append(threshold)
            fpr_arr.append(fpr.value)
            tpr_arr.append(tpr.value)

            fpr_elow_arr.append(fpr.error)
            fpr_eup_arr.append(fpr.error)
            tpr_elow_arr.append(tpr.error)
            tpr_eup_arr.append(tpr.error)

        # Make Plots
        plot_roc(
            threshold_arr,
            fpr_arr, fpr_elow_arr, fpr_eup_arr,
            tpr_arr, tpr_elow_arr, tpr_eup_arr,
            min_x=min_x, max_x=1.,
            min_y=min_y, max_y=1.,
            target_x=0.1, target_y=0.9
        )


def plot_roc(
        threshold_arr,
        fpr_arr, fpr_elow_arr, fpr_eup_arr,
        tpr_arr, tpr_elow_arr, tpr_eup_arr,
        min_x=1e-2, max_x=1.,
        min_y=1e-2, max_y=1.,
        target_x=0.1, target_y=0.9
):
    # Build Graph
    graph = TGraphAsymmErrors(
        len(threshold_arr),
        array.array('d', fpr_arr), array.array('d', tpr_arr),
        array.array('d', fpr_elow_arr), array.array('d', fpr_eup_arr),
        array.array('d', tpr_elow_arr), array.array('d', tpr_eup_arr)
    )

    # Draw Graphs
    gStyle.SetOptStat(0)

    plot_canvas = TCanvas('roc', '')
    plot_canvas.SetLeftMargin(0.175)
    plot_canvas.SetTopMargin(0.125)
    plot_canvas.SetBottomMargin(0.175)
    plot_canvas.SetLogx(1)
    # plot_canvas.SetLogy(1)
    plot_canvas.cd(0)

    frame = plot_canvas.DrawFrame(
        min_x, min_y, max_x, max_y,
        'ROC Curve'
    )
    frame.GetXaxis().SetTitle('False Positive Rate')
    frame.GetXaxis().SetRangeUser(min_x, max_x)
    frame.GetYaxis().SetTitle('True Positive Rate')
    frame.GetYaxis().SetRangeUser(min_y, max_y)

    # Draw Frame
    frame.Draw('SAME AXIS')
    frame.Draw('SAME AXIG')

    # Draw x Target
    x_target_line = TLine(target_x, min_y, target_x, max_y)
    x_target_line.SetLineStyle(10)
    x_target_line.SetLineWidth(5)
    x_target_line.SetLineColor(kSpring + 4)
    x_target_line.Draw('SAME')

    # Draw y Target
    y_target_line = TLine(min_x, target_y, max_x, target_y)
    y_target_line.SetLineStyle(10)
    y_target_line.SetLineWidth(5)
    y_target_line.SetLineColor(kSpring + 4)
    y_target_line.Draw('SAME')

    # Draw Graphs
    graph.SetMarkerSize(2.25)
    graph.SetMarkerStyle(33)
    graph.SetMarkerColor(cm_tab10[0])
    graph.Draw('SAME P')

    # GPad
    gPad.Modified()
    gPad.Update()

    # Save
    plot_canvas.SaveAs('roc.png')
