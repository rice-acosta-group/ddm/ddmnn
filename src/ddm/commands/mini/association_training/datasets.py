import numpy as np

from ddm.core.execution.runtime import get_logger


class Dataset(object):

    def __init__(self, data_file):
        self.data_file = data_file

    def get_samples(self):
        get_logger().info(f'Loading data from {self.data_file}')

        loaded_file = np.load(self.data_file)

        x_train = loaded_file['variables_train']
        y_train = loaded_file['parameters_train']
        x_test = loaded_file['variables_test']
        y_test = loaded_file['parameters_test']

        # Count NaN
        x_train_nan = np.count_nonzero(np.isnan(x_train))
        x_test_nan = np.count_nonzero(np.isnan(x_test))

        get_logger().info(f"""
        NaN count in train variables {x_train_nan}
        NaN count in test variables {x_test_nan}
        Removing Nan
        """)

        # Remove NaN
        for i in range(x_train.shape[1]):
            col = x_train[:, i]
            x_train[:, i] = np.where(np.isnan(col), np.zeros_like(col), col)

            col = x_test[:, i]
            x_test[:, i] = np.where(np.isnan(col), np.zeros_like(col), col)

        # Log
        get_logger().info(f"""
        Loaded the train variables with shape {x_train.shape}
        Loaded the train parameters with shape {y_train.shape}
        Loaded the test variables with shape {x_test.shape}
        Loaded the test parameters with shape {y_test.shape}        
        """)

        # Convert to float32
        x_train = x_train.astype(np.float32)
        x_test = x_test.astype(np.float32)

        y_train = y_train.astype(np.float32)
        y_test = y_test.astype(np.float32)

        return x_train, y_train, x_test, y_test
