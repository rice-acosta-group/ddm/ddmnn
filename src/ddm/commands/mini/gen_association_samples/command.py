from ddm.commands.mini.gen_association_samples.analyzers.signal import SignalAnalyzer
from ddm.mini.analysis.mini_analysis import MiniAnalysis
from ddm.mini.commons.user_options import UserOptions
from ddm.mini.selection.cutflow import CutflowAnalyzer
from ddm.mini.selection.selector import Selector


class GenAssociationSignalSample(MiniAnalysis):

    def __init__(self):
        super().__init__()
        self.branches_required = set()

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

        parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                            type=str, default='sig_ml_sample',
                            help='Output File Name')

        parser.add_argument('--ftrain', dest='train_fraction', metavar='FRACTION_FOR_TRAINING',
                            type=float, default=0.5,
                            help='fraction of events for training')

    def configure(self, args):
        # Unpack args
        self.filename = args.filename
        self.train_fraction = args.train_fraction

    def create_producers(self):
        return [
            Selector(
                UserOptions('_NPP_NS_NH_HLT_REPGEN_REPFORML'),
                is_llp_gun=True
            )
        ]

    def create_analyzers(self):
        return [
            SignalAnalyzer(self.filename, self.train_fraction),
            CutflowAnalyzer('sel_cutflow')
        ]

    def consolidate(self):
        for analyzer in self.analyzers:
            analyzer.write()
