import math

import numpy as np

from ddm.commands.mini.check_performance.utils.pat import encode_pat_flags
from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.commons.combinatorics import yield_combinations
from ddm.core.commons.plotting import plot_var_walls
from ddm.core.execution.runtime import get_logger
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.mini.commons.bins import ddm_pt_bins, ddm_abs_eta_bins, ddm_phi_deg_bins, ddm_lxy_bins, ddm_lz_bins, \
    ddm_min_threshold
from ddm.mini.utils.dsa_seg_match import DEFAULT_DSA_PAT_SEG_MATCH, DEFAULT_DSA_PAT_SEG_MATCH_DR
from ddm.mini.utils.muon_matching import compare_muons
from ddm.mini.utils.nn_association import get_association_vars, get_association_params

# Bins
lxy_bins = np.concatenate((
    np.arange(0.0, 100.0, 2.0),
    np.arange(100.0, 208.0, 4.0),
    np.array([
        208.0, 214.0, 220.0, 230.0,
        240.0, 255.0, 270.0, 290.0,
        310.0, 340.0, 370.0, 400.0,
    ]),
))


class SignalAnalyzer(AbstractAnalyzer):

    def __init__(self, filename, train_fraction):
        super().__init__()

        self.filename = filename
        self.train_fraction = train_fraction
        self.list_variables = list()
        self.list_parameters = list()

        # GEN
        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_pt',
            'Muon Distribution',
            'GEN p_{T} [GeV]', 'a.u.',
            xbins=ddm_pt_bins,
            density=True,
            logy=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'Muon Distribution',
            'GEN #left| #eta #right|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'Muon Distribution',
            'GEN #phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

        self.gen_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lxy',
            'Muon Distribution',
            'GEN L_{xy} [cm]', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.gen_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lz',
            'Muon Distribution',
            'GEN |L_{z}| [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.gen_lxy_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_lxy_vs_lz', 'Muon Distribution',
            'GEN |L_{z}| [cm]', 'GEN L_{xy} [cm]',
            xbins=ddm_lz_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        # RECO
        self.true_pat_flags_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'true_pat_flags',
            'True PAT Distribution',
            'True L_{xy} [cm]', 'PAT flags',
            xbins=lxy_bins,
            ybins=uniform_step_axis(1, 0, 7, align='center'),
            ylabels=[
                'None', 'Global (G)', 'Medium (M)', 'GM', 'Tracker (T)', 'GT', 'MT', 'GMT'
            ],
            min_val=0,
            density=False, normalize_by='column')

    def process_entry(self, entry):
        # Must Pass Trigger
        if len(entry.ext_hlt_paths) == 0:
            return

        # Get muons
        matched_pat_muons = list()
        pat_muon_by_dsa_idx = dict()

        for pat_muon in entry.sel_pat_muons:
            if pat_muon.matched_gen_muon is None:
                continue

            if pat_muon.ideal_dsa_muon is None:
                continue

            if pat_muon.isDisp:
                if pat_muon.ideal_std_pat_muon is None:
                    continue

                matched_pat_muons.append(pat_muon.ideal_std_pat_muon)
                pat_muon_by_dsa_idx[pat_muon.ideal_dsa_muon.idx] = pat_muon.ideal_std_pat_muon
            else:
                matched_pat_muons.append(pat_muon)
                pat_muon_by_dsa_idx[pat_muon.ideal_dsa_muon.idx] = pat_muon

        # Short-Circuit: No PAT muons found
        if len(matched_pat_muons) == 0:
            return

        # Get Variables and Parameters
        # Loop over all possible DSA and PAT combinations
        for dsa_muon, pat_muon in yield_combinations(entry.all_dsa_muons, matched_pat_muons):
            # Get PAT
            true_pat_muon = pat_muon_by_dsa_idx.get(dsa_muon.idx, None)

            # Short-Circuit: No match
            if true_pat_muon is None:
                continue

            # Check if it's a match
            true_is_match = (true_pat_muon.idx == pat_muon.idx)

            # Get SegMatch info
            var_nsegments, var_dR_pd, var_dR_pp = entry.dsa_pat_seg_match.get(
                (dsa_muon.idx, pat_muon.idx),
                DEFAULT_DSA_PAT_SEG_MATCH
            )

            # Short-Circuit: We need at least one of these parameters
            # Standard PAT muon will only have var_dR_pp when pt < 4.5 GeV
            # NOTE: This will only select std pat muons as well since displaced pat don't have dR_pd
            if (var_dR_pd == DEFAULT_DSA_PAT_SEG_MATCH_DR) and (var_dR_pp == DEFAULT_DSA_PAT_SEG_MATCH_DR):
                continue

            # Make sure they're a bad match
            if not true_is_match:
                # Compare muons
                deltaR_mom, deltaR_pos = compare_muons(
                    dsa_muon, pat_muon,
                    deltaR_threshold=0.2,
                    deltaPos_threshold=20.0
                )

                # Short-Circuit: Decent match
                if deltaR_mom is not None:
                    continue

            # Get GEN muon
            gen_muon = true_pat_muon.matched_gen_muon

            # Get Parameters and Variables
            parameters = get_association_params(gen_muon, true_is_match)
            variables = get_association_vars(dsa_muon, pat_muon, var_nsegments, var_dR_pd, var_dR_pp)

            # Append
            self.list_variables.append(variables)
            self.list_parameters.append(parameters)

            # Fill GEN plots
            if true_is_match:
                gen_lxy = math.hypot(gen_muon.x, gen_muon.y)

                self.gen_pt_plotter.fill(gen_muon.pt)
                self.gen_eta_plotter.fill(abs(gen_muon.eta))
                self.gen_phi_plotter.fill(np.rad2deg(gen_muon.phi))
                self.gen_lxy_plotter.fill(gen_lxy)
                self.gen_lz_plotter.fill(abs(gen_muon.z))
                self.gen_lxy_vs_lz_plotter.fill(abs(gen_muon.z), gen_lxy)

                self.true_pat_flags_plotter.fill(gen_lxy, encode_pat_flags(pat_muon))

    def merge(self, other):
        self.list_variables += other.list_variables
        self.list_parameters += other.list_parameters

        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)
        self.gen_lxy_plotter.add(other.gen_lxy_plotter)
        self.gen_lz_plotter.add(other.gen_lz_plotter)
        self.gen_lxy_vs_lz_plotter.add(other.gen_lxy_vs_lz_plotter)

        self.true_pat_flags_plotter.add(other.true_pat_flags_plotter)

    def post_production(self):
        # Short-Circuit: Sample empty
        if len(self.list_variables) == 0:
            return

        # Convert
        self.list_variables = np.asarray(self.list_variables, dtype=np.float64)
        self.list_parameters = np.asarray(self.list_parameters, dtype=np.float64)

        # Remove NaN
        for i in range(self.list_variables.shape[1]):
            col = self.list_variables[:, i]
            self.list_variables[:, i] = np.where(np.isnan(col), np.zeros_like(col), col)

        # Shuffle
        random_index_array = np.arange(self.list_variables.shape[0])
        np.random.shuffle(random_index_array)

        self.list_variables = self.list_variables[random_index_array]
        self.list_parameters = self.list_parameters[random_index_array]

        # Plot
        entries_that_match = (self.list_parameters[:, 0] == 1)
        entries_that_dont_match = (self.list_parameters[:, 0] == 0)

        plot_var_walls('variables', self.list_variables)
        plot_var_walls('parameters', self.list_parameters)
        plot_var_walls('variables_match', self.list_variables[entries_that_match])
        plot_var_walls('parameters_match', self.list_parameters[entries_that_match])
        plot_var_walls('variables_no_match', self.list_variables[entries_that_dont_match])
        plot_var_walls('parameters_no_match', self.list_parameters[entries_that_dont_match])

        # Save
        self.save_npz(self.filename, self.list_variables, self.list_parameters)

    def save_npz(self, name, variables, parameters):
        # Split Datasets
        total_events = int(variables.shape[0])
        train_events = int(self.train_fraction * total_events)
        test_events = total_events - train_events

        from_event = 0
        to_event = from_event + train_events
        variables_train = variables[from_event:to_event]
        parameters_train = parameters[from_event:to_event]

        from_event = to_event
        to_event = from_event + test_events
        variables_test = variables[from_event:to_event]
        parameters_test = parameters[from_event:to_event]

        # Log
        get_logger().info(f"""
        *********************************************************
        Signal Sample Summary
        *********************************************************
        Total Events: {total_events}
        Train Events: {train_events}
        Test Events: {test_events}
        """)

        # Save
        np.savez_compressed(
            name + '.npz',
            variables_train=variables_train, parameters_train=parameters_train,
            variables_test=variables_test, parameters_test=parameters_test
        )
