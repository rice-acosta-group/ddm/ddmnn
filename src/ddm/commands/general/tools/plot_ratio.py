from ROOT import TCanvas
from ROOT import TFile
from ROOT import TLegend
from ROOT import TLine
from ROOT import TPad
from ROOT import gStyle
from ROOT import kBlue
from ROOT import kFullDotLarge
from ROOT import kRed
from ROOT import kWhite

from ddm.core.commands import Command
from ddm.core.commons.histograms import get_th1_ideal_ranges
from ddm.core.commons.measurements import Measurement


class PlotRatio(Command):

    def configure_parser(self, parser):
        parser.add_argument('--output', dest='output_name', metavar='OUTPUT_NAME',
                            type=str, default=None,
                            help='Name to save the plot as')

        parser.add_argument('--num', dest='num', metavar='NUMERATOR_HIST',
                            type=str, default=None,
                            help='Numerator Histogram')

        parser.add_argument('--den', dest='den', metavar='DENOMINATOR_HIST',
                            type=str, default=None,
                            help='Denominator Histogram')

        parser.add_argument('--scale', dest='scale_factor', metavar='SCALE_FACTOR',
                            type=float, default=None,
                            help='SCALE FACTOR')

        parser.add_argument('--normalize-by-bin', dest='norm_by_bin_en', action='store_const',
                            const=True, default=False,
                            help='Normalize histograms by bin width')

        parser.add_argument('--num-color', dest='num_color', metavar='NUM_COLOR',
                            type=int, default=kRed,
                            help='Plot numerator color')

        parser.add_argument('--den-color', dest='den_color', metavar='DEN_COLOR',
                            type=int, default=kBlue,
                            help='Plot denominator color')

        parser.add_argument('--num-label', dest='num_label', metavar='NUM_LABEL',
                            type=str, default=None,
                            help='Plot numerator label')

        parser.add_argument('--den-label', dest='den_label', metavar='DEN_LABEL',
                            type=str, default=None,
                            help='Plot denominator label')

        parser.add_argument('--xtitle', dest='xtitle', metavar='XTITLE',
                            type=str, default=None,
                            help='Plot x-axis title')

        parser.add_argument('--ytitle', dest='ytitle', metavar='YTITLE',
                            type=str, default=None,
                            help='Plot y-axis title')

        parser.add_argument('--rtitle', dest='rtitle', metavar='RTITLE',
                            type=str, default=None,
                            help='Plot r-axis title')

        parser.add_argument('--ymin', dest='ymin', metavar='YMIN',
                            type=float, default=None,
                            help='Y Min')

        parser.add_argument('--ymax', dest='ymax', metavar='YMAX',
                            type=float, default=None,
                            help='Y Max')

        parser.add_argument('--rmin', dest='rmin', metavar='RMIN',
                            type=float, default=None,
                            help='Ratio Min')

        parser.add_argument('--rmax', dest='rmax', metavar='RMAX',
                            type=float, default=None,
                            help='Ratio Max')

        parser.add_argument('--rtarget', dest='rtarget', metavar='RTARGET',
                            type=float, default=None,
                            help='Ratio Target')

        parser.add_argument('--logy', dest='logy', metavar='LOGY',
                            type=int, default=1,
                            help='Use logarithmic scale for Y-axis')

        parser.add_argument('--logr', dest='logr', metavar='LOGR',
                            type=int, default=0,
                            help='Use logarithmic scale for R-axis')

    def run(self, args):
        # Unpack args
        output_name = args.output_name
        scale_factor = args.scale_factor
        norm_by_bin_en = args.norm_by_bin_en
        num_file = args.num
        den_file = args.den
        num_label = args.num_label
        den_label = args.den_label
        num_color = args.num_color
        den_color = args.den_color
        xtitle = args.xtitle
        ytitle = args.ytitle
        rtitle = args.rtitle
        user_ymin = args.ymin
        user_ymax = args.ymax
        user_rmin = args.rmin
        user_rmax = args.rmax
        user_rtarget = args.rtarget
        user_logy = bool(args.logy)
        user_logr = bool(args.logr)

        # Collect histograms
        root_files = dict()

        def get_root_file(path):
            root_file = root_files.get(path, None)

            if root_file is None:
                root_file = TFile.Open(path, 'READ')
                root_files[path] = root_file

            return root_file

        num_file = num_file.split(':')
        num_file, num_hist = num_file[0], num_file[1]
        num_file = get_root_file(num_file)
        num_hist = num_file.Get(num_hist)

        den_file = den_file.split(':')
        den_file, den_hist = den_file[0], den_file[1]
        den_file = get_root_file(den_file)
        den_hist = den_file.Get(den_hist)

        # Calculate transfer factor plot
        tf_hist = num_hist.Clone(output_name)
        tf_hist.Sumw2()
        tf_hist.Divide(den_hist)

        # Scale
        if scale_factor is not None:
            for hist in (num_hist, den_hist):
                hist.Scale(scale_factor)

        # Normalize
        if norm_by_bin_en:
            for hist in (num_hist, den_hist):
                for bin_id in range(hist.GetNcells()):
                    bin_count = Measurement(
                        hist.GetBinContent(bin_id),
                        hist.GetBinError(bin_id)
                    )

                    bin_width = Measurement(hist.GetXaxis().GetBinWidth(bin_id), 0)
                    bin_norm_count = bin_count.divide(bin_width)

                    hist.SetBinContent(bin_id, bin_norm_count.value)
                    hist.SetBinError(bin_id, bin_norm_count.error)

        # Build Entries
        hist_entries = [
            (num_hist, num_label, num_color, kFullDotLarge),
            (den_hist, den_label, den_color, kFullDotLarge),
        ]

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(output_name, '')
        available_height = 1 - plot_canvas.GetTopMargin() - plot_canvas.GetBottomMargin()
        bottom_height = plot_canvas.GetBottomMargin() + available_height * 0.3
        top_height = available_height - bottom_height
        gap_height = top_height * 0.05

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetBottomMargin(bottom_height + gap_height)
        top_pad.SetLogy(user_logy)
        top_pad.Draw()
        top_pad.cd()

        # Find min and max
        xmin, xmax, ymin, ymax = get_th1_ideal_ranges(
            den_hist, num_hist,
            logy=user_logy
        )

        ymin = 0.1

        if user_ymin is not None:
            ymin = user_ymin

        if user_ymax is not None:
            ymax = user_ymax

        # Draw top frame
        frame_top = plot_canvas.DrawFrame(
            xmin, ymin,
            xmax, ymax,
            ''
        )
        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)
        frame_top.GetYaxis().SetTitle(ytitle)

        legend_x0 = 0.500
        legend_y0 = 0.725

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.150)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kWhite, 0.8)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        for plot, label, color, style in hist_entries:
            plot.SetMarkerColor(color)
            plot.SetMarkerStyle(style)
            plot.SetLineColor(color)

            plot.Draw('SAME P0 E0 E1')

            legend.AddEntry(plot, label, 'P E1')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        legend.Draw()

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("bottom", "bottom", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(1. - bottom_height)
        bottom_pad.SetLogy(user_logr)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw SF
        _, _, rmin, rmax = get_th1_ideal_ranges(
            tf_hist, logy=user_logr
        )

        if user_rmin is not None:
            rmin = user_rmin

        if user_rmax is not None:
            rmax = user_rmax

        frame_bottom = plot_canvas.DrawFrame(
            xmin, max(rmin, 0.),
            xmax, rmax,
            ''
        )

        frame_bottom.GetXaxis().SetTitle(xtitle)
        frame_bottom.GetYaxis().SetTitle(rtitle)
        frame_bottom.GetYaxis().SetNdivisions(204)

        tf_hist.Draw('SAME P E1')

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Draw x Target
        if user_rtarget is not None:
            r_target_line = TLine(xmin, user_rtarget, xmax, user_rtarget)
            r_target_line.SetLineStyle(10)
            r_target_line.SetLineColor(kRed)
            r_target_line.Draw('SAME')

        # Draw Titles
        plot_canvas.cd(0)

        # Write
        plot_canvas.SaveAs(f'{output_name}.png')
