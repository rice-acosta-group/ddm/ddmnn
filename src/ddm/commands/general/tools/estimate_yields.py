from ROOT import TFile

from ddm.core.commands import Command
from ddm.core.commons.measurements import Constant
from ddm.core.commons.transfer_factors import TransferFactor


class EstimateYields(Command):

    def configure_parser(self, parser):
        parser.add_argument('--tf', dest='fixed_tf', metavar='TRANSFER_FACTOR',
                            type=float, default=None,
                            help='Override calculated transfer factor')

        parser.add_argument('--tf-num', dest='tf_num', metavar='TRANSFER_FACTOR_NUM_HIST',
                            type=str, default=None,
                            help='Transfer factor numerator histogram')

        parser.add_argument('--tf-den', dest='tf_den', metavar='TRANSFER_FACTOR_DEN_HIST',
                            type=str, default=None,
                            help='Transfer factor denominator histogram')

        parser.add_argument('--flat-tf', dest='flat_tf_en',
                            action='store_true', default=False,
                            help='Calculates a flat transfer factor form the transfer factor num/den histograms')

        parser.add_argument('--control', dest='control', metavar='CONTROL_HIST',
                            type=str, default=None,
                            help='Control Histogram')

        parser.add_argument('--estimate', dest='estimate', metavar='ESTIMATE_HIST',
                            type=str, default=None,
                            help='Estimate Histogram')

    def run(self, args):
        # Unpack args
        estimate_file = args.estimate
        control_file = args.control
        fixed_tf = args.fixed_tf
        tf_num_file = args.tf_num
        tf_den_file = args.tf_den
        flat_tf_en = args.flat_tf_en

        # Sanity Checks
        assert (tf_num_file is not None or tf_den_file is not None) ^ (fixed_tf is not None), 'Ambiguous or missing transfer factor definition. Use either --tf or --fixed-tf to specify the transfer factor.'

        # Collect histograms
        root_files = dict()

        def get_root_file(path):
            root_file = root_files.get(path, None)

            if root_file is None:
                root_file = TFile.Open(path, 'READ')
                root_files[path] = root_file

            return root_file

        # Unpack control
        control_file = control_file.split(':')
        control_file, control_hist = control_file[0], control_file[1]
        control_file = get_root_file(control_file)
        control_hist = control_file.Get(control_hist)

        # Unpack transfer factor
        if fixed_tf is None:
            tf_num_file = tf_num_file.split(':')
            tf_num_file, tf_num_hist = tf_num_file[0], tf_num_file[1]
            tf_num_file = get_root_file(tf_num_file)
            tf_num_hist = tf_num_file.Get(tf_num_hist)

            tf_den_file = tf_den_file.split(':')
            tf_den_file, tf_den_hist = tf_den_file[0], tf_den_file[1]
            tf_den_file = get_root_file(tf_den_file)
            tf_den_hist = tf_den_file.Get(tf_den_hist)

            tf = TransferFactor(
                num_hist=tf_num_hist,
                den_hist=tf_den_hist
            )
        else:
            tf = TransferFactor(
                value=Constant(fixed_tf)
            )

        # Multiply control histogram by transfer factor
        estimate_file = estimate_file.split(':')
        estimate_file, estimate_hist = estimate_file[0], estimate_file[1]

        estimate_file = TFile.Open(estimate_file, 'UPDATE')
        estimate_file.cd()

        estimate_hist = control_hist.Clone(estimate_hist)
        tf.apply(control_hist, estimate_hist, flat_tf_en=flat_tf_en)

        estimate_hist.Write()
        estimate_file.Close()
