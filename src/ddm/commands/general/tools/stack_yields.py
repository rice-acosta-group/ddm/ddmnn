from ROOT import TBox
from ROOT import TCanvas
from ROOT import TFile
from ROOT import THStack
from ROOT import TLatex
from ROOT import TLegend
from ROOT import TLine
from ROOT import TPad
from ROOT import kGray
from ROOT import kGreen
from ROOT import kOrange
from ROOT import kRed
from ROOT import kWhite

from ddm.core.commands import Command
from ddm.core.commons.histograms import get_th1_ideal_ranges
from ddm.core.commons.measurements import Measurement, Constant
from ddm.core.root.cms import set_style


class StackYields(Command):

    def configure_parser(self, parser):
        parser.add_argument('--output', dest='output_name', metavar='OUTPUT_NAME',
                            type=str, default=None,
                            help='Name to save the plot as')

        parser.add_argument('--scale', dest='scale_factor', metavar='SCALE_FACTOR',
                            type=float, default=None,
                            help='SCALE FACTOR')

        parser.add_argument('--normalize-by-bin', dest='norm_by_bin_en', action='store_const',
                            const=True, default=False,
                            help='Normalize histograms by bin width')

        parser.add_argument('--observed', dest='observed', metavar='obs_HIST',
                            type=str, default=None,
                            help='Observed Histogram')

        parser.add_argument('--dy', dest='dy', metavar='DY_HIST',
                            type=str, default=None,
                            help='DY Histogram')

        parser.add_argument('--qcd', dest='qcd', metavar='QCD_HIST',
                            type=str, default=None,
                            help='QCD Histogram')

        parser.add_argument('--xtitle', dest='xtitle', metavar='XTITLE',
                            type=str, default=None,
                            help='Plot x-axis title')

        parser.add_argument('--ytitle', dest='ytitle', metavar='YTITLE',
                            type=str, default=None,
                            help='Plot y-axis title')

        parser.add_argument('--ymin', dest='ymin', metavar='YMIN',
                            type=float, default=None,
                            help='Y Min')

        parser.add_argument('--ymax', dest='ymax', metavar='YMAX',
                            type=float, default=None,
                            help='Y Max')

        parser.add_argument('--rmin', dest='rmin', metavar='RMIN',
                            type=float, default=None,
                            help='Ratio Min')

        parser.add_argument('--rmax', dest='rmax', metavar='RMAX',
                            type=float, default=None,
                            help='Ratio Max')

        parser.add_argument('--rtarget', dest='rtarget', metavar='RTARGET',
                            type=float, default=None,
                            help='Ratio Target')

        parser.add_argument('--logy', dest='logy', metavar='LOGY',
                            type=int, default=1,
                            help='Use logarithmic scale for Y-axis')

    def run(self, args):
        # Unpack args
        output_name = args.output_name
        scale_factor = args.scale_factor
        norm_by_bin_en = args.norm_by_bin_en
        obs_file = args.observed
        dy_file = args.dy
        qcd_file = args.qcd
        xtitle = args.xtitle
        ytitle = args.ytitle
        user_ymin = args.ymin
        user_ymax = args.ymax
        user_rmin = args.rmin
        user_rmax = args.rmax
        user_rtarget = args.rtarget
        user_logy = bool(args.logy)

        # Collect histograms
        root_files = dict()

        def get_root_file(path):
            root_file = root_files.get(path, None)

            if root_file is None:
                root_file = TFile.Open(path, 'READ')
                root_files[path] = root_file

            return root_file

        obs_file = obs_file.split(':')
        obs_file, obs_hist = obs_file[0], obs_file[1]
        obs_file = get_root_file(obs_file)
        obs_hist = obs_file.Get(obs_hist)

        bkg_hist_col = list()

        if dy_file is not None:
            dy_file = dy_file.split(':')
            dy_file, dy_hist = dy_file[0], dy_file[1]
            dy_file = get_root_file(dy_file)
            dy_hist = dy_file.Get(dy_hist)
            bkg_hist_col.append(dy_hist)

        if qcd_file is not None:
            qcd_file = qcd_file.split(':')
            qcd_file, qcd_hist = qcd_file[0], qcd_file[1]
            qcd_file = get_root_file(qcd_file)
            qcd_hist = qcd_file.Get(qcd_hist)
            bkg_hist_col.append(qcd_hist)

        # Scale
        if scale_factor is not None:
            for hist in (obs_hist, *bkg_hist_col):
                hist.Scale(scale_factor)

        # Normalize
        if norm_by_bin_en:
            for hist in (obs_hist, *bkg_hist_col):
                for bin_id in range(hist.GetNcells()):
                    bin_count = Measurement(
                        hist.GetBinContent(bin_id),
                        hist.GetBinError(bin_id)
                    )

                    bin_width = Measurement(hist.GetXaxis().GetBinWidth(bin_id), 0)
                    bin_norm_count = bin_count.divide(bin_width)

                    hist.SetBinContent(bin_id, bin_norm_count.value)
                    hist.SetBinError(bin_id, bin_norm_count.error)

        # Calculate total
        bkg_hist = obs_hist.Clone('_estimation')
        bkg_hist.Reset()
        bkg_hist.Sumw2()

        for hist in bkg_hist_col:
            bkg_hist.Add(hist)

        # Calculate ratio
        ratio_hist = obs_hist.Clone('ratio')
        ratio_hist.Sumw2()
        ratio_hist.Divide(bkg_hist)

        # Build Estimation stack
        bkg_stack = THStack("estimation", "")

        if qcd_file is not None:
            qcd_hist.SetFillColor(kOrange)
            qcd_hist.SetLineColor(kOrange)
            bkg_stack.Add(qcd_hist)

        if dy_file is not None:
            dy_hist.SetFillColor(kGreen - 6)
            dy_hist.SetLineColor(kGreen - 6)
            bkg_stack.Add(dy_hist)

        # Make RTitle
        rtitle = 'Obs. / Pred.'

        # Style
        set_style()

        # CREATE CANVAS
        plot_canvas = TCanvas('hist_comparison', '')
        available_height = 1 - plot_canvas.GetTopMargin() - plot_canvas.GetBottomMargin()
        bottom_height = plot_canvas.GetBottomMargin() + available_height * 0.3
        top_height = available_height - bottom_height
        gap_height = top_height * 0.05

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetBottomMargin(bottom_height + gap_height)
        top_pad.SetLogy(user_logy)
        top_pad.Draw()
        top_pad.cd()

        # Find min and max
        xmin, xmax, ymin, ymax = get_th1_ideal_ranges(
            bkg_hist, obs_hist,
            logy=user_logy
        )

        ymin = 0.1

        if user_ymin is not None:
            ymin = user_ymin

        if user_ymax is not None:
            ymax = user_ymax

        # Draw top frame
        frame_top = plot_canvas.DrawFrame(
            xmin, ymin,
            xmax, ymax,
            ''
        )
        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)
        frame_top.GetYaxis().SetTitle(ytitle)

        legend_x0 = 0.600
        legend_y0 = 0.700

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.150)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kWhite, 0.8)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        bkg_stack.Draw('SAME HIST')

        err_boxes = list()

        for ibin in range(bkg_hist.GetNbinsX()):
            ibin += 1
            xlowedge = bkg_hist.GetBinLowEdge(ibin)
            xupedge = xlowedge + bkg_hist.GetBinWidth(ibin)

            pred_count = Constant(0)

            for hist in bkg_hist_col:
                bin_count = Measurement(
                    hist.GetBinContent(ibin),
                    hist.GetBinError(ibin)
                )
                pred_count = pred_count.add(bin_count)

            pred_err_low = pred_count.error
            pred_err_up = pred_count.error
            pred_count = pred_count.value
            err_lowedge = max(ymin, pred_count - pred_err_low)
            err_upedge = min(ymax, pred_count + pred_err_up)

            err_box = TBox(xlowedge, err_lowedge, xupedge, err_upedge)
            err_box.SetLineColor(kGray + 2)
            err_box.SetFillColor(kGray + 2)
            err_box.SetLineWidth(0)
            err_box.SetFillStyle(3144)
            err_box.Draw()
            err_boxes.append(err_box)

        obs_hist.Draw('SAME P0 E0 E1')

        legend.AddEntry(obs_hist, 'Observed', 'P E1')

        if dy_file is not None:
            legend.AddEntry(dy_hist, 'Drell-Yan (predicted)', 'F')

        if qcd_file is not None:
            legend.AddEntry(qcd_hist, 'QCD (predicted)', 'F')

        legend.AddEntry(err_boxes[0], 'Stat. uncertainty', 'F')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')
        frame_top.Draw('SAME AXIS X+')
        frame_top.Draw('SAME AXIS Y+')

        legend.Draw()

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("bottom", "bottom", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(1. - bottom_height)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw Ratio
        _, _, rmin, rmax = get_th1_ideal_ranges(ratio_hist)

        if user_rmin is not None:
            rmin = user_rmin

        if user_rmax is not None:
            rmax = user_rmax

        frame_bottom = plot_canvas.DrawFrame(
            xmin, max(rmin, 0.),
            xmax, rmax,
            ''
        )

        frame_bottom.GetXaxis().SetTitle(xtitle)
        frame_bottom.GetYaxis().SetTitle(rtitle)
        frame_bottom.GetYaxis().SetNdivisions(204)

        for ibin in range(bkg_hist.GetNbinsX()):
            ibin += 1
            xlowedge = bkg_hist.GetBinLowEdge(ibin)
            xupedge = xlowedge + bkg_hist.GetBinWidth(ibin)

            pred_count = Constant(0)

            for hist in bkg_hist_col:
                bin_count = Measurement(
                    hist.GetBinContent(ibin),
                    hist.GetBinError(ibin)
                )
                pred_count = pred_count.add(bin_count)

            pred_err_low = pred_count.error
            pred_err_up = pred_count.error
            pred_count = pred_count.value

            pred_err_low = min(pred_err_low / pred_count if pred_count > 0 else 1, 1)
            pred_err_up = min(pred_err_up / pred_count if pred_count > 0 else 999, 1)
            err_lowedge = max(rmin, 1 - pred_err_low)
            err_upedge = min(rmax, 1 + pred_err_up)

            err_box = TBox(xlowedge, err_lowedge, xupedge, err_upedge)
            err_box.SetLineColor(kGray + 2)
            err_box.SetFillColor(kGray + 2)
            err_box.SetFillStyle(3144)
            err_box.Draw()
            err_boxes.append(err_box)

        ratio_hist.Draw('SAME P E1')

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')
        frame_bottom.Draw('SAME AXIS X+')
        frame_bottom.Draw('SAME AXIS Y+')

        # Draw x Target
        if user_rtarget is not None:
            r_target_line = TLine(xmin, user_rtarget, xmax, user_rtarget)
            r_target_line.SetLineStyle(10)
            r_target_line.SetLineColor(kRed)
            r_target_line.Draw('SAME')

        # Draw Titles
        plot_canvas.cd(0)

        main_title = TLatex(0.045, 0.95, '')
        main_title.SetTextAlign(12)
        main_title.Draw()

        plot_canvas.SaveAs(f'{output_name}.png')
