from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.mini.commons.bins import ddm_lz_bins, ddm_min_threshold


class LLPDistributionAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.h_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'h_mass',
            'LLP Distribution',
            'GEN m_{H} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 1000),
            density=True,
            logy=True)

        self.llp_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_mass',
            'LLP Distribution',
            'GEN m_{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.delta_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'delta_mass',
            'LLP Distribution',
            'GEN m_{H} - m_{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 1000),
            density=True,
            logy=True)

        self.llp_mass_vs_h_mass_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_mass_vs_h_mass', 'LLP Distribution',
            'GEN m_{H} [GeV]', 'GEN m_{LLP} [GeV]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=uniform_step_axis(10, 0, 500),
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.llp_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_pt',
            'LLP Distribution',
            'GEN p_{T}^{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.llp_beta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_beta',
            'LLP Distribution',
            'GEN #beta', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.llp_ctau_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ctau',
            'LLP Distribution',
            'GEN c#tau [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.llp_ctau_vs_llp_mass_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_ctau_vs_llp_mass', 'LLP Distribution',
            'GEN m_{LLP} [GeV]', 'GEN c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 500),
            ybins=ddm_lz_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.llp_ctau_vs_h_mass_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_ctau_vs_h_mass', 'LLP Distribution',
            'GEN m_{H} [GeV]', 'GEN c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=ddm_lz_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.llp_ctau_vs_delta_mass_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_ctau_vs_delta_mass', 'LLP Distribution',
            'GEN m_{H} - m_{LLP} [GeV]', 'GEN c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=ddm_lz_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.llp_ct_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ct',
            'LLP Distribution',
            'GEN ct [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.llp_ctp_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ctp',
            'LLP Distribution',
            'GEN ct_{proper} [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.llp_4p_balance_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_4p_balance',
            'LLP Distribution',
            '4-Momentum Balanced', 'a.u.',
            xbins=uniform_step_axis(1, 0, 1, align='center'),
            density=True,
            logy=True)

    def process_entry(self, entry):
        for h_m, llp_m, llp_ctau, llp_tp, llp_p4, llp_vtx, llp_4p_balance in entry:
            # Check lxy
            if llp_vtx.Perp() > 300:
                continue

            # Check lz
            if abs(llp_vtx.Z()) > 500:
                continue

            # Plot
            self.h_mass_plotter.fill(h_m)
            self.llp_mass_plotter.fill(llp_m)
            self.delta_mass_plotter.fill(h_m - llp_m)
            self.llp_mass_vs_h_mass_plotter.fill(h_m, llp_m)
            self.llp_pt_plotter.fill(llp_p4.Pt())
            self.llp_beta_plotter.fill(llp_p4.Beta())
            self.llp_ctau_plotter.fill(llp_ctau * 100)
            self.llp_ctau_vs_llp_mass_plotter.fill(llp_m, llp_ctau * 100)
            self.llp_ctau_vs_h_mass_plotter.fill(h_m, llp_ctau * 100)
            self.llp_ctau_vs_delta_mass_plotter.fill(h_m - llp_m, llp_ctau * 100)
            self.llp_ct_plotter.fill(llp_vtx.T())
            self.llp_ctp_plotter.fill(llp_tp)
            self.llp_4p_balance_plotter.fill(llp_4p_balance * 1.)

    def merge(self, other):
        pass
