import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.commons.kinematics import calc_dxy, calc_d0, calc_z0_cmssw, calc_d0_cmssw
from ddm.core.commons.tools import nz_sign
from ddm.core.execution.runtime import get_logger
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.mini.commons.bins import ddm_d0_sign_bins, ddm_d0_bins, ddm_z0_bins, ddm_lz_bins, \
    ddm_uz0_bins, ddm_min_threshold, ddm_lxy_bins, ddm_mismatch_threshold, ddm_q_bins, \
    ddm_dxy_bins, ddm_abs_eta_bins, ddm_phi_deg_bins, ddm_pt_bins


class MuonDistributionAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # Counters
        self.muon_count = 0

        # Plots
        self.pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_pt',
            'Muon Distribution',
            'GEN p_{T} [GeV]', 'a.u.',
            xbins=ddm_pt_bins,
            density=True,
            logy=True)

        self.lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lxy',
            'Muon Distribution',
            'GEN L_{xy} [cm]', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lz',
            'Muon Distribution',
            'GEN L_{z} [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0',
            'Muon Distribution',
            'GEN d_{0} [cm]', 'a.u.',
            xbins=ddm_d0_bins,
            density=True,
            logy=True)

        self.d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0_sign',
            'Muon Distribution',
            'GEN Sign', 'a.u.',
            xbins=ddm_d0_sign_bins,
            density=True,
            logy=True)

        self.z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_z0',
            'Muon Distribution',
            'GEN z_{0} [cm]', 'a.u.',
            xbins=ddm_z0_bins,
            density=True,
            logy=True)

        self.lz_vs_z0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_lz_vs_z0', 'Muon Distribution',
            'GEN z_{0} [cm]', 'GEN L_{z} [cm]',
            xbins=ddm_uz0_bins,
            ybins=ddm_lz_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.lxy_vs_z0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_lxy_vs_z0', 'Muon Distribution',
            'GEN z_{0} [cm]', 'GEN L_{xy} [cm]',
            xbins=ddm_uz0_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.lxy_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_lxy_vs_lz', 'Muon Distribution',
            'GEN L_{z} [cm]', 'GEN L_{xy} [cm]',
            xbins=ddm_lz_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        self.lxy_vs_d0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_lxy_vs_d0',
            'Muon Distribution',
            'GEN d_{0} [cm]', 'GEN L_{xy} [cm]',
            xbins=ddm_d0_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_mismatch_threshold,
            density=True,
            logz=True
        )

        self.q_vs_d0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_q_vs_d0',
            'Muon Distribution',
            'GEN d_{0} [cm]', 'GEN q',
            xbins=ddm_d0_bins,
            ybins=ddm_q_bins,
            min_val=ddm_mismatch_threshold,
            density=True,
            logz=True
        )

        self.cmssw_vs_d0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_cmssw_vs_d0',
            'Muon Distribution',
            'GEN d_{0} [cm]', 'GEN CMSSW d_{0} [cm]',
            xbins=ddm_d0_bins,
            ybins=ddm_d0_bins,
            min_val=ddm_mismatch_threshold,
            density=True,
            logz=True
        )

        self.cmssw_vs_dxy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_cmssw_vs_dxy',
            'Muon Distribution',
            'GEN d_{xy} [cm]', 'GEN CMSSW d_{0} [cm]',
            xbins=ddm_d0_bins,
            ybins=ddm_d0_bins,
            min_val=ddm_mismatch_threshold,
            density=True,
            logz=True
        )

        self.dxy_vs_d0_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_dxy_vs_d0',
            'Muon Distribution',
            'GEN d_{0} [cm]', 'GEN d_{xy} [cm]',
            xbins=ddm_d0_bins,
            ybins=ddm_dxy_bins,
            min_val=ddm_mismatch_threshold,
            density=True,
            logz=True
        )

        self.eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'Muon Distribution',
            'GEN |#eta|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'Muon Distribution',
            'GEN #phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

        self.n_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_n',
            'Muon Distribution',
            'GEN n', 'a.u.',
            xbins=uniform_step_axis(1, 0, 10, align='center'),
            density=True,
            logy=True)

    def process_entry(self, entry):
        valid_muon_count = 0

        for gen_q, gen_p4, gen_vtx in entry:
            gen_pt, gen_eta, gen_phi = gen_p4.Pt(), gen_p4.Eta(), gen_p4.Phi()
            gen_vx, gen_vy, gen_vz = gen_vtx.X(), gen_vtx.Y(), gen_vtx.Z()
            gen_dxy = calc_dxy(gen_q, gen_pt, gen_phi, gen_vx, gen_vy)
            gen_d0 = calc_d0(gen_q, gen_pt, gen_phi, gen_vx, gen_vy)
            gen_d0_cmssw = calc_d0_cmssw(gen_phi, gen_vx, gen_vy)
            gen_z0_cmssw = calc_z0_cmssw(gen_pt, gen_eta, gen_phi, gen_vx, gen_vy, gen_vz)
            gen_lxy = gen_vtx.Perp()
            gen_lz = abs(gen_vz)

            # Check lxy
            if gen_lxy > 300:
                continue

            # Check lz
            if gen_lz > 500:
                continue

            # Global Counts
            self.muon_count += 1

            # Local Counts
            valid_muon_count += 1

            # Plot
            self.pt_plotter.fill(gen_pt)
            self.lxy_plotter.fill(gen_lxy)
            self.lz_plotter.fill(gen_lz)
            self.d0_sign_plotter.fill(nz_sign(gen_d0))
            self.d0_plotter.fill(gen_d0)
            self.z0_plotter.fill(gen_z0_cmssw)
            self.lz_vs_z0_plotter.fill(abs(gen_z0_cmssw), gen_lz)
            self.lxy_vs_z0_plotter.fill(abs(gen_z0_cmssw), gen_lxy)
            self.q_vs_d0_plotter.fill(gen_d0, gen_q)
            self.lxy_vs_lz_plotter.fill(gen_lz, gen_lxy)
            self.lxy_vs_d0_plotter.fill(gen_d0, gen_lxy)
            self.cmssw_vs_d0_plotter.fill(gen_d0, gen_d0_cmssw)
            self.cmssw_vs_dxy_plotter.fill(gen_dxy, gen_d0_cmssw)
            self.dxy_vs_d0_plotter.fill(gen_d0, gen_dxy)
            self.eta_plotter.fill(abs(gen_eta))
            self.phi_plotter.fill(np.rad2deg(gen_phi))

        # Plot
        self.n_plotter.fill(valid_muon_count)

    def merge(self, other):
        pass

    def post_production(self):
        get_logger().info(f'Total Muon Count: {self.muon_count:d}')
