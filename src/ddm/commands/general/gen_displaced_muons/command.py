import math
import random

from ROOT import TLorentzVector

from ddm.commands.general.gen_displaced_muons.analyzers.llp_distributions import LLPDistributionAnalyzer
from ddm.commands.general.gen_displaced_muons.analyzers.muon_distributions import MuonDistributionAnalyzer
from ddm.core.commands import Command
from ddm.core.commons.kinematics import calc_theta_rad_from_eta
from ddm.core.execution.runtime import get_logger

# Constants
MAX_M_H = 1000  # GeV
MIN_M_H = 20  # GeV
MAX_PT_H = 120  # GeV
MIN_PT_H = 1  # GeV
MAX_CTAU_LLP = 5000 / 1000  # m
MIN_CTAU_LLP = 10 / 1000  # m
MAX_ETA_LLP = 3.5
MIN_ETA_LLP = 1e-6
MAX_THETA_LLP = calc_theta_rad_from_eta(MIN_ETA_LLP)
MIN_THETA_LLP = calc_theta_rad_from_eta(MAX_ETA_LLP)
MAX_INVPT_H = 1 / MIN_PT_H
MIN_INVPT_H = 1 / MAX_PT_H

M_MU = 0.105  # GeV
SPEED_OF_LIGHT = 299792458  # m/s


class GenerateDisplacedMuons(Command):

    def configure_parser(self, parser):
        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to generate')

    def run(self, args):
        # Unpack args
        max_events = args.max_events

        # Analyzer
        muon_analyzer = MuonDistributionAnalyzer()
        llp_analyzer = LLPDistributionAnalyzer()

        # Generate Events
        for evt in range(max_events):
            # Calculate Higgs mass
            max_h_mass = MAX_M_H
            min_h_mass = MIN_M_H
            h_mass = random.random() * (max_h_mass - min_h_mass) + min_h_mass
            h_mass = min_h_mass if h_mass < min_h_mass else h_mass
            h_mass = max_h_mass if h_mass > max_h_mass else h_mass
            # h_mass = 125

            # Choose Random ctau
            llp_ctau = random.random() * (MAX_CTAU_LLP - MIN_CTAU_LLP) + MIN_CTAU_LLP
            llp_ctau = MIN_CTAU_LLP if llp_ctau < MIN_CTAU_LLP else llp_ctau
            llp_ctau = MAX_CTAU_LLP if llp_ctau > MAX_CTAU_LLP else llp_ctau
            # llp_ctau = 5000 / 1000

            # Calculate LLP mass
            max_llp_mass = h_mass / 2
            min_llp_mass = 10

            # max_dmass = h_mass - min_llp_mass
            # min_dmass = h_mass - max_llp_mass
            # max_invdmass = 1 / min_dmass
            # min_invdmass = 1 / max_dmass
            # dmass = 1 / (random.random() * (max_invdmass - min_invdmass) + min_invdmass)
            # llp_mass = h_mass - dmass
            # llp_mass = max_llp_mass if llp_mass > max_llp_mass else llp_mass

            llp_mass = random.random() * (max_llp_mass - min_llp_mass) + min_llp_mass
            llp_mass = min_llp_mass if llp_mass < min_llp_mass else llp_mass
            llp_mass = max_llp_mass if llp_mass > max_llp_mass else llp_mass
            # llp_mass = 50

            # Calculate LLP 4-momentum
            h_vtx, llp1_p4, llp2_p4 = self.shoot_LLP(h_mass, llp_mass)

            # Calculate Muon 4-momentum
            llp1_vtx, llp1_mu1_p4, llp1_mu2_p4, llp1_tp = self.decay_particle(llp_mass, llp_ctau, llp1_p4, M_MU)
            llp2_vtx, llp2_mu1_p4, llp2_mu2_p4, llp2_tp = self.decay_particle(llp_mass, llp_ctau, llp2_p4, M_MU)

            llp1_vtx = h_vtx + llp1_vtx
            llp2_vtx = h_vtx + llp2_vtx

            llp1_4p_balance = abs((llp1_mu1_p4 + llp1_mu2_p4 - llp1_p4).Mag()) < 1e-6
            llp2_4p_balance = abs((llp2_mu1_p4 + llp2_mu2_p4 - llp2_p4).Mag()) < 1e-6

            if not llp1_4p_balance:
                get_logger().warn('LLP1 4-momentum not balanced')

            if not llp2_4p_balance:
                get_logger().warn('LLP2 4-momentum not balanced')

            # Analyze LLP
            llps = list()
            llps.append((
                h_mass, llp_mass, llp_ctau, llp1_tp, llp1_p4, llp1_vtx, llp1_4p_balance
            ))
            llps.append((
                h_mass, llp_mass, llp_ctau, llp2_tp, llp2_p4, llp2_vtx, llp2_4p_balance
            ))

            llp_analyzer.process_entry(llps)

            # Analyze Muons
            muons = list()

            mu_q = (-1 if (random.random() < 0.5) else 1)
            muons.append((mu_q, llp1_mu1_p4, llp1_vtx))
            muons.append((-mu_q, llp1_mu2_p4, llp1_vtx))

            mu_q = (-1 if (random.random() < 0.5) else 1)
            muons.append((mu_q, llp2_mu1_p4, llp2_vtx))
            muons.append((-mu_q, llp2_mu2_p4, llp2_vtx))

            muon_analyzer.process_entry(muons)

        # Write
        muon_analyzer.write()
        llp_analyzer.write()

    def shoot_LLP(self, h_mass, llp_mass):
        # Calculate width
        h_width = 0.027 * h_mass
        h_ctau = 0.19733e-15 / h_width

        # Calculate h 4-momentum in Lab Frame
        randval = random.random()
        h_endcap = (-1 if (random.random() < 0.5) else 1)
        h_pt = 1 / math.exp((1 - randval) * math.log(MIN_INVPT_H) + randval * math.log(MAX_INVPT_H))
        h_eta = h_endcap * (random.random() * (MAX_ETA_LLP - MIN_ETA_LLP) + MIN_ETA_LLP)
        h_theta = 2 * math.atan(math.exp(-h_eta))

        h_phi = random.random() * 2 * math.pi - math.pi
        h_p = h_pt / math.sin(h_theta)
        h_e = math.hypot(h_p, h_mass)
        h_px = h_p * math.sin(h_theta) * math.cos(h_phi)
        h_py = h_p * math.sin(h_theta) * math.sin(h_phi)
        h_pz = h_p * math.cos(h_theta)

        h_p4 = TLorentzVector(h_px, h_py, h_pz, h_e)

        # Decay h
        h_vtx, llp1_p4, llp2_p4, h_ctp = self.decay_particle(h_mass, h_ctau, h_p4, llp_mass)

        return h_vtx, llp1_p4, llp2_p4

    def decay_particle(self, parent_mass, parent_ctau, parent_p4, daughter_mass):
        # Calculate beta
        parent_beta3 = parent_p4.BoostVector()

        # Calculate daughter momentum in parent frame
        daughter_p = 0.5 * math.sqrt(parent_mass ** 2 - 4 * daughter_mass ** 2)
        daughter_e = parent_mass / 2.
        daughter_theta = random.random() * math.pi - math.pi / 2
        daughter_phi = random.random() * 2 * math.pi - math.pi
        daughter_px = daughter_p * math.sin(daughter_theta) * math.cos(daughter_phi)
        daughter_py = daughter_p * math.sin(daughter_theta) * math.sin(daughter_phi)
        daughter_pz = daughter_p * math.cos(daughter_theta)

        # Boost daughter 4-momentum to lab frame
        daughter1_p4 = TLorentzVector(daughter_px, daughter_py, daughter_pz, daughter_e)
        daughter1_p4.Boost(parent_beta3)

        daughter2_p4 = TLorentzVector(-daughter_px, -daughter_py, -daughter_pz, daughter_e)
        daughter2_p4.Boost(parent_beta3)

        # Calculate decay time
        parent_ctp = - parent_ctau * math.log(1 - random.random()) * 100  # cm

        # Boost decay vertex to lab frame
        decay_vtx = TLorentzVector(0, 0, 0, parent_ctp)
        decay_vtx.Boost(parent_beta3)

        return decay_vtx, daughter1_p4, daughter2_p4, parent_ctp
