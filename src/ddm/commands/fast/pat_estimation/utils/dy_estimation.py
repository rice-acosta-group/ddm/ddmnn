from ddm.core.commons.measurements import Measurement
from ddm.core.execution.runtime import get_logger


def subtract_qcd(
        total_hist, qcd_hist
):
    for bin_id in range(0, total_hist.GetNbinsX() + 2):
        total_count = Measurement(
            total_hist.GetBinContent(bin_id),
            total_hist.GetBinError(bin_id)
        )

        qcd_count = Measurement(
            qcd_hist.GetBinContent(bin_id),
            qcd_hist.GetBinError(bin_id)
        )

        dy_count = total_count.subtract(qcd_count)

        if dy_count.value < 0:
            get_logger().warn(f"\tSetting Drell-Yan Content {dy_count.value:5.3f} to 0")
            dy_count.value = 0.0

        if dy_count.value > 1e9:
            get_logger().warn(f"\tSetting Drell-Yan Content {dy_count.value:5.3f} to 1e9")
            dy_count.value = 1e9

        total_hist.SetBinContent(bin_id, dy_count.value)
        total_hist.SetBinError(bin_id, dy_count.error)
