from ROOT import TFile

from ddm.commands.fast.pat_estimation.analyzers.abcd_estimator import ABCDEstimator
from ddm.commands.fast.pat_estimation.config.dy_regions import get_dy_regions
from ddm.commands.fast.pat_estimation.utils.dy_estimation import subtract_qcd
from ddm.core.commons.histograms import get_th1_integral
from ddm.core.execution.runtime import get_logger
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class EstimatePATDY(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='Fraction of events to process')

        parser.add_argument('--cuts', dest='cuts', metavar='CUTS',
                            type=str, nargs='+', default=[],
                            help='Ordered list of cuts to test')

        parser.add_argument('--var', dest='varname', metavar='VARIABLE_NAME',
                            type=str, default=None,
                            help='Variable name to plot')

        parser.add_argument('--bin-edges', dest='bin_edges', metavar='BIN_EDGES',
                            type=str, default=None,
                            help='Bin edges')

        parser.add_argument('--tf', dest='fixed_tf', metavar='TRANSFER_FACTOR',
                            type=float, default=None,
                            help='Override calculated transfer factor')

        parser.add_argument('--flat-tf', dest='flat_tf_en',
                            action='store_true', default=False,
                            help='Uses a flat transfer factor in place of one calculated per bin')

    def configure(self, args):
        self.cuts = args.cuts
        self.varname = args.varname
        self.bin_edges = args.bin_edges
        self.fixed_tf = args.fixed_tf
        self.flat_dy_tf_en = args.flat_tf_en
        self.flat_qcd_tf_en = args.varname == 'mass'

    def create_producers(self):
        return [
            FormulaBranchProducer(**get_dy_regions(self.cuts))
        ]

    def create_analyzers(self):
        analyzers = [
            ABCDEstimator(
                'dim_pat_dy', self.varname, 'DY',
                self.flat_dy_tf_en, self.fixed_tf,
                'dy_control', 'dy_tf_num', 'dy_tf_den',
                self.bin_edges
            )
        ]

        if self.fixed_tf is None:
            analyzers.append(ABCDEstimator(
                'dim_pat_dy_tf_num_qcd', self.varname, 'DY TF_{num} QCD',
                self.flat_qcd_tf_en, None,
                'dy_tf_num_qcd_control', 'dy_tf_num_qcd_tf_num', 'dy_tf_num_qcd_tf_den',
                self.bin_edges
            ))

            analyzers.append(ABCDEstimator(
                'dim_pat_dy_tf_den_qcd', self.varname, 'DY TF_{den} QCD',
                self.flat_qcd_tf_en, None,
                'dy_tf_den_qcd_control', 'dy_tf_den_qcd_tf_num', 'dy_tf_den_qcd_tf_den',
                self.bin_edges
            ))

        return analyzers

    def consolidate(self):
        # Subtract QCD
        if self.fixed_tf is None:
            # Unpack analyzers
            dy_estimator = self.analyzers[0]
            dy_num_qcd_estimator = self.analyzers[1]
            dy_den_qcd_estimator = self.analyzers[2]

            # Estimate numerator and denominator QCD
            get_logger().info('Estimate DY TF Num QCD')
            dy_num_qcd_estimator.consolidate()

            get_logger().info('Estimate DY TF Den QCD')
            dy_den_qcd_estimator.consolidate()

            # Subtract QCD from numerator and denominator
            num_plot = dy_estimator.transfer_factor.num_hist
            den_plot = dy_estimator.transfer_factor.den_hist
            num_qcd_plot = dy_num_qcd_estimator.estimate.plot
            den_qcd_plot = dy_den_qcd_estimator.estimate.plot

            get_logger().info("Begin Subtract QCD")
            num_int = get_th1_integral(num_plot)
            den_int = get_th1_integral(den_plot)
            get_logger().info(f"\tQCD+DY - Numerator: {num_int.value:5.3f}+/-{num_int.error:5.3f}")
            get_logger().info(f"\tQCD+DY - Denominator: {den_int.value:5.3f}+/-{den_int.error:5.3f}")

            subtract_qcd(num_plot, num_qcd_plot)
            subtract_qcd(den_plot, den_qcd_plot)

            num_int = get_th1_integral(num_plot)
            den_int = get_th1_integral(den_plot)
            get_logger().info(f"\tDY - Numerator: {num_int.value:5.3f}+/-{num_int.error:5.3f}")
            get_logger().info(f"\tDY - Denominator: {den_int.value:5.3f}+/-{den_int.error:5.3f}")
            get_logger().info("End Subtract QCD")

            # Estimate DY
            dy_estimator.consolidate()

        # Write
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
