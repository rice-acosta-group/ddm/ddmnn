from ddm.commands.fast.pat_estimation.config.qcd_regions import get_qcd_cuts
from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.cuts.cut_utils import remove_duplicate_cuts


def get_dy_regions(cuts):
    # Initialize selection dict
    selections = dict()

    # Generate DY selections
    dy_control_cuts, dy_tf_num_cuts, dy_tf_den_cuts = get_dy_cuts(cuts)
    control_branch_name = 'dim_fsel_dy_control'
    tf_num_branch_name = 'dim_fsel_dy_tf_num'
    tf_den_branch_name = 'dim_fsel_dy_tf_den'

    selections[control_branch_name] = get_cutstring('pat', dy_control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', dy_tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', dy_tf_den_cuts, 2022, False)

    # Generate TF numerator QCD selections
    control_cuts, tf_num_cuts, tf_den_cuts = get_qcd_cuts(dy_tf_num_cuts)
    control_branch_name = 'dim_fsel_dy_tf_num_qcd_control'
    tf_num_branch_name = 'dim_fsel_dy_tf_num_qcd_tf_num'
    tf_den_branch_name = 'dim_fsel_dy_tf_num_qcd_tf_den'

    selections[control_branch_name] = get_cutstring('pat', control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', tf_den_cuts, 2022, False)

    # Generate TF denominator QCD selections
    control_cuts, tf_num_cuts, tf_den_cuts = get_qcd_cuts(dy_tf_den_cuts)
    control_branch_name = 'dim_fsel_dy_tf_den_qcd_control'
    tf_num_branch_name = 'dim_fsel_dy_tf_den_qcd_tf_num'
    tf_den_branch_name = 'dim_fsel_dy_tf_den_qcd_tf_den'

    selections[control_branch_name] = get_cutstring('pat', control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', tf_den_cuts, 2022, False)

    # Return
    return selections


def get_dy_cuts(cuts):
    # Copy
    cuts = cuts[:]

    # Sanitize
    dphi_cut = []
    inv_dphi_cut = []
    chi2_cut = []
    inv_chi2_cut = []

    if 'DPHI1' in cuts:
        cuts.remove('DPHI1')
        dphi_cut += ['DPHI1']
        inv_dphi_cut += ['DPHI4']

    if 'DPHI2' in cuts:
        cuts.remove('DPHI2')
        dphi_cut += ['DPHI2']
        inv_dphi_cut += ['DPHI3']

    if 'DPHIPIBY30' in cuts:
        cuts.remove('DPHIPIBY30')
        dphi_cut = ['DPHIPIBY30']
        inv_dphi_cut = ['DPHIPIBY30OPP']

    if 'DPHI0p1' in cuts:
        cuts.remove('DPHI0p1')
        dphi_cut += ['DPHI0p1']
        inv_dphi_cut += ['DPHI4T']

    if 'CHI2' in cuts:
        cuts.remove('CHI2')
        chi2_cut += ['CHI2']
        inv_chi2_cut += ['ICHI2']

    # Extend
    control = remove_duplicate_cuts(cuts + chi2_cut + inv_dphi_cut)
    tf_num = remove_duplicate_cuts(cuts + inv_chi2_cut + dphi_cut)
    tf_den = remove_duplicate_cuts(cuts + inv_chi2_cut + inv_dphi_cut)

    # Return
    return control, tf_num, tf_den
