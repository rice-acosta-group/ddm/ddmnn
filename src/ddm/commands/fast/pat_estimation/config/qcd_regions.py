from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.cuts.cut_utils import remove_duplicate_cuts


def get_qcd_regions(cuts):
    # Initialize selection dict
    selections = dict()

    # Generate selections
    control_cuts, tf_num_cuts, tf_den_cuts = get_qcd_cuts(cuts)
    control_branch_name = 'dim_fsel_qcd_control'
    tf_num_branch_name = 'dim_fsel_qcd_tf_num'
    tf_den_branch_name = 'dim_fsel_qcd_tf_den'

    selections[control_branch_name] = get_cutstring('pat', control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', tf_den_cuts, 2022, False)

    # Return
    return selections


def get_qcd_cuts(cuts):
    # Copy
    cuts = cuts[:]

    # Sanitize
    charge_cut = []
    inv_charge_cut = []
    iso_cut = []
    inv_iso_cut = []

    if 'OS' in cuts:
        cuts.remove('OS')
        charge_cut += ['OS']
        inv_charge_cut += ['SS']

    if 'ISO' in cuts:
        cuts.remove('ISO')
        iso_cut += ['ISO']
        inv_iso_cut += ['IISO', 'ISO0P5']

    if 'PSCORE0p5' in cuts:
        cuts.remove('PSCORE0p5')
        charge_cut += ['PSCORE0p5']
        inv_charge_cut += ['IPSCORE0p5']

    # Extend
    control = remove_duplicate_cuts(cuts + iso_cut + inv_charge_cut)
    tf_num = remove_duplicate_cuts(cuts + inv_iso_cut + charge_cut)
    tf_den = remove_duplicate_cuts(cuts + inv_iso_cut + inv_charge_cut)

    # Return
    return control, tf_num, tf_den
