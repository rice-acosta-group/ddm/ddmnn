from ROOT import TFile

from ddm.commands.fast.pat_estimation.analyzers.abcd_estimator import ABCDEstimator
from ddm.commands.fast.pat_estimation.config.qcd_regions import get_qcd_regions
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class EstimatePATQCD(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='Fraction of events to process')

        parser.add_argument('--cuts', dest='cuts', metavar='CUTS',
                            type=str, nargs='+', default=[],
                            help='Ordered list of cuts to test')

        parser.add_argument('--var', dest='varname', metavar='VARIABLE_NAME',
                            type=str, default=None,
                            help='Variable name to plot')

        parser.add_argument('--bin-edges', dest='bin_edges', metavar='BIN_EDGES',
                            type=str, default=None,
                            help='Bin edges')

        parser.add_argument('--tf', dest='fixed_tf', metavar='TRANSFER_FACTOR',
                            type=float, default=None,
                            help='Override calculated transfer factor')

        parser.add_argument('--flat-tf', dest='flat_tf_en',
                            action='store_true', default=False,
                            help='Uses a flat transfer factor in place of one calculated per bin')

    def configure(self, args):
        self.cuts = args.cuts
        self.varname = args.varname
        self.bin_edges = args.bin_edges
        self.fixed_tf = args.fixed_tf
        self.flat_tf_en = args.flat_tf_en

        # Sanity checks
        if self.fixed_tf is not None:
            raise NotImplementedError('Fixed TF has not been implemented yet')

    def create_producers(self):
        return [
            FormulaBranchProducer(**get_qcd_regions(self.cuts))
        ]

    def create_analyzers(self):
        return [
            ABCDEstimator(
                'dim_pat_qcd', self.varname, 'QCD',
                self.flat_tf_en, self.fixed_tf,
                'qcd_control', 'qcd_tf_num', 'qcd_tf_den',
                self.bin_edges
            )
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
