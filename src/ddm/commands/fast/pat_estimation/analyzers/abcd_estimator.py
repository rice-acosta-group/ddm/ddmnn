from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import parse_bin_edges
from ddm.core.commons.measurements import Constant
from ddm.core.commons.transfer_factors import TransferFactor
from ddm.core.plotters.basic import Hist1DPlotter
from ddm.core.plotters.physics import TransferFactorPlotter
from ddm.fast.variables.variable_registry import get_var


class ABCDEstimator(AbstractAnalyzer):

    def __init__(
            self, prefix, varname, crname,
            flat_tf_en=False, fixed_tf=False,
            control_tag='control', tf_num_tag='tf_num', tf_den_tag='tf_den',
            bin_edges=None
    ):
        super().__init__()

        # Init variables
        self.var_tag = f'fvar_pat_{varname}'
        self.control_tag = f'fsel_{control_tag}'
        self.tf_num_tag = f'fsel_{tf_num_tag}'
        self.tf_den_tag = f'fsel_{tf_den_tag}'
        self.flat_tf_en = flat_tf_en
        self.fixed_tf = fixed_tf

        # Checkout histograms
        var = get_var('pat', varname)
        bin_edges = parse_bin_edges(bin_edges)

        if bin_edges is None:
            bin_edges = var.bin_edges

        self.estimate = self.checkout_plotter(
            Hist1DPlotter,
            f'{prefix}_{varname}_estimate',
            f'PAT Dimuon {crname} Estimate',
            var.get_latex(), 'a.u.',
            xbins=bin_edges,
            logy=True)

        self.control = self.checkout_plotter(
            Hist1DPlotter,
            f'{prefix}_{varname}_control',
            f'PAT Dimuon {crname} Control',
            var.get_latex(), 'a.u.',
            xbins=bin_edges,
            logy=True)

        if self.fixed_tf is None:
            self.transfer_factor = self.checkout_plotter(
                TransferFactorPlotter,
                f'{prefix}_{varname}_tf',
                f'PAT Dimuon {crname} TF',
                var.get_latex(), 'a.u.', 'Ratio',
                num_label='NUM', den_label='DEN',
                xbins=bin_edges,
                logy=True)

    def process_entry(self, entry):
        for dimuon in entry.ext_dimuons:
            if getattr(dimuon, self.control_tag, False):
                self.control.fill(getattr(dimuon, self.var_tag))
            elif self.fixed_tf is None:
                if getattr(dimuon, self.tf_num_tag, False):
                    self.transfer_factor.fill_numerator(getattr(dimuon, self.var_tag))
                elif getattr(dimuon, self.tf_den_tag, False):
                    self.transfer_factor.fill_denominator(getattr(dimuon, self.var_tag))

    def merge(self, other):
        self.control.add(other.control)

        if self.fixed_tf is None:
            self.transfer_factor.add(other.transfer_factor)

    def post_production(self):
        # Get Transfer Factor
        if self.fixed_tf is None:
            tf = TransferFactor(
                num_hist=self.transfer_factor.num_hist,
                den_hist=self.transfer_factor.den_hist
            )
        else:
            tf = TransferFactor(
                value=Constant(self.fixed_tf)
            )

        # Apply transfer factor
        tf.apply(self.control.plot, self.estimate.plot, flat_tf_en=self.flat_tf_en)
