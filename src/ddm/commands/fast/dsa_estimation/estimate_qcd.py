from ROOT import TFile

from ddm.commands.fast.dsa_estimation.analyzers.qcd_estimator import QCDEstimator
from ddm.commands.fast.dsa_estimation.config.qcd_regions import get_qcd_regions
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class EstimateDSAQCD(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='Fraction of events to process')

        parser.add_argument('--var', dest='varname', metavar='VARIABLE_NAME',
                            type=str, default=None,
                            help='Variable name to plot')

    def configure(self, args):
        self.varname = args.varname

    def create_producers(self):
        return [
            FormulaBranchProducer(**get_qcd_regions())
        ]

    def create_analyzers(self):
        return [
            QCDEstimator('dim_dsa_qcd', self.varname, control_tag='qcd_control', tf_num_tag='qcd_tf_num', tf_den_tag='qcd_tf_den'),
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
