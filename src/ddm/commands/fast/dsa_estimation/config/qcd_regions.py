from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.cuts.cut_utils import remove_duplicate_cuts


def get_qcd_regions(cuts):
    # Initialize selection dict
    selections = dict()

    # Generate selections
    control_cuts, tf_num_cuts, tf_den_cuts = get_qcd_cuts(cuts)
    control_branch_name = 'dim_fsel_qcd_control'
    tf_num_branch_name = 'dim_fsel_qcd_tf_num'
    tf_den_branch_name = 'dim_fsel_qcd_tf_den'

    selections[control_branch_name] = get_cutstring('pat', control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', tf_den_cuts, 2022, False)

    # Return
    return selections


def get_qcd_cuts(cuts):
    # Copy
    cuts = cuts[:]

    # Sanitize
    charge_cut = []
    inv_charge_cut = []
    iso_cut = []
    inv_iso_cut = []

    if 'OS' in cuts:
        cuts.remove('OS')
        cuts.remove('DETASEG')
        cuts.remove('SEG')
        cuts.remove('DSATIME')

        charge_cut += ['OS', 'DETASEG', 'SEG', 'DSATIME']
        inv_charge_cut += ['SS']

    if 'DSAISO0p15' in cuts:
        cuts.remove('DSAISO0p15')
        iso_cut += ['DSAISO0p15']
        inv_iso_cut += ['IDSAISO0p15']

    # Extend
    control = remove_duplicate_cuts(cuts + charge_cut + inv_iso_cut)
    tf_num = remove_duplicate_cuts(cuts + inv_charge_cut + iso_cut)
    tf_den = remove_duplicate_cuts(cuts + inv_charge_cut + inv_iso_cut)

    # Return
    return control, tf_num, tf_den
