from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.cuts.cut_utils import remove_duplicate_cuts


def get_dy_regions(cuts):
    # Initialize selection dict
    selections = dict()

    # Generate DY selections
    dy_control_cuts, dy_tf_num_cuts, dy_tf_den_cuts = get_dy_cuts(cuts)
    control_branch_name = 'dim_fsel_dy_control'
    tf_num_branch_name = 'dim_fsel_dy_tf_num'
    tf_den_branch_name = 'dim_fsel_dy_tf_den'

    selections[control_branch_name] = get_cutstring('pat', dy_control_cuts, 2022, False)
    selections[tf_num_branch_name] = get_cutstring('pat', dy_tf_num_cuts, 2022, False)
    selections[tf_den_branch_name] = get_cutstring('pat', dy_tf_den_cuts, 2022, False)

    # Return
    return selections


def get_dy_cuts(cuts):
    # Copy
    cuts = cuts[:]

    # Sanitize
    dphi_cut = []
    inv_dphi_cut = []
    rep_cut = []
    inv_rep_cut = []

    if 'DPHI' in cuts:
        cuts.remove('DPHI')
        dphi_cut += ['DPHI']
        inv_dphi_cut += ['IDPHI3']

    if 'DPHIb10' in cuts:
        cuts.remove('DPHIb10')
        dphi_cut += ['DPHIb10']
        inv_dphi_cut += ['iDPHIb10']

    if 'REP' in cuts:
        cuts.remove('REP')
        cuts.remove('DSAISO0p15')
        rep_cut += ['REP', 'DSAISO0p15']
        inv_rep_cut += ['DSA3', 'DYTFPATCUTS']

    # Extend
    control = remove_duplicate_cuts(cuts + rep_cut + inv_dphi_cut)
    tf_num = remove_duplicate_cuts(cuts + inv_rep_cut + dphi_cut)
    tf_den = remove_duplicate_cuts(cuts + inv_rep_cut + inv_dphi_cut)

    # Return
    return control, tf_num, tf_den
