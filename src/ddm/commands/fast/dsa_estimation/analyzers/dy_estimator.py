from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.measurements import Measurement
from ddm.core.commons.transfer_factors import TransferFactor
from ddm.core.plotters.basic import Hist1DPlotter
from ddm.core.plotters.physics import TransferFactorPlotter
from ddm.fast.variables.variable_registry import get_var


class DYEstimator(AbstractAnalyzer):

    def __init__(
            self, prefix, varname,
            control_tag='control',
            tf_num_tag='tf_num',
            tf_den_tag='tf_den',
    ):
        super().__init__()

        # Init variables
        self.var_tag = f'fvar_dsa_{varname}'
        self.control_tag = f'fsel_{control_tag}'
        self.tf_num_tag = f'fsel_{tf_num_tag}'
        self.tf_den_tag = f'fsel_{tf_den_tag}'

        # Checkout histograms
        var = get_var('dsa', varname)

        self.estimate = self.checkout_plotter(
            Hist1DPlotter,
            f'{prefix}_{varname}_estimate',
            'DSA Dimuon DY Estimate',
            var.get_latex(), 'a.u.',
            xbins=var.bin_edges,
            logy=True)

        self.control = self.checkout_plotter(
            Hist1DPlotter,
            f'{prefix}_{varname}_control',
            'DSA Dimuon DY Control',
            var.get_latex(), 'a.u.',
            xbins=var.bin_edges,
            logy=True)

        self.transfer_factor = self.checkout_plotter(
            TransferFactorPlotter,
            f'{prefix}_{varname}_tf',
            'DSA Dimuon DY TF',
            var.get_latex(), 'a.u.', 'Ratio',
            num_label='NUM', den_label='DEN',
            xbins=var.bin_edges,
            logy=True)

    def process_entry(self, entry):
        for dimuon in entry.ext_dimuons:
            if getattr(dimuon, self.control_tag, False):
                self.control.fill(getattr(dimuon, self.var_tag))
            elif getattr(dimuon, self.tf_num_tag, False):
                self.transfer_factor.fill_numerator(getattr(dimuon, self.var_tag))
            elif getattr(dimuon, self.tf_den_tag, False):
                self.transfer_factor.fill_denominator(getattr(dimuon, self.var_tag))

    def merge(self, other):
        self.control.add(other.control)
        self.transfer_factor.add(other.transfer_factor)

    def post_production(self):
        # Get Transfer Factor
        tf = TransferFactor(
            num_hist=self.transfer_factor.num_hist,
            den_hist=self.transfer_factor.den_hist
        )

        # Multiply control histogram by transfer factor
        control = self.control.plot
        estimate = self.estimate.plot

        for bin_id in range(0, control.GetNbinsX() + 2):
            control_count = Measurement(
                control.GetBinContent(bin_id),
                control.GetBinError(bin_id)
            )

            scaled_count = control_count.multiply(tf.value(bin_id))

            estimate.SetBinContent(bin_id, scaled_count.value)
            estimate.SetBinError(bin_id, scaled_count.error)
