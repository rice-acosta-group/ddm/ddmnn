import numpy as np

from ddm.commands.mini.check_performance.utils.pat import encode_pat_flags
from ddm.commands.mini.check_performance.utils.plotters import DimuonHistPlotter, DimuonCompositionPlotter, DimuonEfficiencyPlotter
from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.plotters.basic import Hist2DPlotter, Hist1DPlotter
from ddm.mini.commons.bins import pt_eff_bins, lxy_eff_bins, eta_eff_bins, phi_deg_eff_bins
from ddm.mini.utils.dimuon_matching import match_gen_dimuons

lxy_bins = np.concatenate((
    np.arange(0.0, 100.0, 2.0),
    np.arange(100.0, 208.0, 4.0),
    np.array([
        208.0, 214.0, 220.0, 230.0,
        240.0, 255.0, 270.0, 290.0,
        310.0, 340.0, 370.0, 400.0,
    ]),
))


class DimuonPerfomance(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # Histograms
        self.dim_kind_vs_lxy = self.checkout_plotter(
            DimuonHistPlotter,
            'dim_kind_vs_lxy', 'Dimuon Distribution (RECO)',
            'LLP L_{xy} [cm]', 'a.u.',
            xbins=lxy_bins)

        self.dim_fraction_vs_lxy = self.checkout_plotter(
            DimuonCompositionPlotter,
            'dim_fraction_vs_lxy', 'Dimuon Composition',
            'LLP L_{xy} [cm]', 'Fraction of Dimuon',
            xbins=lxy_bins)

        # Flags
        self.dim_llp_deltaR_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_llp_deltaR',
            'Dimuon Distribution',
            '#Delta R(LLP, Dimuon)', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.dim_llp_deltaVtx_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'dim_llp_deltaVtx',
            'Dimuon Distribution',
            '#Delta Vtx(LLP, Dimuon) [cm]', 'a.u.',
            xbins=uniform_step_axis(1, 0, 50),
            density=True,
            logy=True)

        self.dim_pat_flags_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'dim_pat_flags',
            'Dimuon Distribution',
            'LLP L_{xy} [cm]', 'PAT flags',
            xbins=lxy_bins,
            ybins=uniform_step_axis(1, 0, 15, align='center'),
            ylabels=[
                'None', 'Global (G)', 'Medium (M)', 'GM', 'Tracker (T)', 'GT', 'MT', 'GMT',
                'Disp (D)', 'GD', 'MD', 'GMD', 'TD', 'GTD', 'MTD', 'GMTD'
            ],
            min_val=0,
            density=False, normalize_by='column')

        # Efficiency
        self.dim_eff_vs_pt = self.checkout_plotter(
            DimuonEfficiencyPlotter,
            'dim_eff_vs_pt', 'Dimuon Reconstruction',
            'LLP p_{T} [GeV]', 'Efficiency',
            xbins=pt_eff_bins)

        self.dim_eff_vs_eta = self.checkout_plotter(
            DimuonEfficiencyPlotter,
            'dim_eff_vs_eta', 'Dimuon Reconstruction',
            'LLP #eta', 'Efficiency',
            xbins=eta_eff_bins)

        self.dim_eff_vs_phi = self.checkout_plotter(
            DimuonEfficiencyPlotter,
            'dim_eff_vs_phi', 'Dimuon Reconstruction',
            'LLP #phi', 'Efficiency',
            xbins=phi_deg_eff_bins)

        self.dim_eff_vs_lxy = self.checkout_plotter(
            DimuonEfficiencyPlotter,
            'dim_eff_vs_lxy', 'Dimuon Reconstruction',
            'LLP L_{xy} [cm]', 'Efficiency',
            xbins=lxy_eff_bins)

    def process_entry(self, entry):
        # Select gen_dimuons
        sel_gen_dimuons = list()

        for gen_dimuon in entry.ext_gen_dimuons:
            if gen_dimuon.lxy >= 500.:
                continue

            if abs(gen_dimuon.z) >= 700.:
                continue

            any_failed = False

            for gen_muon in (gen_dimuon.mu1, gen_dimuon.mu2):
                # Check momentum
                if gen_muon.pt <= 25.:
                    any_failed = True
                    break

                # Check position at station 2
                mom, pos = gen_muon.at_st2

                if abs(pos.Eta()) > 2.4:
                    any_failed = True
                    break

            if any_failed:
                continue

            sel_gen_dimuons.append(gen_dimuon)

        # Short-Circuit: No Dimuons
        if len(sel_gen_dimuons) == 0:
            return

        # Match gen dimuons to reco dimuons
        sel_dimuons = [
            dimuon for dimuon in entry.ext_dimuons
            if dimuon.is_base
        ]

        matched_reco_dimuon = {
            gen_dimuon.idx: reco_dimuon
            for gen_dimuon, reco_dimuon, _
            in match_gen_dimuons(sel_gen_dimuons, sel_dimuons, deltaR_threshold=0.05, deltaVtx_threshold=20.0)
        }

        # Get RECO
        def get_reco_muon(ref_muon):
            if ref_muon.kind == 'PAT':
                return entry.ext_pat_muons[ref_muon.idx]
            elif ref_muon.kind == 'DSA':
                return entry.ext_dsa_muons[ref_muon.idx]
            else:
                raise ValueError('Invalid RECO type')

        # Loop GEN dimuons
        for gen_dimuon in sel_gen_dimuons:
            # Get Matched Reco Dimuon
            reco_dimuon = matched_reco_dimuon.get(gen_dimuon.idx, None)

            # Composition
            if reco_dimuon is None:
                reco_composition = None
            else:
                reco_composition = reco_dimuon.composition

            # Efficiency
            self.dim_eff_vs_pt.fill(
                reco_composition, gen_dimuon.p4.Pt()
            )

            self.dim_eff_vs_eta.fill(
                reco_composition, gen_dimuon.p4.Eta()
            )

            self.dim_eff_vs_phi.fill(
                reco_composition, np.rad2deg(gen_dimuon.p4.Phi())
            )

            self.dim_eff_vs_lxy.fill(
                reco_composition, gen_dimuon.lxy
            )

            # Short-Circuit: Matched only
            if reco_dimuon is None:
                continue

            # Composition
            self.dim_kind_vs_lxy.fill(reco_composition, gen_dimuon.lxy)
            self.dim_fraction_vs_lxy.fill(reco_composition, gen_dimuon.lxy)

            # Deltas
            deltaR_llp = gen_dimuon.p4.DeltaR(reco_dimuon.p4)
            deltaVtx_llp = (gen_dimuon.pos - reco_dimuon.pos).Mag()

            self.dim_llp_deltaR_plotter.fill(reco_composition, deltaR_llp)
            self.dim_llp_deltaVtx_plotter.fill(reco_composition, deltaVtx_llp)

            # PAT Flags
            for ref_muon in (reco_dimuon.mu1, reco_dimuon.mu2):
                if ref_muon.kind != 'PAT':
                    continue

                reco_muon = get_reco_muon(ref_muon)
                pat_flags = encode_pat_flags(reco_muon)

                self.dim_pat_flags_plotter.fill(gen_dimuon.lxy, pat_flags)

    def merge(self, other):
        self.dim_kind_vs_lxy.add(other.dim_kind_vs_lxy)
        self.dim_fraction_vs_lxy.add(other.dim_fraction_vs_lxy)
        self.dim_llp_deltaR_plotter.add(other.dim_llp_deltaR_plotter)
        self.dim_llp_deltaVtx_plotter.add(other.dim_llp_deltaVtx_plotter)
        self.dim_pat_flags_plotter.add(other.dim_pat_flags_plotter)

        self.dim_eff_vs_pt.add(other.dim_eff_vs_pt)
        self.dim_eff_vs_eta.add(other.dim_eff_vs_eta)
        self.dim_eff_vs_phi.add(other.dim_eff_vs_phi)
        self.dim_eff_vs_lxy.add(other.dim_eff_vs_lxy)
