import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import uniform_step_axis
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.mini.commons.bins import ddm_lxy_bins, ddm_lz_bins, ddm_min_threshold, ddm_pt_bins, ddm_abs_eta_bins, \
    ddm_phi_deg_bins
from ddm.mini.utils.dimuon_matching import match_gen_dimuons


class GENDistributions(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # LLP
        self.llp_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_mass',
            'LLP Distribution',
            'LLP m [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.llp_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_pt',
            'LLP Distribution',
            'LLP p_{T} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.llp_beta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_beta',
            'LLP Distribution',
            'LLP #beta', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.llp_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_eta',
            'LLP Distribution',
            'LLP #left| #eta #right|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.llp_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_phi',
            'LLP Distribution',
            'LLP #phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

        self.llp_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_lxy',
            'LLP Distribution',
            'LLP L_{xy} [cm]', 'a.u.',
            xbins=ddm_lxy_bins,
            density=True,
            logy=True)

        self.llp_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_lz',
            'LLP Distribution',
            'LLP L_{z} [cm]', 'a.u.',
            xbins=ddm_lz_bins,
            density=True,
            logy=True)

        self.llp_lxy_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'llp_lxy_vs_lz', 'LLP Distribution',
            'LLP L_{z} [cm]', 'LLP L_{xy} [cm]',
            xbins=ddm_lz_bins,
            ybins=ddm_lxy_bins,
            min_val=ddm_min_threshold,
            density=True,
            logz=True)

        # GEN muon
        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_pt',
            'Muon Distribution',
            'GEN p_{T} [GeV]', 'a.u.',
            xbins=ddm_pt_bins,
            density=True,
            logy=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'Muon Distribution',
            'GEN #left| #eta #right|', 'a.u.',
            xbins=ddm_abs_eta_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'Muon Distribution',
            'GEN #phi [deg]', 'a.u.',
            xbins=ddm_phi_deg_bins,
            density=True,
            logy=True)

    def process_entry(self, entry):
        # Short-Circuit: No Dimuons
        if len(entry.ext_gen_dimuons) == 0:
            return

        # Get GEN Dimuons
        sel_dimuons = [
            dimuon for dimuon in entry.ext_dimuons
            if dimuon.is_base
        ]

        matched_gen_dimuons = [
            gen_dimuon for gen_dimuon, _, _
            in match_gen_dimuons(entry.ext_gen_dimuons, sel_dimuons, deltaR_threshold=0.05, deltaVtx_threshold=20.0)
        ]

        # Short-Circuit: No Dimuons
        if len(matched_gen_dimuons) == 0:
            return

        # Loop True Dimuons
        for gen_dimuon in matched_gen_dimuons:
            # Fill LLP plots
            self.llp_mass_plotter.fill(gen_dimuon.p4.M())
            self.llp_pt_plotter.fill(gen_dimuon.p4.Pt())
            self.llp_beta_plotter.fill(gen_dimuon.p4.Beta())
            self.llp_eta_plotter.fill(abs(gen_dimuon.p4.Eta()))
            self.llp_phi_plotter.fill(np.rad2deg(gen_dimuon.p4.Phi()))
            self.llp_lxy_plotter.fill(gen_dimuon.lxy)
            self.llp_lz_plotter.fill(abs(gen_dimuon.z))
            self.llp_lxy_vs_lz_plotter.fill(abs(gen_dimuon.z), gen_dimuon.lxy)

            # Fill GEN muon plots
            gen_mu1 = gen_dimuon.mu1
            gen_mu2 = gen_dimuon.mu2

            self.gen_pt_plotter.fill(gen_mu1.pt)
            self.gen_pt_plotter.fill(gen_mu2.pt)
            self.gen_eta_plotter.fill(abs(gen_mu1.eta))
            self.gen_eta_plotter.fill(abs(gen_mu2.eta))
            self.gen_phi_plotter.fill(np.rad2deg(gen_mu1.phi))
            self.gen_phi_plotter.fill(np.rad2deg(gen_mu2.phi))

    def merge(self, other):
        # LLP plots
        self.llp_mass_plotter.add(other.llp_mass_plotter)
        self.llp_pt_plotter.add(other.llp_pt_plotter)
        self.llp_beta_plotter.add(other.llp_beta_plotter)
        self.llp_eta_plotter.add(other.llp_eta_plotter)
        self.llp_phi_plotter.add(other.llp_phi_plotter)
        self.llp_lxy_plotter.add(other.llp_lxy_plotter)
        self.llp_lz_plotter.add(other.llp_lz_plotter)
        self.llp_lxy_vs_lz_plotter.add(other.llp_lxy_vs_lz_plotter)

        # GEN muon plots
        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)
