from ROOT import TFile

from ddm.commands.fast.check_performance.analyzers.dimuon_distributions import DimuonDistributions
from ddm.commands.fast.check_performance.analyzers.dimuon_performance import DimuonPerfomance
from ddm.commands.fast.check_performance.analyzers.gen_distributions import GENDistributions
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class CheckFastPerformance(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

    def create_producers(self):
        pat_cutstr = get_cutstring('pat', ['BASE'], 2022, False)
        hyb_cutstr = get_cutstring('hyb', ['BASE'], 2022, False)
        dsa_cutstr = get_cutstring('dsa', ['BASE'], 2022, False)
        base_cutstr = '({})||({})||({})'.format(pat_cutstr, hyb_cutstr, dsa_cutstr)

        return [
            FormulaBranchProducer(dim_is_base=base_cutstr)
        ]

    def create_analyzers(self):
        return [
            DimuonPerfomance(),
            DimuonDistributions(),
            GENDistributions(),
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
