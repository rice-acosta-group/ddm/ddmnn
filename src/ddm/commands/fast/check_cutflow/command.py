from ddm.commands.fast.check_cutflow.analyzers.cutflow import CutflowAnalyzer
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.cuts.cut_utils import remove_duplicate_cuts
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class CheckFastCutflow(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

        parser.add_argument('--dimuon-type', dest='dimuon_type', metavar='DIMUON_TYPE',
                            type=str, default='dsa',
                            help='Dimuon type to test')

        parser.add_argument('--base-cuts', dest='base_cuts', metavar='BASE_CUTS',
                            type=str, nargs='+', default=[],
                            help='Base cuts to apply alongside each individual cut')

        parser.add_argument('--cuts', dest='cuts', metavar='CUTS',
                            type=str, nargs='+', default=[],
                            help='Ordered list of cuts to test')

    def configure(self, args):
        # Unpack args
        self.dimuon_type = args.dimuon_type
        self.base_cuts = args.base_cuts
        self.cuts = args.cuts

        # Get cutstrings
        self.cutstrings = [
            get_cutstring(
                self.dimuon_type, remove_duplicate_cuts(self.base_cuts + [cut]),
                2022, False
            ) for cut in self.cuts
        ]

        self.branch_cutstrings = {
            f'dim_fsel_{cut}': cutstring
            for cut, cutstring in zip(self.cuts, self.cutstrings)
        }

    def create_producers(self):
        return [
            FormulaBranchProducer(**self.branch_cutstrings),
        ]

    def create_analyzers(self):
        return [
            CutflowAnalyzer(self.cuts, self.cutstrings),
        ]

    def consolidate(self):
        for analyzer in self.analyzers:
            analyzer.consolidate()
