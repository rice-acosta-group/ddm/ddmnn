import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.execution.runtime import get_logger


class CutflowAnalyzer(AbstractAnalyzer):

    def __init__(self, cuts, cutstrings):
        super().__init__()

        self.cuts = cuts
        self.cutstrings = cutstrings

        self.initial_count = 0
        self.vsize_cuts = len(self.cuts)
        self.cutflow_count = np.zeros((self.vsize_cuts,))

    def process_entry(self, entry):
        # Accumulate initial counts
        self.initial_count += len(entry.ext_dimuons)

        # Loop dimuons
        for dimuon in entry.ext_dimuons:
            for cut_idx, cut in enumerate(self.cuts):
                passed_cut = getattr(dimuon, f'fsel_{cut}', False)

                if not passed_cut:
                    break

                self.cutflow_count[cut_idx] += 1

    def merge(self, other):
        self.initial_count += other.initial_count
        self.cutflow_count += other.cutflow_count

    def post_production(self):
        # Print
        header_format = '| %30s | %20s | %s'
        entry_format = '| %30s | %20d | %s'

        header = header_format % ('Name', 'Cutflow', 'Cutstring')
        vsize_header = len(header)

        summary = '-' * vsize_header
        summary += '\n' + header
        summary += '\n' + '-' * vsize_header
        summary += '\n' + entry_format % ('Initial', self.initial_count, '')

        for idx in range(self.vsize_cuts - 1):
            summary += '\n' + entry_format % (self.cuts[idx], self.cutflow_count[idx], self.cutstrings[idx])

        summary += '\n' + '-' * vsize_header
        summary += '\n' + entry_format % ('End: ' + self.cuts[-1], self.cutflow_count[-1], self.cutstrings[-1])
        summary += '\n' + '-' * vsize_header
        summary += '\n'

        get_logger().info('\n' + summary)
