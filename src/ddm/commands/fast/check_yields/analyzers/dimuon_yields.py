from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.commons.binning import parse_bin_edges
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.fast.variables.variable_registry import get_var


class DimuonYields(AbstractAnalyzer):

    def __init__(self, prefix, dimuon_type, varname, bin_edges, min_value, max_value):
        super().__init__()

        # Check ranges
        if min_value is None:
            min_value = 0.9

        # Checkout histogram
        if ':' in varname:
            varnames = varname.split(':')
            varnamex = varnames[0]
            varnamey = varnames[1]
            tagx = f'fvar_{dimuon_type}_{varnamex}'
            tagy = f'fvar_{dimuon_type}_{varnamey}'
            varx = get_var(dimuon_type, varnamex)
            vary = get_var(dimuon_type, varnamey)

            if bin_edges is None:
                xbin_edges = varx.bin_edges
                ybin_edges = vary.bin_edges
            else:
                bin_edges = bin_edges.split(';')
                xbin_edges = bin_edges[0]
                ybin_edges = bin_edges[1]

                xbin_edges = parse_bin_edges(xbin_edges)
                ybin_edges = parse_bin_edges(ybin_edges)

            self.var_tag = (tagx, tagy)
            self.plot = self.checkout_plotter(
                Hist2DPlotter,
                f'{prefix}_{varnamey}_vs_{varnamex}',
                'Dimuon Distributions',
                varx.get_latex(), vary.get_latex(),
                xbins=xbin_edges,
                ybins=ybin_edges,
                min_val=min_value,
                max_val=max_value,
                logz=True,
                draw_option='COLZ1'
            )
        else:
            var = get_var(dimuon_type, varname)
            bin_edges = parse_bin_edges(bin_edges)

            if bin_edges is None:
                bin_edges = var.bin_edges

            self.var_tag = f'fvar_{dimuon_type}_{varname}'
            self.plot = self.checkout_plotter(
                Hist1DPlotter,
                f'{prefix}_{varname}',
                'Dimuon Distributions',
                var.get_latex(), 'a.u.',
                xbins=bin_edges,
                min_val=min_value,
                max_val=max_value,
                logy=True
            )

    def process_entry(self, entry):
        # Loop dimuons
        for dimuon in entry.ext_dimuons:
            # Short-Circuit: Skip ignored dimuons
            if not dimuon.is_selected:
                continue

            # Fill
            if isinstance(self.var_tag, tuple):
                varx_value = getattr(dimuon, self.var_tag[0])
                vary_value = getattr(dimuon, self.var_tag[1])
                self.plot.fill(varx_value, vary_value)
            else:
                var_value = getattr(dimuon, self.var_tag)
                self.plot.fill(var_value)

    def merge(self, other):
        self.plot.add(other.plot)
