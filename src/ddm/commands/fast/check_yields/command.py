from ROOT import TFile

from ddm.commands.fast.check_yields.analyzers.dimuon_yields import DimuonYields
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class CheckFastYields(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

        parser.add_argument('--prefix', dest='prefix', metavar='PREFIX',
                            type=str, default=None,
                            help='Prefix used to identify this selection')

        parser.add_argument('--dimuon-type', dest='dimuon_type', metavar='DIMUON_TYPE',
                            type=str, default='dsa',
                            help='Dimuon type to test')

        parser.add_argument('--cuts', dest='cuts', metavar='CUTS',
                            type=str, nargs='+', default=[],
                            help='Ordered list of cuts to test')

        parser.add_argument('--var', dest='varname', metavar='VARIABLE_NAME',
                            type=str, default=None,
                            help='Variable name to plot')

        parser.add_argument('--bin-edges', dest='bin_edges', metavar='BIN_EDGES',
                            type=str, default=None,
                            help='Bin edges')

        parser.add_argument('--min', dest='min_value', metavar='MIN_VALUE',
                            type=float, default=None,
                            help='Value Min')

        parser.add_argument('--max', dest='max_value', metavar='MAX_VALUE',
                            type=float, default=None,
                            help='Value Max')

    def configure(self, args):
        # Unpack args
        self.prefix = args.prefix
        self.dimuon_type = args.dimuon_type
        self.cuts = args.cuts
        self.varname = args.varname
        self.bin_edges = args.bin_edges
        self.min_value = args.min_value
        self.max_value = args.max_value

        # Get cutstring
        self.cutstring = get_cutstring(self.dimuon_type, self.cuts, 2022, False)

    def create_producers(self):
        return [
            FormulaBranchProducer(dim_is_selected=self.cutstring)
        ]

    def create_analyzers(self):
        # Get Prefix
        prefix = f'dim_{self.dimuon_type}'

        if self.prefix is not None:
            prefix = f'{prefix}_{self.prefix}'

        # Return Analyzers
        return [
            DimuonYields(prefix, self.dimuon_type, self.varname, self.bin_edges, self.min_value, self.max_value),
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
