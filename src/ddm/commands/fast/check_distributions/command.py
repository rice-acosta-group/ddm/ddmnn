from ROOT import TFile

from ddm.commands.fast.check_distributions.analyzers.dimuon_distributions import DimuonDistributions
from ddm.fast.analysis.fast_analysis import FastAnalysis
from ddm.fast.cuts.cut_registry import get_cutstring
from ddm.fast.producers.FormulaBranchProducer import FormulaBranchProducer


class CheckFastDistributions(FastAnalysis):

    def __init__(self):
        super().__init__()

        self.branches_required = {'dim_eta'}

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

        parser.add_argument('--prefix', dest='prefix', metavar='prefix',
                            type=str, default=None,
                            help='Prefix used to identify this selection')

        parser.add_argument('--dimuon-type', dest='dimuon_type', metavar='DIMUON_TYPE',
                            type=str, default='dsa',
                            help='Dimuon type to test')

        parser.add_argument('--cuts', dest='cuts', metavar='CUTS',
                            type=str, nargs='+', default=[],
                            help='Ordered list of cuts to test')

        parser.add_argument('--vars', dest='vars', metavar='VARS',
                            type=str, nargs='+', default=None,
                            help='List of variables to plot')

    def configure(self, args):
        # Unpack args
        self.prefix = args.prefix
        self.dimuon_type = args.dimuon_type
        self.cuts = args.cuts
        self.vars = args.vars

        # Get cutstring
        self.cutstring = get_cutstring(self.dimuon_type, self.cuts, 2022, False)

    def create_producers(self):
        return [
            FormulaBranchProducer(dim_is_selected=self.cutstring)
        ]

    def create_analyzers(self):
        # Get Prefix
        prefix = f'dim_{self.dimuon_type}'

        if self.prefix is not None:
            prefix = f'{prefix}_{self.prefix}'

        # Return Analyzers
        return [
            DimuonDistributions(prefix, self.dimuon_type, self.vars),
        ]

    def consolidate(self):
        output_root_file = TFile.Open('outputs.root', 'UPDATE')

        for analyzer in self.analyzers:
            analyzer.write()

        output_root_file.Close()
