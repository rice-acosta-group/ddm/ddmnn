dsa_vars = [
    'pscore',
    'beta', 'ct',
    'largemass', 'mass', 'smallmass', 'masssig',
    'dimpt', 'dimp', 'dimeta', 'dimphi',
    'diffeta', 'diffphi',
    'lxypv', 'largelxypv',
    'lxysigpv',
    'lxyerrpv', 'largelxyerrpv',
    'lxyphipv',
    'lzpv', 'deltaphi', 'deltar',
    'cosalpha', 'cosalphao',
    'dca', 'largedca',
    'vtxnormchi2', 'qsum', 'maxtime',
    'mind0sigpv', 'maxiso', 'maxmuoniso', 'maxlargeiso', 'maxpt',
    'difflz', 'dimetavtx', 'dimphivtx', 'maxhitsb4vtx',
    'mind0sigpv:lxysigpv',
    'maxmuoniso:isoLxy', 'maxmuoniso:isoPmumu', 'isoPmumu:isoLxy', 'maxDSAiso:maxDSAcleanedIso',
    'maxDSAiso:isoLxy', 'maxDSAiso:isoPmumu', 'maxDSAcleanedIso:isoLxy', 'maxDSAcleanedIso:isoPmumu',
    'dimz:dimr', 'lzp4:lzpv', 'dimphivtx:dimr', 'lzp4:lzpv',
]

hyb_vars = [
    'pscore',
    'beta', 'ct',
    'largemass', 'mass', 'smallmass', 'masssig',
    'dimpt', 'dimp', 'dimeta', 'dimphi',
    'diffeta', 'diffphi',
    'lxypv', 'largelxypv',
    'lxysigpv',
    'lxyerrpv', 'largelxyerrpv',
    'lxyphipv',
    'lzpv', 'deltaphi', 'deltar',
    'cosalpha', 'cosalphao',
    'dca', 'largedca',
    'vtxnormchi2', 'qsum',
    'mind0sigpv', 'maxiso', 'maxmuoniso', 'maxlargeiso', 'maxpt',
    'pxldiff', 'maxhitsb4vtx', 'minntrklayslxy',
    'difflz', 'dimetavtx', 'dimphivtx', 'maxhitsb4vtx',
    'mind0sigpv:lxysigpv',
    'maxmuoniso:isoLxy', 'maxmuoniso:isoPmumu', 'isoPmumu:isoLxy', 'maxHYBiso:maxHYBcleanedIso',
    'maxHYBiso:isoLxy', 'maxHYBiso:isoPmumu', 'maxHYBcleanedIso:isoLxy', 'maxHYBcleanedIso:isoPmumu',
    'dimz:dimr', 'lzp4:lzpv', 'dimphivtx:dimr', 'lzp4:lzpv'
]

pat_vars = [
    'pscore',
    'beta', 'ct',
    'largemass', 'mass', 'smallmass', 'masssig',
    'dimpt', 'dimp', 'dimeta', 'dimphi',
    'diffeta', 'diffphi',
    'lxypv', 'largelxypv',
    'lxysigpv',
    'lxyerrpv', 'largelxyerrpv',
    'lxyphipv',
    'lzpv', 'deltaphi', 'deltar',
    'cosalpha', 'cosalphao',
    'dca', 'largedca',
    'vtxnormchi2', 'qsum',
    'mind0sigpv', 'maxiso', 'maxmuoniso', 'maxlargeiso', 'maxpt',
    'pxldiff', 'maxhitsb4vtx', 'minntrklayslxy',
    'difflz', 'dimetavtx', 'dimphivtx', 'maxhitsb4vtx',
    'mind0sigpv:lxysigpv',
    'maxmuoniso:isoLxy', 'maxmuoniso:isoPmumu', 'isoPmumu:isoLxy', 'maxPATiso:maxPATcleanedIso',
    'maxPATiso:isoLxy', 'maxPATiso:isoPmumu', 'maxPATcleanedIso:isoLxy', 'maxPATcleanedIso:isoPmumu',
    'dimz:dimr', 'lzp4:lzpv', 'dimphivtx:dimr', 'lzp4:lzpv'
]
