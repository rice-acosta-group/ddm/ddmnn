from ddm.commands.fast.check_distributions.config.vars import dsa_vars, hyb_vars, pat_vars
from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from ddm.fast.variables.variable_registry import get_var


class DimuonDistributions(AbstractAnalyzer):

    def __init__(self, prefix, dimuon_type, var_list):
        super().__init__()

        # Select variables
        if var_list is None:
            if dimuon_type == 'dsa':
                var_list = dsa_vars
            elif dimuon_type == 'hyb':
                var_list = hyb_vars
            else:
                var_list = pat_vars

        # Initialize Histograms
        self.plt_vars = list()
        self.histograms_1d = dict()
        self.histograms_2d = dict()

        for varname in var_list:
            if ':' in varname:
                varnames = varname.split(':')
                tagx = f'fvar_{dimuon_type}_{varnames[0]}'
                tagy = f'fvar_{dimuon_type}_{varnames[1]}'
                varx = get_var(dimuon_type, varnames[0])
                vary = get_var(dimuon_type, varnames[1])

                self.plt_vars.append((varname, (tagx, tagy)))
                self.histograms_2d[varname] = self.checkout_plotter(
                    Hist2DPlotter,
                    f'{prefix}_{varnames[1]}_vs_{varnames[0]}',
                    'Dimuon Distributions',
                    varx.get_latex(), vary.get_latex(),
                    xbins=varx.bin_edges,
                    ybins=vary.bin_edges,
                    min_val=0.1,
                    logz=True,
                    draw_option='COLZ1'
                )
            else:
                var = get_var(dimuon_type, varname)
                tag = f'fvar_{dimuon_type}_{varname}'

                self.plt_vars.append((varname, tag))
                self.histograms_1d[varname] = self.checkout_plotter(
                    Hist1DPlotter,
                    f'{prefix}_{varname}',
                    'Dimuon Distributions',
                    var.get_latex(), 'a.u.',
                    xbins=var.bin_edges,
                    min_val=0.9,
                    logy=True
                )

    def process_entry(self, entry):
        # Loop dimuons
        for dimuon in entry.ext_dimuons:
            # Short-Circuit: Skip ignored dimuons
            if not dimuon.is_selected:
                continue

            # Loop Vars
            for var_name, var_tag in self.plt_vars:
                if isinstance(var_tag, tuple):
                    varx = getattr(dimuon, var_tag[0])
                    vary = getattr(dimuon, var_tag[1])
                    self.histograms_2d[var_name].fill(varx, vary)
                else:
                    var_value = getattr(dimuon, var_tag)
                    self.histograms_1d[var_name].fill(var_value)

    def merge(self, other):
        # Merge histograms
        for varname, hist in other.histograms_1d.items():
            self.histograms_1d[varname].add(hist)

        for varname, hist in other.histograms_2d.items():
            self.histograms_2d[varname].add(hist)
