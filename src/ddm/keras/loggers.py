from keras.src.callbacks import Callback
from keras.src.optimizers.schedules import learning_rate_schedule
from tensorflow.keras import backend


class LearningRateLogger(Callback):
    """Learning rate logger."""

    def __init__(self):
        super().__init__()

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'learning_rate'):
            raise ValueError('Optimizer must have a "learning_rate" attribute.')

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        learning_rate = self.model.optimizer.learning_rate

        if isinstance(learning_rate, learning_rate_schedule.LearningRateSchedule):
            logs['lr'] = backend.get_value(learning_rate(self.model.optimizer.iterations))
        else:
            logs['lr'] = backend.get_value(self.model.optimizer.learning_rate)
