from ddm.core.commons.combinatorics import yield_pairs
from ddm.mini.commons.isolation import yield_iso_thresholds
from ddm.mini.utils.isolation_utils import should_add_to_iso
from ddm.mini.utils.muon_matching import calc_deltaR


def append_pat_isolation(sel_pat_muons):
    """Cleans isolation calculated from overlapping PAT muons and appends to class"""

    # Short-Circuit: No PAT muons
    if len(sel_pat_muons) == 0:
        return

    # Clean PAT isolation with default thresholds
    clean_pat_isolation(
        sel_pat_muons,
        'trackIso', 'cleanedIso'
    )

    if getattr(sel_pat_muons[0], 'tkiso0p3_sumPt_dz0p2_dxy0p1', None) is None:
        return

    # Clean PAT isolation with all thresholds
    for dR, dz, d0 in yield_iso_thresholds():
        source_branch_name = 'tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
            int(10 * dR), int(10 * dz), int(10 * d0)
        )
        cleaned_branch_name = f'clnd_{source_branch_name}'

        clean_pat_isolation(
            sel_pat_muons,
            source_branch_name,
            cleaned_branch_name,
            dR, dz, d0
        )


def clean_pat_isolation(
        sel_pat_muons,
        source_branch_name,
        cleaned_branch_name,
        dR=0.3, dz=0.2, d0=0.1,
        dR_veto=0.01,
):
    """
    sel_pat_muons        - muons which to clean
    source_branch_name   - string for  track isolation value from which to subtract from
    cleaned_branch_name  - string where we should store the new cleaned isolation
    dR                   - cone size in which track isolation was summed
    dz                   - [cm] window in z in which tracks are added
    d0                   - [cm] d0 of tracks added
    """

    # Short-Circuit: Only one pat muon
    if len(sel_pat_muons) == 1:
        mu = sel_pat_muons[0]
        iso = getattr(mu, source_branch_name)
        setattr(mu, cleaned_branch_name, iso)
        return

    # Subtract pat muons from each other
    def get_cleaned_iso(mu1, mu2, pair_deltaR):
        # Get iso
        mu1_iso = getattr(mu1, source_branch_name)

        # Short-Circuit: Outside of cone
        if pair_deltaR >= dR:
            return mu1_iso

        # Short-Circuit: Failed isolation cuts
        add_to_isolation = should_add_to_iso(
            mu1, mu2, pair_deltaR,
            dz, d0, dR_veto
        )

        if not add_to_isolation:
            return mu1_iso

        # Subtract from iso and ignore floating point errors,
        # so we don't have weird things happen later
        return max(0.0, mu1_iso - mu2.p3_tk.Perp())

    for mu1, mu2 in yield_pairs(sel_pat_muons):
        pair_deltaR = calc_deltaR(mu1, mu2, use_tracker_track=True)

        cleaned_iso = get_cleaned_iso(mu1, mu2, pair_deltaR)
        setattr(mu1, cleaned_branch_name, cleaned_iso)

        cleaned_iso = get_cleaned_iso(mu2, mu1, pair_deltaR)
        setattr(mu2, cleaned_branch_name, cleaned_iso)
