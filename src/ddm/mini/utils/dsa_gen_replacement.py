from ddm.mini.utils.dimuon_utils import align_dimuons
from ddm.mini.utils.muon_matching import match_muons


def replace_dsa_muons_using_gen(
        gen_dimuons, dsa_muons, pat_muons, dimuons,
        no_std_pat_en=False
):
    # Short-Circuit: No Dimuons
    if len(gen_dimuons) == 0:
        return list()

    # Get GEN muons
    gen_muons = list()

    for dim in gen_dimuons:
        if dim.lxy > 120.:
            continue

        if abs(dim.z) > 300.:
            continue

        gen_muons.append(dim.mu1)
        gen_muons.append(dim.mu2)

    # Get DSA by Disp PAT Index
    dsa_muons_by_disp_pat_idx = {
        dsa_muon.idx_DispMatch: dsa_muon
        for dsa_muon in dsa_muons
        if dsa_muon.idx_DispMatch >= 0
    }

    # Get DSA muons
    sel_dsa_muons = dsa_muons + [
        mu for mu in pat_muons
        if mu.idx in dsa_muons_by_disp_pat_idx
    ]

    # Get Std PAT muons
    std_pat_muons = [
        mu for mu in pat_muons
        if not mu.isDisp
    ]

    # Match Std PAT muons to DSA muons
    def get_key(entry):
        dsa_muon, std_pat_muon = entry[0], entry[1]

        if dsa_muon.kind == 'PAT':
            dsa_muon = dsa_muons_by_disp_pat_idx[dsa_muon.idx]

        return dsa_muon.idx, std_pat_muon.idx

    gen_dsa_pairs = match_muons(
        gen_muons, sel_dsa_muons,
        deltaR_threshold=0.05,
        deltaPos_threshold=5.0,
        keys_fn=get_key
    )

    gen_std_pat_pairs = match_muons(
        gen_muons, std_pat_muons,
        deltaR_threshold=0.05,
        deltaPos_threshold=5.0,
    )

    # Get GEN by DSA index
    gen_by_dsa_idx = dict()

    for gen_muon, dsa_muon, metric in gen_dsa_pairs:
        if dsa_muon.kind == 'PAT':
            dsa_muon = dsa_muons_by_disp_pat_idx[dsa_muon.idx]

        gen_by_dsa_idx[dsa_muon.idx] = gen_muon

    # Get Std PAT by GEN index
    std_pat_by_gen_idx = {
        gen_muon.idx: std_pat_muon
        for gen_muon, std_pat_muon, metric in gen_std_pat_pairs
    }

    # Get displaced pat muons by idx
    disp_pat_muons_by_idx = {
        mu.idx: mu
        for mu in pat_muons
        if mu.isDisp
    }

    # Select muons
    keep_dsa_muons = list()
    keep_pat_muons = list()

    for dsa_muon in dsa_muons:
        # Get GEN muon
        gen_muon = gen_by_dsa_idx.get(dsa_muon.idx, None)

        # Save DSA-GEN Match
        dsa_muon.matched_gen_muon = gen_muon

        # Get Displaced PAT
        disp_pat_muon = None

        if dsa_muon.idx_DispMatch >= 0:
            disp_pat_muon = disp_pat_muons_by_idx[dsa_muon.idx_DispMatch]

        # if gen_muon is not None:
        #     if gen_muon.charge != dsa_muon.charge:
        #         if disp_pat_muon is not None:
        #             print(f"GEN {gen_muon.pt},{gen_muon.eta} and DISP PAT Q==GEN Q={disp_pat_muon.charge==gen_muon.charge} and DSA Q==DISP PAT Q={disp_pat_muon.charge==dsa_muon.charge}")
        #         else:
        #             print(f"GEN {gen_muon.pt},{gen_muon.eta}; DSA {dsa_muon.pt},{dsa_muon.eta}")

        # Get Standard PAT
        if gen_muon is not None:
            std_pat_muon = std_pat_by_gen_idx.get(gen_muon.idx, None)
        else:
            std_pat_muon = None

        # Save Displaced Match
        if disp_pat_muon is not None:
            disp_pat_muon.matched_gen_muon = gen_muon
            disp_pat_muon.ideal_dsa_muon = dsa_muon
            disp_pat_muon.ideal_std_pat_muon = std_pat_muon

        # Save Standard Match
        if std_pat_muon is not None:
            std_pat_muon.matched_gen_muon = gen_muon
            std_pat_muon.ideal_dsa_muon = dsa_muon

        if disp_pat_muon is not None:
            keep_pat_muons.append(disp_pat_muon)
        elif no_std_pat_en:
            keep_dsa_muons.append(dsa_muon)
        elif std_pat_muon is not None:
            keep_pat_muons.append(std_pat_muon)
        else:
            keep_dsa_muons.append(dsa_muon)

    # Align dimuons
    keep_dimuons = align_dimuons(
        dimuons,
        dsa_muons=keep_dsa_muons,
        pat_muons=keep_pat_muons
    )

    # Return
    return keep_dsa_muons, keep_pat_muons, keep_dimuons
