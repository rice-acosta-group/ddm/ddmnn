from ddm.core.commons.collections import reduce_collection
from ddm.core.commons.combinatorics import yield_combinations
from ddm.core.commons.kinematics import propagate_particle
from ddm.mini.primitives.gen_muon import GenMuon


# Calculates the deltaR between two muons, in any order.
# In the case of calculating cleaned isolation, it is proper to use the tracker track direction
# to check if the muons fall within the cone.
def calc_deltaR(muon1, muon2, vertex=None, use_tracker_track=False):
    # If vertex = 'BS', and we have gen muons, make any gen muons use the BS version of quantities
    # this will actually also work for two gen muons, in addition to gen + reco in any order
    if vertex == "BS" and (isinstance(muon1, GenMuon) or isinstance(muon2, GenMuon)):
        mu1 = muon1.BS if isinstance(muon1, GenMuon) else muon1
        mu2 = muon2.BS if isinstance(muon2, GenMuon) else muon2

        # Deal with refitted gen tracks with 0 pT -- pretend deltaR is very large
        # gen 0 pT occurs only when the BS extrapolated gen track doesn't work properly
        # this happens when gen eta is very large, so they would probably be outside acceptance anyway
        # to prevent stderr warning lines, return None immediately
        if mu1.pt < 1.0e-10 or mu2.pt < 1.0e-10:
            return None

        mu1_p3 = mu1.p3
        mu2_p3 = mu2.p3
    elif use_tracker_track:
        mu1_p3 = muon1.p3_tk
        mu2_p3 = muon2.p3_tk
    else:
        mu1_p3 = muon1.p3
        mu2_p3 = muon2.p3

    # Compute deltaR
    return mu1_p3.DeltaR(mu2_p3)


# Returns pairs of (type1, type2) muons with the types being GEN, DSA or PAT;
# sorted from smallest dR to largest without duplication
def match_muons(
        muons_type1, muons_type2,
        deltaR_threshold=0.2,
        deltaPos_threshold=None,
        propagate_en=True,
        keys_fn=None
):
    # Short-Circuit: No gen muons
    if len(muons_type1) == 0:
        return list()

    # Short-Circuit: No reco muons
    if len(muons_type2) == 0:
        return list()

    # Default key getter
    if keys_fn is None:
        keys_fn = lambda entry: (entry[0].idx, entry[1].idx)

    # Comparisons
    comparisons = list()

    for mu1, mu2 in yield_combinations(muons_type1, muons_type2):
        # Calculate metric
        deltaR_mom, deltaR_pos = compare_muons(
            mu1, mu2,
            deltaR_threshold=deltaR_threshold,
            deltaPos_threshold=deltaPos_threshold,
            propagate_en=propagate_en
        )

        # Short-Circuit: No match
        if deltaR_mom is None:
            continue

        # Calculate metric
        metric = deltaR_mom + deltaR_pos

        # Append
        comparisons.append((mu1, mu2, metric))

    # Reduce pairs
    return reduce_collection(
        comparisons,
        keys_fn=keys_fn,
        metric_fn=lambda entry: entry[2]
    )


def compare_muons(
        muon_1, muon_2,
        deltaR_threshold=0.2,
        deltaPos_threshold=None,
        propagate_en=True
):
    # Get momentum and position vectors
    mu1_pos, mu1_mom = muon_1.pos, muon_1.p3
    mu2_pos, mu2_mom = muon_2.pos, muon_2.p3

    # Propagate muons if they're going in the same direction
    if propagate_en and (mu1_mom.z() * mu2_mom.z()) > 0:
        if mu1_mom.z() > 0:
            # Moving in positive z-direction
            if mu2_pos.z() < mu1_pos.z():
                mu2_mom, mu2_pos = propagate_particle(
                    muon_2.charge, mu2_mom, mu2_pos,
                    z_star=mu1_pos.z()
                )
            elif mu1_pos.z() < mu2_pos.z():
                mu1_mom, mu1_pos = propagate_particle(
                    muon_1.charge, mu1_mom, mu1_pos,
                    z_star=mu2_pos.z()
                )
        else:
            # Moving in negative z-direction
            if mu1_pos.z() < mu2_pos.z():
                mu2_mom, mu2_pos = propagate_particle(
                    muon_2.charge, mu2_mom, mu2_pos,
                    z_star=mu1_pos.z()
                )
            elif mu2_pos.z() < mu1_pos.z():
                mu1_mom, mu1_pos = propagate_particle(
                    muon_1.charge, mu1_mom, mu1_pos,
                    z_star=mu2_pos.z()
                )

    # Short-Circuit: Momentum not aligned
    deltaR_mom = mu2_mom.DeltaR(mu1_mom)

    if deltaR_threshold is not None and deltaR_mom >= deltaR_threshold:
        return None, None

    # Short-Circuit: Position not aligned
    deltaR_pos = mu2_pos.DeltaR(mu1_pos)

    if deltaR_threshold is not None and deltaR_pos >= deltaR_threshold:
        return None, None

    # Short-Circuit: Reference point too far
    if deltaPos_threshold is not None:
        deltaPos = (mu2_pos - mu1_pos).Mag()

        if deltaPos >= deltaPos_threshold:
            return None, None

    # Return
    return deltaR_mom, deltaR_pos
