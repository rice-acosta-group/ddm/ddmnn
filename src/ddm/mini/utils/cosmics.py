####################################################
##### N PARALLEL PAIRS -- COSMIC SHOWER FINDER #####
####################################################
# Finds the number of parallel (or antiparallel) pairs from the original list of DSA muons
# if this number is particularly high, it's very likely a cosmic event
from ddm.core.commons.combinatorics import yield_pairs


def get_number_of_parallel_pairs(dsa_col):
    muon = [
        mu for mu in dsa_col
        if (mu.nCSCHits + mu.nDTHits > 12) and (mu.pt > 5.0)
    ]

    n_minus, n_plus = 0, 0

    def calc_cos_alpha(particle1, particle2):
        return particle1.p4.Vect().Dot(particle2.p4.Vect()) / particle1.p4.P() / particle2.p4.P()

    for mu1, mu2 in yield_pairs(muon):
        original_cos_alpha = calc_cos_alpha(mu1, mu2)

        if original_cos_alpha < -0.99:
            n_minus += 1

        if original_cos_alpha > 0.99:
            n_plus += 1

    return n_minus, n_plus
