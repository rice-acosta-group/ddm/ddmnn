from ddm.core.commons.caching import Cached, Memoize
from ddm.core.execution.runtime import get_logger
from ddm.mini.commons.constants import YEAR, SIGNAL_HLT_PATH
from ddm.mini.utils.muon_matching import calc_deltaR


# If no triggering pair was found, this output dictionary will be empty, otherwise keys are (i,j) - indices of HLT muons
# match_results['matchFound'] is True if 2 DSA muons matched 2 HLTMuons below given threshold (default 0.4)
# match_results['bestMatches'] is a list of 0-2 match dictionaries containing hlt_idx, rec_idx (a.k.a. oIdx),
# HLT path name and deltaR
# If keep_all_deltaR_en is True, this list may contain deltaR values > threshold; otherwise, it definitely won't
# To get the two closest deltaR regardless of threshold, e.g., do find_dsa_hlt_matches(HLTPaths, HLTMuons, DSAMuons, keep_all_deltaR_en=True)
# Then do: for match in match_results['bestMatches']: dR = match['deltaR'] (can fill a histogram)
# To check if there are any matches at all, simply do any([match_results[ij]['matchFound'] for ij in match_results])
def match_hlt_dimuons(
        hlt_paths, hlt_dimuons, dsa_muons,
        deltaR_threshold=0.4,
        keep_all_deltaR_en=False
):
    # Cache DSA muon selection
    @Cached
    def get_sel_dsa_muons():
        return [mu for mu in dsa_muons if (mu.pt > 10.0) and (abs(mu.eta) < 2.0)]

    # Cache paths by hlt path idx
    @Cached
    def get_paths_by_hlt_path_idx():
        paths_by_hlt_path_idx = dict()
        next_hlt_paths = hlt_paths

        for path, hlt_path_base in SIGNAL_HLT_PATH[YEAR].items():
            remaining_hlt_paths = next_hlt_paths
            next_hlt_paths = list()

            for hlt_path in remaining_hlt_paths:
                hlt_path_name = str(hlt_path.name)

                if not hlt_path_name.startswith(hlt_path_base):
                    next_hlt_paths.append(hlt_path)
                    continue

                if hlt_path.idx in paths_by_hlt_path_idx:
                    raise Exception(
                        'Ambiguous mapping between HLT muon and HLT path. Found multiple HLT path matches'
                    )

                paths_by_hlt_path_idx[hlt_path.idx] = path

        return paths_by_hlt_path_idx

    # Cache matches
    # Note: use the vector primitive index for caching, since a hlt path can have multiple hlt muons
    @Memoize(lambda hlt_mu: hlt_mu._idx)
    def match_reco_muons(hlt_mu):
        matches = list()

        for dsa_mu in get_sel_dsa_muons():
            deltaR = calc_deltaR(hlt_mu, dsa_mu)

            if (not keep_all_deltaR_en) and (deltaR >= deltaR_threshold):
                continue

            matches.append((dsa_mu, deltaR))

        return matches

    # Loop HLT dimuons
    match_results = dict()

    for hlt_dimuon in hlt_dimuons:
        # Found a pair of muons that fired; now look for closest DSA muons
        match_dsa_muons_en = False

        if YEAR == 2016:
            # mass cut is 9.99996 because of DoubleMuon2016G-07Aug17 Run 279844 Lumi 73 Event 15943296
            if hlt_dimuon.mass > 9.99996 and hlt_dimuon.angle < 2.5:
                match_dsa_muons_en = True

            # Sanity check for the 2016 trigger: Make sure that there
            # are no events with a single pair of HLT muons failing
            # angular or mass cuts
            if (not match_dsa_muons_en) and (len(hlt_dimuons) == 1):
                get_logger().warn(f"""
                +++ Warning in matchedTrigger: inconsistency in the trigger +++
                found single online dimuon with mass = {hlt_dimuon.mass} and angle = {hlt_dimuon.angle}
                hlt_idxs: {hlt_mu1._idx}, {hlt_mu2._idx}; list of HLT muons:
                """)

                print(hlt_dimuon.mu1, hlt_dimuon.mu2)
        elif YEAR == 2018 or YEAR == 2022:
            match_dsa_muons_en = True
        else:
            raise Exception('Invalid value of YEAR: {}'.format(YEAR))

        # Short-Circuit: No need to match dsa muons
        if not match_dsa_muons_en:
            continue

        # Unpack hlt muons
        hlt_mu1 = hlt_dimuon.mu1
        hlt_mu2 = hlt_dimuon.mu2

        # Find all the matching DSA muons
        candidates_by_path = dict()

        for hlt_mu in (hlt_mu1, hlt_mu2):
            # Match to reco muons
            reco_mu_matches = match_reco_muons(hlt_mu)

            # Short-Circuit: No matches
            if len(reco_mu_matches) == 0:
                continue

            # Get path
            paths_by_hlt_path_idx = get_paths_by_hlt_path_idx()
            path = paths_by_hlt_path_idx[hlt_mu.idx]

            # Get path candidates
            candidates = candidates_by_path.get(path, None)

            if candidates is None:
                candidates = list()
                candidates_by_path[path] = candidates

            # Collect candidates
            candidates += [
                (hlt_mu._idx, reco_mu.idx, deltaR)
                for reco_mu, deltaR in reco_mu_matches
            ]

        # Find the closest matches
        # Walk through the list of matches. The first one (0) is the best one for
        # whatever match[0]['hlt_idx'] is. Keep walking through until you hit another
        # hlt_idx. Make sure its reco muon is not the same as the one you have, and
        # that's the other match. This accounts for multiple matches.
        best_matches_by_path = dict()

        for path, candidates in candidates_by_path.items():
            # Init buffer
            best_matches = list()
            best_matches_by_path[path] = best_matches

            # Collect the two best matches
            first_hlt_idx = None
            first_rec_idx = None

            candidates.sort(key=lambda candidate: candidate[2])

            for hlt_idx, rec_idx, deltaR in candidates:
                if first_hlt_idx is None:
                    first_hlt_idx = hlt_idx
                    first_rec_idx = rec_idx

                    best_matches.append({
                        'hlt_idx': hlt_idx,
                        'rec_idx': rec_idx,
                        'deltaR': deltaR,
                    })
                else:
                    if first_hlt_idx == hlt_idx:
                        continue

                    if first_rec_idx == rec_idx:
                        continue

                    best_matches.append({
                        'hlt_idx': hlt_idx,
                        'rec_idx': rec_idx,
                        'deltaR': deltaR,
                    })

                    break

        # At this point, best_matches has either 0, 1, or 2 entries.
        # 'matchFound' is True if both HLT muons matched different DSA muons.
        # keep_all_deltaR_en=True means we didn't cut on deltaR.
        # Determine if a match was truly found by testing deltaR wrt threshold.
        hlt_dimuon_match = dict()
        hlt_dimuon_id = (hlt_mu1._idx, hlt_mu2._idx)
        match_results[hlt_dimuon_id] = hlt_dimuon_match

        for path, best_matches in best_matches_by_path.items():
            match_found = len(best_matches) == 2

            if match_found and keep_all_deltaR_en:
                match_found = best_matches[0]['deltaR'] < deltaR_threshold
                match_found = match_found and (best_matches[1]['deltaR'] < deltaR_threshold)

            hlt_dimuon_match['bestMatches_' + path] = best_matches
            hlt_dimuon_match['matchFound_' + path] = match_found

        # Alias for Proton-Proton
        if YEAR == 2016 or YEAR == 2018 or YEAR == 2022:
            best_matches_pp = hlt_dimuon_match.get('bestMatches_ppseed', list())
            match_found_pp = hlt_dimuon_match.get('matchFound_ppseed', False)

            hlt_dimuon_match['bestMatches_pp'] = best_matches_pp
            hlt_dimuon_match['matchFound_pp'] = match_found_pp

        # Alias for Cosmics
        if YEAR == 2018 or YEAR == 2022:
            best_matches_cosmic = hlt_dimuon_match.get('bestMatches_cosmicseed', list())
            match_found_cosmic = hlt_dimuon_match.get('matchFound_cosmicseed', False)

            hlt_dimuon_match['bestMatches_cosmic'] = best_matches_cosmic
            hlt_dimuon_match['matchFound_cosmic'] = match_found_cosmic

    return match_results
