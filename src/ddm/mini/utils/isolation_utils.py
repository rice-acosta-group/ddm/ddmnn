# Tracks added to isolation variable must pass certain
# quality cuts, so to properly subtract, need to do the
# same thing.
#   mu1 is the base muon which has the isolation we are recalculating
#   mu2 is the muon found within the cone we are applying cuts to
def should_add_to_iso(
        mu1, mu2, deltaR,
        dz, d0, dR_veto
):
    if abs(mu1.z - mu2.z) >= dz:
        # greater than 2 mm
        return False
    elif abs(mu2.d0_bs) >= d0:
        # greater than 1 mm
        return False
    elif deltaR < dR_veto:
        # veto in dR < 0.01
        return False

    return True
