import math

import numpy as np

pairing_var_names = [
    # Muon 1 [0, 30]
    'mu1_is_pat',
    'mu1_x', 'mu1_y', 'mu1_z',
    'mu1_px', 'mu1_py', 'mu1_pz',
    'mu1_pt', 'mu1_pterr',
    'mu1_eta', 'mu1_phi', 'mu1_q',
    'mu1_d0_pv', 'mu1_d0sig_pv',
    'mu1_dz_pv', 'mu1_dzsig_pv',
    'mu1_chi2', 'mu1_ndof',
    'mu1_timeAtIpInOut', 'mu1_direction',
    'mu1_iso', 'mu1_trackIso',
    'mu1_is_global', 'mu1_is_medium',
    'mu1_is_tracker', 'mu1_is_high_purity',
    'mu1_nTrackerLayers',
    'mu1_ndthits', 'mu1_ncschits',
    'mu1_ndtstations', 'mu1_ncscstations',
    # Muon 2 [31, 61]
    'mu2_is_pat',
    'mu2_x', 'mu2_y', 'mu2_z',
    'mu2_px', 'mu2_py', 'mu2_pz',
    'mu2_pt', 'mu2_pterr',
    'mu2_eta', 'mu2_phi', 'mu2_q',
    'mu2_d0_pv', 'mu2_d0sig_pv',
    'mu2_dz_pv', 'mu2_dzsig_pv',
    'mu2_chi2', 'mu2_ndof',
    'mu2_timeAtIpInOut', 'mu2_direction',
    'mu2_iso', 'mu2_trackIso',
    'mu2_is_global', 'mu2_is_medium',
    'mu2_is_tracker', 'mu2_is_high_purity',
    'mu2_nTrackerLayers',
    'mu2_ndthits', 'mu2_ncschits',
    'mu2_ndtstations', 'mu2_ncscstations',
    # Dimuon [62, 80]
    'dim_vx', 'dim_vy', 'dim_vz',
    'dim_dx', 'dim_dy', 'dim_dz',
    'dim_pt', 'dim_eta', 'dim_phi',
    'dim_mass', 'dim_massunc', 'dim_dca',
    'dim_cosAlpha', 'dim_cosAlphaOrig',
    'dim_deltaPhi', 'dim_deltaR',
    'dim_isoLxy', 'dim_isoPmumu',
    'dim_normChi2',
]


def get_pairing_vars(dimuon, all_dsa_muons, all_pat_muons):
    # Unpack dimuon
    ref_mu1 = dimuon.mu1
    ref_mu2 = dimuon.mu2

    # Muon 1
    var_mu1_is_pat = 1 if (ref_mu1.kind == 'PAT') else 0
    var_mu1_iso = ref_mu1.iso

    if ref_mu1.kind == 'PAT':
        pat_mu1 = all_pat_muons[ref_mu1.idx]
        var_mu1_x = pat_mu1.x
        var_mu1_y = pat_mu1.y
        var_mu1_z = pat_mu1.z
        var_mu1_px = pat_mu1.px
        var_mu1_py = pat_mu1.py
        var_mu1_pz = pat_mu1.pz
        var_mu1_pt = math.hypot(var_mu1_px, var_mu1_py)
        var_mu1_pterr = pat_mu1.ptError
        var_mu1_eta = pat_mu1.eta
        var_mu1_phi = pat_mu1.phi
        var_mu1_q = pat_mu1.charge
        var_mu1_d0_pv = pat_mu1.d0_pv
        var_mu1_d0sig_pv = pat_mu1.d0sig_pv
        var_mu1_dz_pv = pat_mu1.dz_pv
        var_mu1_dzsig_pv = pat_mu1.dzsig_pv
        var_mu1_chi2 = pat_mu1.chi2
        var_mu1_ndof = pat_mu1.ndof
        var_mu1_timeAtIpInOut = 0
        var_mu1_direction = 0
        var_mu1_trackIso = pat_mu1.trackIso
        var_mu1_is_global = pat_mu1.isGlobal
        var_mu1_is_medium = pat_mu1.isMedium
        var_mu1_is_tracker = pat_mu1.isTracker
        var_mu1_is_high_purity = pat_mu1.highPurity
        var_mu1_nTrackerLayers = pat_mu1.nTrackerLayers
        var_mu1_ndthits = pat_mu1.nDTHits
        var_mu1_ncschits = pat_mu1.nCSCHits
        var_mu1_ndtstations = pat_mu1.nDTStations
        var_mu1_ncscstations = pat_mu1.nCSCStations
    else:
        dsa_mu1 = all_dsa_muons[ref_mu1.idx]
        var_mu1_x = dsa_mu1.x
        var_mu1_y = dsa_mu1.y
        var_mu1_z = dsa_mu1.z
        var_mu1_px = dsa_mu1.px
        var_mu1_py = dsa_mu1.py
        var_mu1_pz = dsa_mu1.pz
        var_mu1_pt = math.hypot(var_mu1_px, var_mu1_py)
        var_mu1_pterr = dsa_mu1.ptError
        var_mu1_eta = dsa_mu1.eta
        var_mu1_phi = dsa_mu1.phi
        var_mu1_q = dsa_mu1.charge
        var_mu1_d0_pv = dsa_mu1.d0_pv
        var_mu1_d0sig_pv = dsa_mu1.d0sig_pv
        var_mu1_dz_pv = dsa_mu1.dz_pv
        var_mu1_dzsig_pv = dsa_mu1.dzsig_pv
        var_mu1_chi2 = dsa_mu1.chi2
        var_mu1_ndof = dsa_mu1.ndof
        var_mu1_timeAtIpInOut = dsa_mu1.timeAtIpInOut
        var_mu1_direction = dsa_mu1.direction
        var_mu1_trackIso = 0
        var_mu1_is_global = 0
        var_mu1_is_medium = 0
        var_mu1_is_tracker = 0
        var_mu1_is_high_purity = 0
        var_mu1_nTrackerLayers = 0
        var_mu1_ndthits = dsa_mu1.nDTHits
        var_mu1_ncschits = dsa_mu1.nCSCHits
        var_mu1_ndtstations = dsa_mu1.nDTStations
        var_mu1_ncscstations = dsa_mu1.nCSCStations

    # Muon 2
    var_mu2_is_pat = 1 if (ref_mu2.kind == 'PAT') else 0
    var_mu2_iso = ref_mu2.iso

    if ref_mu2.kind == 'PAT':
        pat_mu2 = all_pat_muons[ref_mu2.idx]
        var_mu2_x = pat_mu2.x
        var_mu2_y = pat_mu2.y
        var_mu2_z = pat_mu2.z
        var_mu2_px = pat_mu2.px
        var_mu2_py = pat_mu2.py
        var_mu2_pz = pat_mu2.pz
        var_mu2_pt = math.hypot(var_mu2_px, var_mu2_py)
        var_mu2_pterr = pat_mu2.ptError
        var_mu2_eta = pat_mu2.eta
        var_mu2_phi = pat_mu2.phi
        var_mu2_q = pat_mu2.charge
        var_mu2_d0_pv = pat_mu2.d0_pv
        var_mu2_d0sig_pv = pat_mu2.d0sig_pv
        var_mu2_dz_pv = pat_mu2.dz_pv
        var_mu2_dzsig_pv = pat_mu2.dzsig_pv
        var_mu2_chi2 = pat_mu2.chi2
        var_mu2_ndof = pat_mu2.ndof
        var_mu2_timeAtIpInOut = 0
        var_mu2_direction = 0
        var_mu2_trackIso = pat_mu2.trackIso
        var_mu2_is_global = pat_mu2.isGlobal
        var_mu2_is_medium = pat_mu2.isMedium
        var_mu2_is_tracker = pat_mu2.isTracker
        var_mu2_is_high_purity = pat_mu2.highPurity
        var_mu2_nTrackerLayers = pat_mu2.nTrackerLayers
        var_mu2_ndthits = pat_mu2.nDTHits
        var_mu2_ncschits = pat_mu2.nCSCHits
        var_mu2_ndtstations = pat_mu2.nDTStations
        var_mu2_ncscstations = pat_mu2.nCSCStations
    else:
        dsa_mu2 = all_dsa_muons[ref_mu2.idx]
        var_mu2_x = dsa_mu2.x
        var_mu2_y = dsa_mu2.y
        var_mu2_z = dsa_mu2.z
        var_mu2_px = dsa_mu2.px
        var_mu2_py = dsa_mu2.py
        var_mu2_pz = dsa_mu2.pz
        var_mu2_pt = math.hypot(var_mu2_px, var_mu2_py)
        var_mu2_pterr = dsa_mu2.ptError
        var_mu2_eta = dsa_mu2.eta
        var_mu2_phi = dsa_mu2.phi
        var_mu2_q = dsa_mu2.charge
        var_mu2_d0_pv = dsa_mu2.d0_pv
        var_mu2_d0sig_pv = dsa_mu2.d0sig_pv
        var_mu2_dz_pv = dsa_mu2.dz_pv
        var_mu2_dzsig_pv = dsa_mu2.dzsig_pv
        var_mu2_chi2 = dsa_mu2.chi2
        var_mu2_ndof = dsa_mu2.ndof
        var_mu2_timeAtIpInOut = dsa_mu2.timeAtIpInOut
        var_mu2_direction = dsa_mu2.direction
        var_mu2_trackIso = 0
        var_mu2_is_global = 0
        var_mu2_is_medium = 0
        var_mu2_is_tracker = 0
        var_mu2_is_high_purity = 0
        var_mu2_nTrackerLayers = 0
        var_mu2_ndthits = dsa_mu2.nDTHits
        var_mu2_ncschits = dsa_mu2.nCSCHits
        var_mu2_ndtstations = dsa_mu2.nDTStations
        var_mu2_ncscstations = dsa_mu2.nCSCStations

    # Dimuon
    var_dim_vx = dimuon.x
    var_dim_vy = dimuon.y
    var_dim_vz = dimuon.z
    var_dim_dx = dimuon.dx
    var_dim_dy = dimuon.dy
    var_dim_dz = dimuon.dz
    var_dim_pt = dimuon.pt
    var_dim_eta = dimuon.eta
    var_dim_phi = dimuon.phi
    var_dim_mass = dimuon.mass
    var_dim_massunc = dimuon.massunc
    var_dim_dca = dimuon.dca
    var_dim_cosAlpha = dimuon.cosAlpha
    var_dim_cosAlphaOrig = dimuon.cosAlphaOrig
    var_dim_deltaPhi = dimuon.deltaPhi
    var_dim_deltaR = dimuon.deltaR
    var_dim_isoLxy = dimuon.isoLxy
    var_dim_isoPmumu = dimuon.isoPmumu
    var_dim_normChi2 = dimuon.normChi2

    # Pack Variables
    variables = np.asarray([
        # Muon 1 [0, 30]
        var_mu1_is_pat,
        var_mu1_x, var_mu1_y, var_mu1_z,
        var_mu1_px, var_mu1_py, var_mu1_pz,
        var_mu1_pt, var_mu1_pterr,
        var_mu1_eta, var_mu1_phi, var_mu1_q,
        var_mu1_d0_pv, var_mu1_d0sig_pv,
        var_mu1_dz_pv, var_mu1_dzsig_pv,
        var_mu1_chi2, var_mu1_ndof,
        var_mu1_timeAtIpInOut, var_mu1_direction,
        var_mu1_iso, var_mu1_trackIso,
        var_mu1_is_global, var_mu1_is_medium,
        var_mu1_is_tracker, var_mu1_is_high_purity,
        var_mu1_nTrackerLayers,
        var_mu1_ndthits, var_mu1_ncschits,
        var_mu1_ndtstations, var_mu1_ncscstations,
        # Muon 2 [31, 61]
        var_mu2_is_pat,
        var_mu2_x, var_mu2_y, var_mu2_z,
        var_mu2_px, var_mu2_py, var_mu2_pz,
        var_mu2_pt, var_mu2_pterr,
        var_mu2_eta, var_mu2_phi, var_mu2_q,
        var_mu2_d0_pv, var_mu2_d0sig_pv,
        var_mu2_dz_pv, var_mu2_dzsig_pv,
        var_mu2_chi2, var_mu2_ndof,
        var_mu2_timeAtIpInOut, var_mu2_direction,
        var_mu2_iso, var_mu2_trackIso,
        var_mu2_is_global, var_mu2_is_medium,
        var_mu2_is_tracker, var_mu2_is_high_purity,
        var_mu2_nTrackerLayers,
        var_mu2_ndthits, var_mu2_ncschits,
        var_mu2_ndtstations, var_mu2_ncscstations,
        # Dimuon [62, 78]
        var_dim_vx, var_dim_vy, var_dim_vz,
        var_dim_dx, var_dim_dy, var_dim_dz,
        var_dim_pt, var_dim_eta, var_dim_phi,
        var_dim_mass, var_dim_massunc, var_dim_dca,
        var_dim_cosAlpha, var_dim_cosAlphaOrig,
        var_dim_deltaPhi, var_dim_deltaR,
        var_dim_isoLxy, var_dim_isoPmumu,
        var_dim_normChi2
    ])

    # Return
    return variables


def get_pairing_params(gen_dimuon, reco_dimuon):
    # Composition
    compositions = {'DSA': 1, 'HYBRID': 2, 'PAT': 3}
    reco_composition = compositions[reco_dimuon.composition]

    # Short-Circuit: No GEN info
    if gen_dimuon is None:
        return np.asarray([
            0,
            0, 0,
            0, 0,
            0, 0, 0,
            reco_composition
        ])

    # Get GEN Parameters
    gen_param_q = gen_dimuon.charge
    gen_param_pt = gen_dimuon.p4.Pt()
    gen_param_eta = gen_dimuon.p4.Eta()
    gen_param_phi = gen_dimuon.p4.Phi()
    gen_param_vx = gen_dimuon.x
    gen_param_vy = gen_dimuon.y
    gen_param_vz = gen_dimuon.z

    # Pack Gen Parameters
    gen_parameters = np.asarray([
        1,
        gen_param_q, gen_param_pt,
        gen_param_eta, gen_param_phi,
        gen_param_vx, gen_param_vy, gen_param_vz,
        reco_composition
    ])

    # Return
    return gen_parameters
