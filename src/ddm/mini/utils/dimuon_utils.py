def align_reco_muons(dsa_muons, pat_muons, dimuons):
    # Collect RECO Indexes
    pat_idx_col = set()
    dsa_idx_col = set()

    for dimuon in dimuons:
        for ref_muon in (dimuon.mu1, dimuon.mu2):
            if ref_muon.kind == 'PAT':
                pat_idx_col.add(ref_muon.idx)
            elif ref_muon.kind == 'DSA':
                dsa_idx_col.add(ref_muon.idx)

    # Build RECO Index
    pat_muons_by_idx = {mu.idx: mu for mu in pat_muons}
    dsa_muons_by_idx = {mu.idx: mu for mu in dsa_muons}

    # Get RECO muons
    keep_pat_muons = [pat_muons_by_idx[idx] for idx in pat_idx_col]
    keep_dsa_muons = [dsa_muons_by_idx[idx] for idx in dsa_idx_col]

    # Return
    return keep_dsa_muons, keep_pat_muons


def align_dimuons(
        dimuons,
        dsa_muons=None, pat_muons=None,
        dsa_muon_idx_col=None, pat_muon_idx_col=None,
        reco_muons=None,
        require_both=True
):
    # Build Index Sets
    if dsa_muons is not None and pat_muons is not None:
        selected_indices = {
            "DSA": set([mu.idx for mu in dsa_muons]),
            "PAT": set([mu.idx for mu in pat_muons]),
        }
    elif dsa_muon_idx_col is not None and pat_muon_idx_col is not None:
        selected_indices = {
            "DSA": dsa_muon_idx_col,
            "PAT": pat_muon_idx_col,
        }
    elif reco_muons is not None:
        selected_indices = {
            "DSA": set([mu.idx for mu in reco_muons if mu.kind == 'DSA']),
            "PAT": set([mu.idx for mu in reco_muons if mu.kind == 'PAT']),
        }
    else:
        raise ValueError('Invalid arguments.')

    # Given a sel_indices dictionary of DSA and PAT indices and a dimuon,
    # determine if this dimuon should be kept
    def dimuon_filter(dimuon, sel_indices):
        if dimuon.composition != "HYBRID":
            tag1, tag2 = dimuon.composition, dimuon.composition
        else:
            tag1, tag2 = "DSA", "PAT"

        if require_both:
            return (dimuon.mu1.idx in sel_indices[tag1]) and (dimuon.mu2.idx in sel_indices[tag2])

        return (dimuon.mu1.idx in sel_indices[tag1]) or (dimuon.mu2.idx in sel_indices[tag2])

    dimuons = [
        dim for dim in dimuons
        if dimuon_filter(dim, selected_indices)
    ]

    # Return
    return dimuons
