import math

import numpy as np

association_var_names = [
    # DSA [0, 16]
    'dsa_x', 'dsa_y', 'dsa_z',
    'dsa_px', 'dsa_py', 'dsa_pz',
    'dsa_pt', 'dsa_pterr',
    'dsa_eta', 'dsa_phi', 'dsa_q',
    'dsa_d0_pv', 'dsa_d0sig_pv',
    'dsa_dz_pv', 'dsa_dzsig_pv',
    'dsa_chi2', 'dsa_ndof',
    # PAT [17, 33]
    'pat_x', 'pat_y', 'pat_z',
    'pat_px', 'pat_py', 'pat_pz',
    'pat_pt', 'pat_pterr',
    'pat_eta', 'pat_phi', 'pat_q',
    'pat_d0_pv', 'pat_d0sig_pv',
    'pat_dz_pv', 'pat_dzsig_pv',
    'pat_chi2', 'pat_ndof',
    # Matching [34, 36]
    'dsa_nsegments', 'matched_nsegments',
    'dR_pd', 'dR_pp',
    # PAT EXTRA [37, 41]
    'pat_is_global', 'pat_is_medium',
    'pat_is_tracker', 'pat_is_high_purity',
    'pat_nTrackerLayers',
    # DSA HITS [42, 45]
    'dsa_ndthits', 'dsa_ncschits',
    'dsa_ndtstations', 'dsa_ncscstations',
    # PAT HITS [46, 49]
    'pat_ndthits', 'pat_ncschits',
    'pat_ndtstations', 'pat_ncscstations'
]


def get_association_vars(
        dsa_muon, pat_muon,
        var_nsegments, var_dR_pd, var_dR_pp
):
    # When dR_pd is 999 this means that dsamu track should be used for comparison
    # Propagate reco muons and compare them
    if var_dR_pd == 999:
        var_dR_pd = dsa_muon.p3.DeltaR(pat_muon.p3)
        var_dR_pp = dsa_muon.pos.DeltaR(pat_muon.pos)

    # Get DSA Vars
    var_dsa_x = dsa_muon.x
    var_dsa_y = dsa_muon.y
    var_dsa_z = dsa_muon.z
    var_dsa_px = dsa_muon.px
    var_dsa_py = dsa_muon.py
    var_dsa_pz = dsa_muon.pz
    var_dsa_pt = math.hypot(var_dsa_px, var_dsa_py)
    var_dsa_pterr = dsa_muon.ptError
    var_dsa_eta = dsa_muon.eta
    var_dsa_phi = dsa_muon.phi
    var_dsa_q = dsa_muon.charge
    var_dsa_d0_pv = dsa_muon.d0_pv
    var_dsa_d0sig_pv = dsa_muon.d0sig_pv
    var_dsa_dz_pv = dsa_muon.dz_pv
    var_dsa_dzsig_pv = dsa_muon.dzsig_pv
    var_dsa_chi2 = dsa_muon.chi2
    var_dsa_ndof = dsa_muon.ndof
    var_dsa_nsegments = dsa_muon.nSegments

    # Get PAT Vars
    var_pat_x = pat_muon.x
    var_pat_y = pat_muon.y
    var_pat_z = pat_muon.z
    var_pat_px = pat_muon.px
    var_pat_py = pat_muon.py
    var_pat_pz = pat_muon.pz
    var_pat_pt = math.hypot(var_pat_px, var_pat_py)
    var_pat_pterr = pat_muon.ptError
    var_pat_eta = pat_muon.eta
    var_pat_phi = pat_muon.phi
    var_pat_q = pat_muon.charge
    var_pat_d0_pv = pat_muon.d0_pv
    var_pat_d0sig_pv = pat_muon.d0sig_pv
    var_pat_dz_pv = pat_muon.dz_pv
    var_pat_dzsig_pv = pat_muon.dzsig_pv
    var_pat_chi2 = pat_muon.chi2
    var_pat_ndof = pat_muon.ndof

    # PAT Extra
    var_pat_is_global = pat_muon.isGlobal
    var_pat_is_medium = pat_muon.isMedium
    var_pat_is_tracker = pat_muon.isTracker
    var_pat_is_high_purity = pat_muon.highPurity
    var_pat_nTrackerLayers = pat_muon.nTrackerLayers

    # DSA Hits
    var_dsa_ndthits = dsa_muon.nDTHits
    var_dsa_ncschits = dsa_muon.nCSCHits
    var_dsa_ndtstations = dsa_muon.nDTStations
    var_dsa_ncscstations = dsa_muon.nCSCStations

    # PAT Hits
    var_pat_ndthits = pat_muon.nDTHits
    var_pat_ncschits = pat_muon.nCSCHits
    var_pat_ndtstations = pat_muon.nDTStations
    var_pat_ncscstations = pat_muon.nCSCStations

    # Pack Variables
    variables = np.asarray([
        # DSA [0, 16]
        var_dsa_x, var_dsa_y, var_dsa_z,
        var_dsa_px, var_dsa_py, var_dsa_pz,
        var_dsa_pt, var_dsa_pterr,
        var_dsa_eta, var_dsa_phi, var_dsa_q,
        var_dsa_d0_pv, var_dsa_d0sig_pv,
        var_dsa_dz_pv, var_dsa_dzsig_pv,
        var_dsa_chi2, var_dsa_ndof,
        # PAT [17, 33]
        var_pat_x, var_pat_y, var_pat_z,
        var_pat_px, var_pat_py, var_pat_pz,
        var_pat_pt, var_pat_pterr,
        var_pat_eta, var_pat_phi, var_pat_q,
        var_pat_d0_pv, var_pat_d0sig_pv,
        var_pat_dz_pv, var_pat_dzsig_pv,
        var_pat_chi2, var_pat_ndof,
        # Matching [34, 36]
        var_dsa_nsegments, var_nsegments,
        var_dR_pd, var_dR_pp,
        # PAT EXTRA [37, 41]
        var_pat_is_global, var_pat_is_medium,
        var_pat_is_tracker, var_pat_is_high_purity,
        var_pat_nTrackerLayers,
        # DSA HITS [42, 45]
        var_dsa_ndthits, var_dsa_ncschits,
        var_dsa_ndtstations, var_dsa_ncscstations,
        # PAT HITS [46, 49]
        var_pat_ndthits, var_pat_ncschits,
        var_pat_ndtstations, var_pat_ncscstations
    ])

    # Return
    return variables


def get_association_params(gen_muon, is_match):
    # Short-Circuit: No GEN info
    if gen_muon is None:
        return np.asarray([
            is_match,
            0, 0,
            0, 0,
            0, 0, 0
        ])

    # Get GEN Parameters
    gen_param_q = gen_muon.charge
    gen_param_pt = gen_muon.pt
    gen_param_eta = gen_muon.eta
    gen_param_phi = gen_muon.phi
    gen_param_vx = gen_muon.x
    gen_param_vy = gen_muon.y
    gen_param_vz = gen_muon.z

    # Pack Gen Parameters
    gen_parameters = np.asarray([
        is_match,
        gen_param_q, gen_param_pt,
        gen_param_eta, gen_param_phi,
        gen_param_vx, gen_param_vy, gen_param_vz
    ])

    # Return
    return gen_parameters
