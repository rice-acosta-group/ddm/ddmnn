import copy
import math

from ROOT import TLorentzVector


# Takes a muon and TRandom and smears the
# muon pT from a Gaussian
def smear_muon_pt(muon, random, debug=False):
    if debug:
        print("Original Muon")
        print(muon)

    smear = random.Gaus(1, 0.13)

    if smear < 0:
        smear = 1e-9

    # 1/pT -> sigma / pT
    new_pt = muon.pt / smear
    new_px = muon.px / smear
    new_py = muon.py / smear
    new_p = math.hypot(new_pt, muon.pz)
    new_energy = math.hypot(new_p, muon.mass)
    new_four_vector = TLorentzVector(new_px, new_py, muon.pz, new_energy)

    smeared_muon = copy.deepcopy(muon)

    # set all the new stuff
    smeared_muon.pt = new_pt
    smeared_muon.px = new_px
    smeared_muon.py = new_py
    smeared_muon.p = new_p
    smeared_muon.eta = new_four_vector.Eta()
    smeared_muon.energy = new_energy

    if debug:
        print("Smeared Muon")
        print(smeared_muon)

    return smeared_muon


# Takes a dimuon and TRandom and smears the dimuon
# mass by smearing its constituent muons individually,
# then recalculating the invariant mass
def smear_dimuon_pt(dimuon, random, debug=False):
    # Smear both muons individually
    smeared_mu1 = smear_muon_pt(dimuon.mu1, random, debug)
    smeared_mu2 = smear_muon_pt(dimuon.mu2, random, debug)

    mu1_four_vector = TLorentzVector(
        smeared_mu1.px, smeared_mu1.py, smeared_mu1.pz, smeared_mu1.energy
    )
    mu2_four_vector = TLorentzVector(
        smeared_mu2.px, smeared_mu2.py, smeared_mu2.pz, smeared_mu2.energy
    )

    dimuon_four_vector = mu1_four_vector + mu2_four_vector

    # copy so we don't edit original muon accidentally
    smeared_dimuon = copy.deepcopy(dimuon)

    # Only change mass, to not affect other kinematic cuts (e.g. cos(a), etc)
    smeared_dimuon.mass = dimuon_four_vector.M()

    # Replace muons in dimuon
    smeared_dimuon.mu1 = smeared_mu1
    smeared_dimuon.mu2 = smeared_mu2

    return smeared_dimuon
