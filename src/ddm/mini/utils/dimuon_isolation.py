from ddm.core.execution.runtime import get_logger
from ddm.mini.commons.isolation import yield_iso_thresholds
from ddm.mini.utils.isolation_utils import should_add_to_iso
from ddm.mini.utils.muon_matching import calc_deltaR


def append_dimuon_isolation(sel_dimuons, all_pat_muons):
    """
    Cleans isolation calculated between dimuon constituent muons and appends to dimuon class
    """

    for sel_dimuon in sel_dimuons:
        if sel_dimuon.composition not in ('PAT', 'HYBRID'):
            clean_dimuon_isolation(
                sel_dimuon, all_pat_muons,
                'iso', 'cleanedIso',
            )
        else:
            clean_dimuon_isolation(
                sel_dimuon, all_pat_muons,
                'trackIso', 'cleanedIso',
            )

            if len(all_pat_muons) == 0:
                continue

            if getattr(all_pat_muons[0], 'tkiso0p3_sumPt_dz0p2_dxy0p1', None) is None:
                continue

            # Loop Branches
            for dR, dz, d0 in yield_iso_thresholds():
                source_branch_name = 'tkiso0p{}_sumPt_dz0p{}_dxy0p{}'.format(
                    int(10 * dR), int(10 * dz), int(10 * d0)
                )
                cleaned_branch_name = f'clnd_{source_branch_name}'

                clean_dimuon_isolation(
                    sel_dimuon, all_pat_muons,
                    source_branch_name,
                    cleaned_branch_name,
                    dR, dz, d0,
                )


def clean_dimuon_isolation(
        sel_dimuon, all_pat_muons,
        source_branch_name,
        cleaned_branch_name,
        dR=0.3, dz=0.2, d0=0.1,
        dRVeto=0.01,
        debug=False,
):
    """
    sel_dimuon          - dimuon which needs cleaning
    all_pat_muons       - list of original PAT muons that contain those belonging to the Dimuon
    source_branch_name  - string for track isolation value from which to subtract from
    cleaned_branch_name - string where we should store the new cleaned isolation
    dR                  - cone size in which track isolation was summed
    dz                  - [cm] window in z in which tracks are added
    d0                  - [cm] d0 of tracks added
    """

    def get_reco_muons(dim):
        if dim.composition == 'PAT':
            return all_pat_muons[dim.mu1.idx], all_pat_muons[dim.mu2.idx]
        elif dim.composition == 'DSA':
            return dim.mu1, dim.mu2
        else:
            return dim.mu1, all_pat_muons[dim.mu2.idx]

    assert sel_dimuon.composition in (
        'PAT', 'HYBRID', 'DSA',
    ), 'Invalid dimuon type: {}'.format(sel_dimuon.composition)

    # mu2 is always a PAT muon, if PAT or hybrid
    mu1, mu2 = get_reco_muons(sel_dimuon)

    mu1_cleaned_iso = getattr(mu1, source_branch_name, -100.0)
    mu2_cleaned_iso = getattr(mu2, source_branch_name, -100.0)

    # This will remove PAT muon tracks from DSA muon isolation sum in a cone of dR.
    if sel_dimuon.composition == 'DSA':
        for pat_muon in all_pat_muons:
            # Need to subtract PAT muon
            deltaR = calc_deltaR(mu1, pat_muon)

            if deltaR < dR:
                mu1_cleaned_iso -= pat_muon.p3_tk.Perp() / mu1.pt

                if debug and (mu1_cleaned_iso < -0.001):
                    get_logger().warn('\nWarning Mu1 Cleaned Isolation Calculated as < 0: {}\n\n'.format(mu1_cleaned_iso))
                elif debug:
                    get_logger().debug('\nSensible Mu1 Cleaned Isolation Calculated: {}\n\n'.format(mu1_cleaned_iso))

            # Need to subtract PAT muon
            deltaR = calc_deltaR(mu2, pat_muon)

            if deltaR < dR:
                mu2_cleaned_iso -= pat_muon.p3_tk.Perp() / mu2.pt

                if debug and (mu2_cleaned_iso < -0.001):
                    get_logger().warn('\nWarning Mu2 Cleaned Isolation Calculated as < 0: {}\n\n'.format(mu2_cleaned_iso))
                elif debug:
                    get_logger().debug('\nSensible Mu2 Cleaned Isolation Calculated: {}\n\n'.format(mu2_cleaned_iso))

    # This will remove PAT muon tracks from DSA muon isolation sum in a cone of dR.
    elif sel_dimuon.composition == 'HYBRID':
        for pat_muon in all_pat_muons:
            # Need to subtract PAT muon
            deltaR = calc_deltaR(mu1, pat_muon)

            if deltaR < dR:
                mu1_cleaned_iso -= pat_muon.p3_tk.Perp() / mu1.pt

                if debug and (mu1_cleaned_iso < -0.001):
                    get_logger().warn('\nWarning Mu1 Cleaned Isolation Calculated as < 0: {}\n\n'.format(mu1_cleaned_iso))
                elif debug:
                    get_logger().debug('\nSensible Mu1 Cleaned Isolation Calculated: {}\n\n'.format(mu1_cleaned_iso))

    elif sel_dimuon.composition == 'PAT':
        # Need to subtract other muon
        deltaR = calc_deltaR(mu1, mu2, use_tracker_track=True)

        if deltaR < dR:
            add_to_isolation = should_add_to_iso(
                mu1, mu2, deltaR,
                dz, d0, dRVeto
            )

            if add_to_isolation:
                mu1_cleaned_iso -= mu2.p3_tk.Perp()

            if debug and (mu1_cleaned_iso < -0.001):
                get_logger().warn('\nWarning Mu1 Cleaned Isolation Calculated as < 0: {}\n\n'.format(mu1_cleaned_iso))

            add_to_isolation = should_add_to_iso(
                mu2, mu1, deltaR,
                dz, d0, dRVeto
            )

            if add_to_isolation:
                mu2_cleaned_iso -= mu1.p3_tk.Perp()

            if debug and (mu2_cleaned_iso < -0.001):
                get_logger().debug('\nWarning Mu2 Cleaned Isolation Calculated as < 0: {}\n\n'.format(mu2_cleaned_iso))

    # Ignore floating point errors
    # Set to 0, so we don't have weird things happen later
    mu1_cleaned_iso = max(0.0, mu1_cleaned_iso)
    mu2_cleaned_iso = max(0.0, mu2_cleaned_iso)

    # Save Cleaned Isolation
    setattr(sel_dimuon.mu1, cleaned_branch_name, mu1_cleaned_iso)
    setattr(sel_dimuon.mu2, cleaned_branch_name, mu2_cleaned_iso)

    if debug:
        get_logger().debug(f'Dimuon Type: {sel_dimuon.composition}')

        dim_mu2_iso = getattr(sel_dimuon.mu2, cleaned_branch_name)
        pat_mu2_iso = getattr(mu2, cleaned_branch_name)
        diff_mu2 = abs(dim_mu2_iso - pat_mu2_iso) > 1e-3

        if debug and diff_mu2:
            get_logger().debug(f"""
            mu2 (pt: {mu2.p3_tk.Perp():3.2f} eta: {mu2.eta:3.2f} phi: {mu2.phi:3.2f})
                initial iso: {getattr(mu2, source_branch_name):3.2f}
                cleaned Dim iso: {getattr(sel_dimuon.mu2, cleaned_branch_name):3.2f}
                cleaned PAT iso: {getattr(mu2, cleaned_branch_name):3.2f}
            """)

        if sel_dimuon.composition == 'PAT':
            # don't bother showing this unless we have two PAT muons
            dim_mu1_iso = getattr(sel_dimuon.mu1, cleaned_branch_name)
            pat_mu1_iso = getattr(mu1, cleaned_branch_name)
            diff_mu1 = abs(dim_mu1_iso - pat_mu1_iso) > 1e-3

            if debug and diff_mu1:
                get_logger().debug(f"""
                === dR {dR:3.2f} dz: {dz:3.2f} d0: {d0:3.2f} ===
                mu1 (pt: {mu1.p3_tk.Perp():3.2f} eta: {mu1.eta:3.2f} phi: {mu1.phi:3.2f})
                    initial iso: {getattr(mu1, source_branch_name):3.2f}
                    cleaned Dim iso: {getattr(sel_dimuon.mu1, cleaned_branch_name):3.2f}
                    cleaned PAT iso: {getattr(mu1, cleaned_branch_name):3.2f}
                """)
