DEFAULT_DSA_PAT_SEG_MATCH_DR = 999
DEFAULT_DSA_PAT_SEG_MATCH = (0, DEFAULT_DSA_PAT_SEG_MATCH_DR, DEFAULT_DSA_PAT_SEG_MATCH_DR)


def get_dsa_pat_seg_match(entry):
    dsa_pat_seg_match = dict()

    for dsa_muon in entry.ext_dsa_muons:
        for match_idx, pat_idx in enumerate(dsa_muon.idx_SegmMatch):
            dsapat_nsegments = dsa_muon.nSegms_SegmMatch[match_idx]
            dsapat_dR_pd = dsa_muon.deltaR_pd_SegmMatch[match_idx]
            dsapat_dR_pp = dsa_muon.deltaR_pp_SegmMatch[match_idx]

            if dsapat_dR_pd == -999:
                dsapat_dR_pd = DEFAULT_DSA_PAT_SEG_MATCH_DR

            if dsapat_dR_pp == -999:
                dsapat_dR_pp = DEFAULT_DSA_PAT_SEG_MATCH_DR

            key = (dsa_muon.idx, pat_idx)
            dsa_pat_seg_match[key] = (dsapat_nsegments, dsapat_dR_pd, dsapat_dR_pp)

    return dsa_pat_seg_match
