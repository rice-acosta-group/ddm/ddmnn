import math

from ddm.core.execution.runtime import get_logger
from ddm.mini.commons.constants import YEAR
from ddm.mini.utils.muon_matching import match_muons

"""
Look at the expected efficiency (Jun 14, 2021)

Derived from N(expected JPsi) in both portions of 2016
after correcting for luminosity ratios

See
fast/studies/PAT-PAT/jpsi_hip_effect_study.py
fast/studies/PAT-PAT/jpsi_hip_comparison.py
"""


def get_hip_eff(lxy):
    """Get the efficiency via linear fit."""
    slope = -0.09335854
    offset = 1.0271236
    efficiency = slope * lxy + offset

    # keep bounds in check
    if efficiency > 1.0:
        efficiency = 1.0
    elif efficiency < 0.0:
        efficiency = 0.0

    # dimuon eff = muon eff * muon eff
    # therefore take the square root
    return math.sqrt(efficiency)


def get_hip_corrected_pat_muons(entry, pat_muons, debug=False):
    """Return PAT muons after corrections accounting for HIP effect have been applied."""
    if YEAR != 2016:
        get_logger().info("No HIP effect in 2018")
        return pat_muons

    # Percentage of 2016 data effected by the HIP effect
    PERCENT_HIP = 0.53
    random_for_lumi = entry.random.Rndm()

    # Select if we are in the right lumi section, otherwise do nothing
    if random_for_lumi > PERCENT_HIP:
        return pat_muons

    # For the remaining events, we need Lxy, so we need gen information
    try:
        gen_particles = entry.ext_gen_particles
    except:
        get_logger().info("Don't need to HIP correct in data")
        return pat_muons

    # Get signal muons
    gen_muons = [
        gen for gen in gen_particles
        if abs(gen.pdgID) == 13 and gen.is_final_state and (gen.kind == 'signal')
    ]

    # Match GEN muons
    matched_pat = []
    hip_corrected_matched_pat = []

    for gen, pat, dR in match_muons(gen_muons, pat_muons):
        matched_pat.append(pat)

        random_for_hip = entry.random.Rndm()

        if debug:
            get_logger().debug(
                f'Lxy: {gen.Lxy:5.2f}, '
                f'eff: {get_hip_eff(gen.Lxy):.2f}, '
                f'rnd: {random_for_hip:.2f}, '
                f'pass? {random_for_hip < get_hip_eff(gen.Lxy):.2f}'
            )

        if random_for_hip >= get_hip_eff(gen.Lxy):
            continue

        hip_corrected_matched_pat.append(pat)

    unmatched_pat = [
        pat for pat in pat_muons
        if pat not in matched_pat
    ]

    return hip_corrected_matched_pat + unmatched_pat
