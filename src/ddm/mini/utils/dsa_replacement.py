from ddm.mini.utils.dimuon_utils import align_dimuons


def replace_dsa_muons(
        dsa_muons, pat_muons, dimuons,
        nn_en=False,
):
    # Index PAT muons
    pat_muons_by_idx = {mu.idx: mu for mu in pat_muons}

    # Filter DSA muons based on whether there was a PAT match.
    # After this there are two lists:
    #   1. PAT muons which replaced a DSA muon
    #   2. DSA muons which matched no PAT muon
    keep_dsa_muons = list()
    keep_pat_muons = list()
    keep_pat_muons_idx_col = set()

    for dsa_muon in dsa_muons:
        if nn_en:
            candidate = find_displaced_pat_candidate(
                dsa_muon, pat_muons_by_idx
            )

            if candidate is None:
                candidate = find_nn_candidate(
                    dsa_muon, pat_muons_by_idx
                )
        else:
            candidate = find_displaced_pat_candidate(
                dsa_muon, pat_muons_by_idx
            )

            if candidate is None:
                candidate = find_standard_pat_candidate(
                    dsa_muon, pat_muons_by_idx
                )

        if candidate is not None:
            # Short-Circuit: It was already used
            if candidate in keep_pat_muons_idx_col:
                continue

            # Attach the index of the DSA muon
            # to the PAT muon replacing it
            pat_muon = pat_muons_by_idx[candidate]
            pat_muon.replaced_dsa_muon = dsa_muon

            keep_pat_muons.append(pat_muon)
            keep_pat_muons_idx_col.add(candidate)
        else:
            keep_dsa_muons.append(dsa_muon)

    # Filter Dimuons
    keep_dimuons = align_dimuons(
        dimuons,
        dsa_muons=keep_dsa_muons,
        pat_muons=keep_pat_muons
    )

    # Return
    return keep_dsa_muons, keep_pat_muons, keep_dimuons


def find_displaced_pat_candidate(dsa_muon, pat_muons_by_idx):
    # Short-Circuit: No displaced PAT match
    if dsa_muon.idx_DispMatch < 0:
        return None

    # No available input PATs to match to
    if dsa_muon.idx_DispMatch not in pat_muons_by_idx:
        return None

    # Update flag
    dsa_muon.replacement_status = 30

    # Return
    return dsa_muon.idx_DispMatch


def find_standard_pat_candidate(dsa_muon, pat_muons_by_idx):
    # Short-Circuit: No standard PAT match
    if dsa_muon.stdMatch_idx < 0:
        return None

    # No available input PATs to match to
    if dsa_muon.stdMatch_idx not in pat_muons_by_idx:
        return None

    # Short-Circuit: deltaR is too big
    if dsa_muon.stdMatch_minDR_std > 0.4:
        return None

    # Update flag
    dsa_muon.replacement_status = 31

    # Return
    return dsa_muon.stdMatch_idx


def find_nn_candidate(dsa_muon, pat_muons_by_idx):
    # Short-Circuit: If NNMatch didn't find a PAT Muon then return None
    if dsa_muon.idx_NNMatch < 0:
        return None

    # No available input PATs to match to
    if dsa_muon.idx_NNMatch not in pat_muons_by_idx:
        return None

    # Short-Circuit: MScore above threshold
    if dsa_muon.mscore_NNMatch < 0.99999:
        return None

    # Update flag
    dsa_muon.replacement_status = 40

    # Return
    return dsa_muon.idx_NNMatch
