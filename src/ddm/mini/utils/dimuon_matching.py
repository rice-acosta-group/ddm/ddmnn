from ddm.core.commons.combinatorics import yield_combinations


def match_gen_dimuons(
        gen_dimuons, reco_dimuons,
        deltaR_threshold=0.2,
        deltaVtx_threshold=20.0,
        return_metric_en=False
):
    # Short-Circuit: No gen dimuons
    if len(gen_dimuons) == 0:
        return list()

    # Short-Circuit: No dimuons
    if len(reco_dimuons) == 0:
        return list()

    # Compare Dimuons
    def build_entry(gen_dimuon, reco_dimuon):
        # Compare dimuons
        metric, are_aligned = compare_gen_dimuons(
            gen_dimuon, reco_dimuon,
            deltaR_threshold=deltaR_threshold,
            deltaVtx_threshold=deltaVtx_threshold
        )

        # Short-Circuit: No match
        if metric is None:
            return None

        # Return
        return gen_dimuon, reco_dimuon, metric, are_aligned

    comparisons = [
        build_entry(gen_dimuon, reco_dimuon)
        for gen_dimuon, reco_dimuon in yield_combinations(gen_dimuons, reco_dimuons)
    ]

    comparisons = sorted([
        entry for entry in comparisons
        if (entry is not None)
    ], key=lambda entry: entry[2])

    # Reduce Comparisons
    results = list()
    keep_gen_indices = set()
    keep_reco_indices = {"DSA": set(), "PAT": set()}

    for gen_dimuon, reco_dimuon, metric, are_aligned in comparisons:
        # Unpack Dimuons
        ref_mu1 = reco_dimuon.mu1
        ref_mu2 = reco_dimuon.mu2

        # Short-Circuit: Make sure all objects are unique
        if gen_dimuon.idx in keep_gen_indices:
            continue

        if ref_mu1.idx in keep_reco_indices[ref_mu1.kind]:
            continue

        if ref_mu2.idx in keep_reco_indices[ref_mu2.kind]:
            continue

        # if reco_dimuon.composition == 'DSA':
        #     if are_aligned:
        #         mu1_matched = reco_dimuon.mu1.matched_gen_muon is not None and gen_dimuon.mu1.idx == reco_dimuon.mu1.matched_gen_muon.idx
        #         mu2_matched = reco_dimuon.mu2.matched_gen_muon is not None and gen_dimuon.mu2.idx == reco_dimuon.mu2.matched_gen_muon.idx
        #     else:
        #         mu1_matched = reco_dimuon.mu2.matched_gen_muon is not None and gen_dimuon.mu2.idx == reco_dimuon.mu1.matched_gen_muon.idx
        #         mu2_matched = reco_dimuon.mu1.matched_gen_muon is not None and gen_dimuon.mu1.idx == reco_dimuon.mu2.matched_gen_muon.idx
        #
        #     if not (mu1_matched and mu2_matched):
        #         print(
        #             mu1_matched, mu2_matched, gen_dimuon.pos.Mag(), gen_dimuon.p3.Mag(),
        #             (gen_dimuon.pos-reco_dimuon.pos).Mag(),
        #             (gen_dimuon.p3-reco_dimuon.p3).Mag()
        #         )

        # Save indices
        keep_gen_indices.add(gen_dimuon.idx)
        keep_reco_indices[ref_mu1.kind].add(ref_mu1.idx)
        keep_reco_indices[ref_mu2.kind].add(ref_mu2.idx)

        # Save Results
        if return_metric_en:
            results.append(
                (gen_dimuon, reco_dimuon, are_aligned, metric)
            )
        else:
            results.append(
                (gen_dimuon, reco_dimuon, are_aligned)
            )

    # Return
    return results


def compare_gen_dimuons(
        gen_dimuon, reco_dimuon,
        deltaR_threshold=0.2,
        deltaVtx_threshold=20.
):
    # Check momentum deltaR
    deltaR_mom = gen_dimuon.p4.DeltaR(reco_dimuon.p4)

    if deltaR_mom >= deltaR_threshold:
        return None, None

    # Check position deltaR
    deltaR_pos = gen_dimuon.pos.DeltaR(reco_dimuon.pos)

    if deltaR_pos >= deltaR_threshold:
        return None, None

    # Check vertex distance
    deltaVtx = (reco_dimuon.pos - gen_dimuon.pos).Mag()

    if deltaVtx >= deltaVtx_threshold:
        return None, None

    # Match muon orientation
    deltaR_col, are_aligned = compare_gen_muons(gen_dimuon, reco_dimuon, deltaR_threshold=deltaR_threshold)

    # Short-Circuit: No match
    if deltaR_col is None:
        return None, None

    # Calculate metric
    metric = deltaR_mom + deltaR_pos + deltaR_col[0] + deltaR_col[1]

    # Return
    return metric, are_aligned


def compare_gen_muons(gen_dimuon, reco_dimuon, deltaR_threshold=0.2):
    # Calculate Deltas
    def calc_deltas(gen_dimuon, reco_dimuon, crossed_en=False):
        if crossed_en:
            gen_mu1 = gen_dimuon.mu2
            gen_mu2 = gen_dimuon.mu1
        else:
            gen_mu1 = gen_dimuon.mu1
            gen_mu2 = gen_dimuon.mu2

        # Compare muon 1
        deltaR_1 = gen_mu1.p3.DeltaR(reco_dimuon.mu1.p3)

        if deltaR_1 >= deltaR_threshold:
            deltaR_1 = None

        # Compare muon 2
        deltaR_2 = gen_mu2.p3.DeltaR(reco_dimuon.mu2.p3)

        if deltaR_2 >= deltaR_threshold:
            deltaR_2 = None

        # Return
        return deltaR_1, deltaR_2

    aligned_deltas = calc_deltas(
        gen_dimuon, reco_dimuon, crossed_en=False
    )
    crossed_deltas = calc_deltas(
        gen_dimuon, reco_dimuon, crossed_en=True
    )

    aligned_is_valid = None not in aligned_deltas
    crossed_is_valid = None not in crossed_deltas

    # Short-Circuit: None valid
    if not (aligned_is_valid or crossed_is_valid):
        return None, None

    # Select best alignment
    if aligned_is_valid and (not crossed_is_valid):
        are_aligned = True
        selected_deltas = aligned_deltas
    elif crossed_is_valid and (not aligned_is_valid):
        are_aligned = False
        selected_deltas = crossed_deltas
    elif (aligned_deltas[0] < crossed_deltas[0]) and (aligned_deltas[1] < crossed_deltas[1]):
        are_aligned = True
        selected_deltas = aligned_deltas
    elif (aligned_deltas[0] > crossed_deltas[0]) and (aligned_deltas[1] > crossed_deltas[1]):
        are_aligned = False
        selected_deltas = crossed_deltas
    else:
        sum_deltas_aligned = sum(aligned_deltas)
        sum_deltas_crossed = sum(crossed_deltas)

        if sum_deltas_aligned < sum_deltas_crossed:
            are_aligned = True
            selected_deltas = aligned_deltas
        elif sum_deltas_aligned > sum_deltas_crossed:
            are_aligned = False
            selected_deltas = crossed_deltas
        else:
            return None, None

    # Return
    return selected_deltas, are_aligned
