from ddm.core.execution.runtime import get_logger
from ddm.mini.utils.muon_matching import calc_deltaR


def append_dsa_isolation(sel_dimuons, sel_dsa_muons, all_pat_muons):
    for sel_dimuon in sel_dimuons:
        clean_dsa_isolation(sel_dimuon, sel_dsa_muons, all_pat_muons)


def clean_dsa_isolation(sel_dimuon, sel_dsa_muons, all_pat_muons, dR=0.3):
    """
    sel_dimuon             - dimuon for linking
    sel_dsa_muons          - muons which to clean
    all_pat_muons          - muons to remove tracks from
    dR                     - cone size in which track isolation was summed
    """

    for dsa_muon in sel_dsa_muons:
        # Short-Circuit: Make sure the dsa muon doesn't get processed twice
        is_dsaiso = getattr(dsa_muon, "iso", -50.)

        if is_dsaiso != -50. and is_dsaiso != -100.:
            continue

        # Initialize values
        iso = -100.
        cleaned_iso = -100.
        pat_corrected_iso = -100.
        pat_cleaned_iso = -100.

        if sel_dimuon.composition == "DSA":
            dimuon_ref_mu = None

            if dsa_muon.idx == sel_dimuon.mu1.idx:
                dimuon_ref_mu = sel_dimuon.mu1
            elif dsa_muon.idx == sel_dimuon.mu2.idx:
                dimuon_ref_mu = sel_dimuon.mu2

            if not dimuon_ref_mu:
                iso = -50.
                cleaned_iso = -50.
                pat_corrected_iso = -50.
                pat_cleaned_iso = -50.
            else:
                iso = getattr(dimuon_ref_mu, "iso", -30.)
                cleaned_iso = getattr(dimuon_ref_mu, "cleanedIso", -30.)
                pat_corrected_iso = iso
                pat_cleaned_iso = iso

                associated_pat_idx = getattr(dimuon_ref_mu, "assocPATidx", -1.)

                if associated_pat_idx == -1:
                    pass
                elif associated_pat_idx >= 1000:
                    pat_muon = all_pat_muons[associated_pat_idx - 1000]

                    deltaR = calc_deltaR(pat_muon, dsa_muon)

                    if deltaR < dR:
                        pat_cleaned_iso = iso - pat_muon.p3_tk.Perp() / dsa_muon.pt

                    if (pat_muon.p3_tk.Perp() / dsa_muon.pt) < iso:
                        pat_corrected_iso = iso - pat_muon.p3_tk.Perp() / dsa_muon.pt
                else:
                    get_logger().error("assocPATmu index makes no sense")

        # Save values
        setattr(dsa_muon, "iso", iso)
        setattr(dsa_muon, "cleanedIso", cleaned_iso)
        setattr(dsa_muon, "PATcorrectedIso", pat_corrected_iso)
        setattr(dsa_muon, "PATcleanedIso", pat_cleaned_iso)
