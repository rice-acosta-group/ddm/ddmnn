from ddm.mini.cuts.cut_definitions import CUT_LIST_DEFINITIONS, CUT_DEFINITIONS


class CutList(object):

    def __init__(self, id, *objs):
        self.objs = objs
        self.cuts = CUT_LIST_DEFINITIONS.get(id, tuple())
        self.results = dict()

    def __getitem__(self, cut_id):
        # Special Cases
        if cut_id == 'all':
            return self.passed_all()
        elif cut_id == 'none':
            return self.passed_none()

        return self.passed_cut(cut_id)

    def __bool__(self):
        return self.passed_all()

    __nonzero__ = __bool__

    def passed_all(self):
        # Check if it's been computed
        passed = self.results.get('all', None)

        if passed is not None:
            return passed

        # Test all cuts
        passed = True

        for a_cut_id in self.cuts:
            if not self.passed_cut(a_cut_id):
                passed = False
                break

        self.results['all'] = passed

        return passed

    def passed_none(self):
        # Check if it's been computed
        passed = self.results.get('none', None)

        if passed is not None:
            return passed

        # Test all cuts
        passed = True

        for a_cut_id in self.cuts:
            if self.passed_cut(a_cut_id):
                passed = False
                break

        self.results['none'] = passed

        return passed

    def passed_all_ignoring(self, *ignore_cuts):
        for cut_id in self.cuts:
            if cut_id in ignore_cuts:
                continue

            passed = self.passed_cut(cut_id)

            if not passed:
                return False

        return True

    def passed_all_of(self, *require_cuts):
        for cut_id in require_cuts:
            passed = self.passed_cut(cut_id)

            if not passed:
                return False

        return True

    def passed_cut(self, cut_id):
        # Check if the cut has been tested
        passed = self.results.get(cut_id, None)

        if passed is not None:
            return passed

        # Check the cut list contains the key
        if cut_id not in self.cuts:
            raise KeyError('Cut requested is not part of the cut list')

        # Apply cut to all objects
        passed = True

        for obj in self.objs:
            obj_passed = CUT_DEFINITIONS[cut_id].apply(obj)

            if not obj_passed:
                passed = False
                break

        self.results[cut_id] = passed

        return passed
