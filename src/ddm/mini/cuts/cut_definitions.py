import math
import operator

from ddm.mini.commons.constants import COSMICS_NTUPLES_COMPATIBILITY_MODE, YEAR
from ddm.mini.cuts.cuts import SimpleCut, ConditionalCut, DimMuCut

NPIXELHITSLAMBDA = lambda mu1, mu2: abs(mu1.nPixelHits - mu2.nPixelHits)
NTRKLAYLAMBDA = lambda dim: min(
    getattr(dim.mu1, 'nTrackerLayers', 100),
    getattr(dim.mu2, 'nTrackerLayers', 100)
) + math.floor(dim.Lxy_pv / 15.0)

# dictionaries of Cut objects
CUT_DEFINITIONS = {
    ### PRIMARY VERTEX CUT ###
    'goodVtx': SimpleCut('goodVtx', lambda fil: fil.PrimaryVertexFilter, operator.eq, True),
    ### N(DSA) MUONS CUT ###
    'nPP': SimpleCut('nPP', lambda npp: npp, operator.lt, 6),
    'inPP': SimpleCut('inPP', lambda npp: npp, operator.ge, 6),
    ### GEN ACCEPTANCE CUTS ###
    'a_pT': SimpleCut('pT', lambda mu: mu.pt, operator.gt, 25.0),
    'a_eta': SimpleCut('eta', lambda mu: abs(mu.eta), operator.lt, 2.0),
    'a_Lxy': SimpleCut('Lxy', lambda mu: mu.Lxy, operator.lt, 500.0),
    ### EXTENDED GEN ACCEPTANCE CUTS ###
    'ext_a_pT': SimpleCut('pT', lambda mu: mu.pt, operator.gt, 23.0),
    'ext_a_eta': SimpleCut('eta', lambda mu: abs(mu.eta), operator.lt, 2.4),
    ### DSA MUON QUALITY CUTS ###
    'q_nStations': SimpleCut('nStations', lambda mu: mu.nCSCStations + mu.nDTStations, operator.gt, 1),
    'q_nMuonHits': SimpleCut('nMuonHits', lambda mu: mu.nCSCHits + mu.nDTHits, operator.gt, 12),
    ### PAT MUON QUALITY CUTS ###
    'p_isGlobal': SimpleCut('isGlobal', lambda mu: mu.isGlobal, operator.eq, True),
    'p_isMedium': SimpleCut('isMedium', lambda mu: mu.isMedium, operator.eq, True),
    'p_isTracker': SimpleCut('isTracker', lambda mu: mu.isTracker, operator.eq, True),
    'p_isHighPurity': SimpleCut('isHighPurity', lambda mu: mu.highPurity, operator.eq, True),
    'p_nTrkLays': SimpleCut('nTrkLays', lambda mu: mu.nTrackerLayers, operator.gt, 6),
    'p_globChi2ndof': SimpleCut('globChi2ndof', lambda mu: mu.chi2_Global / mu.ndof_Global, operator.lt, 10),
    'p_globHits': ConditionalCut(
        'globHits',
        lambda mu: mu.nCSCHits + mu.nDTHits,
        operator.gt,
        {True: 0, False: -1},
        lambda mu: mu.isGlobal,
    ),
    'p_trkrStats': ConditionalCut(
        'trkrStats',
        lambda mu: mu.nMatchedStations,
        operator.gt,
        {True: 1, False: -1},
        lambda mu: mu.isTracker,
    ),
    'p_kinkChi2': SimpleCut('kinkChi2', lambda mu: mu.kinkchi2, operator.lt, 20.0),
    'p_posMatch': SimpleCut('posMatch', lambda mu: mu.tkstachi2, operator.lt, 12.0),
    'p_pTtopTwo': SimpleCut('pTtopTwo', lambda mu: mu.pt, operator.gt, 15.0),
    ### MUON CUTS ###
    'm_pT': SimpleCut('pT', lambda mu: mu.pt, operator.gt, 10.0),
    'm_d0Sig': ConditionalCut(
        'd0Sig',
        lambda mu: mu.d0Sig(),
        operator.gt,
        {'DSA': 0.0, 'PAT': 6.0},
        lambda mu: mu.kind,
    ),
    'm_trkChi2': ConditionalCut(
        'trkChi2',
        lambda mu: mu.normChi2,
        operator.lt,
        {'DSA': 2.5, 'PAT': float('inf')},
        lambda mu: mu.kind,
    ),
    'm_nDTHits': ConditionalCut(
        'nDTHits',
        lambda mu: mu.nDTHits,
        operator.gt,
        {True: 18, False: -1},
        lambda mu: mu.nCSCHits == 0 and mu.kind == 'DSA',
    ),
    'm_FPTE': SimpleCut('FPTE', lambda mu: mu.ptError / mu.pt, operator.lt, 1.0),
    ### DIMUON CUTS ###
    'd_LxyErr': SimpleCut('LxyErr', lambda dim: dim.Lxy_pv / dim.LxySig_pv, operator.lt, 99.0),
    'd_mass': SimpleCut('mass', lambda dim: dim.mass, operator.gt, 10.0),
    'd_Zmass': SimpleCut(
        'Zmass', lambda dim: abs(dim.mass - 90), operator.lt, 10.0
    ),  # within 10 GeV of the Z mass
    'd_vtxChi2': ConditionalCut(
        'vtxChi2',
        lambda dim: dim.normChi2,
        operator.lt,
        {'DSA': 20.0, 'PAT': 20.0, 'HYBRID': 20.0},
        lambda dim: dim.composition,
    ),
    'd_cosAlpha': ConditionalCut(
        'cosAlpha',
        lambda dim: dim.cosAlpha,
        operator.gt,
        {
            'DSA': -0.8 if YEAR == 2016 else -0.90,
            'PAT': -0.8 if YEAR == 2016 else -0.99,
            'HYBRID': -0.8 if YEAR == 2016 else -0.90,
        },
        lambda dim: dim.composition,
    ),
    'd_cosAlphaO': ConditionalCut(
        'cosAlphaO',
        lambda dim: dim.cosAlphaOriginal,
        operator.gt,
        {
            'DSA': -0.8 if YEAR == 2016 else -0.90,
            'PAT': -0.8 if YEAR == 2016 else -0.99,
            'HYBRID': -0.8 if YEAR == 2016 else -0.90,
        },
        lambda dim: dim.composition,
    ),
    'd_DCA': ConditionalCut(
        'DCA',
        lambda dim: dim.dca,
        operator.lt,
        {'DSA': 50.0, 'PAT': 50.0, 'HYBRID': 50.0},
        lambda dim: dim.composition,
    ),
    'd_missHits': DimMuCut(
        'missHits',
        lambda ref: getattr(ref, 'missingHitsAfterVtx', 100.0),
        operator.lt,
        {'DSA': float('inf'), 'PAT': 2.0},
        lambda ref: ref.kind,
    ),
    'd_customTrkIso': DimMuCut(
        'customIso',
        lambda ref: ref.inhouseTrackerIsolation(),
        operator.lt,
        {'DSA': float('inf'), 'PAT': 0.075},
        lambda ref: ref.kind,
    ),
    'd_LxySig': ConditionalCut(
        'LxySig',
        lambda dim: dim.LxySig_pv,
        operator.gt,
        {'DSA': 6.0, 'PAT': 6.0, 'HYBRID': 3.0},
        lambda dim: dim.composition,
    ),
    'd_InvLxySig': ConditionalCut(
        'InvLxySig',
        lambda dim: dim.LxySig_pv,
        operator.lt,
        {'DSA': 6.0, 'PAT': 6.0, 'HYBRID': 3.0},
        lambda dim: dim.composition,
    ),
    'd_maxpT': ConditionalCut(
        'maxpT',
        lambda dim: max(dim.mu1.pt, dim.mu2.pt),
        operator.gt,
        {'DSA': 0.0, 'PAT': 25.0, 'HYBRID': 0.0},
        lambda dim: dim.composition,
    ),
    'd_deltaPhi': ConditionalCut(
        'deltaPhi',
        lambda dim: dim.deltaPhi,
        operator.le,
        {'DSA': math.pi / 4.0, 'PAT': math.pi / 4.0, 'HYBRID': math.pi / 2.0},
        lambda dim: dim.composition,
    ),
    'd_IDeltaPhi': ConditionalCut(
        'IDeltaPhi',
        lambda dim: dim.deltaPhi,
        operator.gt,
        {'DSA': 3.0 * math.pi / 4.0, 'PAT': 3.0 * math.pi / 4.0, 'HYBRID': math.pi / 2.0},
        lambda dim: dim.composition,
    ),
    'd_oppSign': SimpleCut(
        'oppSign', lambda dim: dim.mu1.charge + dim.mu2.charge, operator.eq, 0
    ),
    'd_nTrkLay': ConditionalCut(
        'nTrkLay',
        NTRKLAYLAMBDA,
        operator.gt,
        {'DSA': 0.0, 'PAT': 5.0, 'HYBRID': 5.0},
        lambda dim: dim.composition,
    ),
    ### SPECIAL DIMUON CUTS
    'd_pxlHits': SimpleCut('pxlHits', NPIXELHITSLAMBDA, operator.lt, 3.0),
    ### RUN 1 RECO MUON CUTS ###
    '8_pT': SimpleCut('pT', lambda mu: mu.pt, operator.gt, 30.0),
    '8_eta': SimpleCut('eta', lambda mu: abs(mu.eta), operator.lt, 2.0),
    '8_normChi2': SimpleCut('normChi2', lambda mu: mu.normChi2, operator.lt, 2.0),
    '8_nMuonHits': SimpleCut('nMuonHits', lambda mu: mu.nMuonHits, operator.ge, 17),
    '8_nStations': SimpleCut('nStations', lambda mu: mu.nCSCStations + mu.nDTStations, operator.ge, 3),
    '8_d0Sig': SimpleCut('d0Sig', lambda mu: mu.d0Sig(), operator.gt, 4.0),
    ### RUN 1 DIMUON CUTS ###
    '8_vtxChi2': SimpleCut('vtxChi2', lambda dim: dim.normChi2, operator.lt, 4.0),
    '8_deltaR': SimpleCut('deltaR', lambda dim: dim.deltaR, operator.gt, 0.2),
    '8_mass': SimpleCut('mass', lambda dim: dim.mass, operator.gt, 15.0),
    '8_deltaPhi': SimpleCut('deltaPhi', lambda dim: dim.deltaPhi, operator.lt, math.pi / 2.0),
    '8_cosAlpha': SimpleCut('cosAlpha', lambda dim: dim.cosAlpha, operator.gt, -0.75),
    '8_LxySig': SimpleCut('LxySig', lambda dim: dim.LxySig_pv, operator.gt, 12.0),
}

# CutLists for access convenience (and ordering)
CUT_LIST_DEFINITIONS = {
    'AcceptanceCutList': ('a_pT', 'a_eta', 'a_Lxy'),
    'AcceptanceCutList2': ('ext_a_pT', 'a_eta', 'a_Lxy'),
    'AcceptanceCutList3': ('ext_a_pT', 'ext_a_eta', 'a_Lxy'),
    'AcceptanceCutList4': ('a_pT', 'ext_a_eta', 'a_Lxy'),
    'DSAQualityCutList': ('q_nStations', 'q_nMuonHits'),
    'PATQualityCutList': (
        (
            'p_isGlobal', 'p_isMedium', 'p_isTracker', 'p_isHighPurity',
            'p_nTrkLays', 'p_globHits', 'p_trkrStats', 'p_kinkChi2',
            'p_posMatch', 'p_pTtopTwo',
        ) if not COSMICS_NTUPLES_COMPATIBILITY_MODE else (
            'p_isGlobal', 'p_isMedium', 'p_isTracker', 'p_isHighPurity',
            'p_nTrkLays', 'p_globHits', 'p_trkrStats', 'p_pTtopTwo',
        )
    ),
    'AllMuonCutList': ('m_pT', 'm_d0Sig', 'm_trkChi2', 'm_nDTHits', 'm_FPTE'),
    'DimuonCutList': (
        (
            'd_LxyErr', 'd_mass', 'd_vtxChi2', 'd_cosAlpha',
            'd_cosAlphaO', 'd_DCA', 'd_LxySig', 'd_maxpT',
            'd_customTrkIso', 'd_deltaPhi', 'd_oppSign', 'd_missHits',
            'd_nTrkLay',
        ) if not COSMICS_NTUPLES_COMPATIBILITY_MODE else (
            'd_LxyErr', 'd_mass', 'd_vtxChi2', 'd_cosAlpha',
            'd_cosAlphaO', 'd_DCA', 'd_LxySig', 'd_maxpT',
            'd_deltaPhi', 'd_oppSign', 'd_missHits',
        )
    ),
    'InvertedDimuonCutList': (
        'd_LxyErr', 'd_mass', 'd_vtxChi2', 'd_cosAlpha',
        'd_cosAlphaO', 'd_DCA', 'd_LxySig', 'd_maxpT',
        'd_customTrkIso', 'd_IDeltaPhi', 'd_oppSign', 'd_missHits',
        'd_nTrkLay',
    ),
    'ZDimuonCutList': (
        'd_LxyErr', 'd_mass', 'd_vtxChi2', 'd_cosAlpha',
        'd_cosAlphaO', 'd_DCA', 'd_maxpT', 'd_customTrkIso',
        'd_oppSign', 'd_missHits', 'd_Zmass', 'd_InvLxySig',
        'd_IDeltaPhi',
    ),
    'Run1MuonCutList': (
        '8_pT', '8_eta', '8_normChi2', '8_nMuonHits',
        '8_nStations', '8_d0Sig',
    ),
    'Run1DimuonCutList': (
        '8_vtxChi2', '8_deltaR', '8_mass', '8_deltaPhi',
        '8_cosAlpha', '8_LxySig',
    ),
}


def print_cut_lists():
    for cut_list_id in (
            'AcceptanceCutList',
            'DSAQualityCutList',
            'PATQualityCutList',
            'AllMuonCutList',
            'DimuonCutList',
    ):
        print('\033[1m{cutkey}\033[m'.format(cutkey=cut_list_id))

        for key in CUT_LIST_DEFINITIONS[cut_list_id]:
            print(' ', str(CUT_DEFINITIONS[key]))

        print('')


# Print full cut list as strings
if __name__ == '__main__':
    print_cut_lists()
