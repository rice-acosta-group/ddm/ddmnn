# abstract cut object: name string, lambda expression to evaluate, comparison operator, and cut value
# apply returns a bool of the applied cut given an object (or list of objects)
# lambda expression will evaluate on the given objects in apply, so it should expect, for example, a Primitives class
import operator
from abc import ABC, abstractmethod

operator_to_str_lut = {
    operator.gt: ">",
    operator.ge: "\u2265",
    operator.lt: "<",
    operator.le: "\u2264",
    operator.eq: "=",
}


class Cut(ABC):

    @abstractmethod
    def apply(self, *objs):
        raise NotImplementedError()


class SimpleCut(Cut):

    def __init__(self, var_name, var_expression, operator, parameter):
        self.var_name = var_name
        self.var_expression = var_expression
        self.operator = operator
        self.parameter = parameter

    def apply(self, *objs):
        value = self.var_expression(*objs)

        return self.operator(value, self.parameter)

    def __str__(self):
        return self.var_name + " " + operator_to_str_lut[self.operator] + " " + str(self.parameter)


# MultiCut derives from Cut, specifying a set of closely related cuts
# the parameters are given in a dictionary (parameters), and the key to access the right parameter
# is given by choice_expresion.
class ConditionalCut(Cut):
    def __init__(self, var_name, var_expression, operator, choices, choice_expresion):
        self.var_name = var_name
        self.var_expression = var_expression
        self.operator = operator
        self.choices = choices
        self.choice_expresion = choice_expresion

    def apply(self, *objs):
        value = self.var_expression(*objs)
        choice = self.choice_expresion(*objs)
        parameter = self.choices[choice]

        return self.operator(value, parameter)

    def __str__(self):
        param_str = " ".join(
            ["{} when '{}',".format(param_value, param_key) for param_key, param_value in self.choices.items()]
        )

        return self.var_name + " " + operator_to_str_lut[self.operator] + " (" + param_str + ")"


# a special class for a special purpose
# This is a MultiCut that applies its cuts to the constituent muons in a dimuon
# So everything -- expr, op, val, keyExpr -- should work on a constituent muon
# and not on the encapsulating dimuon
class DimMuCut(ConditionalCut):
    def __init__(self, var_name, var_expression, operator, choices, choice_expresion):
        ConditionalCut.__init__(self, var_name, var_expression, operator, choices, choice_expresion)

    def apply(self, dim):
        return all([ConditionalCut.apply(self, mu) for mu in (dim.mu1, dim.mu2)])
