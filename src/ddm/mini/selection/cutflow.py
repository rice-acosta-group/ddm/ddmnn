import numpy as np

from ddm.core.analyzers import AbstractAnalyzer
from ddm.core.execution.runtime import get_logger


class CutflowAnalyzer(AbstractAnalyzer):

    def __init__(self, field_name):
        super().__init__()

        self.field_name = field_name
        self.accumulator = CutflowAccumulator()

    def process_entry(self, entry):
        self.accumulator.add(
            getattr(entry, self.field_name)
        )

    def merge(self, other):
        self.accumulator.add(
            other.accumulator
        )

    def post_production(self):
        # Log
        get_logger().info("""\n
        *********************************************************
        Cutflow Summary
        *********************************************************
        """)

        # Print Cutflow
        self.accumulator.pretty_print()


class CutflowAccumulator(object):

    def __init__(self):
        self.layers = None
        self.initial_counts = None
        self.dsa_count = None
        self.pat_count = None
        self.dimuon_count = None
        self.initialized = False

    def add(self, other):
        # Short-Circuit: Skip Empty Accumulator
        if (type(other) == CutflowAccumulator) and (not other.initialized):
            return

        # Accumulate
        if not self.initialized:
            self.layers = other.layers[:]
            self.initial_counts = other.initial_counts[:]
            self.dsa_count = other.dsa_count[:]
            self.pat_count = other.pat_count[:]
            self.dimuon_count = other.dimuon_count[:]
            self.initialized = True
        elif self.layers != other.layers:
            raise ValueError('Layers present in cutflow are different')
        else:
            self.initial_counts += other.initial_counts
            self.dsa_count += other.dsa_count
            self.pat_count += other.pat_count
            self.dimuon_count += other.dimuon_count

    def pretty_print(self):
        # Short-Circuit: It's Empty
        if not self.initialized:
            return

        # Print
        header_format = '| %30s | %10s | %10s | %10s |'
        entry_format = '| %30s | %10d | %10d | %10d |'

        header = header_format % ('Layer', 'DSA', 'PAT', 'Dimuon')
        vsize_header = len(header)

        summary = '-' * vsize_header
        summary += '\n' + header
        summary += '\n' + '-' * vsize_header
        summary += '\n' + entry_format % ('Initial', *self.initial_counts)

        for idx in range(len(self.layers) - 1):
            summary += '\n' + entry_format % (self.layers[idx], self.dsa_count[idx], self.pat_count[idx], self.dimuon_count[idx])

        summary += '\n' + '-' * vsize_header
        summary += '\n' + entry_format % ('End: ' + self.layers[-1], self.dsa_count[-1], self.pat_count[-1], self.dimuon_count[-1])
        summary += '\n' + '-' * vsize_header
        summary += '\n'

        get_logger().info('\n' + summary)


class Cutflow(object):

    def __init__(self, layers, dsa_count, pat_count, dimuon_count):
        vsize_layers = len(layers)

        self.layers = layers
        self.initial_counts = np.asarray((dsa_count, pat_count, dimuon_count))
        self.dsa_count = np.zeros((vsize_layers,))
        self.pat_count = np.zeros((vsize_layers,))
        self.dimuon_count = np.zeros((vsize_layers,))

    def set_count(self, idx, dsa_cnt, pat_cnt, dimuon_cnt):
        self.dsa_count[idx] = dsa_cnt
        self.pat_count[idx] = pat_cnt
        self.dimuon_count[idx] = dimuon_cnt
