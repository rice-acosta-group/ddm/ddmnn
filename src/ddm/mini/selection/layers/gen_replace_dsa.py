from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dsa_gen_replacement import replace_dsa_muons_using_gen
from ddm.mini.utils.hip_correction import get_hip_corrected_pat_muons


class GENReplaceDSA(Layer):

    def __init__(self, hip_correct_en, is_llp_gun):
        self.hip_correct_en = hip_correct_en
        self.is_llp_gun = is_llp_gun

    def apply(self, entry, selection, options):
        # Apply HIP correction
        if self.hip_correct_en:
            selection.sel_pat_muons = get_hip_corrected_pat_muons(entry, selection.sel_pat_muons)

        # Get GEN Dimuons
        if self.is_llp_gun:
            gen_dimuons = entry.ext_llp_gun_dimuons
        else:
            gen_dimuons = entry.ext_gen_dimuons

        # Replace
        selection.sel_dsa_muons, selection.sel_pat_muons, selection.sel_dimuons = replace_dsa_muons_using_gen(
            gen_dimuons, selection.sel_dsa_muons, selection.sel_pat_muons, selection.sel_dimuons,
            no_std_pat_en=options.REPNOSTD
        )

        # Return
        return selection
