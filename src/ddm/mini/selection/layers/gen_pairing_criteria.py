from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dimuon_matching import match_gen_dimuons


class GENPairingCriteria(Layer):

    def __init__(self, is_llp_gun):
        self.is_llp_gun = is_llp_gun

    def apply(self, entry, selection, options):
        # Calculate how many dimuons can be selected
        n_dsa_muons = len(selection.sel_dsa_muons)
        n_pat_muons = len(selection.sel_pat_muons)
        n_muons = n_dsa_muons + n_pat_muons

        # Short-Circuit: No muons
        if n_muons == 0:
            return None

        # Get GEN Dimuons
        if self.is_llp_gun:
            gen_dimuons = entry.ext_llp_gun_dimuons
        else:
            gen_dimuons = entry.ext_gen_dimuons

        # Get best DSA dimuons
        dsa_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'DSA']
        dsa_dimuons = self.select_best(n_dsa_muons, gen_dimuons, dsa_dimuons)

        # Get best PAT dimuons
        pat_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'PAT']
        pat_dimuons = self.select_best(n_pat_muons, gen_dimuons, pat_dimuons)

        # Get best hybrid dimuons
        hyb_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'HYBRID']
        hyb_dimuons = self.select_best(n_muons, gen_dimuons, hyb_dimuons)

        # Sort selection by metric
        best_dimuons = sorted(
            [*dsa_dimuons, *pat_dimuons, *hyb_dimuons],
            key=lambda e: e[3]
        )

        # Reduce selection
        sel_dimuons = list()
        max_dimuons = min(1, 2 // n_muons)

        used_gen_idx = set()
        used_reco_idx = {"DSA": set(), "PAT": set()}

        for gen_dimuon, reco_dimuon, are_aligned, metric in best_dimuons:
            # Short-Circuit: GEN dimuon used
            if gen_dimuon.idx in used_gen_idx:
                continue

            # Short-Circuit: Reco muon used
            if reco_dimuon.composition != "HYBRID":
                tag1, tag2 = reco_dimuon.composition, reco_dimuon.composition
            else:
                tag1, tag2 = "DSA", "PAT"

            if (reco_dimuon.mu1.idx in used_reco_idx[tag1]) or (reco_dimuon.mu2.idx in used_reco_idx[tag2]):
                continue

            # if gen_dimuon.charge != reco_dimuon.charge:
            #     print(f"Dimuon charge mismatch, RECO={reco_dimuon.composition}")

            # Associate
            reco_dimuon.matched_gen_dimuon = gen_dimuon
            reco_dimuon.matched_gen_dimuon_are_aligned = are_aligned

            # Select
            used_gen_idx.add(reco_dimuon.matched_gen_dimuon.idx)
            used_reco_idx[tag1].add(reco_dimuon.mu1.idx)
            used_reco_idx[tag2].add(reco_dimuon.mu2.idx)
            sel_dimuons.append(reco_dimuon)

            # Short-Circuit: Too many
            if len(sel_dimuons) == max_dimuons:
                break

        # Update selection
        if not options.PCFORML:
            selection.sel_dimuons = sel_dimuons

        # Return
        return selection

    @staticmethod
    def select_best(n_muons, gen_dimuons, reco_dimuons):
        # Short-Circuit: No muons
        if n_muons < 2:
            return list()

        # Match
        dimuon_matches = match_gen_dimuons(
            gen_dimuons, reco_dimuons,
            deltaR_threshold=0.05, deltaVtx_threshold=20.0,
            return_metric_en=True
        )

        # Loop matches
        best_matches = list()
        max_matches = min(1, 2 // n_muons)

        for match in dimuon_matches:
            # Select
            best_matches.append(match)

            # Short-Circuit: Too many
            if len(best_matches) == max_matches:
                break

        # Return
        return best_matches
