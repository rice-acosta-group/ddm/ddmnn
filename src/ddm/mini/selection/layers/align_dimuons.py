from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dimuon_utils import align_dimuons


class AlignDimuons(Layer):

    def apply(self, entry, selection, options):
        # Align Dimuons
        selection.sel_dimuons = align_dimuons(
            selection.sel_dimuons,
            dsa_muons=selection.sel_dsa_muons,
            pat_muons=selection.sel_pat_muons
        )

        # Return
        return selection
