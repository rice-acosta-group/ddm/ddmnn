from ddm.core.commons.combinatorics import yield_pairs
from ddm.mini.selection.layers.layer import Layer


class NNPairingCriteria(Layer):

    def apply(self, entry, selection, options):
        # Calculate how many dimuons can be selected
        n_dsa_muons = len(selection.sel_dsa_muons)
        n_pat_muons = len(selection.sel_pat_muons)
        n_muons = n_dsa_muons + n_pat_muons

        # Short-Circuit: No muons
        if n_muons == 0:
            return None

        # Get best DSA dimuons
        dsa_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'DSA']
        dsa_dimuons = self.select_best(n_dsa_muons, dsa_dimuons)

        # Get best PAT dimuons
        pat_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'PAT']
        pat_dimuons = self.select_best(n_pat_muons, pat_dimuons)

        # Get best hybrid dimuons
        hyb_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'HYBRID']
        hyb_dimuons = self.select_best(n_muons, hyb_dimuons)

        # Sort selection by pscore
        best_dimuons = sorted(
            [*dsa_dimuons, *pat_dimuons, *hyb_dimuons],
            key=lambda dim: dim.pscore, reverse=True
        )

        # Reduce selection
        sel_dimuons = list()
        max_dimuons = min(1, 2 // n_muons)
        used_reco_idx = {"DSA": set(), "PAT": set()}

        for dimuon in best_dimuons:
            if dimuon.composition != "HYBRID":
                tag1, tag2 = dimuon.composition, dimuon.composition
            else:
                tag1, tag2 = "DSA", "PAT"

            if (dimuon.mu1.idx in used_reco_idx[tag1]) or (dimuon.mu2.idx in used_reco_idx[tag2]):
                continue

            used_reco_idx[tag1].add(dimuon.mu1.idx)
            used_reco_idx[tag2].add(dimuon.mu2.idx)
            sel_dimuons.append(dimuon)

            if len(sel_dimuons) == max_dimuons:
                break

        # if n_muons == 3:
        #     minchi2 = sorted(
        #         selection.sel_dimuons,
        #         key=lambda dim: dim.normChi2
        #     )[0]
        #
        #     print(
        #         len(selection.sel_dimuons),
        #         (minchi2.composition, minchi2.pscore, minchi2.normChi2),
        #         [
        #             (dim.composition, dim.pscore, dim.normChi2)
        #             for dim in sel_dimuons
        #         ]
        #     )

        # Update selection
        selection.sel_dimuons = sel_dimuons

        # Return
        return selection

    @staticmethod
    def select_best(n_muons, dimuons):
        # Short-Circuit: Not enough muons
        if n_muons < 2:
            return list()

        # Short-Circuit: Only one dimuon
        if n_muons == 2:
            return dimuons[:1]

        # Sort dimuons by pscore
        best_dimuons = sorted(dimuons, key=lambda dim: dim.pscore, reverse=True)

        # Short-Circuit: Get best dimuon
        if n_muons == 3:
            return best_dimuons[:1]

        # Find best pair of dimuons
        best_pair = None
        best_pscore_sum = 0

        for dim1, dim2 in yield_pairs(best_dimuons):
            # Collect muon ids
            muon_id_col = {
                (dim1.mu1.kind, dim1.mu1.idx),
                (dim1.mu2.kind, dim1.mu2.idx),
                (dim2.mu1.kind, dim2.mu1.idx),
                (dim2.mu2.kind, dim2.mu2.idx)
            }

            # Short-Circuit: Anything different from 4 means there's an overlap
            if len(muon_id_col) != 4:
                continue

            # Short-Circuit: New pair is worse
            pscore_sum = dim1.pscore + dim2.pscore

            if pscore_sum <= best_pscore_sum:
                continue

            # Update best
            best_pair = [dim1, dim2]
            best_pscore_sum = pscore_sum

        # Short-Circuit: Found the best pair
        if best_pair is not None:
            return best_pair

        # No pairs found, use the dimuon with the highest pscore
        # If there were NO pairings, there had to have been <= 3 dimuons
        # because any 4 dimuons can always make at least 1 pair
        # this means 1 of the 4 muons formed no dimuons at all
        # so treat this case like the 3 mu case
        return best_dimuons[:1]
