from ddm.mini.cuts.cut_lists import CutList
from ddm.mini.selection.layers.layer import Layer


class DSAQualFilter(Layer):

    def apply(self, entry, selection, options):
        # Figure out which cuts we actually care about
        required_cuts = options.get_muon_cuts()

        # Short-Circuit: No cuts given
        if len(required_cuts) == 0:
            return selection

        # Keep only the muons that pass the cuts in required_cuts
        selection.sel_dsa_muons = [
            muon for muon in selection.sel_dsa_muons
            if CutList('DSAQualityCutList', muon).passed_all_of(*required_cuts)
        ]

        # Return
        return selection
