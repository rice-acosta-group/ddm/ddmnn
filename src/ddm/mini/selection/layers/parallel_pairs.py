from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS
from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.cosmics import get_number_of_parallel_pairs


class ParallelPairFilter(Layer):

    def apply(self, entry, selection, options):
        nPPm, nPPp = get_number_of_parallel_pairs(selection.sel_dsa_muons)

        if options.NPP and not CUT_DEFINITIONS['nPP'].apply(nPPm + nPPp):
            return None

        if options.INPP and not CUT_DEFINITIONS['inPP'].apply(nPPm + nPPp):
            return None

        return selection
