from ddm.core.execution.runtime import get_logger
from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS
from ddm.mini.selection.layers.layer import Layer


class DimuonPixelCut(Layer):

    def __init__(self, debug_en=False):
        self.debug_en = debug_en

    def apply(self, entry, selection, options):
        if self.debug:
            get_logger().debug("==== Calling pixel hit cutter ====")

        new_sel_dimuons = list()

        for dim in selection.sel_dimuons:
            # Short-Circuit: Skip DSA / HYBRID muons for now
            if dim.composition != "PAT":
                if self.debug:
                    get_logger().debug("Not a PAT muon, skipping")

                new_sel_dimuons.append(dim)
                continue

            # Process PAT-PAT Dimuon
            mu1 = selection.all_pat_muons[dim.idx1]
            mu2 = selection.all_pat_muons[dim.idx2]

            if self.debug:
                get_logger().debug("Applying pixel hit cuts to PAT muons")
                get_logger().debug("mu1.nPixelHits: {}".format(mu1.nPixelHits))
                get_logger().debug("mu2.nPixelHits: {}".format(mu2.nPixelHits))

            if CUT_DEFINITIONS["d_pxlHits"].apply(mu1, mu2):
                if self.debug:
                    get_logger().debug("Passed pixel hits cut!")

                new_sel_dimuons.append(dim)
            elif self.debug:
                get_logger().debug("Failed pixel hits cut!")

        if self.debug:
            get_logger().debug("{} dimuons passed pixel hits cut".format(len(new_sel_dimuons)))

        # Update Collection
        selection.sel_dimuons = new_sel_dimuons

        # Return
        return selection
