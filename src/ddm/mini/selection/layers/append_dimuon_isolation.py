from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dimuon_isolation import append_dimuon_isolation


class AppendDimuonIsolation(Layer):

    def apply(self, entry, selection, options):
        # Append isolation
        append_dimuon_isolation(selection.sel_dimuons, selection.all_pat_muons)

        # Return
        return selection
