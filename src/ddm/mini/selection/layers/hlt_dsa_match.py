from ddm.core.commons.triggers import TriggerMenu
from ddm.mini.commons.constants import YEAR, SIGNAL_HLT_PATH
from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.hlt_matching import match_hlt_dimuons


class HLTDSAMatch(Layer):

    def apply(self, entry, selection, options):
        # Init HLT flags
        hlt_flags = dict()

        # Match DSA to HLT muons
        hlt_dimuon_matches = match_hlt_dimuons(
            entry.ext_hlt_paths, entry.ext_hlt_dimuons, selection.sel_dsa_muons
        )

        # Build Trigger Menu
        def get_path_cond(path):
            flag = 'matchFound_' + path
            return lambda match: match.get(flag, False)

        menu = TriggerMenu()
        menu.register('matchFound_pp', get_path_cond('pp'))
        menu.register('matchFound_cosmic', get_path_cond('cosmic'))

        for path in SIGNAL_HLT_PATH[YEAR]:
            menu.register('matchFound_' + path, get_path_cond(path))

        # Process dimuon matches
        for match in hlt_dimuon_matches.values():
            menu.process(match)

            if menu.all_triggers_passed:
                break

        # Extract Conditions
        passed_HLT = False
        hlt_flags['HLT'] = False

        if YEAR == 2016 or YEAR == 2018 or YEAR == 2022:
            hlt_flags['HLTPP'] = False
            hlt_flags['HLTPUREPP'] = False
            hlt_flags['HLTCOSMIC'] = False
            hlt_flags['HLTPURECOSMIC'] = False

        if YEAR == 2016:
            # Filter and tag events by HLT path
            if menu.trigger_states['matchFound_pp']:
                passed_HLT = True
                hlt_flags['HLT'] = True
                hlt_flags['HLTPP'] = True

        if YEAR == 2018 or YEAR == 2022:
            # Filter events by HLT paths
            if (
                    ((not options.HLTPP and not options.HLTCOSMIC) or (options.HLTPP and options.HLTCOSMIC))
                    and not options.HLTPUREPP and not options.HLTPURECOSMIC
            ):
                if menu.trigger_states['matchFound_pp'] or menu.trigger_states['matchFound_cosmic']:
                    passed_HLT = True

            if options.HLTPP:
                if menu.trigger_states['matchFound_pp']:
                    passed_HLT = True

            if options.HLTPUREPP:
                if menu.trigger_states['matchFound_pp'] and not menu.trigger_states['matchFound_cosmic']:
                    passed_HLT = True

            if options.HLTCOSMIC:
                if menu.trigger_states['matchFound_cosmic']:
                    passed_HLT = True

            if options.HLTPURECOSMIC:
                if menu.trigger_states['matchFound_cosmic'] and not menu.trigger_states['matchFound_pp']:
                    passed_HLT = True

            # tag events by HLT paths
            if menu.trigger_states['matchFound_pp']:
                hlt_flags['HLT'] = True
                hlt_flags['HLTPP'] = True

                if not menu.trigger_states['matchFound_cosmic']:
                    hlt_flags['HLTPUREPP'] = True

            if menu.trigger_states['matchFound_cosmic']:
                hlt_flags['HLT'] = True
                hlt_flags['HLTCOSMIC'] = True

                if menu.trigger_states['matchFound_pp']:
                    hlt_flags['HLTPURECOSMIC'] = True

        if YEAR == 2016 or YEAR == 2018 or YEAR == 2022:
            for path in SIGNAL_HLT_PATH[YEAR]:
                hlt_flags[path] = False

                if menu.trigger_states['matchFound_' + path]:
                    passed_HLT = True
                    hlt_flags['HLT'] = True
                    hlt_flags[path] = True

        if not options.TRIG and not passed_HLT:
            return None

        # Save HLT Flags
        selection.sel_info.hlt_flags = hlt_flags

        # Return
        return selection
