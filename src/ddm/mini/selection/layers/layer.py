from abc import abstractmethod, ABC


class Graph:

    def __init__(self):
        self.graph = None
        self.layer_count = 0
        self.layer_names = list()

    def append(self, layer):
        # Build new graph
        parent_graph = self.graph
        layer_idx = self.layer_count

        def new_graph(cutflow, entry, selection, options):
            # Run parent
            if parent_graph is not None:
                selection = parent_graph(cutflow, entry, selection, options)

                # Short-Circuit: Invalid selection
                if selection is None:
                    return None

            # Run layer
            selection = layer.apply(entry, selection, options)

            # Short-Circuit: Invalid selection
            if selection is None:
                return None

            # Update Counts
            dsa_count = len(selection.sel_dsa_muons)
            pat_count = len(selection.sel_pat_muons)
            dimuon_count = len(selection.sel_dimuons)
            cutflow.set_count(layer_idx, dsa_count, pat_count, dimuon_count)

            # Short-Circuit: No dimuons
            if dimuon_count == 0:
                return None

            # Return
            return selection

        # Update graph
        self.graph = new_graph
        self.layer_count += 1
        self.layer_names.append(type(layer).__name__)

    def apply(self, cutflow, entry, selection, options):
        # Short-Circuit: No layers
        if self.graph is None:
            raise Exception('No layers have been added.')

        # Run graph
        return self.graph(cutflow, entry, selection, options)


class Layer(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def apply(self, entry, selection, options):
        raise NotImplementedError
