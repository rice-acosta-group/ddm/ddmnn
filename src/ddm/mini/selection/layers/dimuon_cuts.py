from ddm.mini.cuts.cut_lists import CutList
from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.smearing import smear_dimuon_pt


class DimuonCuts(Layer):

    def __init__(self, smear_mass_en):
        self.smear_mass_en = smear_mass_en

    def apply(self, entry, selection, options):
        # Figure out which cuts we actually care about
        required_cuts = options.get_dimuon_cuts()

        # Short-Circuit: No cuts given
        if len(required_cuts) == 0:
            return selection

        # Select Cut List
        if options.Z:
            dimuon_cutlists = {
                dimuon.idx: CutList(
                    'ZDimuonCutList',
                    dimuon
                )
                for dimuon in selection.sel_dimuons
            }
        elif options.IDPHI:
            if self.smear_mass_en:
                dimuon_cutlists = {
                    dimuon.idx: CutList(
                        'InvertedDimuonCutList',
                        smear_dimuon_pt(dimuon, entry.random)
                    )
                    for dimuon in selection.sel_dimuons
                }
            else:
                dimuon_cutlists = {
                    dimuon.idx: CutList('InvertedDimuonCutList', dimuon)
                    for dimuon in selection.sel_dimuons
                }
        else:
            if self.smear_mass_en:
                dimuon_cutlists = {
                    dimuon.idx: CutList(
                        'DimuonCutList',
                        smear_dimuon_pt(dimuon, entry.random)
                    )
                    for dimuon in selection.sel_dimuons
                }
            else:
                dimuon_cutlists = {
                    dimuon.idx: CutList('DimuonCutList', dimuon)
                    for dimuon in selection.sel_dimuons
                }

        # Apply cuts
        selection.sel_dimuons = [
            dimuon for dimuon in selection.sel_dimuons
            if dimuon_cutlists[dimuon.idx].passed_all_of(*required_cuts)
        ]

        # Return
        return selection
