from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS
from ddm.mini.selection.layers.layer import Layer


class DCACut(Layer):

    def apply(self, entry, selection, options):
        # Apply Cuts
        selection.sel_dimuons = [
            dim for dim in selection.sel_dimuons
            if CUT_DEFINITIONS["d_DCA"].apply(dim)
        ]

        # Return
        return selection
