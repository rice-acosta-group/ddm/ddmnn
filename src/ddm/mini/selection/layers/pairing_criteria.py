from ddm.core.commons.combinatorics import yield_pairs
from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dimuon_utils import align_dimuons


class PairingCriteria(Layer):

    def apply(self, entry, selection, options):
        # Get best DSA dimuons
        dsa_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'DSA']
        dsa_dimuons = self.select_best(selection.sel_dsa_muons, list(), dsa_dimuons)

        # Get best PAT dimuons
        pat_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'PAT']
        pat_dimuons = self.select_best(list(), selection.sel_pat_muons, pat_dimuons)

        # Get best hybrid dimuons
        hyb_dimuons = [dim for dim in selection.sel_dimuons if dim.composition == 'HYBRID']
        hyb_dimuons = self.select_best(selection.sel_dsa_muons, selection.sel_pat_muons, hyb_dimuons)

        # Sort selection by normChi2
        dimuon_candidates = sorted(
            [*dsa_dimuons, *pat_dimuons, *hyb_dimuons],
            key=lambda dim: dim.normChi2
        )

        # Reduce selection
        # The candidate dimuon list may have overlapping muons across dimuons
        # We should select the two best dimuon pairs without overlap
        sel_dimuons = list()
        used_reco_idx = {"DSA": set(), "PAT": set()}

        for dimuon in dimuon_candidates:
            if len(sel_dimuons) == 2:
                break

            if dimuon.composition != "HYBRID":
                tag1, tag2 = dimuon.composition, dimuon.composition
            else:
                tag1, tag2 = "DSA", "PAT"

            if (dimuon.mu1.idx in used_reco_idx[tag1]) or (dimuon.mu2.idx in used_reco_idx[tag2]):
                continue

            used_reco_idx[tag1].add(dimuon.mu1.idx)
            used_reco_idx[tag2].add(dimuon.mu2.idx)
            sel_dimuons.append(dimuon)

        # Update selection
        selection.sel_dimuons = sel_dimuons

        # Return
        return selection

    @staticmethod
    def select_best(dsa_muons, pat_muons, dimuons, use_amd_en=False):
        # Short-Circuit: No dimuons
        if len(dimuons) == 0:
            return list()

        # Count Muons
        n_muons = len(dsa_muons) + len(pat_muons)

        # Short-Circuit: No muons
        if n_muons == 0:
            return list()

        # Align dimuons
        dimuons = align_dimuons(
            dimuons,
            dsa_muons=dsa_muons,
            pat_muons=pat_muons
        )

        if len(dimuons) == 0:
            return list()

        # if there are any dimuons at all, there had to be >= 2 muons
        # if there's 2, just return the one dimuon that was made
        # if there's 3, return the dimuon with the lowest vertex chi^2/dof
        # if there's 4, find all pairs of dimuons, sort the pairs by chi^2/dof sum, and return the lowest one (2 dimuons)
        # with optional doAMD mode, return the lowest pair by mass difference instead if both dimuons have Lxy < 30 cm
        # if there were no pairings with 4 muons, treat it like a 3 muon case
        if n_muons == 2:
            return dimuons[:1]
        elif n_muons == 3:
            return sorted(dimuons, key=lambda dim: dim.normChi2)[:1]
        elif n_muons >= 4:
            # Get muons with highest pT
            sel_muons = dsa_muons + pat_muons
            sel_muons.sort(key=lambda mu: mu.pt, reverse=True)
            sel_dimuons = align_dimuons(dimuons, reco_muons=sel_muons[:4])

            # Find unique pairs of dimuons
            sel_pairs = list()

            for dim1, dim2 in yield_pairs(sel_dimuons):
                # Collect muon ids
                muon_id_col = {
                    (dim1.mu1.kind, dim1.mu1.idx),
                    (dim1.mu2.kind, dim1.mu2.idx),
                    (dim2.mu1.kind, dim2.mu1.idx),
                    (dim2.mu2.kind, dim2.mu2.idx)
                }

                # Short-Circuit: Anything different from 4 means there's an overlap
                if len(muon_id_col) != 4:
                    continue

                # Save Pair
                sel_pairs.append((dim1, dim2))

            # Short-Circuit: No pairs found, use the dimuon with lowest chi2
            # If there were NO pairings, there had to have been <= 3 dimuons
            # because any 4 dimuons can always make at least 1 pair
            # this means 1 of the 4 muons formed no dimuons at all
            # so treat this case like the 3 mu case
            if len(sel_pairs) == 0:
                return sorted(dimuons, key=lambda dim: dim.normChi2)[:1]

            # Compare pairs using various criteria
            def normalized_chi2_sum(dimuon_pair):
                return dimuon_pair[0].normChi2 + dimuon_pair[1].normChi2

            def absolute_mass_difference(dimuon_pair):
                return abs(dimuon_pair[0].mass - dimuon_pair[1].mass)

            criteria_predicates = {
                "C2S": normalized_chi2_sum,
                "AMD": absolute_mass_difference
            }

            # Sort the pairings by a pairing criteria
            best_pairs = {criteria: None for criteria in criteria_predicates}

            for criteria, predicate in criteria_predicates.items():
                best_pairs[criteria] = sorted(sel_pairs, key=predicate)[0]

            # Try to use AMD for low Lxy
            if use_amd_en:
                dim1, dim2 = best_pairs["AMD"]

                if (dim1.Lxy_pv < 30.0) and (dim2.Lxy_pv < 30.0):
                    return best_pairs["AMD"]
                else:
                    return best_pairs["C2S"]
            else:
                return best_pairs["C2S"]
