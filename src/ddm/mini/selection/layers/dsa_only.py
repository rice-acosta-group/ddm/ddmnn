from ddm.mini.selection.layers.layer import Layer


class DSAOnly(Layer):

    def apply(self, entry, selection, options):
        # Only keep dsa dimuons
        selection.sel_dimuons = [
            dimuon for dimuon in selection.sel_dimuons
            if dimuon.composition == "DSA"
        ]

        # Update selection
        selection.sel_pat_muons = list()

        # Return
        return selection
