from ddm.mini.selection.layers.layer import Layer


class DimuonTimingCut(Layer):

    def apply(self, entry, selection, options):
        # Apply cuts
        sel_dimuons = []

        for dimuon in selection.sel_dimuons:
            # Short-Circuit: Skip HYBRID / PAT muons
            if dimuon.composition != "DSA":
                sel_dimuons.append(dimuon)
                continue

            # Collect DSA muons
            muons = [
                selection.all_dsa_muons[dimuon.mu1.idx],
                selection.all_dsa_muons[dimuon.mu2.idx]
            ]

            # Check if muons passed cut
            any_failed_cut = False

            for muon in muons:
                threshold = 12.0
                duration = muon.timeAtIP_InOut

                if (duration > threshold) or (duration < -1.0 * threshold and duration > -998.0):
                    any_failed_cut = True
                    break

            if any_failed_cut:
                continue

            # Select dimuon
            sel_dimuons.append(dimuon)

        # Update selection
        selection.sel_dimuons = sel_dimuons

        # Return
        return selection
