from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.pat_isolation import append_pat_isolation


class AppendPATIsolation(Layer):

    def apply(self, entry, selection, options):
        # Append Isolation
        append_pat_isolation(selection.sel_pat_muons)

        # Return
        return selection
