from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dimuon_utils import align_reco_muons


class AlignRECO(Layer):

    def apply(self, entry, selection, options):
        # Align Reco Muons
        selection.sel_dsa_muons, selection.sel_pat_muons = align_reco_muons(
            selection.sel_dsa_muons,
            selection.sel_pat_muons,
            selection.sel_dimuons
        )

        # Return
        return selection
