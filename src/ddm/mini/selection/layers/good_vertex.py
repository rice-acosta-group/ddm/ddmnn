from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS
from ddm.mini.selection.layers.layer import Layer


class GoodVertexFilter(Layer):

    def apply(self, entry, selection, options):
        if not CUT_DEFINITIONS['goodVtx'].apply(entry.ext_filters):
            return None

        return selection
