from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.dsa_replacement import replace_dsa_muons
from ddm.mini.utils.hip_correction import get_hip_corrected_pat_muons


class ReplaceDSA(Layer):

    def __init__(self, hip_correct_en):
        self.hip_correct_en = hip_correct_en

    def apply(self, entry, selection, options):
        # Apply HIP correction
        if self.hip_correct_en:
            selection.sel_pat_muons = get_hip_corrected_pat_muons(entry, selection.sel_pat_muons)

        # Replace
        selection.sel_dsa_muons, selection.sel_pat_muons, selection.sel_dimuons = replace_dsa_muons(
            selection.sel_dsa_muons, selection.sel_pat_muons, selection.sel_dimuons,
            nn_en=options.REPNN,
        )

        # Return
        return selection
