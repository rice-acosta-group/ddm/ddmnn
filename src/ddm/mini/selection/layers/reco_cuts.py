from ddm.mini.cuts.cut_lists import CutList
from ddm.mini.selection.layers.layer import Layer
from ddm.mini.utils.smearing import smear_muon_pt


class RECOCuts(Layer):

    def __init__(self, smear_pt_en, smear_mass_en):
        self.smear_pt_en = smear_pt_en
        self.smear_mass_en = smear_mass_en

    def apply(self, entry, selection, options):
        # Figure out which cuts we actually care about
        required_cuts = options.get_all_muon_cuts()

        # Short-Circuit: No cuts given
        if len(required_cuts) == 0:
            return selection

        # Apply DSA Cuts
        if self.smear_pt_en:
            selection.sel_dsa_muons = [
                muon for muon in selection.sel_dsa_muons
                if CutList('AllMuonCutList', smear_muon_pt(muon, entry.random)).passed_all_of(*required_cuts)
            ]
        else:
            selection.sel_dsa_muons = [
                muon for muon in selection.sel_dsa_muons
                if CutList('AllMuonCutList', muon).passed_all_of(*required_cuts)
            ]

        # Apply PAT Cuts
        selection.sel_pat_muons = [
            muon for muon in selection.sel_pat_muons
            if CutList('AllMuonCutList', muon).passed_all_of(*required_cuts)
        ]

        # Return
        return selection
