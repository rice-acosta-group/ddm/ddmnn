from ddm.mini.cuts.cut_definitions import CUT_DEFINITIONS
from ddm.mini.selection.layers.layer import Layer


class PATTopPtCut(Layer):

    def __init__(self, ncut=2):
        self.ncut = ncut

    def apply(self, entry, selection, options):
        """
        Applies a pT cut to the top "ncut" muons sorted by pT
        """
        pat_sorted_by_pt = sorted(selection.sel_pat_muons, key=lambda mu: -mu.pt)

        remove_idx = [
            mu.idx for mu in pat_sorted_by_pt[:self.ncut]
            if CUT_DEFINITIONS["p_pTtopTwo"].apply(mu)
        ]

        if len(pat_sorted_by_pt) > self.ncut:
            remove_idx += pat_sorted_by_pt[self.ncut:]

        selection.sel_pat_muons = [
            mu for mu in selection.sel_pat_muons
            if mu.idx in remove_idx
        ]

        # Return
        return selection
