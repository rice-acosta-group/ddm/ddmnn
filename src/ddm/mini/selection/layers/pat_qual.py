from ddm.mini.cuts.cut_lists import CutList
from ddm.mini.selection.layers.layer import Layer


class PATQualFilter(Layer):

    def apply(self, entry, selection, options):
        # Check if we should apply quality cuts
        conditions = [
            options.ISMEDIUM, options.ISTRACKER, options.ISHP,
            options.NTRKLAYS, options.GBLHITS, options.KCHI2,
            options.TSTAT, options.PMATCH
        ]

        if not any(conditions):
            return selection

        # Figure out which cuts we actually care about
        required_cuts = options.get_pat_qual_cuts()

        if len(required_cuts) == 0:
            return selection

        # apply quality selection to PAT muons to select out cleaned muons
        selection.sel_pat_muons = [
            muon for muon in selection.sel_pat_muons
            if CutList('PATQualityCutList', muon).passed_all_of(*required_cuts)
        ]

        # Return
        return selection
