from ddm.core.commons.duck_typing import Duck
from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer
from ddm.mini.commons.constants import COSMICS_NTUPLES_COMPATIBILITY_MODE
from ddm.mini.commons.ntuple_primitives import get_dsa_muons, get_pat_muons, get_dimuons
from ddm.mini.selection.cutflow import Cutflow
from ddm.mini.selection.layers.align_dimuons import AlignDimuons
from ddm.mini.selection.layers.align_reco import AlignRECO
from ddm.mini.selection.layers.append_dimuon_isolation import AppendDimuonIsolation
from ddm.mini.selection.layers.append_pat_isolation import AppendPATIsolation
from ddm.mini.selection.layers.dca_cut import DCACut
from ddm.mini.selection.layers.dimuon_cuts import DimuonCuts
from ddm.mini.selection.layers.dimuon_pixel_cut import DimuonPixelCut
from ddm.mini.selection.layers.dimuon_timing_cut import DimuonTimingCut
from ddm.mini.selection.layers.dsa_only import DSAOnly
from ddm.mini.selection.layers.dsa_qual import DSAQualFilter
from ddm.mini.selection.layers.gen_pairing_criteria import GENPairingCriteria
from ddm.mini.selection.layers.gen_replace_dsa import GENReplaceDSA
from ddm.mini.selection.layers.good_vertex import GoodVertexFilter
from ddm.mini.selection.layers.hlt_dsa_match import HLTDSAMatch
from ddm.mini.selection.layers.layer import Graph
from ddm.mini.selection.layers.nn_pairing_criteria import NNPairingCriteria
from ddm.mini.selection.layers.pairing_criteria import PairingCriteria
from ddm.mini.selection.layers.parallel_pairs import ParallelPairFilter
from ddm.mini.selection.layers.pat_qual import PATQualFilter
from ddm.mini.selection.layers.pat_top_pt_cut import PATTopPtCut
from ddm.mini.selection.layers.reco_cuts import RECOCuts
from ddm.mini.selection.layers.replace_dsa import ReplaceDSA


class Selection:

    def __init__(
            self,
            all_dsa_muons, all_pat_muons, all_dimuons,
            sel_dsa_muons, sel_pat_muons, sel_dimuons
    ):
        self.all_dsa_muons = all_dsa_muons
        self.all_pat_muons = all_pat_muons
        self.all_dimuons = all_dimuons
        self.sel_info = Duck()
        self.sel_dsa_muons = sel_dsa_muons
        self.sel_pat_muons = sel_pat_muons
        self.sel_dimuons = sel_dimuons


class Selector(Producer, metaclass=LazyMeta):

    def __init__(
            self, options,
            branch_prefix='',
            hip_correct_en=False,
            smear_pt_en=False, smear_mass_en=False,
            is_llp_gun=False, align_reco_muons_en=True,
    ):
        self.branch_prefix = branch_prefix

        self.layers = list()
        self.options = options
        self.hip_correct_en = hip_correct_en
        self.smear_pt_en = smear_pt_en
        self.smear_mass_en = smear_mass_en
        self.is_llp_gun = is_llp_gun
        self.align_reco_muons_en = align_reco_muons_en

        # Graph
        self.graph = self.build_graph()

    def build_graph(self):
        # Init graph
        graph = Graph()

        # Append layers
        if self.options.VTX:
            graph.append(GoodVertexFilter())

        if self.options.NPP or self.options.INPP:
            graph.append(ParallelPairFilter())

        if self.options.NSTATIONS or self.options.NMUONHITS:
            graph.append(DSAQualFilter())
            graph.append(AlignDimuons())

        if self.options.HLT:
            graph.append(HLTDSAMatch())

        if self.options.REPGEN and self.options.REPFORML:
            graph.append(GENReplaceDSA(self.hip_correct_en, self.is_llp_gun))
            graph.append(AlignDimuons())
            return graph
        elif self.options.REP or self.options.REPNN or (self.options.REPGEN and not self.options.REPFORML):
            if self.options.REPGEN:
                graph.append(GENReplaceDSA(self.hip_correct_en, self.is_llp_gun))
            else:
                graph.append(ReplaceDSA(self.hip_correct_en))

            if not COSMICS_NTUPLES_COMPATIBILITY_MODE:
                graph.append(AppendPATIsolation())

            if self.options.TOPPT:
                graph.append(PATTopPtCut())

            graph.append(PATQualFilter())
            graph.append(AlignDimuons())
        else:
            graph.append(DSAOnly())
            graph.append(AlignDimuons())

        if self.options.PT or self.options.TRKCHI2 or self.options.NDTHITS or self.options.FPTERR or self.options.D0SIG:
            graph.append(RECOCuts(self.smear_pt_en, self.smear_mass_en))
            graph.append(AlignDimuons())

        if self.options.DCA:
            graph.append(DCACut())

        if self.options.PCGEN:
            graph.append(GENPairingCriteria(self.is_llp_gun))

            if self.options.PCFORML and self.align_reco_muons_en:
                graph.append(AlignRECO())
                return graph
        elif self.options.PCNN:
            graph.append(NNPairingCriteria())
        elif self.options.PC:
            graph.append(PairingCriteria())

        if not COSMICS_NTUPLES_COMPATIBILITY_MODE:
            graph.append(AppendDimuonIsolation())

        graph.append(DimuonCuts(self.smear_mass_en))

        if self.options.PXLHITS:
            graph.append(DimuonPixelCut())

        if self.options.TIMING:
            graph.append(DimuonTimingCut())

        if self.options.OPACT:
            pass

        if self.align_reco_muons_en:
            graph.append(AlignRECO())

        # Return
        return graph

    def provides(self):
        return [
            self.branch_prefix + 'all_dsa_muons',
            self.branch_prefix + 'all_pat_muons',
            self.branch_prefix + 'all_dimuons',
            self.branch_prefix + 'sel_info',
            self.branch_prefix + 'sel_dsa_muons',
            self.branch_prefix + 'sel_pat_muons',
            self.branch_prefix + 'sel_dimuons',
            self.branch_prefix + 'sel_cutflow'
        ]

    def extract(self, entry):
        # Get Collections
        all_dsa_muons = get_dsa_muons(entry)
        all_pat_muons = get_pat_muons(entry)
        all_dimuons = get_dimuons(entry)

        # Init cut flow count
        dsa_count = len(all_dsa_muons)
        pat_count = len(all_pat_muons)
        dimuon_count = len(all_dimuons)

        cutflow = Cutflow(
            self.graph.layer_names,
            dsa_count, pat_count, dimuon_count
        )

        # Run graph
        if dimuon_count == 0:
            selection = None
        else:
            selection = Selection(
                all_dsa_muons, all_pat_muons, all_dimuons,
                all_dsa_muons[:], all_pat_muons[:], all_dimuons[:]
            )

            selection = self.graph.apply(cutflow, entry, selection, self.options)

        # Return empty selection
        if selection is None:
            return {
                self.branch_prefix + 'all_dsa_muons': all_dsa_muons,
                self.branch_prefix + 'all_pat_muons': all_pat_muons,
                self.branch_prefix + 'all_dimuons': all_dimuons,
                self.branch_prefix + 'sel_info': None,
                self.branch_prefix + 'sel_dsa_muons': list(),
                self.branch_prefix + 'sel_pat_muons': list(),
                self.branch_prefix + 'sel_dimuons': list(),
                self.branch_prefix + 'sel_cutflow': cutflow
            }

        # Return selection
        return {
            self.branch_prefix + 'all_dsa_muons': selection.all_dsa_muons,
            self.branch_prefix + 'all_pat_muons': selection.all_pat_muons,
            self.branch_prefix + 'all_dimuons': selection.all_dimuons,
            self.branch_prefix + 'sel_info': selection.sel_info,
            self.branch_prefix + 'sel_dsa_muons': selection.sel_dsa_muons,
            self.branch_prefix + 'sel_pat_muons': selection.sel_pat_muons,
            self.branch_prefix + 'sel_dimuons': selection.sel_dimuons,
            self.branch_prefix + 'sel_cutflow': cutflow
        }
