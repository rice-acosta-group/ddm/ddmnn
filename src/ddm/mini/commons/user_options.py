from ddm.core.execution.runtime import get_logger
from ddm.mini.commons.constants import YEAR


class UserOptions(object):

    def __init__(self, opt_str):
        # Get Tokens
        tokens = opt_str.split('_')

        # decide what set of cuts to apply based on opt_string
        self.PROMPT = 'Prompt' in tokens
        self.NOPROMPT = 'NoPrompt' in tokens
        self.COMBINED = 'Combined' in tokens
        self.NSTATIONS = 'NS' in tokens
        self.NMUONHITS = 'NH' in tokens
        self.FPTERR = 'FPTE' in tokens
        self.HLT = 'HLT' in tokens
        self.TRIG = 'TRIG' in tokens
        self.HLTPP = 'HLTPP' in tokens
        self.HLTPUREPP = 'HLTPUREPP' in tokens
        self.HLTCOSMIC = 'HLTCOSMIC' in tokens
        self.HLTPURECOSMIC = 'HLTPURECOSMIC' in tokens
        self.PPHLT = 'PPHLT' in tokens
        self.COSMICHLT = 'COSMICHLT' in tokens
        self.REP = 'REP' in tokens
        self.REPNN = 'REPNN' in tokens
        self.REPGEN = 'REPGEN' in tokens
        self.REPNOSTD = 'REPNOSTD' in tokens
        self.REPFORML = 'REPFORML' in tokens
        self.PT = 'PT' in tokens
        self.TOPPT = 'TOPPT' in tokens
        self.TRKCHI2 = 'TRK' in tokens
        self.NDTHITS = 'NDT' in tokens
        self.TIMING = 'TIME' in tokens
        self.DCA = 'DCA' in tokens
        self.PC = 'PC' in tokens
        self.PCNN = 'PCNN' in tokens
        self.PCGEN = 'PCGEN' in tokens
        self.PCFORML = 'PCFORML' in tokens
        self.LXYERR = 'LXYE' in tokens
        self.MASS = 'MASS' in tokens
        self.CHI2 = 'CHI2' in tokens
        self.VTX = 'VTX' in tokens
        self.COSA = 'COSA' in tokens
        self.NPP = 'NPP' in tokens
        self.INPP = 'INPP' in tokens
        self.LXYSIG = 'LXYSIG' in tokens
        self.MAXPT = 'MAXPT' in tokens
        self.DPHI = 'DPHI' in tokens
        self.IDPHI = 'IDPHI' in tokens
        self.OS = 'OS' in tokens
        self.GBLHITS = 'GBLHITS' in tokens
        self.MISSHITS = 'MISSHITS' in tokens
        self.KCHI2 = 'KCHI2' in tokens
        self.ISTRACKER = 'ISTM' in tokens
        self.TSTAT = 'TSTAT' in tokens
        self.ISHP = 'ISHP' in tokens
        self.ISO = 'ISO' in tokens
        self.PMATCH = 'PMATCH' in tokens
        self.PXLHITS = 'PXLHITS' in tokens
        self.NLAY = 'NLAY' in tokens
        self.Z = 'Z' in tokens

        # not (yet) used (or deprecated)
        self.D0SIG = 'D0SIG' in tokens
        self.ISMEDIUM = 'MED' in tokens
        self.NTRKLAYS = 'NTL' in tokens
        self.CRTI = 'CRTI' in tokens
        self.CTKISO = 'CTKISO' in tokens
        self.TKISO = 'TKISO' in tokens
        self.OPACT = 'OPACT' in tokens

        # Retupler options
        self.DSAPATLINK = 'DSAPATLINK' in tokens
        self.DSAPATLINKNN = 'DSAPATLINKNN' in tokens

        # validate deltaPhi
        if self.DPHI and self.IDPHI:
            raise ValueError(
                '[OPTION ERROR]: Cannot select DPHI and IDPHI simultaneously'
            )

        # check if we are trying to apply deprecated cuts
        if self.PROMPT or self.NOPROMPT or self.COMBINED or self.CRTI or self.TKISO or self.CTKISO:
            raise ValueError(
                '[OPTION ERROR]: PROMPT, NOPROMPT, COMBINED, CRTI, TKISO, CTKISO are deprecated cuts'
            )

        # other checks
        if (self.HLTCOSMIC or self.HLTPURECOSMIC) and YEAR == 2016:
            get_logger().warn('+++ Warning: 2016 samples only have pp-seeded HLT paths, but \'_HLTCOSMIC\'/\'_HLTPURECOSMIC\' is in the cutstring +++')
        if self.HLTPP and self.HLTPUREPP:
            get_logger().warn('+++ Warning: HLTPUREPP has no effect if HLPP +++')

        if self.HLTCOSMIC and self.HLTPURECOSMIC:
            get_logger().warn('+++ Warning: HLTPURECOSMIC has no effect if HLTCOSMIC +++')

        # Retupler checks
        if (self.DSAPATLINK or self.DSAPATLINKNN) and (self.REP or self.REPNN or self.REPGEN):
            raise ValueError('Cannot use REP, REPNN, or REPGEN when DSAPATLINK or DSAPATLINKNN is enabled')

    def get_muon_cuts(self):
        cuts = []

        if self.NSTATIONS: cuts.append('q_nStations')
        if self.NMUONHITS: cuts.append('q_nMuonHits')

        return cuts

    def get_pat_qual_cuts(self):
        cuts = []

        if self.ISMEDIUM:
            cuts.append('p_isMedium')

        if self.ISTRACKER:
            cuts.append('p_isTracker')

        if self.ISHP:
            cuts.append('p_isHighPurity')

        if self.NTRKLAYS:
            cuts.append('p_nTrkLays')

        if self.GBLHITS:
            cuts.append('p_globHits')

        if self.KCHI2:
            cuts.append('p_kinkChi2')

        if self.TSTAT:
            cuts.append('p_trkrStats')

        if self.PMATCH:
            cuts.append('p_posMatch')

        return cuts

    def get_dimuon_cuts(self):
        cuts = []

        if self.LXYERR:
            cuts.append('d_LxyErr')

        if self.MASS:
            cuts.append('d_mass')

        if self.CHI2:
            cuts.append('d_vtxChi2')

        if self.COSA:
            cuts.append('d_cosAlpha')
            cuts.append('d_cosAlphaO')

        # if self.D0SIG: cutList.append('d_d0Sig')
        if self.DCA:
            cuts.append('d_DCA')

        if self.LXYSIG:
            cuts.append('d_LxySig')

        if self.DPHI:
            cuts.append('d_deltaPhi')

        if self.IDPHI:
            cuts.append('d_IDeltaPhi')

        if self.OS:
            cuts.append('d_oppSign')

        if self.MISSHITS:
            cuts.append('d_missHits')

        if self.ISO:
            cuts.append('d_customTrkIso')

        if self.MAXPT:
            cuts.append('d_maxpT')

        if self.NLAY:
            cuts.append('d_nTrkLay')

        if self.Z:
            cuts.append('d_Zmass')
            cuts.append('d_InvLxySig')

        return cuts

    def get_all_muon_cuts(self):
        cuts = []

        if self.PT:
            cuts.append('m_pT')

        if self.TRKCHI2:
            cuts.append('m_trkChi2')

        if self.NDTHITS:
            cuts.append('m_nDTHits')

        if self.FPTERR:
            cuts.append('m_FPTE')

        if self.D0SIG:
            cuts.append('m_d0Sig')

        return cuts
