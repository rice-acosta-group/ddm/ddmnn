YEAR = 2022
COSMICS_NTUPLES_COMPATIBILITY_MODE = False

SIGNAL_HLT_PATH = {
    2016: {
        "ppseed": "HLT_L2DoubleMu28_NoVertex_2Cha_Angle2p5_Mass10_v",
    },
    2018: {
        "ppseed": "HLT_DoubleL2Mu23NoVtx_2Cha_v",
        "cosmicseed": "HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v",
    },
    2022: {
        "ppseed": "HLT_DoubleL2Mu23NoVtx_2Cha_v",
        "cosmicseed": "HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v",
        "ppseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_Eta2p4_v",
        "cosmicseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_Eta2p4_v",
        "ppseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_VetoL3Mu0DxyMax1cm_v",
        "cosmicseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_CosmicSeed_VetoL3Mu0DxyMax1cm_v",
        "hybrid": "HLT_DoubleL2Mu_L3Mu16NoVtx_VetoL3Mu0DxyMax0p1cm_v",
        "ppseed_doubleL3": "HLT_DoubleL3Mu16_10NoVtx_DxyMin0p01cm_v",
        "doubleL3dTks": "HLT_DoubleL3dTksMu16_10NoVtx_DxyMin0p01cm_v",
    },
    2023: {
        "ppseed": "HLT_DoubleL2Mu23NoVtx_2Cha_v",
        "cosmicseed": "HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v",
        "ppseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_Eta2p4_v",
        "cosmicseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_Eta2p4_v",
        "ppseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_VetoL3Mu0DxyMax1cm_v",
        "cosmicseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_CosmicSeed_VetoL3Mu0DxyMax1cm_v",
        "hybrid": "HLT_DoubleL2Mu_L3Mu16NoVtx_VetoL3Mu0DxyMax0p1cm_v",
        "ppseed_doubleL3": "HLT_DoubleL3Mu16_10NoVtx_DxyMin0p01cm_v",
        "doubleL3dTks": "HLT_DoubleL3dTksMu16_10NoVtx_DxyMin0p01cm_v",
    },
    2024: {
        "ppseed": "HLT_DoubleL2Mu23NoVtx_2Cha_v",
        "cosmicseed": "HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v",
        "ppseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_Eta2p4_v",
        "cosmicseed_er2p4": "HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_Eta2p4_v",
        "ppseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_VetoL3Mu0DxyMax1cm_v",
        "cosmicseed_L3veto": "HLT_DoubleL2Mu10NoVtx_2Cha_CosmicSeed_VetoL3Mu0DxyMax1cm_v",
        "hybrid": "HLT_DoubleL2Mu_L3Mu16NoVtx_VetoL3Mu0DxyMax0p1cm_v",
        "ppseed_doubleL3": "HLT_DoubleL3Mu16_10NoVtx_DxyMin0p01cm_v",
        "doubleL3dTks": "HLT_DoubleL3dTksMu16_10NoVtx_DxyMin0p01cm_v",
    },
}

L1TSEEDS = {
    2022: [
        "L1_DoubleMu_15_5_SQ",
        "L1_DoubleMu_15_7",
        "L1_TripleMu_5_3_3",
        "L1_DoubleMu0er1p5_SQ_OS_dR_Max1p4",
        "L1_DoubleMu4p5_SQ_OS_dR_Max1p2",
        "L1_DoubleMu0_Upt15_Upt7",
        "L1_DoubleMu0_Upt6_IP_Min1_Upt4",
    ],
    2023: [
        "L1_DoubleMu_15_5_SQ",
        "L1_DoubleMu_15_7",
        "L1_TripleMu_5_3_3",
        "L1_DoubleMu0er1p5_SQ_OS_dR_Max1p4",
        "L1_DoubleMu4p5_SQ_OS_dR_Max1p2",
        "L1_DoubleMu0_Upt15_Upt7",
        "L1_DoubleMu0_Upt6_IP_Min1_Upt4",
    ],
    2024: [
        "L1_DoubleMu_15_5_SQ",
        "L1_DoubleMu_15_7",
        "L1_TripleMu_5_3_3",
        "L1_DoubleMu0er1p5_SQ_OS_dR_Max1p4",
        "L1_DoubleMu4p5_SQ_OS_dR_Max1p2",
        "L1_DoubleMu0_Upt15_Upt7",
        "L1_DoubleMu0_Upt6_IP_Min1_Upt4",
        "L1_DoubleMu0er1p4_SQ_OS_dR_Max1p4",  # New from 2024
        "L1_DoubleMu6_Upt6_SQ_er2p0",
        "L1_DoubleMu7_Upt7_SQ_er2p0",
        "L1_DoubleMu8_Upt8_SQ_er2p0",
        "L1_DoubleMu0_Upt6_SQ_er2p0",
        "L1_DoubleMu0_Upt7_SQ_er2p0",
        "L1_DoubleMu0_Upt8_SQ_er2p0",
    ]
}
