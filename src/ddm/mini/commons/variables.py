from ROOT import Math
from ROOT import TMath


def calc_deltaphi(phi1, phi2):
    """Python reimplementation of macros/deltaphi.C"""
    pi = TMath.Pi()
    dphi = phi1 - phi2

    # Reduce range
    if abs(dphi) < pi:
        return abs(dphi)
    else:
        n = round(dphi / (2.0 * pi))
        return abs(dphi - (n * 2.0 * pi))


def calc_phi(x, y, z=0, tx=0, ty=0, tz=0):
    """Python reimplementation of macros/phi.C"""
    head = Math.XYZPoint(x, y, z)
    tail = Math.XYZPoint(tx, ty, tz)
    vec = Math.XYZVector(head - tail)
    return vec.Phi()
