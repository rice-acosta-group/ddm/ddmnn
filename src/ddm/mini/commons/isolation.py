from ddm.core.commons.combinatorics import yield_combinations


def yield_iso_thresholds():
    dR_list = [0.3, 0.4]
    dz_list = [0.2, 0.3, 0.4, 0.5]
    d0_list = [0.1, 0.2]

    return yield_combinations(dR_list, dz_list, d0_list)
