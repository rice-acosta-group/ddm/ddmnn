from ddm.core.commons.caching import CachedVector
from ddm.mini.commons import particles
from ddm.mini.primitives.beamspot import Beamspot
from ddm.mini.primitives.dimuon import Dimuon
from ddm.mini.primitives.dsa_muon import DSAMuon
from ddm.mini.primitives.event_info import EventInfo
from ddm.mini.primitives.filters import Filters
from ddm.mini.primitives.gen_dimuon import GENDimuon
from ddm.mini.primitives.gen_muon import GenMuon
from ddm.mini.primitives.gen_particle import GenParticle
from ddm.mini.primitives.hlt_dimuon import HLTDimuon
from ddm.mini.primitives.hlt_path import HLTPath
from ddm.mini.primitives.l1t_seed import L1TSeed
from ddm.mini.primitives.llp_gun_dimuon import LLPGunDimuon
from ddm.mini.primitives.met import MET
from ddm.mini.primitives.pat_muon import PATMuon
from ddm.mini.primitives.trg_muon import HLTMuon, L1TMuon
from ddm.mini.primitives.vertex import Vertex


def get_event_info(entry):
    return EventInfo(entry)


def get_hlt_paths(entry):
    return CachedVector(len(entry.trig_hlt_idx), lambda idx: HLTPath(entry, idx))


def get_hlt_muons(entry):
    return CachedVector(len(entry.trig_hltmu_idx), lambda idx: HLTMuon(entry, idx))


def get_hlt_dimuons(entry):
    return CachedVector(len(entry.hdim_mu1_idx), lambda idx: HLTDimuon(entry, idx))


def get_l1t_muons(entry):
    return CachedVector(len(entry.trig_l1tmu_idx), lambda idx: L1TMuon(entry, idx))


def get_l1t_seeds(entry):
    return CachedVector(len(entry.trig_l1tmu_algo), lambda idx: L1TSeed(entry, idx))


def get_met(entry):
    return MET(entry)


def get_filters(entry):
    return Filters(entry)


def get_beamspot(entry):
    return Beamspot(entry)


def get_vertex(entry):
    return Vertex(entry)


def get_gen_particles(entry):
    # automatically detect whether this sample is HTo2XTo2Mu2J, HTo2XTo4Mu, SquarkToNeutralinoTo2LNu or Background
    # if the LLX pdgID is in the list of pdg IDs, it's a signal sample
    # then check if the third element is a muon: if it is, then it's 4Mu, otherwise it's 2Mu2J
    # otherwise, it's a background sample
    # Signal
    if particles.LLX_PDGID in entry.gen_pdgID:
        # 2Mu2J
        if abs(entry.gen_pdgID[2]) != particles.ABS_MUON_PDGID:
            muons = [GenMuon(entry, idx, 'signal') for idx in range(2)]
            jets = [GenMuon(entry, idx, 'jet') for idx in range(2, 4)]
            mothers = [GenParticle(entry, idx) for idx in range(4, 8)]

            extra_muons = []

            if len(entry.gen_eta) > 8:
                extra_muons = [GenMuon(entry, i, 'pileup') for i in range(8, len(entry.gen_eta))]

            return muons + jets + mothers + extra_muons
        # 4Mu
        else:
            muons = [GenMuon(entry, idx, 'signal') for idx in range(4)]
            mothers = [GenParticle(entry, idx) for idx in range(4, 8)]

            extra_muons = []

            if len(entry.gen_eta) > 8:
                extra_muons = [GenMuon(entry, idx, 'pileup') for idx in range(8, len(entry.gen_eta))]

            return muons + mothers + extra_muons
    # Dark photon
    elif particles.LLZD_PDGID in entry.gen_pdgID:
        muons = [GenMuon(entry, idx, 'signal') for idx in range(2)]
        other_final_states = [GenMuon(entry, idx) for idx in range(2, 4)]
        mothers = [GenParticle(entry, idx) for idx in range(4, 8)]

        extra_muons = []

        if len(entry.gen_eta) > 8:
            extra_muons = [GenMuon(entry, idx, 'pileup') for idx in range(8, len(entry.gen_eta))]

        # In case the other final states are muons,
        # this is the 4 muon final state
        for x in other_final_states:
            if abs(x.pdgID) == 13:
                x.kind = 'signal'

        return muons + other_final_states + mothers + extra_muons
    # RPV SUSY
    elif particles.LLNEUTRALINO_PDGID in entry.gen_pdgID:
        muons = [GenMuon(entry, idx, 'signal') for idx in range(4)]
        mothers = [GenParticle(entry, idx) for idx in range(4, 8)]

        extra_muons = []

        if len(entry.gen_eta) > 8:
            extra_muons = [GenMuon(entry, idx, 'pileup') for idx in range(8, len(entry.gen_eta))]

        return muons + mothers + extra_muons
    # Background
    else:
        return [GenParticle(entry, i) for i in range(len(entry.gen_eta))]


def get_gen_dimuons(entry):
    return CachedVector(len(entry.gdim_mu1_idx), lambda idx: GENDimuon(entry, idx))


def get_llp_gun_dimuons(entry):
    return CachedVector(len(entry.lgdim_mu1_idx), lambda idx: LLPGunDimuon(entry, idx))


def get_pat_muons(entry):
    return CachedVector(len(entry.patmu_eta), lambda idx: PATMuon(entry, idx))


def get_dsa_muons(entry):
    return CachedVector(len(entry.dsamu_eta), lambda idx: DSAMuon(entry, idx))


def get_dimuons(entry):
    return CachedVector(len(entry.dim_eta), lambda idx: Dimuon(entry, idx))
