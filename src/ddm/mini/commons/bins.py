import numpy as np

from ddm.core.commons.binning import uniform_step_axis, inverted_uniform_step_axis

pt_eff_bins = np.asarray([
    0., 10., 20., 30., 40., 50., 60., 80.,
    100., 200., 300., 400., 500., 600., 800.
])
lxy_eff_bins = np.concatenate((
    np.arange(0.0, 50.0, 2.0),
    np.arange(50.0, 200.0, 10.0),
    np.array([
        220.0, 240.0, 255.0, 270.0,
        290.0, 310.0, 340.0, 370.0,
        400.0,
    ]),
))
lz_eff_bins = np.concatenate((
    np.arange(0.0, 50.0, 2.0),
    np.arange(50.0, 200.0, 10.0),
    np.array([
        220.0, 240.0, 255.0, 270.0,
        290.0, 310.0, 340.0, 370.0,
        400.0, 450.0, 500.0
    ]),
))
lxyz_eff_bins = np.asarray([
    0., 10., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.,
    150., 200., 250., 300., 350., 400., 450., 500.
])
eta_eff_bins = uniform_step_axis(0.1, -3., 3.)
phi_deg_eff_bins = uniform_step_axis(5.0, -180., 180.)

# Emulator Validation
ddm_min_threshold = 1e-5
ddm_mismatch_threshold = 1e-5

ddm_q_bins = uniform_step_axis(1, -1, 1, align='center')
ddm_qpt_bins = uniform_step_axis(1, -140, 140, align='center')
ddm_qpt_err_bins = uniform_step_axis(2, -140, 140, align='center')
ddm_qinvpt_bins = inverted_uniform_step_axis(1, 2, 140, align='center', add_negatives=True)
ddm_qinvpt_err_bins = inverted_uniform_step_axis(2, 2, 140, align='center', add_negatives=True)

ddm_pt_bins = uniform_step_axis(1, 0, 140)
ddm_pt_err_bins = uniform_step_axis(2, -140, 140)
ddm_pt_rerr_bins = uniform_step_axis(0.05, -1, 2)
ddm_invpt_bins = inverted_uniform_step_axis(1, 2, 140, add_negatives=False)
ddm_invpt_err_bins = inverted_uniform_step_axis(2, 2, 140, add_negatives=False)
ddm_rels_bins = uniform_step_axis(0.01, 0, 1)

ddm_d0_sign_bins = uniform_step_axis(1, -1, 1, align='center')
ddm_dxy_sign_bins = uniform_step_axis(1, -1, 1, align='center')
ddm_d0_bins = uniform_step_axis(10, -140, 140)
ddm_d0_err_bins = uniform_step_axis(10, -140, 140)
ddm_dxy_bins = uniform_step_axis(10, -140, 140)
ddm_dxy_err_bins = uniform_step_axis(10, -140, 140)
ddm_udxy_bins = uniform_step_axis(10, 0, 140)
ddm_udxy_err_bins = uniform_step_axis(10, -140, 140)
ddm_z0_bins = uniform_step_axis(10, -500, 500)
ddm_uz0_bins = uniform_step_axis(10, 0, 500)

ddm_lxy_bins = uniform_step_axis(10, 0, 300)
ddm_lz_bins = uniform_step_axis(10, 0, 500)
ddm_phi_deg_bins = uniform_step_axis(5.0, -180, 180)
ddm_phi_deg_err_bins = uniform_step_axis(10.0, -180, 180)
ddm_abs_eta_bins = uniform_step_axis(0.1, 0., 3.30)
ddm_abs_eta_err_bins = uniform_step_axis(0.2, -2.40, 2.40)
