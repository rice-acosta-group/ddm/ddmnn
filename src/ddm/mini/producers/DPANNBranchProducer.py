import numpy as np

from ddm.core.commons.lazy import LazyMeta
from ddm.core.commons.ml_models import load_protobuf_model
from ddm.core.entries import Producer
from ddm.mini.utils.nn_association import get_association_vars


class DPANNBranchProducer(Producer, metaclass=LazyMeta):

    def __init__(self, protobuf_file):
        self.pb_graph, self.pb_session = None, None
        self.pb_input_tensor = None
        self.pb_output_tensor = None

        if protobuf_file is not None:
            self.pb_graph, self.pb_session = load_protobuf_model(protobuf_file)
            self.pb_input_tensor = self.pb_graph.get_tensor_by_name('inputs:0')
            self.pb_output_tensor = self.pb_graph.get_tensor_by_name('Identity:0')

    def provides(self):
        return [
            'dsamu_idx_NNMatch',
            'dsamu_nSegms_NNMatch',
            'dsamu_mscore_NNMatch',
            'dsamu_mscore_SegmMatch',
        ]

    def extract(self, entry):
        # Init products
        dsa_muons = entry.ext_dsa_muons
        vsize_dsamu = len(dsa_muons)
        dsamu_idx_NNMatch = np.ones((vsize_dsamu,)) * -1
        dsamu_nSegms_NNMatch = np.zeros((vsize_dsamu,))
        dsamu_mscore_NNMatch = np.zeros((vsize_dsamu,))
        dsamu_mscore_SegmMatch = list()

        products = {
            'dsamu_idx_NNMatch': dsamu_idx_NNMatch,
            'dsamu_nSegms_NNMatch': dsamu_nSegms_NNMatch,
            'dsamu_mscore_NNMatch': dsamu_mscore_NNMatch,
            'dsamu_mscore_SegmMatch': dsamu_mscore_SegmMatch,
        }

        # Short-Circuit: No dsa dimuons
        if vsize_dsamu == 0:
            return products

        # Collect variables
        evaluation_info_col = list()
        evaluation_var_col = list()

        for dsa_muon in dsa_muons:
            # Init mscore for SegmMatch with 0
            vsize_SegmMatch = len(dsa_muon.idx_SegmMatch)

            if vsize_SegmMatch == 0:
                dsamu_mscore_SegmMatch.append(list())
            else:
                dsamu_mscore_SegmMatch.append(np.zeros((vsize_SegmMatch,)))

            # Collect Seg Match
            for seg_match_idx, pat_idx in enumerate(dsa_muon.idx_SegmMatch):
                # Get PAT muon
                pat_muon = entry.ext_pat_muons[pat_idx]

                # Get SegMatch info
                var_nsegments = dsa_muon.nSegms_SegmMatch[seg_match_idx]
                var_dR_pd = dsa_muon.deltaR_pd_SegmMatch[seg_match_idx]
                var_dR_pp = dsa_muon.deltaR_pp_SegmMatch[seg_match_idx]

                # Get Variables
                variables = get_association_vars(
                    dsa_muon, pat_muon,
                    var_nsegments, var_dR_pd, var_dR_pp
                )

                # Remove NaN
                variables = np.where(np.isnan(variables), np.zeros_like(variables), variables)

                # Append
                evaluation_info_col.append((dsa_muon, pat_muon, seg_match_idx, var_nsegments))
                evaluation_var_col.append(variables)

        # Short-Circuit: Evaluations not needed
        if len(evaluation_info_col) == 0:
            return products

        # Predict Parameters
        pb_predictions = self.pb_session.run(
            self.pb_output_tensor,
            feed_dict={self.pb_input_tensor: evaluation_var_col}
        )

        pred_mscore = pb_predictions[:, 0]

        # Collect results
        all_seg_match = [
            (dsa_muon.idx, pat_muon.idx, seg_match_idx, seg_match_nsegments, seg_match_mscore)
            for (dsa_muon, pat_muon, seg_match_idx, seg_match_nsegments), seg_match_mscore
            in zip(evaluation_info_col, pred_mscore)
        ]

        all_seg_match.sort(key=lambda val: val[4], reverse=True)

        # Collect Products
        used_dsa_idx = set()
        used_pat_idx = set()

        for dsa_idx, pat_idx, seg_match_idx, seg_match_nsegments, seg_match_mscore in all_seg_match:
            # Record segmatch mscore
            dsamu_mscore_SegmMatch[dsa_idx][seg_match_idx] = seg_match_mscore

            # Short-Circuit: DSA already used
            if dsa_idx in used_dsa_idx:
                continue

            # Short-Circuit: PAT already used
            if pat_idx in used_pat_idx:
                continue

            # Record indices
            used_dsa_idx.add(dsa_idx)
            used_pat_idx.add(pat_idx)

            # Record best match
            dsamu_idx_NNMatch[dsa_idx] = pat_idx
            dsamu_nSegms_NNMatch[dsa_idx] = seg_match_nsegments
            dsamu_mscore_NNMatch[dsa_idx] = seg_match_mscore

        # Return
        return products
