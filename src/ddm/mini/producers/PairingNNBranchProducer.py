from ddm.core.commons.lazy import LazyMeta
from ddm.core.commons.ml_models import load_protobuf_model
from ddm.core.entries import Producer
from ddm.mini.utils.nn_pairing import get_pairing_vars


class PairingNNBranchProducer(Producer, metaclass=LazyMeta):

    def __init__(self, protobuf_file):
        self.pb_graph, self.pb_session = None, None
        self.pb_input_tensor = None
        self.pb_output_tensor = None

        if protobuf_file is not None:
            self.pb_graph, self.pb_session = load_protobuf_model(protobuf_file)
            self.pb_input_tensor = self.pb_graph.get_tensor_by_name('inputs:0')
            self.pb_output_tensor = self.pb_graph.get_tensor_by_name('Identity:0')

    def provides(self):
        return [
            'dim_pscore'
        ]

    def extract(self, entry):
        # Collect variables
        evaluation_var_col = [
            get_pairing_vars(dimuon, entry.ext_dsa_muons, entry.ext_pat_muons)
            for dimuon in entry.ext_dimuons
        ]

        # Short-Circuit: Evaluation not needed
        if len(evaluation_var_col) == 0:
            return {
                'dim_pscore': list()
            }

        # Predict Parameters
        pb_predictions = self.pb_session.run(
            self.pb_output_tensor,
            feed_dict={self.pb_input_tensor: evaluation_var_col}
        )

        # Return
        return {
            'dim_pscore': pb_predictions[:, 0]
        }
