from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer


class HLTDimuonBranchProducer(Producer, metaclass=LazyMeta):

    def provides(self):
        return [
            'hdim_mu1_idx',
            'hdim_mu2_idx',
        ]

    def extract(self, entry):
        # Init branches
        hdim_mu1_idx = list()
        hdim_mu2_idx = list()

        # Loop HLT muon pairs
        vsize_hlt_mu = len(entry.trig_hltmu_idx)

        for hlt_mu1_idx in range(vsize_hlt_mu):
            for hlt_mu2_idx in range(hlt_mu1_idx + 1, vsize_hlt_mu):
                hdim_mu1_idx.append(hlt_mu1_idx)
                hdim_mu2_idx.append(hlt_mu2_idx)

        # Return
        return {
            'hdim_mu1_idx': hdim_mu1_idx,
            'hdim_mu2_idx': hdim_mu2_idx
        }
