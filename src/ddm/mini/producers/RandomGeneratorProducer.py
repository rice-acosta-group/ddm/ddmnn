from ROOT import TRandom

from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer


class DPANNProducer(Producer, metaclass=LazyMeta):

    def provides(self):
        return [
            'random',
        ]

    def extract(self, entry):
        random = TRandom()
        event_info = entry.ext_info(entry)
        random.SetSeed(event_info.entry)

        return {
            'random': random,
        }
