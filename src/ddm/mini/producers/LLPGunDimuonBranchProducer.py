from ddm.core.commons.collections import reduce_collection
from ddm.core.commons.combinatorics import yield_pairs
from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer


class LLPGunDimuonBranchProducer(Producer, metaclass=LazyMeta):

    def provides(self):
        return [
            'lgdim_mu1_idx',
            'lgdim_mu2_idx',
        ]

    def extract(self, entry):
        # Get GEN muons
        gen_muons = entry.ext_gen_particles[:4]

        # Pair GEN muons
        def calc_vtx_distance(mu1, mu2):
            return (mu1.pos - mu2.pos).Mag()

        pairs = reduce_collection(
            [pair for pair in yield_pairs(gen_muons)],
            keys_fn=lambda pair: (pair[0].idx, pair[1].idx),
            metric_fn=lambda pair: calc_vtx_distance(pair[0], pair[1])
        )

        # Build dimuons
        lgdim_mu1_idx = list()
        lgdim_mu2_idx = list()

        for mu1, mu2 in pairs:
            lgdim_mu1_idx.append(mu1.idx)
            lgdim_mu2_idx.append(mu2.idx)

        # Return
        return {
            'lgdim_mu1_idx': lgdim_mu1_idx,
            'lgdim_mu2_idx': lgdim_mu2_idx
        }
