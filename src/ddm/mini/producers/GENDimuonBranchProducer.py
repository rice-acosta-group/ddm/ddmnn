import math

from ddm.core.commons.lazy import LazyMeta
from ddm.core.entries import Producer


class GENDimuonBranchProducer(Producer, metaclass=LazyMeta):

    def provides(self):
        return [
            'gdim_gen_idx',
            'gdim_mu1_idx',
            'gdim_mu2_idx',
        ]

    def extract(self, entry):
        # Minitupler puts paired signal muons together by index
        # Hence gen 0 with gen 1, gen 2 with gen 3, etc...
        gdim_gen_idx = list()
        gdim_mu1_idx = list()
        gdim_mu2_idx = list()

        # Currently we have at most 2 gen dimuons
        # In the case of signal samples, the signal muons are always the first muons in the gen collection
        # In the case of the particle gun the signal muons should be the first four muons in the gen collection
        for gdim_idx in range(2):
            gen_idx = gdim_idx + 4
            gen1_idx = gdim_idx * 2
            gen2_idx = gdim_idx * 2 + 1

            gen1_pdgid = entry.gen_pdgID[gen1_idx]
            gen2_pdgid = entry.gen_pdgID[gen2_idx]

            # Short-Circuit: Not a muon
            if abs(gen1_pdgid) != 13:
                continue

            # Short-Circuit: Not a muon
            if abs(gen2_pdgid) != 13:
                continue

            delta_x = entry.gen_x[gen1_idx] - entry.gen_x[gen2_idx]
            delta_y = entry.gen_y[gen1_idx] - entry.gen_y[gen2_idx]
            delta_z = entry.gen_z[gen1_idx] - entry.gen_z[gen2_idx]
            delta_vtx = math.hypot(delta_x, delta_y, delta_z)
            assert delta_vtx == 0, "Muons don't form a dimuon system. Vertex distance is beyond the tolerance."

            gdim_gen_idx.append(gen_idx)
            gdim_mu1_idx.append(gen1_idx)
            gdim_mu2_idx.append(gen2_idx)

        # Return
        return {
            'gdim_gen_idx': gdim_gen_idx,
            'gdim_mu1_idx': gdim_mu1_idx,
            'gdim_mu2_idx': gdim_mu2_idx
        }
