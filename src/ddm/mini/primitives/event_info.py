from ddm.core.primitives import Primitive


class EventInfo(Primitive):

    def __init__(self, entry):
        super().__init__(entry, 'evt_')

    def get_weight(self):
        return self._entry.get_from_tree('gen_weight')

    def get_nTruePV(self):
        return self._entry.get_from_tree('gen_tnpv')
