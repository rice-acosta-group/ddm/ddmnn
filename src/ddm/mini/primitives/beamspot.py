from ROOT import TVector3

from ddm.core.primitives import Primitive


class Beamspot(Primitive):

    def __init__(self, entry):
        super().__init__(entry, 'bs_')

    def get_pos(self):
        return TVector3(self.x, self.y, self.z)

    def get_err(self):
        return TVector3(self.dx, self.dy, self.dz)
