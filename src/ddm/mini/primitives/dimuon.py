from ROOT import TVector3

from ddm.core.primitives import VALUE_IS_MISSING
from ddm.mini.primitives.dsa_muon import DSAMuon
from ddm.mini.primitives.particle import Particle
from ddm.mini.primitives.pat_muon import PATMuon


class Dimuon(Particle):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'dim_')

        self.matched_gen_dimuon = None
        self.matched_gen_dimuon_are_aligned = None

    def get_id(self):
        return self.mu1.idx, self.mu2.idx

    def get_uid(self):
        return self.composition, self.id

    def get_alternate_uid_col(self):
        # Short-Circuit: DSA dimuons don't have alternates
        if self.composition == 'DSA':
            return list()

        # Collect indices
        dsa_idx_col = [None, None]
        pat_idx_col = [None, None]
        std_pat_idx_col = [None, None]

        for mu_idx, ref_mu in enumerate((self.mu1, self.mu2)):
            if ref_mu.kind == 'PAT':
                pat_idx_col[mu_idx] = ref_mu.idx

                if ref_mu.ideal_dsa_muon is not None:
                    dsa_idx_col[mu_idx] = ref_mu.ideal_dsa_muon.idx

                if ref_mu.ideal_std_pat_muon is not None:
                    std_pat_idx_col[mu_idx] = ref_mu.ideal_std_pat_muon.idx
            else:
                dsa_idx_col[mu_idx] = ref_mu.idx

        # Collect DSA Ids
        id_col = list()

        if (dsa_idx_col[0] is not None) and (dsa_idx_col[1] is not None):
            dsa1_idx, dsa2_idx = dsa_idx_col

            if dsa1_idx < dsa2_idx:
                id_col.append(('DSA', (dsa1_idx, dsa2_idx)))
            else:
                id_col.append(('DSA', (dsa2_idx, dsa1_idx)))

        # Collect HYB Ids
        if (dsa_idx_col[0] is not None) and (pat_idx_col[1] is not None):
            id_col.append(('HYBRID', (dsa_idx_col[0], pat_idx_col[1])))

        if (dsa_idx_col[1] is not None) and (pat_idx_col[0] is not None):
            id_col.append(('HYBRID', (dsa_idx_col[1], pat_idx_col[0])))

        if (dsa_idx_col[0] is not None) and (std_pat_idx_col[1] is not None):
            id_col.append(('HYBRID', (dsa_idx_col[0], std_pat_idx_col[1])))

        if (dsa_idx_col[1] is not None) and (std_pat_idx_col[0] is not None):
            id_col.append(('HYBRID', (dsa_idx_col[1], std_pat_idx_col[0])))

        # Collect Pat Ids
        if (pat_idx_col[0] is not None) and (std_pat_idx_col[1] is not None):
            pat1_idx, pat2_idx = pat_idx_col[0], std_pat_idx_col[1]

            if pat1_idx < pat2_idx:
                id_col.append(('PAT', (pat1_idx, pat2_idx)))
            else:
                id_col.append(('PAT', (pat2_idx, pat1_idx)))

        if (pat_idx_col[1] is not None) and (std_pat_idx_col[0] is not None):
            pat1_idx, pat2_idx = pat_idx_col[1], std_pat_idx_col[0]

            if pat1_idx < pat2_idx:
                id_col.append(('PAT', (pat1_idx, pat2_idx)))
            else:
                id_col.append(('PAT', (pat2_idx, pat1_idx)))

        if (std_pat_idx_col[0] is not None) and (std_pat_idx_col[1] is not None):
            pat1_idx, pat2_idx = std_pat_idx_col

            if pat1_idx < pat2_idx:
                id_col.append(('PAT', (pat1_idx, pat2_idx)))
            else:
                id_col.append(('PAT', (pat2_idx, pat1_idx)))

        # Return
        return id_col

    def get_charge(self):
        return self.mu1.charge + self.mu2.charge

    def get_llp_p4(self):
        return self.mu1.matched_gen_muon.p4 + self.mu2.matched_gen_muon.p4

    def get_llp_dR(self):
        return self.llp_p4.DeltaR(self.p4)

    def get_mu1(self):
        if self.mu1_idx < 0:
            return None

        if self.mu1_idx > 999:
            pat_muon = PATMuon(self._entry, self._idx, 'dim_mu1_')
            pat_muon.idx = self.mu1_idx - 1000
            pat_muon.x = self.x
            pat_muon.y = self.y
            pat_muon.z = self.z
            return pat_muon

        dsa_muon = DSAMuon(self._entry, self._idx, 'dim_mu1_')
        dsa_muon.idx = self.mu1_idx
        dsa_muon.x = self.x
        dsa_muon.y = self.y
        dsa_muon.z = self.z
        return dsa_muon

    def get_mu2(self):
        if self.mu2_idx < 0:
            return None

        if self.mu2_idx > 999:
            pat_muon = PATMuon(self._entry, self._idx, 'dim_mu2_')
            pat_muon.idx = self.mu2_idx - 1000
            pat_muon.x = self.x
            pat_muon.y = self.y
            pat_muon.z = self.z
            return pat_muon

        dsa_muon = DSAMuon(self._entry, self._idx, 'dim_mu2_')
        dsa_muon.idx = self.mu2_idx
        dsa_muon.x = self.x
        dsa_muon.y = self.y
        dsa_muon.z = self.z
        return dsa_muon

    def get_composition(self):
        if self.mu1.kind == 'DSA' and self.mu2.kind == 'DSA':
            return 'DSA'
        elif self.mu1.kind == 'PAT' and self.mu2.kind == 'PAT':
            return 'PAT'

        return 'HYBRID'

    def get_pca(self):
        return TVector3(self.pca_x, self.pca_y, self.pca_z)

    def get_pscore(self):
        # Try to get from entry
        pscore = self.get_from_entry('pscore')

        if pscore is VALUE_IS_MISSING:
            return 0.

        # Return
        return pscore
