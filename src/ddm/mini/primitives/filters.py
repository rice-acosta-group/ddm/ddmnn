from ddm.core.primitives import Primitive


class Filters(Primitive):

    def __init__(self, entry):
        super().__init__(entry, 'flag_')
