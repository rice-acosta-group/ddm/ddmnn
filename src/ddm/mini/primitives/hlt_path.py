from ddm.core.primitives import VectorPrimitive


class HLTPath(VectorPrimitive):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'trig_')
