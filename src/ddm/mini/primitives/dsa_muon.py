from ddm.core.primitives import VALUE_IS_MISSING
from ddm.mini.primitives.reco_muon import RecoMuon


class DSAMuon(RecoMuon):

    def __init__(self, entry, idx, branch_prefix='dsamu_'):
        super().__init__(entry, idx, branch_prefix)

        # Constants
        self.kind = 'DSA'
        self.matched_gen_muon = None
        self.replacement_status = 0

        self.ideal_dsa_muon = None
        self.ideal_std_pat_muon = None

    def get_idx_DispMatch(self):
        # Try to get from entry
        idx_DispMatch = self.get_from_entry('idx_DispMatch')

        if idx_DispMatch is not VALUE_IS_MISSING:
            return idx_DispMatch

        # Short-Circuit: Has a displaced match
        if self.stdMatch_minDR_std > 500:
            return self.idx_pd_ProxMatch

        # Return
        return -1

    def get_stdMatch_idx(self):
        # Try to get from entry
        stdMatch_idx = self.get_from_entry('stdMatch_idx')

        if stdMatch_idx is not VALUE_IS_MISSING:
            return stdMatch_idx

        # Short-Circuit: Has a standard match
        if self.stdMatch_minDR_std < 0.4:
            return self.idx_pd_ProxMatch

        # Return
        return -1
