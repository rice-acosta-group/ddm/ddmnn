import math

from ROOT import TLorentzVector
from ROOT import TVector3

from ddm.core.primitives import VectorPrimitive
from ddm.mini.commons.particles import MUON_MASS


class TriggerMuon(VectorPrimitive):

    def __init__(self, entry, idx, branch_prefix):
        super().__init__(entry, idx, branch_prefix)

        # Constants
        self.mass = MUON_MASS

    def get_p4(self):
        p4 = TLorentzVector()
        p4.SetPxPyPzM(self.px, self.py, self.pz, self.mass)

        return p4

    def get_p3(self):
        p3 = TVector3()
        p3.SetPtEtaPhi(self.pt, self.eta, self.phi)
        return p3

    def get_pt(self):
        return math.hypot(self.px, self.py)


class HLTMuon(TriggerMuon):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'trig_hltmu_')

        # Constants
        self.trigger = "HLT"


class L1TMuon(TriggerMuon):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'trig_l1tmu_')

        # Constants
        self.trigger = "L1T"
