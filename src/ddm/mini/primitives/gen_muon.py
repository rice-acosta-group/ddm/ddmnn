from ddm.core.commons.kinematics import propagate_particle, calc_z_star
from ddm.mini.commons.particles import MUON_MASS
from ddm.mini.primitives.gen_particle import GenParticle
from ddm.mini.primitives.particle import Particle


class GenMuon(GenParticle):

    def __init__(self, entry, idx, kind='not_signal'):
        super().__init__(entry, idx, kind=kind)

        # Constants
        self.mass = MUON_MASS

        # these three quantities are specific to signal gen muons
        if kind != 'signal':
            self.cosAlpha = -999
            self.deltaR = -999
            self.lxy = -999

    def get_BS(self):
        particle = Particle(self._entry, 'gen_bs_', self.idx)
        particle._producers['mass'] = lambda: self.mass
        particle._producers['charge'] = lambda: self.charge
        particle._producers['d0_'] = lambda: abs(self.bs_d0)
        particle._producers['dz_'] = lambda: abs(self.bs_dz)
        return particle

    def get_at_st2(self):
        return propagate_particle(
            self.charge, self.p3, self.pos,
            z_star=calc_z_star(self.p3.Eta(), self.pos.Z(), 850.)
        )
