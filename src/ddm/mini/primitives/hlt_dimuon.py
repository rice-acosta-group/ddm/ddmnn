from ddm.core.primitives import VectorPrimitive


class HLTDimuon(VectorPrimitive):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'hdim_')

    def get_id(self):
        return (self.mu1._idx, self.mu2._idx)

    def get_mu1(self):
        if self.mu1_idx < 0:
            return None

        return self._entry.ext_hlt_muons[self.mu1_idx]

    def get_mu2(self):
        if self.mu2_idx < 0:
            return None

        return self._entry.ext_hlt_muons[self.mu2_idx]

    def get_p4(self):
        return self.mu1.p4 + self.mu2.p4

    def get_mass(self):
        return self.p4.M()

    def get_angle(self):
        return self.mu1.p3.Angle(self.mu2.p3)
