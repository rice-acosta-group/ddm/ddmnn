from ddm.core.primitives import Primitive


class MET(Primitive):

    def __init__(self, entry):
        super().__init__(entry, 'met_')
