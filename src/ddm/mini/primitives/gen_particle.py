from ddm.mini.primitives.particle import Particle


class GenParticle(Particle):

    def __init__(self, entry, idx, kind=None):
        super().__init__(entry, idx, 'gen_')

        # Constants
        self.kind = kind

    def get_is_final_state(self):
        return self.status == 1
