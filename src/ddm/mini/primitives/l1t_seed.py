from ddm.core.primitives import VectorPrimitive


class L1TSeed(VectorPrimitive):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, '')

    def get_name(self):
        return self._entry.trig_l1tmu_algo[self._idx]
