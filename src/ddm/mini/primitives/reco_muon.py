from ddm.mini.commons.particles import MUON_MASS
from ddm.mini.primitives.particle import Particle


class RecoMuon(Particle):

    def __init__(self, entry, idx, branch_prefix):
        super().__init__(entry, idx, branch_prefix)

        # Constants
        self.mass = MUON_MASS
        self.kind = 'RECO'

    def get_normChi2(self):
        return (self.chi2 / self.ndof) if (self.ndof != 0) else float("inf")
