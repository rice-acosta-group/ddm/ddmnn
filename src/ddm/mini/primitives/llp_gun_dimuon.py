import math

from ROOT import TVector3

from ddm.core.primitives import VectorPrimitive
from ddm.mini.primitives.gen_muon import GenMuon


class LLPGunDimuon(VectorPrimitive):

    def __init__(self, entry, idx):
        super().__init__(entry, idx, 'lgdim_')

    def get_id(self):
        return (self.mu1.idx, self.mu2.idx)

    def get_mu1(self):
        if self.mu1_idx < 0:
            return None

        return GenMuon(self._entry, self.mu1_idx, 'signal')

    def get_mu2(self):
        if self.mu2_idx < 0:
            return None

        return GenMuon(self._entry, self.mu2_idx, 'signal')

    def get_charge(self):
        return self.mu1.charge + self.mu2.charge

    def get_pos(self):
        return TVector3(self.x, self.y, self.z)

    def get_lxy(self):
        return math.hypot(self.mu1.x, self.mu1.y)

    def get_x(self):
        return self.mu1.x

    def get_y(self):
        return self.mu1.y

    def get_z(self):
        return self.mu1.z

    def get_p3(self):
        return self.p4.Vect()

    def get_p4(self):
        return self.mu1.p4 + self.mu2.p4
