import math

from ROOT import TLorentzVector
from ROOT import TVector3

from ddm.core.primitives import VectorPrimitive, VALUE_IS_MISSING


class Particle(VectorPrimitive):

    def __init__(self, entry, idx, branch_prefix):
        super().__init__(entry, idx, branch_prefix)

    def get_pos(self):
        return TVector3(self.x, self.y, self.z)

    def get_p4(self):
        p4 = TLorentzVector()
        p4.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.energy)
        return p4

    def get_p3(self):
        p3 = TVector3()
        p3.SetPtEtaPhi(self.pt, self.eta, self.phi)
        return p3

    def get_energy(self):
        # Try to get from entry
        energy = self.get_from_entry('energy')

        if energy is not VALUE_IS_MISSING:
            return energy

        # Try to get from momentum and mass
        return math.hypot(self.p, self.mass)

    def get_pt(self):
        # Try to get from entry
        pt = self.get_from_entry('pt')

        if pt is not VALUE_IS_MISSING:
            return pt

        # Try to get from coords
        px = self.get_from_entry('px')
        py = self.get_from_entry('py')

        if (px is not VALUE_IS_MISSING) and (py is not VALUE_IS_MISSING):
            return math.hypot(px, py)

        # Try three vector
        return self.p3.Perp()

    def get_p(self):
        # Try to get from entry
        p = self.get_from_entry('p')

        if p is not VALUE_IS_MISSING:
            return p

        # Try to get from coords
        px = self.get_from_entry('px')
        py = self.get_from_entry('py')
        pz = self.get_from_entry('pz')

        if (px is not VALUE_IS_MISSING) and (py is not VALUE_IS_MISSING) and (pz is not VALUE_IS_MISSING):
            return math.hypot(px, py, pz)

        # Try three vector
        return self.p3.Mag()
