import numpy as np
from ROOT import TLorentzVector
from ROOT import TVector3

from ddm.core.primitives import VALUE_IS_MISSING
from ddm.mini.primitives.particle import Particle
from ddm.mini.primitives.reco_muon import RecoMuon


class PATMuon(RecoMuon):

    def __init__(self, entry, idx, branch_prefix='patmu_'):
        super().__init__(entry, idx, branch_prefix)

        # Constants
        self.kind = 'PAT'
        self.matched_gen_muon = None
        self.replaced_dsa_muon = None

        self.ideal_dsa_muon = None
        self.ideal_std_pat_muon = None

    def get_gen(self):
        return Particle(self._entry, self._idx, 'patmu_gen_')

    def get_p3_tk(self):
        return TVector3(self.px_tk, self.py_tk, self.pz_tk)

    def get_p4_tk(self):
        p4 = TLorentzVector()
        p4.SetPxPyPzM(self.px_tk, self.py_tk, self.pz_tk, self.mass)
        return p4

    def get_normChi2_Global(self):
        if self.ndof_Global != 0:
            return self.chi2_Global / self.ndof_Global

        return np.inf

    def get_isDisp(self):
        isDisp = self.get_from_entry('isDisp')

        if isDisp is not VALUE_IS_MISSING:
            return isDisp

        return self._entry.patmu_isDisp[self.idx]
