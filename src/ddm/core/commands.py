from abc import abstractmethod, ABC


class Command(ABC):

    def configure_parser(self, parser):
        pass

    @abstractmethod
    def run(self, args):
        raise NotImplementedError()
