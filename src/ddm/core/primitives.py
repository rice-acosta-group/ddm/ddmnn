class ValueIsMissing(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ValueIsMissing, cls).__new__(cls)
        return cls.instance


VALUE_IS_MISSING = ValueIsMissing()


class Primitive(object):
    __slots__ = (
        '__dict__',
        '_entry', '_branch_prefix',
        '_producers', '_producer_stack'
    )

    def __init__(self, entry, branch_prefix):
        self.__dict__ = dict()

        self._entry = entry
        self._branch_prefix = branch_prefix

        self._producers = dict()
        self._producer_stack = set()

    def __getattr__(self, field):
        # Try to get from cache
        value = self.get_from_cache(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to produce value
        value = self.get_from_producer(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to get from entry
        value = self.get_from_entry(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Error
        raise AttributeError(f'[{self.__class__.__name__}] Field does not have a value, producer, nor does it exists in the entry: %s' % field)

    def get_from_cache(self, field):
        return self.__dict__.get(field, VALUE_IS_MISSING)

    def get_from_producer(self, field):
        producer = self._producers.get(field, None)

        if producer is None:
            producer = getattr(self.__class__, f'get_{field}', None)

        if producer is None:
            return VALUE_IS_MISSING

        self._producers[field] = producer

        if field in self._producer_stack:
            raise RecursionError(f'[{self.__class__.__name__}] Field has a recursive dependency on itself: %s' % field)

        self._producer_stack.add(field)
        product = producer(self)
        object.__setattr__(self, field, product)
        self._producer_stack.remove(field)

        return product

    def get_from_entry(self, field):
        branch_name = self._branch_prefix + field
        value = getattr(self._entry, branch_name, VALUE_IS_MISSING)
        object.__setattr__(self, field, value)
        return value


class VectorPrimitive(Primitive):
    __slots__ = ('_idx',)

    def __init__(self, entry, idx, branch_prefix):
        super().__init__(entry, branch_prefix)

        # Constants
        self._idx = idx

    def get_idx(self):
        idx = self.get_from_entry('idx')

        if idx is not VALUE_IS_MISSING:
            return idx

        return self._idx

    def get_from_entry(self, field):
        branch_name = self._branch_prefix + field
        vector_value = getattr(self._entry, branch_name, VALUE_IS_MISSING)

        if vector_value is VALUE_IS_MISSING:
            return VALUE_IS_MISSING

        value = vector_value[self._idx]
        object.__setattr__(self, field, value)
        return value
