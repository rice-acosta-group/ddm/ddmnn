from ROOT import TCanvas
from ROOT import TH1D
from ROOT import TLatex
from ROOT import TLegend
from ROOT import TPad
from ROOT import gStyle
from ROOT import kBlue
from ROOT import kRed
from ROOT import kWhite

from ddm.core.analyzers import AbstractPlotter
from ddm.core.commons.histograms import get_th1_ideal_ranges, add_flows
from ddm.core.commons.tools import safe_divide


class TransferFactorPlotter(AbstractPlotter):

    def __init__(
            self, name, title, x_title, y_title, r_title,
            nxbins=None, x_low=None, x_up=None, xbins=None,
            num_label=None, den_label=None,
            logx=False, logy=True
    ):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.r_title = r_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.num_label = num_label
        self.den_label = den_label
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [x_low, x_up]):
            self.num_hist = TH1D(name + '_num', title, nxbins, x_low, x_up)
            self.den_hist = TH1D(name + '_den', title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.num_hist = TH1D(name + '_num', title, nxbins, xbins)
            self.den_hist = TH1D(name + '_den', title, nxbins, xbins)

    def add(self, other):
        self.num_hist.Add(other.num_hist)
        self.den_hist.Add(other.den_hist)

    def fill_numerator(self, value):
        self.num_hist.Fill(value)

    def fill_denominator(self, value):
        self.den_hist.Fill(value)

    def consolidate(self):
        # Add flows
        add_flows(self.num_hist)
        add_flows(self.den_hist)

        # Calculate transfer factors
        self.tf_hist = self.num_hist.Clone(self.name)
        self.tf_hist.Sumw2()
        self.tf_hist.Divide(self.den_hist)

    def write(self):
        # Write
        self.num_hist.Write()
        self.den_hist.Write()
        self.tf_hist.Write()

        # Build Entries
        hist_entries = [
            (self.den_hist, self.den_label, kRed, 3150),
            (self.num_hist, self.num_label, kBlue, 3195),
        ]

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        available_height = 1 - plot_canvas.GetTopMargin() - plot_canvas.GetBottomMargin()
        bottom_height = plot_canvas.GetBottomMargin() + available_height * 0.3
        top_height = available_height - bottom_height
        gap_height = top_height * 0.05

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetBottomMargin(bottom_height + gap_height)
        top_pad.SetLogy(1)
        top_pad.Draw()
        top_pad.cd()

        # Find min and max
        xmin, xmax, ymin, ymax = get_th1_ideal_ranges(
            self.den_hist, self.num_hist,
            logy=self.logy
        )

        # Draw top frame
        frame_top = plot_canvas.DrawFrame(
            xmin, 1e-1,
            xmax, ymax,
            ''
        )
        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)
        frame_top.GetYaxis().SetTitle(self.y_title)

        legend_x0 = 0.600
        legend_y0 = 0.450

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.150)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kWhite, 0.8)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)

        for plot, label, color, style in hist_entries:
            plot.SetFillColor(color)
            plot.SetFillStyle(style)
            plot.SetLineColor(color)
            plot.SetLineWidth(3)

            plot.Draw('SAME HIST')

            legend.AddEntry(plot, label, 'F')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        legend.Draw()

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("bottom", "bottom", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(1. - bottom_height)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw SF
        _, _, rmin, rmax = get_th1_ideal_ranges(self.tf_hist)

        frame_bottom = plot_canvas.DrawFrame(
            xmin, max(rmin, 0.),
            xmax, rmax,
            ''
        )

        frame_bottom.GetXaxis().SetTitle(self.x_title)
        frame_bottom.GetYaxis().SetTitle(self.r_title)
        frame_bottom.GetYaxis().SetNdivisions(204)

        self.tf_hist.SetMarkerStyle(8)
        self.tf_hist.SetMarkerSize(0.5)
        self.tf_hist.SetLineWidth(2)
        self.tf_hist.Draw('SAME P E1')

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Mean
        mean = safe_divide(self.num_hist.Integral(), self.den_hist.Integral())

        # Draw mean label
        mean_label = TLatex()
        mean_label.SetTextAlign(12)
        mean_label.DrawLatexNDC(
            (1.0 - plot_canvas.GetRightMargin()) * 0.8,
            bottom_height * 0.8,
            'Mean=%0.2f' % mean
        )

        # Draw Titles
        plot_canvas.cd(0)

        # Write
        plot_canvas.SaveAs(self.name + '.png')
