import numpy as np


def Constant(value):
    return Measurement(value, 0.0)


def Count(value):
    return Measurement(value, np.sqrt(value))


class Measurement:

    def __init__(self, value, error):
        self.value = value
        self.error = error

    def add(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        v = lhs_value + rhs_value
        e = np.hypot(lhs_error, rhs_error)

        return Measurement(v, e)

    def subtract(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        v = (lhs_value - rhs_value)
        e = np.hypot(lhs_error, rhs_error)

        return Measurement(v, e)

    def multiply(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        v = lhs_value * rhs_value
        e = np.hypot(lhs_error * rhs_value, lhs_value * rhs_error)

        return Measurement(v, e)

    def divide(self, rhs, nan=None):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        if not any([lhs_value, lhs_error, rhs_value, rhs_error]):
            return Measurement(0., 0.)

        if rhs_value == 0.:
            if nan is None:
                raise ValueError("Cannot divide by zero")
            else:
                return Measurement(nan, nan)

        v = lhs_value / rhs_value

        e = np.hypot(
            lhs_error / rhs_value,
            rhs_error * lhs_value / rhs_value / rhs_value
        )

        return Measurement(v, e)

    def divideb(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        if not any([lhs_value, lhs_error, rhs_value, rhs_error]):
            return Measurement(0., 0.)

        if rhs_value <= 0.:
            raise ValueError("Cannot binomially divide num <= 0")

        v = 1.0 * lhs_value / rhs_value
        e = np.sqrt(((1. - 2. * v) * np.power(lhs_error, 2) + np.power(v * rhs_error, 2)) / rhs_value)

        return Measurement(v, e)
