import functools


class ValueIsMissing(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ValueIsMissing, cls).__new__(cls)
        return cls.instance


VALUE_IS_MISSING = ValueIsMissing()


def Memoize(key_func=None):
    def wrapper(func):
        decorated = CachedLookupTable(value_func=func, key_func=key_func)
        functools.update_wrapper(decorated, func)
        return decorated

    return wrapper


class Cached:

    def __init__(self, func):
        self.cache = VALUE_IS_MISSING
        self.func = func

        functools.update_wrapper(self, func)

    def __call__(self, *args, **kwargs):
        # Use cached result
        if self.cache is not VALUE_IS_MISSING:
            return self.cache

        # Evaluate
        self.cache = self.func(*args, **kwargs)

        # Return
        return self.cache


class CachedLookupTable:

    def __init__(self, value_func=None, key_func=None):
        self.cache = dict()
        self.key_func = key_func
        self.value_func = value_func

    def __call__(self, *args, **kwargs):
        # Get key
        if self.key_func is None:
            key = (args, frozenset(kwargs.items()))
        else:
            key = self.key_func(*args, **kwargs)

        # Get cached value
        value = self.cache.get(key, VALUE_IS_MISSING)

        # Use cached result
        if value is not VALUE_IS_MISSING:
            return value

        # Evaluate
        value = self.value_func(*args, **kwargs)
        self.cache[key] = value
        return value


class CachedVector(list):

    def __init__(self, vsize, func):
        self.vsize = vsize
        self.cache = [VALUE_IS_MISSING] * vsize
        self.func = func

    def __len__(self):
        return self.vsize

    def __iter__(self):
        for idx in range(self.vsize):
            yield self._resolve_item(idx)

    def __setitem__(self, key, item):
        if isinstance(key, slice):
            start, stop, step = key.indices(self.vsize)
            splice_vsize = max(0, (stop - start + (step - 1)) // step)

            if len(item) != splice_vsize:
                raise ValueError("Attempt to assign sequence of different size to slice")

            for idx, value in enumerate(item):
                ridx = start + idx * step
                self.cache[ridx] = value
        else:
            if key >= self.vsize or key < 0:
                raise IndexError("Index out of range")

            self.cache[key] = item

    def __getitem__(self, key):
        # Create slice of cached vector
        if isinstance(key, slice):
            start, stop, step = key.indices(self.vsize)
            splice_vsize = max(0, (stop - start + (step - 1)) // step)

            def get_linked_item(idx):
                ridx = start + idx * step
                return self._resolve_item(ridx)

            return CachedVector(splice_vsize, get_linked_item)

        # Check index range
        if key >= self.vsize or key < 0:
            raise IndexError("Index out of range")

        # Return
        return self._resolve_item(key)

    def _resolve_item(self, idx):
        value = self.cache[idx]

        if value is not VALUE_IS_MISSING:
            return value

        value = self.func(idx)
        self.cache[idx] = value
        return value
