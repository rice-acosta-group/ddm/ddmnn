import cmsml
import keras


def save_model(model, name='model'):
    # Store model to file
    model.save(name + '.keras')
    model.save_weights(name + '.weights.h5')

    # Store model to json
    with open(name + '.json', 'w') as outfile:
        outfile.write(model.to_json())

    # Save Protobuf
    cmsml.tensorflow.save_graph(name + '.pb.txt', model, variables_to_constants=True)
    cmsml.tensorflow.save_graph(name + '.pb', model, variables_to_constants=True)


def load_model(name='model', custom_objects=None):
    model = keras.models.load_model(name + '.keras', custom_objects=custom_objects)
    model.load_weights(name + '.weights.h5')
    return model


def load_protobuf_model(name='model'):
    return cmsml.tensorflow.load_graph(name + '.pb', create_session=True)
