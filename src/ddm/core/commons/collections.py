# Dictionaries
def checkout(dictionary, key, val_factory):
    val = dictionary.get(key)

    if val is None:
        val = val_factory()
        dictionary[key] = val

    return val


# Collections
def reduce_collection(entries, keys_fn, metric_fn, **kwargs):
    key_sets = None
    selected_entries = list()
    sorted_entries = sorted(entries, key=metric_fn, **kwargs)

    for entry in sorted_entries:
        keys = keys_fn(entry)

        if key_sets is None:
            key_sets = [set() for i in range(len(keys))]

        any_taken = False

        for key_idx, key in enumerate(keys):
            if key in key_sets[key_idx]:
                any_taken = True
                break

        if any_taken:
            continue

        for key_idx, key in enumerate(keys):
            key_sets[key_idx].add(key)

        selected_entries.append(entry)

    return selected_entries
