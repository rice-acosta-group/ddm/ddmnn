from ddm.core.commons.histograms import get_th1_integral
from ddm.core.commons.measurements import Measurement, Constant
from ddm.core.execution.runtime import get_logger


class TransferFactor:
    def __init__(self, value=None, num_hist=None, den_hist=None):
        if value is None:
            self.__num__ = num_hist
            self.__den__ = den_hist

            self.__tf_hist__ = num_hist.Clone("tf")
            self.__tf_hist__.Sumw2()
            self.__tf_hist__.Divide(den_hist)

            num_int = get_th1_integral(num_hist)
            den_int = get_th1_integral(den_hist)

            if num_int.value == 0. and den_int.value == 0.:
                self.__int_tf__ = Measurement(1., 1.)
            else:
                self.__int_tf__ = num_int.divide(den_int, nan=0.)

            self.__num__ = num_hist
            self.__den__ = den_hist

            get_logger().info(f"""
            Calculating TF:
                Num Int: {num_int.value:5.3f}+/-{num_int.error:5.3f}
                Den Int: {den_int.value:5.3f}+/-{den_int.error:5.3f}
                Integral TF is: {self.__int_tf__.value:5.3f}+/-{self.__int_tf__.error:5.3f}
            """)
        else:
            self.__int_tf__ = value
            self.__num__ = None
            self.__den__ = None

    def value(self, bin_id=None):
        """ Function returns a [value, error] list"""
        if bin_id is None or self.__num__ is None:
            return self.__int_tf__
        else:
            if self.__den__.GetBinContent(bin_id) == 0.:
                return Constant(0.)
            else:
                return Measurement(
                    self.__tf_hist__.GetBinContent(bin_id),
                    self.__tf_hist__.GetBinError(bin_id)
                )

    def apply(self, input_hist, output_hist, flat_tf_en=False):
        for bin_id in range(0, input_hist.GetNbinsX() + 2):
            input_count = Measurement(
                input_hist.GetBinContent(bin_id),
                input_hist.GetBinError(bin_id)
            )

            if flat_tf_en:
                output = input_count.multiply(self.value())
            else:
                output = input_count.multiply(self.value(bin_id))

            output_hist.SetBinContent(bin_id, output.value)
            output_hist.SetBinError(bin_id, output.error)
