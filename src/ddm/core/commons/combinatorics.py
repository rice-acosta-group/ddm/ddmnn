def yield_pairs(col):
    vsize = len(col)

    for i in range(vsize):
        obj_i = col[i]

        for j in range(i + 1, vsize):
            yield (obj_i, col[j])


def yield_combinations(col1, *coln):
    for el1 in col1:
        if len(coln) == 1:
            for eln in coln[0]:
                yield (el1, eln)
        else:
            for eln in yield_combinations(coln[0], *coln[1:]):
                yield (el1, *eln)
