from ROOT import gStyle
from ROOT import kBlack
from ROOT import kFALSE
from ROOT import kFullDotLarge
from ROOT import kGray
from ROOT import kTRUE
from ROOT import kWhite


def set_global_style():
    style = gStyle  # TStyle('style', 'Style')

    # inverted black body radiator
    style.SetPalette(56)
    style.SetNumberContours(100)

    # generic line thicknesses
    style.SetLineWidth(2)

    # canvas
    style.SetCanvasBorderMode(0)  # off
    style.SetCanvasColor(kWhite)  # white

    # pad
    style.SetPadBorderMode(0)  # off
    style.SetPadColor(kWhite)  # white
    style.SetPadGridX(kFALSE)  # grid x
    style.SetPadGridY(kFALSE)  # grid y
    style.SetGridColor(kGray)  # gray
    style.SetGridStyle(3)  # dotted
    style.SetGridWidth(1)  # pixels

    # frame
    style.SetFrameBorderMode(0)  # off
    style.SetFrameFillColor(kWhite)  # white
    style.SetFrameFillStyle(0)  # hollow
    style.SetFrameLineColor(kWhite)  # white
    style.SetFrameLineStyle(1)  # solid
    style.SetFrameLineWidth(0)  # pixels

    # legend
    style.SetLegendBorderSize(0)  # off

    # hist
    style.SetHistLineColor(kBlack)  # black
    style.SetHistLineStyle(1)  # solid
    style.SetHistLineWidth(2)  # pixels
    style.SetMarkerStyle(kFullDotLarge)  # marker
    style.SetMarkerColor(kBlack)  # black
    style.SetEndErrorSize(3)  # slightly increased error bar ticks
    style.SetHatchesSpacing(3)  # increased hatch spacing
    style.SetHatchesLineWidth(1)  # decrease the line width of the shading

    # stats box
    style.SetOptStat(0)  # off

    # fit box
    style.SetOptFit(1)  # on
    style.SetStatStyle(0)  # white
    style.SetStatBorderSize(0)  # default 2

    # title
    style.SetOptTitle(0)  # off
    style.SetTitleTextColor(kBlack)  # black
    style.SetTitleStyle(0)  # hollow
    style.SetTitleFillColor(kWhite)  # white
    style.SetTitleBorderSize(0)  # default 2
    style.SetTitleAlign(22)  # center top

    # axis titles
    style.SetTitleColor(kBlack, 'XYZ')  # black
    style.SetTitleOffset(1.15, 'X')  # default 1
    style.SetTitleOffset(1.25, 'Y')  # default 1

    # axis labels
    style.SetLabelColor(kBlack, 'XYZ')  # black
    style.SetLabelOffset(0.005, 'XYZ')  # default 0.005

    # axis
    style.SetAxisColor(kBlack, 'XYZ')  # black
    style.SetStripDecimals(kTRUE)  # strip decimals
    style.SetPadTickX(1)  # opposite x ticks
    style.SetPadTickY(1)  # opposite y ticks

    style.cd()


def set_tdr_style(improve=True):
    tdrStyle = gStyle  # TStyle("tdrStyle", "Style for P-TDR")

    # generic line thicknesses
    tdrStyle.SetLineWidth(2)

    # for the canvas:
    tdrStyle.SetCanvasBorderMode(0)
    tdrStyle.SetCanvasColor(kWhite)
    tdrStyle.SetCanvasDefH(600)  # Height of canvas
    tdrStyle.SetCanvasDefW(600)  # Width of canvas
    tdrStyle.SetCanvasDefX(0)  # POsition on screen
    tdrStyle.SetCanvasDefY(0)

    tdrStyle.SetPadBorderMode(0)
    # tdrStyle.SetPadBorderSize(Width_t size = 1)
    tdrStyle.SetPadColor(kWhite)
    tdrStyle.SetPadGridX(False)
    tdrStyle.SetPadGridY(False)
    tdrStyle.SetGridColor(0)
    tdrStyle.SetGridStyle(3)
    tdrStyle.SetGridWidth(1)
    # For the frame:
    tdrStyle.SetFrameBorderMode(0)
    tdrStyle.SetFrameBorderSize(1)
    tdrStyle.SetFrameFillColor(0)
    tdrStyle.SetFrameFillStyle(0)
    tdrStyle.SetFrameLineColor(1)
    tdrStyle.SetFrameLineStyle(1)
    tdrStyle.SetFrameLineWidth(1)

    # For the histo:
    # tdrStyle.SetHistFillColor(1)
    # tdrStyle.SetHistFillStyle(0)
    tdrStyle.SetHistLineColor(1)
    tdrStyle.SetHistLineStyle(0)
    tdrStyle.SetHistLineWidth(2)
    # tdrStyle.SetLegoInnerR(Float_t rad = 0.5)
    # tdrStyle.SetNumberContours(Int_t number = 20)

    tdrStyle.SetEndErrorSize(2)
    # tdrStyle.SetErrorMarker(20)
    # tdrStyle.SetErrorX(0.)

    tdrStyle.SetMarkerStyle(20)

    # For the fit/function:
    tdrStyle.SetOptFit(1)
    tdrStyle.SetFitFormat("5.4g")
    tdrStyle.SetFuncColor(2)
    tdrStyle.SetFuncStyle(1)
    tdrStyle.SetFuncWidth(1)

    # For the date:
    tdrStyle.SetOptDate(0)
    # tdrStyle.SetDateX(Float_t x = 0.01)
    # tdrStyle.SetDateY(Float_t y = 0.01)

    # For the statistics box:
    tdrStyle.SetOptFile(0)
    tdrStyle.SetOptStat(0)  # To display the mean and RMS:   SetOptStat("mr")
    tdrStyle.SetStatColor(kWhite)
    tdrStyle.SetStatFont(42)
    tdrStyle.SetStatFontSize(0.025)
    tdrStyle.SetStatTextColor(1)
    tdrStyle.SetStatFormat("6.4g")
    tdrStyle.SetStatBorderSize(1)
    tdrStyle.SetStatH(0.1)
    tdrStyle.SetStatW(0.15)
    # tdrStyle.SetStatStyle(Style_t style = 1001)
    # tdrStyle.SetStatX(Float_t x = 0)
    # tdrStyle.SetStatY(Float_t y = 0)

    # Margins:
    tdrStyle.SetPadTopMargin(0.05)
    tdrStyle.SetPadBottomMargin(0.13)
    tdrStyle.SetPadLeftMargin(0.16)
    tdrStyle.SetPadRightMargin(0.04)

    # For the Global title:

    tdrStyle.SetOptTitle(0)
    tdrStyle.SetTitleFont(42)
    tdrStyle.SetTitleColor(1)
    tdrStyle.SetTitleTextColor(1)
    tdrStyle.SetTitleFillColor(10)
    tdrStyle.SetTitleFontSize(0.05)
    # tdrStyle.SetTitleH(0) # Set the height of the title box
    # tdrStyle.SetTitleW(0) # Set the width of the title box
    # tdrStyle.SetTitleX(0) # Set the position of the title box
    # tdrStyle.SetTitleY(0.985) # Set the position of the title box
    # tdrStyle.SetTitleStyle(Style_t style = 1001)
    # tdrStyle.SetTitleBorderSize(2)

    # For the axis titles:

    tdrStyle.SetTitleColor(1, "XYZ")
    tdrStyle.SetTitleFont(42, "XYZ")
    tdrStyle.SetTitleSize(0.06, "XYZ")
    # tdrStyle.SetTitleXSize(Float_t size = 0.02) # Another way to set the size
    # tdrStyle.SetTitleYSize(Float_t size = 0.02)
    tdrStyle.SetTitleXOffset(0.9)
    tdrStyle.SetTitleYOffset(1.25)
    # tdrStyle.SetTitleOffset(1.1, "Y") # Another way to set the Offset

    # For the axis labels:

    tdrStyle.SetLabelColor(1, "XYZ")
    tdrStyle.SetLabelFont(42, "XYZ")
    tdrStyle.SetLabelOffset(0.007, "XYZ")
    tdrStyle.SetLabelSize(0.05, "XYZ")

    # For the axis:

    tdrStyle.SetAxisColor(1, "XYZ")
    tdrStyle.SetStripDecimals(True)
    tdrStyle.SetTickLength(0.03, "XYZ")
    tdrStyle.SetNdivisions(510, "XYZ")
    tdrStyle.SetPadTickX(1)  # To get tick marks on the opposite side of the frame
    tdrStyle.SetPadTickY(1)

    # Change for log plots:
    tdrStyle.SetOptLogx(0)
    tdrStyle.SetOptLogy(0)
    tdrStyle.SetOptLogz(0)

    # Postscript options:
    tdrStyle.SetPaperSize(20., 20.)
    # tdrStyle.SetLineScalePS(Float_t scale = 3)
    # tdrStyle.SetLineStyleString(Int_t i, const char* text)
    # tdrStyle.SetHeaderPS(const char* header)
    # tdrStyle.SetTitlePS(const char* pstitle)

    # tdrStyle.SetBarOffset(Float_t baroff = 0.5)
    # tdrStyle.SetBarWidth(Float_t barwidth = 0.5)
    # tdrStyle.SetPaintTextFormat(const char* format = "g")
    # tdrStyle.SetPalette(Int_t ncolors = 0, Int_t* colors = 0)
    # tdrStyle.SetTimeOffset(Double_t toffset)
    # tdrStyle.SetHistMinimumZero(kTRUE)

    tdrStyle.SetHatchesLineWidth(5)
    tdrStyle.SetHatchesSpacing(0.05)

    if improve:
        # histogram markers
        tdrStyle.SetMarkerSize(1.3)

        # legend
        tdrStyle.SetLegendBorderSize(0)  # off
        tdrStyle.SetLegendFont(42)

        # hist
        tdrStyle.SetHatchesSpacing(3)  # increased hatch spacing
        #        tdrStyle.SetHatchesLineWidth(2)
        tdrStyle.SetHatchesLineWidth(1)

        tdrStyle.SetPadTopMargin(0.06)
        tdrStyle.SetPadLeftMargin(0.14)
        # tdrStyle.SetPadRightMargin(0.02)

        # pdf
        # tdrStyle.SetLineScalePS(1)

        # axis titles
        tdrStyle.SetTitleYOffset(1.05)

    tdrStyle.cd()


def set_style(width=1440, height=1080, font=42, font_size=0.04):
    style = gStyle

    top_margin = 0.1
    left_margin = 0.125
    font_size = float(font_size)

    right_margin = top_margin * float(height) / float(width)
    bottom_margin = left_margin
    xtitle = left_margin + (1 - left_margin - right_margin) / 2
    ytitle = 1 - (top_margin / 2)

    # canvas
    style.SetCanvasDefW(width)  # width
    style.SetCanvasDefH(height)  # height

    # pad margins
    style.SetPadTopMargin(top_margin)  # default 0.1
    style.SetPadBottomMargin(bottom_margin)  # default 0.1
    style.SetPadLeftMargin(left_margin)  # default 0.1
    style.SetPadRightMargin(right_margin)  # default 0.1

    # legend
    style.SetLegendFont(font)  # helvetica normal
    style.SetLegendTextSize(font_size)  # default 0
    style.SetLegendBorderSize(0)  # off

    # title
    style.SetTitleFont(font, '')  # helvetica normal
    style.SetTitleFontSize(font_size)  # default 0
    style.SetTitleX(xtitle)  # center title horizontally with respect to frame
    style.SetTitleY(ytitle)  # center title vertically within margin

    # axis titles
    style.SetTitleFont(font, 'XYZ')  # helvetica normal
    style.SetTitleSize(font_size, 'XYZ')  # default 0.02

    # axis labels
    style.SetLabelFont(font, 'XYZ')  # helvetica normal
    style.SetLabelSize(font_size, 'XYZ')  # default 0.04

    style.cd()
