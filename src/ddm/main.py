import argparse
import os
import sys

from matplotlib import pyplot as plt

from ddm.commands.general.tools.estimate_yields import EstimateYields


def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    # CONFIGURE RUN
    if sys.version_info[0] < 3:
        raise AssertionError('Please run this code with Python 3.')

    # Configure root
    import ddm.core.root.environment as root_env
    root_env.configure()

    # Configure matplotlib
    from ddm.core.execution import runtime
    plt.style.use(runtime.resource('tdrstyle.mplstyle'))

    # Configure parser
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='DDM Analysis')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Enable debug mode')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    # Configure Commands
    from ddm.commands.general.gen_displaced_muons.command import GenerateDisplacedMuons
    from ddm.commands.mini.gen_association_samples.command import GenAssociationSignalSample
    from ddm.commands.mini.gen_pairing_samples.command import GenPairingSignalSample
    from ddm.commands.mini.association_training.command import TrainAssociationNN
    from ddm.commands.mini.pairing_training.command import TrainPairingNN
    from ddm.commands.mini.check_performance.command import CheckPerformance
    from ddm.commands.mini.retupler.command import RunRetupler
    from ddm.commands.fast.check_distributions.command import CheckFastDistributions
    from ddm.commands.fast.check_performance.command import CheckFastPerformance
    from ddm.commands.fast.check_cutflow.command import CheckFastCutflow
    from ddm.commands.fast.pat_estimation.estimate_qcd import EstimatePATQCD
    from ddm.commands.fast.pat_estimation.estimate_dy import EstimatePATDY
    from ddm.commands.fast.dsa_estimation.estimate_qcd import EstimateDSAQCD
    from ddm.commands.fast.dsa_estimation.estimate_dy import EstimateDSADY
    from ddm.commands.fast.check_yields.command import CheckFastYields
    from ddm.commands.general.tools.stack_yields import StackYields
    from ddm.commands.general.tools.plot_ratio import PlotRatio
    from ddm.core.execution import runtime

    command_entries = {
        # General
        'generate-displaced-muons': ('Generate displaced muons', GenerateDisplacedMuons()),
        # DPA Training
        'generate-dpa-signal': ('Generate DSA-PAT Association ML signal sample', GenAssociationSignalSample()),
        'association': ('Train models for DSA-PAT Association', TrainAssociationNN()),
        # Pairing Training
        'generate-pairing-signal': ('Generate Pairing ML signal sample', GenPairingSignalSample()),
        'pairing': ('Train models for Muon Pairing', TrainPairingNN()),
        # Performance
        'check-performance': ('Check Performance', CheckPerformance()),
        # Retupler
        'run-retupler': ('Run Retupler on Sample', RunRetupler()),
        'r-check-cutflow': ('Check rTuple Cutflow', CheckFastCutflow()),
        'r-check-performance': ('Check rTuple Performance', CheckFastPerformance()),
        'r-check-distributions': ('Check rTuple Distributions', CheckFastDistributions()),
        'r-check-yields': ('Check rTuple Yields', CheckFastYields()),
        'r-pat-qcd-estimation': ('Study rTuple TMS-TMS QCD Estimation', EstimatePATQCD()),
        'r-pat-dy-estimation': ('Study rTuple TMS-TMS DY Estimation', EstimatePATDY()),
        'r-dsa-qcd-estimation': ('Study rTuple STA-STA QCD Estimation', EstimateDSAQCD()),
        'r-dsa-dy-estimation': ('Study rTuple STA-STA DY Estimation', EstimateDSADY()),
        # Tools
        'plot-ratio': ('Plot Ratio', PlotRatio()),
        'stack-yields': ('Stacks observed and estimated yields', StackYields()),
        'estimate-yields': ('Estimates yields', EstimateYields()),
    }

    for key, command_entry in command_entries.items():
        subparser = subparsers.add_parser(key, help=command_entry[0])
        command_entry[1].configure_parser(subparser)

    args = parser.parse_args()

    # DEBUG MODE
    if args.debug_en:
        runtime.debug_en = True

    # RUN COMMAND
    command_entry = command_entries.get(args.parser)

    if command_entry is None:
        parser.print_help()
        exit(1)

    # RUN
    command_entry[1].run(args)
