import sys

sys.path.append('/usr/lib/root')

from ddm.core.commons.histograms import get_th1_user_ranges
from ddm.core.commons.tools import safe_divide

from ROOT import TCanvas
from ROOT import TFile
from ROOT import TLatex
from ROOT import TLegend
from ROOT import TPad
from ROOT import gStyle
from ROOT import kBlue
from ROOT import kRed
from ROOT import kWhite

import ddm.core.root.environment as root_env


def make_hist_ratio_plot(
        res_root, ref_root,
        res_label, ref_label,
        plt_name, filename=None,
        title='Dimuon Reconstruction',
        xtitle='True L_{xy} [cm]',
        ytitle='a.u.',
        xmin=0, xmax=400
):
    # Calculate Ratio
    res_plot = res_root.Get(plt_name)
    ref_plot = ref_root.Get(plt_name)

    ratio_hist = res_plot.Clone(plt_name + '_ratio')
    ratio_hist.Sumw2()
    ratio_hist.Divide(ref_plot)

    # Make RTitle
    rtitle = '%s / %s' % (res_label, ref_label)

    # Build Entries
    hist_entries = [
        (ref_plot, ref_label, kRed, 3150),
        (res_plot, res_label, kBlue, 3195),
    ]

    # IMAGE
    gStyle.SetOptStat(0)

    # CREATE CANVAS
    plot_canvas = TCanvas('hist_comparison', '')
    plot_canvas.SetLeftMargin(0.200)
    plot_canvas.SetRightMargin(0.200)
    plot_canvas.SetBottomMargin(0.200)

    # Draw Top Canvas
    plot_canvas.cd(0)
    top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
    top_pad.SetTopMargin(0.125)
    top_pad.SetLeftMargin(0.175)
    top_pad.SetRightMargin(0.050)
    top_pad.SetBottomMargin(0.400)
    top_pad.SetLogy(1)
    top_pad.Draw()
    top_pad.cd()

    # Find min and max
    xmin, xmax, ymin, ymax = get_th1_user_ranges(
        ref_plot, res_plot,
        logy=True
    )

    # Draw top frame
    frame_top = plot_canvas.DrawFrame(
        xmin, ymin,
        xmax, ymax,
        ''
    )
    frame_top.GetXaxis().SetTitle('')
    frame_top.GetXaxis().SetLabelOffset(999)
    frame_top.GetXaxis().SetLabelSize(0)
    frame_top.GetYaxis().SetTitle(ytitle)
    frame_top.GetYaxis().SetTitleOffset(1.450)
    frame_top.GetYaxis().SetMaxDigits(3)

    legend_x0 = 0.650
    legend_y0 = 0.450

    legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.150)
    legend.SetMargin(0.250)
    legend.SetFillColorAlpha(kWhite, 0.8)
    legend.SetBorderSize(0)
    legend.SetTextSize(.035)

    for plot, label, color, style in hist_entries:
        plot.SetFillColor(color)
        plot.SetFillStyle(style)
        plot.SetLineColor(color)
        plot.SetLineWidth(3)

        plot.Draw('SAME HIST')

        legend.AddEntry(plot, label, 'F')

    frame_top.Draw('SAME AXIS')
    frame_top.Draw('SAME AXIG')

    legend.Draw()

    # Draw Bottom Canvas
    plot_canvas.cd(0)
    bottom_pad = TPad("bottom", "bottom", 0.0, 0.0, 1.0, 1.0)
    bottom_pad.SetFillStyle(4000)
    bottom_pad.SetTopMargin(0.650)
    bottom_pad.SetLeftMargin(0.175)
    bottom_pad.SetRightMargin(0.050)
    bottom_pad.SetBottomMargin(0.160)
    bottom_pad.Draw()
    bottom_pad.cd()

    # Draw SF
    _, _, rmin, rmax = get_th1_user_ranges(ratio_hist)

    frame_bottom = plot_canvas.DrawFrame(
        xmin, rmin,
        xmax, rmax,
        ''
    )

    frame_bottom.GetXaxis().SetTitle(xtitle)
    frame_bottom.GetXaxis().SetTitleOffset(1.350)
    frame_bottom.GetYaxis().SetTitle(rtitle)
    frame_bottom.GetYaxis().SetNdivisions(204)
    frame_bottom.GetYaxis().SetTitleOffset(1.450)

    ratio_hist.SetMarkerStyle(8)
    ratio_hist.SetMarkerSize(0.5)
    ratio_hist.SetLineWidth(2)
    ratio_hist.Draw('P E SAME')

    frame_bottom.Draw('SAME AXIS')
    frame_bottom.Draw('SAME AXIG')

    # Mean
    mean = safe_divide(res_plot.Integral(), ref_plot.Integral())

    # Draw mean label
    mean_label = TLatex()
    mean_label.SetTextAlign(12)
    mean_label.DrawLatexNDC(0.6, 0.3725, 'Mean(#Delta)=%0.2f %%' % ((mean - 1) * 100))

    # Draw Titles
    plot_canvas.cd(0)

    main_title = TLatex(0.045, 0.95, title)
    main_title.SetTextAlign(12)
    main_title.Draw()

    # Get Filename
    if filename is None:
        filename = plt_name

    # Save
    plot_canvas.SaveAs(filename + '.png')


def make_2dhist_ratio_plot(
        res_root, ref_root,
        res_label, ref_label,
        plt_name, filename=None,
        xtitle='True L_{xy} [cm]',
        ytitle='True L_{xy} [cm]'
):
    # Calculate Ratio
    res_plot = res_root.Get(plt_name)
    ref_plot = ref_root.Get(plt_name)

    ratio_hist = res_plot.Clone(plt_name + '_ratio')
    ratio_hist.Divide(ref_plot)

    # Make RTitle
    rtitle = '%s / %s' % (res_label, ref_label)

    # IMAGE
    gStyle.SetOptStat(0)

    # CREATE CANVAS
    plot_canvas = TCanvas('hist_comparison', '')
    plot_canvas.SetLeftMargin(0.175)
    plot_canvas.SetRightMargin(0.200)
    plot_canvas.SetBottomMargin(0.160)
    # plot_canvas.SetLogz(1)
    plot_canvas.cd(0)

    ratio_hist.GetXaxis().SetTitle(xtitle)
    ratio_hist.GetYaxis().SetTitle(ytitle)
    ratio_hist.GetYaxis().SetTitleOffset(1.65)
    ratio_hist.GetYaxis().SetMaxDigits(3)
    ratio_hist.GetZaxis().SetTitle(rtitle)
    ratio_hist.GetZaxis().SetTitleOffset(1.25)
    ratio_hist.SetMaximum(2)
    ratio_hist.SetMinimum(0)
    ratio_hist.Draw('COLZ1')

    # Get Filename
    if filename is None:
        filename = plt_name

    # Save
    plot_canvas.SaveAs(filename + '.png')


if __name__ == '__main__':
    #  Init Env
    root_env.configure(load_macros_en=False)

    # Params
    # res_root = TFile.Open('../../out/rdistributions_REPNN_PCNN_data_doublemu/outputs.root')
    # ref_root = TFile.Open('../../out/rdistributions_REP_PC_data_doublemu/outputs.root')

    # res_root = TFile.Open('../../out/rdistributions_REPNN_PCNN_data/outputs.root')
    # ref_root = TFile.Open('../../out/rdistributions_REP_PC_data/outputs.root')

    res_root = TFile.Open('../../out/rdistributions_REPNN_PCNN_2mu/outputs.root')
    ref_root = TFile.Open('../../out/rdistributions_REP_PC_2mu/outputs.root')

    res_label = 'wNN'
    ref_label = 'noNN'

    # Run
    make_2dhist_ratio_plot(res_root, ref_root, res_label, ref_label,
                           'dim_dsa_sr_dimr_vs_dimz',
                           xtitle='Z', ytitle='R')
    make_2dhist_ratio_plot(res_root, ref_root, res_label, ref_label,
                           'dim_hyb_sr_dimr_vs_dimz',
                           xtitle='Z', ytitle='R')
    make_2dhist_ratio_plot(res_root, ref_root, res_label, ref_label,
                           'dim_pat_sr_dimr_vs_dimz',
                           xtitle='Z', ytitle='R')

    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_dy_mass', title='TMS-TMS',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_qcd_mass', title='TMS-TMS',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_dy_mass', title='STA-TMS',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_qcd_mass', title='STA-TMS',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_dy_mass', title='STA-STA',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_qcd_mass', title='STA-STA',
    #                      xtitle='MASS',
    #                      xmin=0, xmax=100)
    #
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_dy_mind0sigpv', title='TMS-TMS',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_qcd_mind0sigpv', title='TMS-TMS',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_dy_mind0sigpv', title='STA-TMS',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_qcd_mind0sigpv', title='STA-TMS',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_dy_mind0sigpv', title='STA-STA',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_qcd_mind0sigpv', title='STA-STA',
    #                      xtitle='mind0sigpv',
    #                      xmin=0, xmax=30)
    #
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_dy_largelxypv', title='TMS-TMS',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_qcd_largelxypv', title='TMS-TMS',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_dy_largelxypv', title='STA-TMS',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_qcd_largelxypv', title='STA-TMS',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_dy_largelxypv', title='STA-STA',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_qcd_largelxypv', title='STA-STA',
    #                      xtitle='largelxypv',
    #                      xmin=0, xmax=400)
    #
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_dy_lxysigpv', title='TMS-TMS',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_qcd_lxysigpv', title='TMS-TMS',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_dy_lxysigpv', title='STA-TMS',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_qcd_lxysigpv', title='STA-TMS',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_dy_lxysigpv', title='STA-STA',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_qcd_lxysigpv', title='STA-STA',
    #                      xtitle='lxysigpv',
    #                      xmin=0, xmax=50)
    #
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_dy_npscore', title='TMS-TMS',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_pat_qcd_npscore', title='TMS-TMS',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_dy_npscore', title='STA-TMS',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_hyb_qcd_npscore', title='STA-TMS',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_dy_npscore', title='STA-STA',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
    # make_hist_ratio_plot(res_root, ref_root, res_label, ref_label,
    #                      'dim_dsa_qcd_npscore', title='STA-STA',
    #                      xtitle='npscore',
    #                      xmin=0, xmax=1)
